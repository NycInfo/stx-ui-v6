/*
ngOnInit() : Method to loading athe page..
searchClients() : Method for searching clients
showData() : Method for loading All clients Data.
clearmessage() : Method for Clearing  error messages.
*/
import { Component, OnInit, Inject, ViewChild, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApptDetailService } from './appointmentdetail.service';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../common/common.service';
import { AppointmentsService } from '../main/appointments.service';
import { isNullOrUndefined } from 'util';
import { DatePipe } from '@angular/common';
import * as moment from 'moment/moment';
import { JwtHelper } from 'angular2-jwt';
import * as config from '../../app.config';
@Component({
    selector: 'app-appointments-popup',
    templateUrl: './appointmentdetail.html',
    styleUrls: ['./appointmentdetail.css'],
    providers: [ApptDetailService, CommonService, AppointmentsService]
})
export class ApptDetailComponent implements OnInit {
    decodedToken: any;
    apptid: any;
    clientid: any;
    pkgbooking: any = false;
    rebooked: any = false;
    newClient: any = false;
    stdingAppointment: any = false;
    apptStatus = [];
    currentdate: Date = new Date();
    isTodayAppt: boolean;
    isOpenAppt: boolean;
    clientPic = '';
    // apptStatusValue: any;
    visitTypesList: any;
    updateNotes: any = '';
    remEmailButtonValue = '';
    remTextButtonValue = '';
    aptDate = Date;
    bookingDataList: any;
    statusColor = { 'background-color': '' };
    rows: any = [];
    nonPckgSrvcs = [];
    ticketserviceId: any;
    serviceNotes: any = '';
    indexTemp: any = 0;
    subSeaquentAppointment = '';
    subSeaquentAppointmentDate: any;
    nextservicename: any;
    packagesList = [];
    // visitTypeValue: any;
    apptData = {
        Sms_Consent__c: 0,
        apstatus: '',
        apdate: '',
        mbphone: '',
        cltemail: '',
        Booked_Package__c: '',
        cltphone: '',
        notes: '',
        clientName: '',
        srvcname: '',
        netprice: '',
        duration: '',
        workerName: '',
        resource: '',
        visttype: null,
        rebook: 0,
        rebooked: 0,
        newclient: 0,
        clientpic: '',
        apptId: '',
        clientId: '',
        TicketServieId: '',
        pkgbooking: 0,
        standingappt: 0,
        creadate: '',
        lastmofdate: '',
        serviceNotes: '',
        No_Email__c: 0,
        isTicket__c: 0,
        Mobile_Carrier__c: '',
        Appt_Date_Time__c: '',
        Rebooked__c: 0,
        Business_Rebook__c: 0,
        CreatedBy: '',
        LastModifiedBy: '',
        Current_Balance__c: '',
        Booked_Online__c: 0,
        Reminder_Primary_Email__c: 0,
        Reminder_Mobile_Phone__c: 0
    };
    waitApptDate = '';
    workrArr: any = [];
    displayAptdate = ['', ''];
    createdDate = ['', ''];
    lastModifyDate = ['', ''];
    nextAppt = ['', ''];
    error = '';
    isModifyAllowed: boolean;
    isRebookAllowed: boolean;
    isRemainderEmailAllowed: boolean;
    isRemainderTextAllowed: boolean;
    isSaveAllowed: boolean;
    isCheckedInAllowed: boolean;
    isBookApptAllowed: boolean;
    actionMethod: any;
    decodeUserToken: any;
    hideClientInfo: any;
    clientPackages: any = [];
    tempSt = '';
    showHide: boolean;
    waitingDate: any = new Date();
    errorMsg = '';
    clientData: any;
    referredClientData: any = [];
    referralsLength: any = 0;
    imagepath = 'assets/images/play-button.png';
    showEtraveler = false;
    referredClient = '';
    clientServicesList: any = [];
    loadmore = 5;
    clientProductList: any = [];
    clientRewardData: any = [];
    clientFlag = [];
    birthDate = '';
    hideLoadMoreButt = false;
    clientSince = '';
    rewardPoints = '';
    clientTitle = '';
    clientNote = '';
    refferedClient: any;
    refferedClient1: any;
    nextappt: any;
    nextappt1: any;
    serviceName: any;
    createdBy: any;
    lastmodifiedBy: any;
    @ViewChild('serviceNotesModal') public serviceNotesModal: ModalDirective;
    @ViewChild('cancelApptModal') public cancelApptModal: ModalDirective;
    constructor(
        private activatedRoute: ActivatedRoute,
        private apptDetailService: ApptDetailService,
        private toastr: ToastrService,
        private commonService: CommonService,
        private appointmentsService: AppointmentsService,
        private router: Router) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.apptid = activatedRoute.snapshot.params['apptid'];
            this.clientid = activatedRoute.snapshot.params['clientId'];
            this.actionMethod = params['actionMethod'];
        });

    }
    /*Method for page Load */
    ngOnInit() {
        this.showHide = false;
        this.listVisitTypes();
        // this.getApptDetails(this.apptid);
        this.getApptServiceDetails(this.clientid, this.apptid);
        if (this.clientid) {
            this.getpackagesListing();
            this.getClientPackages(this.clientid);
        }

        // this.setStatusList();
        // ---Start of code for Permissions Implementation--- //
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedToken = {};
            this.decodeUserToken = {};
        }
        if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
        } else {
            this.decodedToken = {};
        }
        this.getHideClientContactInfo();
        // ---End of code for permissions Implementation--- //
    }
    changeShowStatus() {
        this.showHide = !this.showHide;
    }
    setStatusList(apptStatus) {
        // this.apptStatus = [];
        this.apptStatus = [
            { 'status': 'Called' },
            { 'status': 'Canceled' },
            { 'status': 'Conflicting' },
            { 'status': 'Confirmed' },
            { 'status': 'Booked' },
            { 'status': 'No Show' },
        ];
        if (apptStatus === 'Reminder Sent') {
            this.apptStatus.push({ 'status': 'Reminder Sent' });
        }
        if (apptStatus === 'Pending Deposit') {
            this.apptStatus.push({ 'status': 'Pending Deposit' });
        }
        // if (!apptStatus && apptStatus !== '') {
        //     this.apptStatus.push({ 'status': apptStatus });
        // }
        // if (apptStatus !== 'Called') {
        //     this.apptStatus.push({ 'status': 'Called' });
        // }
        // if (apptStatus !== 'Canceled') {
        //     this.apptStatus.push({ 'status': 'Canceled' });
        // }
        // if (apptStatus !== 'Conflicting') {
        //     this.apptStatus.push({ 'status': 'Conflicting' });
        // }
        // if (apptStatus !== 'Confirmed') {
        //     this.apptStatus.push({ 'status': 'Confirmed' });
        // }
        // if (apptStatus !== 'Booked') {
        //     this.apptStatus.push({ 'status': 'Booked' });
        // }
        // if (apptStatus !== 'Reminder Sent') {
        //     this.apptStatus.push({ 'status': 'Reminder Sent' });
        // }
        // if (apptStatus !== 'No Show') {
        //     this.apptStatus.push({ 'status': 'No Show' });
        // }
        this.setStatusColor(apptStatus);
    }

    setStatusColor(apptStatus) {
        if (apptStatus === 'Called') {
            this.statusColor = { 'background-color': this.bookingDataList.calledStatusColor };
        } else if (apptStatus === 'Canceled') {
            this.statusColor = { 'background-color': this.bookingDataList.canceledStatusColor };
        } else if (apptStatus === 'Conflicting') {
            this.statusColor = { 'background-color': this.bookingDataList.conflictingStatusColor };
        } else if (apptStatus === 'Confirmed') {
            this.statusColor = { 'background-color': this.bookingDataList.confirmedStatusColor };
        } else if (apptStatus === 'Booked') {
            this.statusColor = { 'background-color': this.bookingDataList.bookedStatusColor };
        } else if (apptStatus === 'Checked In') {
            this.statusColor = { 'background-color': this.bookingDataList.checkedInStatusColor };
        } else if (apptStatus === 'Reminder Sent') {
            this.statusColor = { 'background-color': this.bookingDataList.reminderSentStatusColor };
        } else if (apptStatus === 'No Show') {
            this.statusColor = { 'background-color': this.bookingDataList.noShowStatusColor };
        } else if (apptStatus === 'Complete') {
            this.statusColor = { 'background-color': this.bookingDataList.completeStatusColor };
        } else if (apptStatus === 'Pending Deposit') {
            this.statusColor = { 'background-color': this.bookingDataList.pendingDepositStatusColor };
        }
    }
    sendReminderText() {
        const dataObj = {
            'mobileNum': this.apptData.mbphone,
            'apptId': this.apptid
        };
        this.apptDetailService.sendText(dataObj).subscribe(
            data => {
                const reminderTextStatus = data['result'];
                this.toastr.success('Send Reminder Text: Sent', null, { timeOut: 1500 });

            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2106') {
                    this.toastr.error(JSON.parse(error['_body']).message, null, { timeOut: 2000 });
                } if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    sendReminderEmail() {
        let serviceNames = '';
        for (let i = 0; i < this.rows.length; i++) {
            serviceNames += this.rows[i].Name + ', ';
        }
        const dataObj = {
            'clientFirstName': this.apptData.clientName.split(' ')[0],
            'clientLastName': this.apptData.clientName.split(' ')[1],
            'clientEmail': this.apptData.cltemail,
            'clientPhone': this.apptData.cltphone,
            'aptDate': new DatePipe('en-Us').transform(this.apptData.Appt_Date_Time__c, 'MM/dd/yyyy hh:mm a'),
            'serviceNames': serviceNames.slice(0, -2),
            'status': 'Reminder Sent',
            'clientid': this.clientid,
            'apptid': this.apptid,
            'Status_Date_Time_c': this.commonService.getDBDatTmStr(new Date()),
            'Reminder_Type__c': 'Email'
        };
        this.apptDetailService.sendReminderToClient(dataObj, this.apptid).subscribe(
            data => {
                const dataStatus = data['result'];
                this.toastr.success('Send Reminder Email: Sent', null, { timeOut: 1500 });
                this.getApptServiceDetails(this.clientid, this.apptid);
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2106') {
                    this.toastr.error(JSON.parse(error['_body']).message, null, { timeOut: 2000 });
                } if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    listVisitTypes() {
        this.apptDetailService.getVisitTypes().subscribe(
            data => {
                this.visitTypesList = data['result'];
                // this.visitTypeValue = this.visitTypesList[0].visitType;
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    // getApptDetails(apptid) {
    //     this.apptDetailService.getApptDetails(apptid).subscribe(data => {
    //         this.apptData = data['result'][0];
    //         this.clientPic = this.apiEndPoint + '/' + this.apptData.clientpic;
    //         this.manageStatusButtons();
    //         // this.setStatusList(this.apptData.apstatus);
    //         this.getBookingData();
    //     },
    //         error => {
    //             const status = JSON.parse(error['status']);
    //             const statuscode = JSON.parse(error['_body']).status;
    //             switch (JSON.parse(error['_body']).status) {
    //                 case '2033':
    //                     break;
    //             }
    //         });
    // }
    getApptServiceDetails(clientid, apptid) {
        const reqDate = this.commonService.getDBDatStr(new Date());
        this.apptDetailService.getApptServices(clientid, apptid, reqDate).subscribe(data => {
            const resData = data['result'];
            if (resData.nextapptresult.length > 0) {
                if (resData && resData.nextapptresult && resData.nextapptresult[0].serviceName !== null) {
                    //  this.subSeaquentAppointmentDate = resData.nextapptresult[0].Appt_Date_Time__c + '-' + resData.nextapptresult[0].serviceName.replace(/,/g, ' / ');
                    this.subSeaquentAppointmentDate = resData.nextapptresult[0].Appt_Date_Time__c;
                    this.nextAppt = this.commonService.getUsrDtStrFrmDBStr(resData.nextapptresult[0].Appt_Date_Time__c);
                    // this.nextservicename = resData.nextapptresult[0].serviceName.replace(/,/g, ' / ');
                    this.nextservicename = resData.nextapptresult[0].nextServices.replace(/,/g, ' , ');
                } else {
                    this.subSeaquentAppointmentDate = '';
                    this.subSeaquentAppointment = 'None';
                }
            }

            if (resData.apptrst.length > 0) {
                // const displayName = document.getElementById('displayNameId');
                // if (!isNullOrUndefined(resData.apptrst[0].clientName)) {
                //     displayName.innerHTML = 'Appointment Detail - ' + resData.apptrst[0].clientName;
                // }
                this.apptData = resData.apptrst[0];
                if (this.apptData.visttype === null) {
                    this.apptData.visttype = '';
                }
                this.tempSt = this.apptData['apstatus'];
                this.waitApptDate = this.apptData.Appt_Date_Time__c;
                this.displayAptdate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.Appt_Date_Time__c);
                if (this.apptData.clientName === null || this.apptData.clientName === 'null') {
                    this.apptData.clientName = 'No Client';
                }
                if (this.apptData.notes === null || this.apptData.notes === 'null') {
                    this.apptData.notes = '';
                }
                if (this.apptData.cltemail === null || this.apptData.cltemail === 'null') {
                    this.apptData.cltemail = '';
                }
            }
            for (let i = 0; i < resData.srvcresult.length; i++) {
                if (resData.srvcresult[i]['Notes__c'] === null || resData.srvcresult[i]['Notes__c'] === 'null') {
                    resData.srvcresult[i]['Notes__c'] = '';
                }
            }
            this.rows = resData.srvcresult;
            /* below for loop is used for waiting list routing */
            this.workrArr = [];
            this.rows.forEach(element => {
                this.workrArr.push(element['workerName']);
            });
            /* above for loop is used for waiting list routing ends */
            for (let j = 0; j < this.rows.length; j++) {
                this.rows[j]['Resources__c'] = this.rows[j]['Resources__c'] === 'null' || this.rows[j]['Resources__c'] === null ? '' : this.rows[j]['Resources__c'];
                this.rows[j].servicedate = this.commonService.getUsrDtStrFrmDBStr(this.rows[j].Service_Date_Time__c);
            }
            this.nonPckgSrvcs = [];
            const tempSer = this.rows.filter((obj) => !obj.Booked_Package__c || obj.Booked_Package__c === '' || obj.Booked_Package__c === null);
            this.nonPckgSrvcs = tempSer.map((obj) => {
                return { 'serId': obj.Id, 'ticketServiceId': obj.tsId, 'apptId': this.apptid, 'clientId': this.clientid, 'isclientPackage': 1 };
            });
            this.createdDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.creadate);
            this.lastModifyDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.lastmofdate);
            if (this.apptData.clientpic && this.apptData.clientpic !== '') {
                this.clientPic = config.S3_URL + this.apptData.clientpic;
            }
            this.manageStatusButtons();
            // this.setStatusList(this.apptData.apstatus);
            this.getBookingData();
        },
            error => {
                this.rows = [{}];
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    saveAppointmentDetails() {
        const callback = localStorage.getItem('viewCallback');
        const dataObj = {
            'status': this.apptData.apstatus,
            'notes': this.apptData.notes,
            'visttype': this.apptData.visttype,
            'waitDate': this.waitApptDate,
            'workerArr': this.workrArr
        };
        this.apptDetailService.saveAppointmentDetails(this.apptid, dataObj).subscribe(
            data => {
                if (this.apptData.apstatus === 'Canceled') {
                    this.apptDetailService.sendCancelReminder(this.apptid, true).subscribe(
                        data1 => {
                            const status = data1['result'];
                        }, error1 => {
                            const status = JSON.parse(error1['status']);
                            const statuscode = JSON.parse(error1['_body']).status;
                            switch (status) {
                                case 500:
                                    break;
                                case 400:
                                    break;
                            }
                            if (statuscode === '2085' || statuscode === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
                }
                const saveAppointmentDetails = data['result'];
                if (this.actionMethod === 'AppointmentDetail') {
                    this.router.navigate(['/client/edit/' + this.clientid]);
                } else if (saveAppointmentDetails.waitingRoutCount === 0) {
                    this.router.navigate(['/appointments'], { queryParams: { date: callback } });
                } else {
                    const dt = this.commonService.getDBDatStr(this.waitingDate).split(' ')[0];
                    this.router.navigate(['/appointment/waitinglist'], { queryParams: { date: dt } });
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2105') {
                    this.toastr.error(JSON.parse(error['_body']).message, null, { timeOut: 2000 });
                } else if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    saveNote() {
        this.apptDetailService.saveNotes(this.ticketserviceId, this.serviceNotes).subscribe(
            data => {
                const saveNoteResult = data['result'];
                this.getApptServiceDetails(this.clientid, this.apptid);
                this.serviceNotesModal.hide();
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    manageStatusButtons() {
        this.isRemainderEmailAllowed = false;
        this.isRemainderTextAllowed = false;
        this.isRebookAllowed = false;
        this.isModifyAllowed = false;
        this.isCheckedInAllowed = false;
        this.isSaveAllowed = false;
        this.isBookApptAllowed = false;
        const apptDateandTime = this.commonService.getDateTmFrmDBDateStr(this.apptData.Appt_Date_Time__c);
        if (isNullOrUndefined(this.apptData.No_Email__c)) {
            this.apptData.No_Email__c = 0;
        }
        const apptDate = this.commonService.getDateTmFrmDBDateStr(this.commonService.getDBDatStr(apptDateandTime));
        const todayDateandTime = new Date();
        const todayDate = this.commonService.getDateTmFrmDBDateStr(this.commonService.getDBDatStr(todayDateandTime));
        this.isTodayAppt = apptDate.getTime() === todayDate.getTime() ? true : false;
        /// previously isOpenAppt is with time consideration now considering only date
        const seconds = 24 * 3600 * 1000;
        this.isOpenAppt = apptDate.getTime() >= todayDate.getTime() ? true : false;
        if (this.isOpenAppt || (apptDate.getTime() === todayDate.getTime() - seconds) && this.apptData.apstatus !== 'Complete') {
            this.isSaveAllowed = true;
        }
        if (this.isOpenAppt && this.apptData.apstatus !== 'Complete') {
            this.isRemainderTextAllowed = true;
        }
        if (this.isOpenAppt && this.apptData.cltemail &&
            this.apptData.isTicket__c === 0 && this.apptData.No_Email__c !== 1 && this.apptData.apstatus !== 'Checked In' && this.apptData.apstatus !== 'Complete') {
            this.isRemainderEmailAllowed = true;
        }
        if ((apptDateandTime.getTime() > new Date().getTime()) && this.apptData.cltphone !== null && this.apptData.cltphone !== '' &&
            this.apptData.isTicket__c === 0 && this.apptData.Mobile_Carrier__c !== null && this.apptData.Mobile_Carrier__c !== ''
            && this.apptData.apstatus !== 'Checked In' && this.apptData.apstatus !== 'Complete') {
            this.remTextButtonValue = 'Reminder Text';
        }

        if (apptDate.getTime() >= todayDate.getTime() && this.apptData.apstatus !== 'Complete' && this.apptData.apstatus !== 'Canceled') {
            this.isModifyAllowed = true;
        }
        if (this.isTodayAppt && (this.apptData.apstatus === 'Checked In' || this.apptData.apstatus === 'Complete') && this.apptData.clientId) {
            this.isRebookAllowed = true;
        }
        this.isBookApptAllowed = !this.isRebookAllowed;
        this.isCheckedInAllowed = this.showCheckedIn(todayDate, apptDate, this.apptData.apstatus);
    }

    showCheckedIn(currentDate: Date, apptDate: Date, status: string): boolean {
        const statusName = status.toLowerCase();
        const YesterdayDate = new Date(currentDate.getTime());
        YesterdayDate.setDate(currentDate.getDate() - 1);
        if ((statusName === 'booked' || statusName === 'called' || statusName === 'confirmed' || statusName === 'pending deposit' ||
            statusName === 'reminder sent' || statusName === 'conflicting')
            && (apptDate.getTime() === YesterdayDate.getTime() || apptDate.getTime() === currentDate.getTime())) {
            return true;
        } else {
            return false;
        }
    }
    getpackagesListing() {
        const value = 'false';
        this.appointmentsService.getAllServicePackageDetails(value).subscribe(data => {
            this.packagesList = data['result'];
        },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    this.router.navigate(['/']).then(() => { });
                }
            });
    }
    getClientPackages(clientId) {
        this.apptDetailService.getClientPackagesData(clientId).subscribe(data => {
            this.clientPackages = data['result']['ClientPackageData'];
        },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    this.router.navigate(['/']).then(() => { });
                }
            });
    }
    // validateCheckInData() {

    //     const pckgObj = {};
    //     const result = this.commonService.getCheckInPrepaidPackage(this.packagesList, this.clientPackages, this.apptData, this.rows, this.clientid);

    // }
    checkIn() {

        const checkInData = this.commonService.getCheckInPrepaidPackages(this.packagesList, this.clientPackages, this.apptData, this.rows, this.clientid);
        if (this.clientid !== 'null' || !this.clientid) {
            checkInData['clientId'] = this.clientid;
        } else if (this.clientid === 'null') {
            checkInData['clientId'] = '';
        } else {
            checkInData['clientId'] = '';
        }
        checkInData['Status_Date_Time_c'] = this.commonService.getDBDatTmStr(new Date());
        this.apptDetailService.changeApptStatus(checkInData)
            .subscribe(data => {
                this.toastr.success('Appointment was successfullly checked in', null, { timeOut: 1500 });
                this.getApptServiceDetails(this.clientid, this.apptid);
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            if (statuscode === '2040') {
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                window.scrollTo(0, 0);
                            }
                            if (statuscode === '2085' || statuscode === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            } break;
                    }
                });
    }
    getBookingData() {
        this.apptDetailService.getBookingData().subscribe(
            data => {
                this.bookingDataList = data['result'];
                this.setStatusList(this.apptData.apstatus);
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    showNotesModal(Id, i) {
        this.serviceNotes = this.rows[i].Notes__c;
        this.ticketserviceId = Id;
        const notesTestarea = <HTMLTextAreaElement>document.getElementById('notesTestareaId');
        if (this.isSaveAllowed === true) {
            this.serviceNotesModal.show();
        }
        notesTestarea.value = this.rows[i].Notes__c;
        // this.indexTemp = i;
    }
    closeServiceNotesModal() {
        const notesTestarea = <HTMLTextAreaElement>document.getElementById('notesTestareaId');
        this.serviceNotesModal.hide();
    }
    cancelModel() {
        this.serviceNotesModal.hide();
    }
    cancelAppointmentDetails() {
        const callbacks = localStorage.getItem('viewCallback');
        if (this.actionMethod !== 'AppointmentDetail') {
            // this.router.navigate(['/appointments'], { queryParams: { date: this.apptData.Appt_Date_Time__c.split(' ')[0] } });
            this.router.navigate(['/appointments'], { queryParams: { date: callbacks } });
        } else {
            this.router.navigate(['/client/edit/' + this.clientid]);
        }
    }
    getHideClientContactInfo() {
        this.apptDetailService.getHideCliContactInfo(this.decodeUserToken.data.id).subscribe(data => {
            this.hideClientInfo = data['result'][0].Hide_Client_Contact_Info__c;
        }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    closeCancelApptModal() {
        this.cancelApptModal.hide();
    }
    sendCancelAppts() {
        const callback = JSON.parse(localStorage.getItem('viewCallback'));
        const obj = {
            'clientId': this.clientid,
            'date': callback.date
        };
        this.apptDetailService.cancelStsAppts(obj).subscribe(data => {
            const result = data['result'];
            if (result.length > 0) {
                this.apptDetailService.sendCancelReminder(result, false).subscribe(
                    data1 => {
                        const status = data1['result'];
                    }, error1 => {
                        const status = JSON.parse(error1['status']);
                        const statuscode = JSON.parse(error1['_body']).status;
                        switch (status) {
                            case 500:
                                break;
                            case 400:
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
            }
            this.router.navigate(['/appointments'], { queryParams: { date: localStorage.getItem('viewCallback') } });
            this.cancelApptModal.hide();

        }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }

    /* this code is written for issue no 2240 */
    @HostListener('window:popstate')
    onPopState() {
        this.router.navigate(['/appointments'], { queryParams: { date: this.apptData.Appt_Date_Time__c.split(' ')[0] } });
    }
    /* etraveler starts */
    etraveler() {
        if (this.showEtraveler === true && this.imagepath === 'assets/images/play-down.png') {
            this.showEtraveler = false;
            this.imagepath = 'assets/images/play-button.png';
            // this.clientServicesList = this.clientServicesList.slice(0, 5);
            // this.clientProductList = this.clientProductList.slice(0, 5);
            // this.hideLoadMoreButt = true;
        } else {
            if (this.decodedToken['Clients'] && this.decodedToken['Clients'][4]['allowAcces']) {
                if (this.clientid !== 'null') {
                    this.getClientData(this.clientid);
                    this.getServiceLog(this.clientid);
                    this.getProductLog(this.clientid);
                    this.getClientRewards(this.clientid);
                }
                this.imagepath = 'assets/images/play-down.png';
                this.showEtraveler = true;
            } else {
                this.imagepath = 'assets/images/play-button.png';
                this.showEtraveler = false;
            }
            // this.hideLoadMoreButt = true;
        }
    }
    getClientData(clientId) {
        this.apptDetailService.getClient(clientId)
            .subscribe(data => {
                this.clientData = data['result'].results[0];
                const createDate = this.clientData.CreatedDate.split(' ');
                this.clientSince = createDate[0].split('-')[1] + '/' + createDate[0].split('-')[2] + '/' + createDate[0].split('-')[0];
                if (this.clientData.Client_Flag__c) {
                    this.clientFlag = this.clientData.Client_Flag__c.split(';').join('\n');
                }
                if (this.clientData.Birthdate) {
                    this.birthDate = this.clientData.Birthdate.split('-')[0] + '/' + this.clientData.Birthdate.split('-')[1] + '/' + this.clientData.Birthdate.split('-')[2];
                }
                if (this.clientData.refName) {
                    this.referredClientData = this.clientData.refName.split(',').join(', ');
                }
                if (this.clientData.ReferredClient) {
                    this.referredClient = this.clientData.ReferredClient.split(',');
                }
                if (this.clientData.Title) {
                    this.clientTitle = this.clientData.Title;
                }
                if (this.clientData.Notes__c) {
                    this.clientNote = this.clientData.Notes__c;
                }
            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getServiceLog(clientid) {
        this.apptDetailService.getServiceLog(clientid).subscribe(
            data => {
                this.clientServicesList = data['result'];
                for (let i = 0; i < this.clientServicesList.length; i++) {
                    this.clientServicesList[i].servdate = this.commonService.getUsrDtStrFrmDBStr(this.clientServicesList[i].Service_Date_Time__c);
                }
                this.clientServicesList.forEach(element => {
                    if (element.Net_Price__c === '' || element.Net_Price__c === null || element.Net_Price__c === undefined) {
                        element.Net_Price__c = 0;
                    }
                    if (element.Booked_Package__c !== null && element.Booked_Package__c !== '') {
                        element.promotion = 'Prepaid Package';
                    }
                });
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    getProductLog(clientid) {
        this.apptDetailService.getProductLog(clientid).subscribe(
            data => {
                this.clientProductList = data['result'];
                for (let i = 0; i < this.clientProductList.length; i++) {
                    this.clientProductList[i].displaydate = this.commonService.getUsrDtStrFrmDBStr(this.clientProductList[i].Appt_Date_Time__c);
                }
                this.clientProductList.forEach(element => {
                    if (element.Qty_Sold__c || element.Net_Price__c) {
                        element.totalNetPrice = element.Net_Price__c * element.Qty_Sold__c;
                    }
                });
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    // load more
    showServiceLoadMore(app) {
        this.loadmore += 5;
        if (this.loadmore > app.length || this.loadmore === app.length) {
            this.hideLoadMoreButt = true;
        }
    }
    // load more
    showProductLoadMore(app) {
        this.loadmore += 5;
        if (this.loadmore > app.length || this.loadmore === app.length) {
            this.hideLoadMoreButt = true;
        }
    }
    getClientRewards(clientId) {
        this.apptDetailService.getClientRewardsData(clientId).subscribe(
            data => {
                this.clientRewardData = data['result'];
                if (this.clientRewardData[0].Points_Balance__c) {
                    this.rewardPoints = this.clientRewardData[0].Points_Balance__c;
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    printDiv() {
        if (this.referredClient[0] && this.referredClient[1]) {
            this.refferedClient = this.referredClient[0];
            this.refferedClient1 = this.referredClient[1]
        } else {
            this.refferedClient = '';
            this.refferedClient1 = '';
        }
        if (this.nextAppt[0] && this.nextAppt[1] && this.nextservicename) {
            this.nextappt = this.nextAppt[0];
            this.nextappt1 = this.nextAppt[0];
            this.serviceName = this.nextservicename;
        } else {
            this.nextappt = '';
            this.nextappt1 = '';
            this.serviceName = '';
        }
        if (this.apptData.CreatedBy) {
            this.createdBy = this.apptData.CreatedBy;
        } else {
            this.createdBy = '';
        }
        if (this.apptData.LastModifiedBy) {
            this.lastmodifiedBy = this.apptData.LastModifiedBy;
        } else {
            this.lastmodifiedBy = '';
        }
        const popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        let printContent = `
    <html>
      <head>
        <title>Appointment Details </title>
        <style>
          .reportTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          .reportTable td,
          .reportTable th {border: 1px solid rgb(141, 141, 141);padding: 8px;}
          .reportTable tr:nth-child(even) {background-color: #f2f2f2;}
          .reportTable th {
            padding-top: 6px;
            padding-bottom: 6px;
            text-align: left;
            background-color: #204d74;
            color: white;
          }
        </style>
      </head>
      <body onload="window.print();window.close()">
        <div>
          <table>
            <tr><td><b>${this.apptData.clientName}</b></td></tr>
            <tr><td>${this.apptData.mbphone}  <td></td><td></td><td><b>Appointment status:</b></td><td>${this.apptData.apstatus}</td><td></td></tr> 
            <tr><td>${this.apptData.cltphone} <td></td><td></td><td><b>Visit Type :</b></td><td>${this.apptData.visttype}</td><td></td></tr>
            <tr><td>${this.apptData.cltemail}</td></tr>
            <tr><td>${this.displayAptdate[0]}</td><td>${this.displayAptdate[1]}</td></tr> 
            <tr><td>Upcoming Appointment:</td><td>${this.nextappt}</td><td>${this.nextappt1}</td><td>${this.serviceName}</td></tr>
          </table>
          <br>
          <table class="reportTable">
            {{tableContent}}
          </table>
          </br>
          <table>
          <tr><td> Created By: </td><td>${this.createdBy}<td><td>${this.createdDate[0]}</td><td>${this.createdDate[1]}</td></tr>
          <tr><td> Modified By: </td><td>${this.lastmodifiedBy}<td><td>${this.lastModifyDate[0]}</td><td>${this.lastModifyDate[1]}</td></tr>
          </table>
          </br>
          <table>
          <tr><td><b>${this.apptData.clientName} eTraveler</b></td></tr>
          <tr><td>Referred By:</td><td>${this.refferedClient}</td><td>${this.refferedClient1}</td><td></td><td></td><td>Referred:</td><td>${this.referredClientData}</tr>
          </table>
          </br>
          <table>
          <tr>
          <td><b>Reward Points:</b></td><td>${this.rewardPoints}</td>
          <td></td><td></td><td></td>
          <td><b>Client since:</b></td><td>${this.clientSince}</td><td>
          </td><td></td><td></td><td></td>
          <td><b>Date Of Birth:</b></td><td>${this.birthDate}</td>
          <td></td>
          <td><b>Client title:</b></td><td>${this.clientTitle}</b></td>
          <td></td><td><b>client Flag:</b></td><td>${this.clientFlag}</td>
          <td></td><td><b>Client Note:</b></td><td>${this.clientNote}</td></tr>
          </table>
          </br>
          <tr><td></td><td><b>Service History</b></td></tr>
          <table class="reportTable">
          {{tableContent1}}
          </table>
          <tr><td></td><td><b>Retail History</b></td></tr>
          <table>
          <table class="reportTable">
          {{tableContent2}}
          </table>
          </table>
        </div>
      </body>
    </html>`;
        printContent = printContent.replace('{{tableContent}}', document.getElementById('tableContentId').innerHTML);
        if (this.clientServicesList.length > 0) {
            printContent = printContent.replace('{{tableContent1}}', document.getElementById('tableContentId1').innerHTML);
        } else {
            printContent = printContent.replace('{{tableContent1}}', 'No service history found..');
        }
        if (this.clientProductList.length > 0) {
            printContent = printContent.replace('{{tableContent2}}', document.getElementById('tableContentId2').innerHTML);
        } else {
            printContent = printContent.replace('{{tableContent2}}', 'No Retail history found..');
        }
        popupWin.document.write(printContent);
        popupWin.document.close();
    }
}// main end
