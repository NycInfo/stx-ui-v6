/*
ngOnInit() : Method to loading athe page..
searchClients() : Method for searching clients
showData() : Method for loading All clients Data.
clearmessage() : Method for Clearing  error messages.
*/
import { Component, OnInit, TemplateRef, ViewChild, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { BookOutDetailService } from './bookoutdetail.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../common/common.service';
import { ConfirmDialog } from '../../../custommodules/primeng/primeng';
@Component({
    selector: 'app-appointments-popup',
    templateUrl: './bookoutdetail.html',
    styleUrls: ['./bookoutdetail.css'],
    providers: [BookOutDetailService, CommonService]
})
export class BookOutDetailComponent implements OnInit {
    bsValue: any = new Date();
    endbsValue: any = new Date();
    minDate: any = new Date();
    bookingDataList: any;
    bookingIntervalMinutes: number;
    TimeData: any;
    workerList: any = [];
    workerName: any;
    apptData: any = {};
    apptid: any;
    notes: any;
    apptStatus = [];
    startTime: any;
    timeStartTime: any;
    endTime: any;
    timeEndTime: any;
    statusColor = { 'background-color': '' };
    error: any = '';
    createdDate = ['', ''];
    modifiedDate = ['', ''];
    appointmentDate = ['', ''];
    appointmentEndDate = ['', ''];
    apptEndDate: any;
    datePickerConfig: any;
    wName = '';
    @ViewChild('staticModal') public staticModal: ModalDirective;
    constructor(
        private activatedRoute: ActivatedRoute,
        private bookOutDetailService: BookOutDetailService,
        private toastr: ToastrService,
        private router: Router,
        private modalService: BsModalService,
        private commonService: CommonService) {
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
        this.activatedRoute.queryParams.subscribe(params => {
            this.apptid = activatedRoute.snapshot.params['apptid'];
        });
    }
    /*Method for page Load */
    ngOnInit() {
        //  this.getBookingData();
        this.getApptDetails(this.apptid);
        this.getWorkerList();

    }
    getStartTime(dateObj: Date) {
        let elem = '';
        if (dateObj.getHours() < 12) {
            if (dateObj.getHours() === 0) {
                elem = '12:' + ('0' + dateObj.getMinutes()).slice(-2) + ' AM';
            } else {
                elem = ('0' + dateObj.getHours()).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2) + ' AM';
            }
        } else {
            if ((dateObj.getHours() - 12) === 0) {
                elem = '12:' + ('0' + dateObj.getMinutes()).slice(-2) + ' PM';
            } else {
                elem = ('0' + (dateObj.getHours() - 12)).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2) + ' PM';
            }
        }
        this.startTime = elem;
    }
    getEndTime(dateObj: Date) {
        let elem = '';
        if (dateObj.getHours() < 12) {
            if (dateObj.getHours() === 0) {
                elem = '12:' + ('0' + dateObj.getMinutes()).slice(-2) + ' AM';
            } else {
                elem = ('0' + dateObj.getHours()).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2) + ' AM';
            }
        } else {
            if ((dateObj.getHours() - 12) === 0) {
                elem = '12:' + ('0' + dateObj.getMinutes()).slice(-2) + ' PM';
            } else {
                elem = ('0' + (dateObj.getHours() - 12)).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2) + ' PM';
            }
        }
        this.endTime = elem;
    }
    /**
     * Method to get AppointMent Details
     */
    getApptDetails(apptid) {
        this.bookOutDetailService.getApptDetails(apptid).subscribe(data => {
            this.apptData = data['result'][0];
            this.appointmentDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.apdate);
            this.apptData.workers = this.apptData.workers;
            this.appointmentEndDate = this.commonService.getUsrDtStrFrmDBStr(this.commonService.addMinToDBStr(this.apptData.endDate, parseInt(this.apptData.aptDuration, 10)));
            this.createdDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.creadate);
            this.modifiedDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData.lastmofdate);
            const crtDate = new Date();
            const timeDiff = crtDate.getTimezoneOffset();
            const apptDate = new Date(data.result[0].apdate);
            this.endbsValue = this.commonService.getDateTmFrmDBDateStr(data.result[0].apdate);
            this.endbsValue.setMinutes((this.endbsValue.getMinutes() + parseInt(this.apptData.aptDuration, 10)));
            this.getBookingData();
            this.workerName = this.apptData.workerId;
            this.bsValue = this.commonService.getDateTmFrmDBDateStr(data.result[0].apdate);
            this.timeStartTime = new DatePipe('en-Us').transform(this.bsValue, 'hh:mm a');
            this.getStartTime(apptDate);
            const date1 = apptDate;
            const date2 = data.result[0].duration;
            const time1 = new Date(date1).getTime() + parseInt(this.apptData.aptDuration, 10) * 60000;
            this.endbsValue = this.commonService.getUsrDtStrFrmDBStr(this.commonService.getDBDatTmStr(this.endbsValue));
            this.timeEndTime = new DatePipe('en-Us').transform(time1, 'hh:mm a');
            const time = new Date(apptDate).getTime();
            this.apptData.endTime = new Date(time1);
            this.getEndTime(new Date(time1));
        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    // Method to get booking time hours data
    getBookingData() {
        this.bookOutDetailService.getBookingData().subscribe(
            data => {
                this.bookingDataList = data['result'];
                this.setStatusList(this.apptData.apstatus);
                this.bookingIntervalMinutes = this.bookingDataList.bookingIntervalMinutes;
                this.method();
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    // Method to get timeHours
    method() {
        let datIndex = 0;
        const crDate = new Date();
        const startDate = new Date(0, 0, 0, 0, 0, 0, 0);
        const endDate = new Date(0, 0, 1, 0, 0, 0, 0);
        this.TimeData = [];
        do {
            let elem = '';
            if (startDate.getHours() < 12) {
                if (startDate.getHours() === 0) {
                    elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
                } else {
                    elem = ('0' + startDate.getHours()).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
                }
            } else {
                if ((startDate.getHours() - 12) === 0) {
                    elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
                } else {
                    elem = ('0' + (startDate.getHours() - 12)).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
                }
            }
            this.TimeData.push(elem);
            if (crDate.getHours() < startDate.getHours()) {
                datIndex++;
            }
            startDate.setMinutes(startDate.getMinutes() + this.bookingIntervalMinutes);
        }
        while (startDate < endDate);
    }
    /**
     * Method to get worker List
    */
    getWorkerList() {
        this.bookOutDetailService.getWorkerList().subscribe(data => {
            let temp = 0;
            this.workerList = [];
            this.workerList = data['result'].filter(filterList => filterList.IsActive);
            for (let i = 0; i < this.workerList.length; i++) {
                if (this.workerList[i]['Id'] === this.apptData['workerId']) {
                    temp++;
                }
            }
            if (temp === 0) {
                this.workerList.push({
                    'Id': this.apptData['workerId'],
                    'FullName': '(' + this.apptData['workerName'] + ')'
                })
            }
        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /**
     * Method to change Worker List
    */
    workerListOnChange(value) {
        this.workerName = value;
    }
    clearErrorMsg() {
    }
    setStatusList(apptStatus) {
        this.apptStatus = [
            { 'status': 'Canceled' },
            { 'status': 'Booked' },
        ];
        this.setStatusColor(apptStatus);
    }

    setStatusColor(apptStatus) {
        if (apptStatus === 'Canceled') {
            this.statusColor = { 'background-color': this.bookingDataList.canceledStatusColor };
        }
        if (apptStatus === 'Booked') {
            this.statusColor = { 'background-color': this.bookingDataList.bookedStatusColor };
        }
    }
    clearErrMsg() {
        this.error = '';
    }
    /**
     * Method to send BookoutDetail data
    */
    bookOutDetail() {
        let startTimeHour: any = 0;
        let startTimeMins: any = 0;

        let endTimeHour: any = 0;
        let endTimeMins: any = 0;
        if (this.startTime.split(' ')[1] === 'AM') {
            startTimeHour = this.startTime.split(' ')[0].split(':')[0];
            startTimeMins = this.startTime.split(' ')[0].split(':')[1];
        } else if (this.startTime.split(' ')[1] === 'PM') {
            startTimeHour = this.startTime.split(' ')[0].split(':')[0];
            if (parseInt(startTimeHour, 10) !== 12) {
                startTimeHour = parseInt(startTimeHour, 10) + 12;
            }
            startTimeMins = this.startTime.split(' ')[0].split(':')[1];
        }
        const startDate = new Date(this.bsValue.getFullYear(), this.bsValue.getMonth(),
            this.bsValue.getDate(), startTimeHour, startTimeMins);

        if (this.endTime.split(' ')[1] === 'AM') {
            endTimeHour = this.endTime.split(' ')[0].split(':')[0];
            endTimeMins = this.endTime.split(' ')[0].split(':')[1];
        } else if (this.endTime.split(' ')[1] === 'PM') {
            endTimeHour = this.endTime.split(' ')[0].split(':')[0];
            if (parseInt(endTimeHour, 10) !== 12) {
                endTimeHour = parseInt(endTimeHour, 10) + 12;
            }
            endTimeMins = this.endTime.split(' ')[0].split(':')[1];
        }
        const endDate = new Date(this.bsValue.getFullYear(), this.bsValue.getMonth(),
            this.bsValue.getDate(), endTimeHour, endTimeMins);
        let c = 0;
        let c3 = 0;
        if (parseInt(endTimeHour, 10) > parseInt(startTimeHour, 10)) {
            c = parseInt(endTimeHour, 10) - parseInt(startTimeHour, 10);
        }
        if (parseInt(endTimeMins, 10) > parseInt(startTimeMins, 10)) {
            c3 = parseInt(endTimeMins, 10) - parseInt(startTimeMins, 10);
        }
        if (!(startDate.getTime() < endDate.getTime())) {
            this.error = 'BOOKOUTDETAIL.START_TIME_PRIOR_END_TIME';
        } const bookoutDuration = (endDate.getTime() - startDate.getTime()) / (1000 * 60);
        if (this.error === '') {
            const dataObj = {
                'status': this.apptData.apstatus,
                'notes': this.apptData.notes,
                'workerName': this.workerName,
                // 'startdate': this.bsValue,
                'starttime': this.commonService.getDBDatTmStr(startDate),
                // 'endtime': this.endTime,
                'Duration__c': bookoutDuration
            };
            this.bookOutDetailService.updateBookoutData(dataObj, this.apptid).subscribe(data => {
                const updateStatus = data['result'];
                this.router.navigate(['/appointments'], { queryParams: { date: this.apptData.apdate.split(' ')[0] } });
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }

    getUserTime(dateUtcString) {
        if (dateUtcString) {
            return new Date(this.commonService.UTCStrToUsrTmzStr(dateUtcString));
        }
    }
    opencancelModal() {
        if (this.workerList.length > 0) {
            const temp = this.workerList.filter(ele => ele.Id === this.workerName);
            this.wName = temp[0]['FullName'];
        } else {
            this.wName = '';
        }
        this.staticModal.show();
    }
    cancelAppts(type) {
        const dt = this.apptData.apdate.split(' ')[0];
        const obj = {
            'apptId': this.apptid,
            'date': dt,
        };
        if (type === 'single') {
            obj['workerId'] = this.workerName;
        }
        this.bookOutDetailService.bookOutcancelStsAppts(obj).subscribe(data => {
            const result = data['result'];
            this.router.navigate(['/appointments']);
            this.staticModal.hide();
        }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    cancelBookoutDetails() {
        this.router.navigate(['/appointments'], { queryParams: { date: this.apptData.apdate.split(' ')[0] } });
    }

    /* this code is written for issue no 2240 */
    @HostListener('window:popstate')
    onPopState() {
        this.router.navigate(['/appointments'], { queryParams: { date: this.apptData.apdate.split(' ')[0] } });
    }
    /* end of the code */

}
