import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookOutDetailComponent } from './bookoutdetail.component';
import { BookOutDetailRoutingModule } from './bookoutdetail.routing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from 'ng2-translate';
import { ShareModule } from '../../common/share.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
    imports: [
        CommonModule,
        BookOutDetailRoutingModule,
        FormsModule,
        TranslateModule,
        ShareModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot()

    ],
    declarations: [
        BookOutDetailComponent,
    ]
})
export class BookOutDetailModule {
}
