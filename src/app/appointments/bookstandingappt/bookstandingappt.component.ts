/*
ngOnInit() : Method to loading athe page..
addServices(): Method to add new service
removeServices(index): Method to remove current service
getbookEvery(): Method to get every data
getBookingData():Method to get time intervals
getServiceGroups(): Method to get service groups
categoryOfService(value, i): Method to change service group name
servicesListOnChange(value, i) : Method to change service names
workerListOnChange(): Method to change worker
calculateServiceDurations(): Method to get service durations
getpackagesListing(): Method to get service packages data
method(): Method to get time Hours
getNumberofBookOuts(): Method to get number of appointments
listVisitTypes(): Mehtod to get visit types data
onVisitTypeChange(): Method to change visit types
clearErrorMsg(): Mehtod to clear error messages
searchAppointmentData(): Method to validate search appointment results
*/
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment/moment';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { BookStandingApptService } from './bookstandingappt.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../common/common.service';
import { NewClientService } from '../../../app/clients/newclient/newclient.service';
import { isNullOrUndefined } from 'util';
import { SpawnSyncOptions } from 'child_process';
import { retry } from 'rxjs/operator/retry';

@Component({
    selector: 'app-appointments-popup',
    templateUrl: './bookstandingappt.html',
    styleUrls: ['./bookstandingappt.component.css'],
    providers: [BookStandingApptService, CommonService, NewClientService]
})
export class BookStandingApptComponent implements OnInit {
    clientNotes: '';
    bookAllType: any;
    showMore = false;
    public bsValue: Date = new Date();
    public bsValue1: any = new Date();
    rows = [];
    addServiceGroupName: any;
    serviceGroupName: any;
    serviceName: any;
    serviceDetailsList = [];
    serviceGroupList: any;
    serviceId: any;
    workerList = [];
    sumOfServiceDurations: any = 0;
    bookStandingVisitType: any = '';
    visitTypesList: any;
    visitTypeValue: any;
    bookStandingText: any = '';
    bookStandingTime: any = '';
    numberOfBooks: any;
    numberOfBookStandings: any;
    bookEveryData: any = [];
    bookEvery: any;
    bookEvery1: any;
    everyData: any = [];
    packagesList: any;
    bookStandErr: any;
    appointmentData: any = [];
    TimeData: any;
    bookingDataList: any;
    bookingIntervalMinutes: number;
    workerName: any;
    apptBookStandingData: any;
    scheduleAvailableButton: any = false;
    apptBookoutData: any = [];
    minDate: Date;
    testD: any = new Date();
    enddDate: any;
    clientId: any;
    type: any;
    packageGroupList: any;
    error: any;
    serviceGroupColour: any;
    startDateWithTime: any;
    workerHours: any = [];
    workersWithServiceDuration = [];
    selectedStartDates: any = [];
    datePickerConfig: any;
    clientData: any;
    cliData: any = [];
    checkConflictError: any = '';
    showCliName: any;
    passdate: any;
    serviceTax: any = 0;
    resourceSlot: any;
    resStatus: any = {};
    serviceDetailKeys = ['Duration_1__c', 'Duration_2__c', 'Duration_3__c',
        'Buffer_After__c', 'Guest_Charge__c', 'Net_Price__c'];
    apptEndDate: Date;
    apptEndDate22: Date;
    cliEmail: any = '';
    cliEmailErr = '';
    sentNotify: any = false;
    getWorker = [];
    bookingExpress: any = [];
    isPreferredDuration: any;
    preferredDur: any = 0;
    prefService: 0;
    servicePrice = 0;
    serviceDurations: any;
    searchBtn = false;
    // clientInfo: any;
    @ViewChild('serviceNotesModal') public serviceNotesModal: ModalDirective;
    @ViewChild('notificationModal') public notificationModal: ModalDirective;
    constructor(
        private bookStandingApptService: BookStandingApptService,
        private toastr: ToastrService,
        private translateService: TranslateService,
        private newClientService: NewClientService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private commonService: CommonService) {
        this.minDate = new Date();
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
        this.activatedRoute.queryParams.subscribe(params => {
            this.clientId = activatedRoute.snapshot.params['Id'];
        });
    }
    /*Method for page Load */
    ngOnInit() {
        this.searchBtn = false;
        this.apptEndDate = this.bsValue;
        // this.apptEndDate22 = this.bsValue;
        this.getClientAppointments(this.clientId);
        this.getWorkerList();
        this.getClientData(this.clientId);
        // this.addServices();
        // this.getServiceGroups();
        this.listVisitTypes();
        this.getbookEvery();
        this.getpackagesListing();
        this.getEvery();
        this.getBookingData();
        this.getNumberofBookOuts();
        this.getClientInfo();
        this.getAllActivePackages();
        this.getServRetTaxs();
        // this.endDateChange();
        this.testD.setFullYear(this.testD.getFullYear() + 1); // dependency for onload end date
        // this.getWorkerListHours();
        // this.bsValue.setHours(0, 0, 0, 0);
        this.bsValue1.setFullYear(this.bsValue.getFullYear() + 1);
        this.bsValue1.setHours(23, 59, 59, 0);
        const localTime = this.bsValue.toLocaleString('en-US', { hour: 'numeric', hour12: true });
        const splitLocalTime = localTime.split(' ');
        if (this.bsValue.getHours() < 9) {
            this.bookStandingTime = '0' + splitLocalTime[0] + ':00 ' + splitLocalTime[1];
        } else {
            if (+splitLocalTime[0] < 10) {
                this.bookStandingTime = '0' + splitLocalTime[0] + ':00 ' + splitLocalTime[1];
            } else {
                this.bookStandingTime = splitLocalTime[0] + ':00 ' + splitLocalTime[1];
            }
        }
    }
    populateFromDate() {
        if (this.bsValue > this.bsValue1) {
            this.bsValue1 = new Date(this.bsValue.getFullYear(), this.bsValue.getMonth(), this.bsValue.getDate(), 23, 59, 59, 0);
        }
    }
    endDateChange() {
        const dt = this.bsValue;
        const tt = new Date(dt.setFullYear(dt.getFullYear() + 1));
        // dt.setFullYear(dt.getFullYear());
        // dt.setMonth(dt.getMonth());
        // dt.setDate(dt.getDate());
        this.testD = tt;

        //    this.testD.setFullYear(startDate.getFullYear() + 1);
        //    this.testD.setMonth(startDate.getMonth());
        //    this.testD.setDate(startDate.getDate());
    }
    /**
* Method to get service tax  and retail tax calculation
*/
    getServRetTaxs() {
        this.bookStandingApptService.getServProdTax().subscribe(
            data => {
                const taxData = JSON.parse(data['result'][2].JSON__c);
                this.serviceTax = taxData.serviceTax;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /**
    * Common methods starts
    */
    getClientData(clientId) {
        this.bookStandingApptService.getClient(clientId)
            .subscribe(data => {
                this.clientData = data['result'].results[0];
                this.clientNotes = data['result'].results[0].Notes__c;
                this.cliEmail = this.clientData['Email'] ? this.clientData['Email'] : '';
                this.showCliName = data['result'].results[0].FirstName + ' ' + data['result'].results[0].LastName;
                if (this.clientData.Booking_Restriction_Type__c === 'Do Not Book' || this.clientData.Booking_Restriction_Type__c === 'Alert Only') {
                    this.serviceNotesModal.show();
                }
            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getClientInfo() {
        this.bookStandingApptService.getClient(this.clientId).subscribe(
            data => {
                // const displayName = document.getElementById('displayNameId');
                // displayName.innerHTML = 'Appointments: Book Standing - ' + data.result.results[0].FirstName + ' ' + data.result.results[0].LastName;
                // displayName.innerHTML = '- ' + data.result.results[0].FirstName + ' ' + data.result.results[0].LastName;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    // add new service dynamically
    addServices() {
        // this.rows.push({ Id: '', serviceGroupName: this.serviceGroupName });
        // this.workerList[i] = [];
        // this.serviceDetailsList[i] = [];
        // const index = this.rows.length - 1;
        // if (index !== 0) {
        //     this.categoryOfService(this.serviceGroupName, index);
        // }
        this.rows.push({});
        if (this.getWorker.length > 0) {
            this.expressService(this.getWorker[0].workerId + '$' + this.getWorker[0].workerName, this.rows.length - 1, true);
        }
    }

    bookAnyWay() {
        this.serviceNotesModal.hide();
    }
    // Remove current service dynamically
    removeServices(index) {
        this.rows.splice(index, 1);
        this.workerList.splice(index, 1);
        this.serviceDetailsList.splice(index, 1);
        // this.calculateServiceDurations(index);
        this.calculateServiceDurations1();
        this.searchBtn = true;
    }
    // Method to get book every data
    getbookEvery() {
        this.bookStandingApptService.getbookEveryTypes().subscribe(
            data => {
                this.bookEveryData = data['bookstandingevery'];
                this.bookEvery = this.bookEveryData[0].value;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getEvery() {
        this.bookStandingApptService.getEveryTypes().subscribe(
            data => {
                this.everyData = data['bookstandingEvery'];
                this.bookEvery1 = this.everyData[0].type;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    // Method to get booking time hours data
    getBookingData() {
        this.bookStandingApptService.getBookingData().subscribe(
            data => {
                this.bookingDataList = data['result'];
                this.bookingIntervalMinutes = this.bookingDataList.bookingIntervalMinutes;
                this.method();
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    // Method for service groups
    // getServiceGroups() {
    //     const reqDate = this.commonService.getDBDatStr(this.bsValue);
    //     this.bookStandingApptService.getServiceGroups('Service', reqDate).subscribe(data => {
    //         this.serviceGroupList = data['result']
    //             .filter(filterList => filterList.active && !filterList.isSystem);
    //         if (this.serviceGroupList.length > 0) {
    //             this.serviceGroupName = this.serviceGroupList[0].serviceGroupName;
    //             this.serviceGroupName = this.serviceGroupName + '$' + this.serviceGroupList[0].serviceGroupColor;
    //             this.rows[0].serviceGroupName = this.serviceGroupName;
    //             this.categoryOfService(this.serviceGroupName, 0);
    //         }

    //     },
    //         error => {
    //             const status = JSON.parse(error['status']);
    //             const statuscode = JSON.parse(error['_body']).status;
    //             switch (JSON.parse(error['_body']).status) {
    //                 case '2033':
    //                     break;
    //             }
    //             if (statuscode === '2085' || statuscode === '2071') {
    //                 if (this.router.url !== '/') {
    //                     localStorage.setItem('page', this.router.url);
    //                     this.router.navigate(['/']).then(() => { });
    //                 }
    //             }
    //         });
    // }
    // Method to get package groups
    getAllActivePackages() {
        const currentDate = this.commonService.getDBDatStr(new Date()).split(' ')[0];
        this.bookStandingApptService.getPackageGroups(currentDate)
            .subscribe(data => {
                this.packageGroupList = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }


    categoryOfService(value, i) {
        if (value.indexOf('scale') === 0) {
            this.type = 'Package';
        } else {
            this.type = 'ApptService';
        }
        this.appointmentData = [];
        const serviceGroupName = value.split('$')[0];
        this.rows[i].Id = '';
        this.serviceDetailsList[i] = [];
        this.workerList[i] = [];
        this.rows[i].serviceName = '';
        this.rows[i].workerName = '';
        this.removeServiceDetails(i);
        this.calculateServiceDurations(i);
        this.bookStandingApptService.getServices(serviceGroupName, this.type, this.commonService.getDBDatStr(this.bsValue)).subscribe(data => {
            if (this.type === 'Package') {
                const services: Array<any> = data['result']['serviceresultJson'];
                const serviceRelatedWorkers: Array<any> = data['result']['results'];
                const DupserviceGroupName = serviceGroupName;
                const packageId = serviceGroupName.split(':')[1];
                if (this.serviceDetailsList[i]) {
                    this.serviceDetailsList.splice(i, 1);
                }
                if (this.workerList[i]) {
                    this.workerList.splice(i, 1);
                }
                if (this.rows[i]) {
                    this.rows.splice(i, 1);
                }
                const length = this.rows.length;
                services.filter((service, index) => {
                    this.rows.push({ Id: '', serviceGroupName: DupserviceGroupName });
                    this.serviceDetailsList[length + index] = services;
                    const workers = [];
                    serviceRelatedWorkers.filter((worker) => {

                        if (worker.sId === service.Id) {
                            workers.push(worker);
                        }
                    });

                    this.workerList[length + index] = workers;
                    this.rows[length + index]['IsPackage'] = 1;
                    this.rows[length + index]['Booked_Package__c'] = packageId;
                    this.rows[length + index]['serviceGroupName'] = this.rows[length]['serviceGroupName'];
                    this.rows[length + index]['workerName'] = workers.length > 0 ? workers[0].workername : '';
                    this.rows[length + index]['Id'] = service.Id;
                    this.rows[length + index]['serviceGroupColour'] = service.serviceGroupColor;
                    // this.serviceDetailKeys.map((key) => {
                    //     this.rows[length + index][key] = workers[0][key] ? +workers[0][key] : 0;
                    // });
                    if (this.rows[length + index]['workerName']) {
                        Object.assign(this.rows[length + index], this.commonService.getServiceDurations(workers[0], this.clientId, this.cliData, this.rows[length + index]));
                    }
                    // this.rows[length + index].serviceGroupColour = '';
                    this.calculateServiceDurations(length + index);
                });
                if (this.rows) {
                    this.rows = this.rows.filter((obj) => obj.workerName !== '');
                }
            } else {
                this.serviceDetailsList[i] = data['result']['result'];
                this.rows[i]['IsPackage'] = 0;
                this.rows[i]['Booked_Package__c'] = '';
                this.rows[i].serviceGroupColour = value.split('$')[1];
            }


        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    // Method to chane service list

    // servicesListOnChange(serviceId, i) {
    //     /*  below  description is used to show the service description for the service*/
    //     let temp = [];
    //     temp = this.serviceDetailsList[i].filter((obj) => obj.Id === serviceId);
    //     if (temp && temp.length > 0) {
    //         this.rows[i]['desc'] = temp[0]['Description__c'];
    //     }
    //     /* end of service description */
    //     this.appointmentData = [];
    //     this.workerList[i] = [];
    //     this.rows[i].workerName = '';
    //     this.removeServiceDetails(i);
    //     this.calculateServiceDurations(i);
    //     this.rows[i]['serviceName'] = serviceId;
    //     const bookingdata = {
    //         bookingdate: this.commonService.getDBDatStr(this.bsValue),
    //         serviceIds: [this.rows[i].Id]
    //     };
    //     this.bookStandingApptService.getUsers(bookingdata).subscribe(data => {
    //         this.workerList[i] = data['result'];
    //         if (data['result'] && data['result'].length > 0) {
    //             this.rows[i].workerName = this.workerList[i][0].workername;
    //             this.workerListOnChange(this.rows[i].workerName, i);
    //             // this.rows[i].name = this.workerList[i][0].name;
    //             //     this.showWaitinglist = true;
    //         }
    //     },
    //         error => {
    //             const status = JSON.parse(error['status']);
    //             const statuscode = JSON.parse(error['_body']).status;
    //             switch (JSON.parse(error['_body']).status) {
    //                 case '2033':
    //                     break;
    //             }
    //             if (statuscode === '2085' || statuscode === '2071') {
    //                 if (this.router.url !== '/') {
    //                     localStorage.setItem('page', this.router.url);
    //                     this.router.navigate(['/']).then(() => { });
    //                 }
    //             }
    //         });
    // }
    // Method to chane worker

    workerListOnChange(value, i) {
        this.appointmentData = [];
        this.workerName = value;
        this.workerList[i].filter((worker) => worker.workername === this.workerName).map((worker) => {
            Object.assign(this.rows[i], this.commonService.getServiceDurations(worker, this.clientId, this.cliData, this.rows[i]));
        });
        this.calculateServiceDurations(i);
        // this.clientdata = JSON.parse(localStorage.getItem('bookstanding'));
    }

    removeServiceDetails(index) {
        this.serviceDetailKeys.map((key) => {
            delete this.rows[index][key];
        });
    }


    // Method to calculate the service durations
    calculateServiceDurations(i) {
        this.appointmentData = [];
        if (this.rows && this.rows.length > 0) {
            this.sumOfServiceDurations = 0;
            for (let j = 0; j < this.rows.length; j++) {
                let totalDuration = 0;
                if (!isNullOrUndefined(this.rows[j]['workerName']) && this.rows[j]['workerName'] !== '') {
                    totalDuration += +this.rows[j]['Duration_1__c'];
                    totalDuration += +this.rows[j]['Duration_2__c'];
                    totalDuration += +this.rows[j]['Duration_3__c'];
                    totalDuration += +this.rows[j]['Buffer_After__c'];
                    this.rows[j].Duration__c = totalDuration;
                    this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
                }

            }
        }
    }
    // Method to get packagelisting
    getpackagesListing() {
        this.bookStandingApptService.getAllServiceDetails().subscribe(data => {
            this.packagesList = data['result'];
        },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }

    method() {
        let datIndex = 0;
        const crDate = new Date();
        const startDate = new Date(0, 0, 0, 0, 0, 0, 0);
        const endDate = new Date(0, 0, 1, 0, 0, 0, 0);
        this.TimeData = [];
        do {
            let elem = '';
            if (startDate.getHours() < 12) {
                if (startDate.getHours() === 0) {
                    elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
                } else {
                    elem = ('0' + startDate.getHours()).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
                }
            } else {
                if ((startDate.getHours() - 12) === 0) {
                    elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
                } else {
                    elem = ('0' + (startDate.getHours() - 12)).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
                }
            }
            this.TimeData.push(elem);
            if (crDate.getHours() < startDate.getHours()) {
                datIndex++;
            }
            startDate.setMinutes(startDate.getMinutes() + this.bookingIntervalMinutes);
        }
        while (startDate < endDate);
    }
    // Method to get number of appointments
    getNumberofBookOuts() {
        this.numberOfBooks = [];
        for (let i = 0; i < 20; i++) {
            this.numberOfBooks.push({ 'availability': i + 1 });
        }
        const oneDay = 24 * 60 * 60 * 1000;
        let diffDays = Math.round(Math.abs((this.bsValue1.getTime() - this.bsValue.getTime()) / (oneDay)));
        if (this.bookEvery1 === 'Months') {
            //  appDate.setDate(apptDate[key].getDate() + setDatebyNumber * this.bookEvery);
            diffDays = Math.round(diffDays / 30);
        } else if (this.bookEvery1 === 'Weeks') {
            diffDays = Math.round(diffDays / 7);
            //    appDate.setDate(apptDate[key].getDate() + setDatebyNumber * 7 * this.bookEvery);
        }
        this.numberOfBookStandings = diffDays + 1;
    }


    // Method to get visit types
    listVisitTypes() {
        this.bookStandingApptService.getVisitTypes().subscribe(
            data => {
                this.visitTypesList = data['result'];
                this.visitTypeValue = this.visitTypesList[0].visitType;
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    // Method to chane visit types
    onVisitTypeChange(value) {
        this.visitTypeValue = value;
    }
    // Method to clear error messages
    clearErrorMsg() {
        this.bookStandErr = '';
        this.checkConflictError = '';
        this.cliEmailErr = '';
    }

    // To calculate End Date
    calculateEndDate() {
        const serviceStartDate: any = {};
        serviceStartDate['startTime'] = this.bsValue;
        if (this.numberOfBookStandings === 1) {
            this.apptEndDate = serviceStartDate['startTime'];
        } else {
            const enddate = this.setApptDates(serviceStartDate, this.bookEvery1, +this.numberOfBookStandings - 1);
            this.apptEndDate = enddate['startTime'];
        }
    }
    // Method validating for search appointment results
    searchAppointmentData() {
        // this.getNumberofBookOuts();
        if (this.bsValue.setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
            this.bookStandErr = 'BOOKSTANDING_APPT.VALID_SELECT_START_DATE';
            window.scrollTo(0, 0);
        } else if (this.bsValue1.setHours(23, 59, 59, 0) < this.bsValue.setHours(0, 0, 0, 0)) {
            this.bookStandErr = 'BOOKSTANDING_APPT.VALID_SELECT_END_DATE';
            window.scrollTo(0, 0);
        } else if (this.bookStandingTime === '' || this.bookStandingTime === undefined) {
            this.bookStandErr = 'BOOKSTANDING_APPT.VALID_SELECT_START_TIME';
            window.scrollTo(0, 0);
        } else {
            this.bookStandErr = '';
            let serviceStartDate: Date = this.commonService.timeConversionToDate(this.bookStandingTime, this.bsValue);
            /* below for loop added for worker future start date assigning purpose */
            // for (let i = 0; i < this.rows.length; i++) {
            //     // const tempDt = this.commonService.getDateFrmDBDateStr(this.workerList[i].filter(obj => obj['workername'] === this.rows[i].workerName)[0]['StartDay']);
            //     let tempDt;
            //     if (this.getWorker[i].filter(obj => obj['workerName'] === this.rows[i].worker)[0]['StartDay']) {
            //         tempDt = this.commonService.getDateFrmDBDateStr(this.getWorker[i].filter(obj => obj['workerName'] === this.rows[i].worker)[0]['StartDay']);
            //     } else {
            //         tempDt = this.commonService.getDateFrmDBDateStr(this.rows[i]['StartDay']);
            //     }
            //     if (tempDt > serviceStartDate) {
            //         serviceStartDate = tempDt;
            //     }
            // }
            this.workersWithServiceDuration = [[]];
            this.rows.forEach((obj) => {
                let apptDuration: { 'startTime': Date, 'endTime': Date };
                const duration = obj['service']['sumDurationBuffer'] ? obj['service']['sumDurationBuffer'] : obj['service']['dursergrp'];
                const apptStartDate: Date = serviceStartDate;
                const apptEndDate: Date = this.commonService.setEndTime(apptStartDate, duration);
                apptDuration = {
                    'startTime': apptStartDate,
                    'endTime': apptEndDate
                };
                serviceStartDate = apptEndDate;
                this.workersWithServiceDuration[0].push(apptDuration);
            });
            let workerIds = '';
            // if (this.rows  && this.rows .length > 0) {
            for (let j = 0; j < this.rows.length; j++) {

                workerIds += "'" + this.rows[j].worker.split('$')[0] + "',";
                // }
            }
            workerIds = workerIds.slice(0, -1);
            this.getNumberofBookOuts();
            if (this.numberOfBookStandings && this.numberOfBookStandings > 0) {
                for (let i = 1; i < this.numberOfBookStandings; i++) {
                    this.workersWithServiceDuration.push([]);
                    this.rows.forEach((obj, j) => {
                        const apptTime = this.workersWithServiceDuration[0][j];
                        this.workersWithServiceDuration[i].push(this.setApptDates(apptTime, this.bookEvery1, i));
                    });
                }
            }
            const ids = this.rows.map((obj) => obj.worker);
            this.workersWithServiceDuration.forEach((obj) => {
                obj.map((obj1, i) => Object.assign(obj1, { workerId: ids[i] }));
            });
            const startdate = this.workersWithServiceDuration[0][0].startTime;
            // const workerIds: any = this.rows.map((obj) => obj.workerName.split('$')[0]);
            const endDate = this.workersWithServiceDuration[+this.numberOfBookStandings - 1][this.rows.length - 1].endTime;
            // const servicesData = this.rows;
            // for (let i = 0; i < servicesData.length; i++) {
            //     for (let j = 0; j < this.serviceDetailsList[i].length; j++) {
            //         if (this.serviceDetailsList[i][j]['Id'] === servicesData[i]['Id']) {
            //             servicesData[i]['Resources__c'] = this.serviceDetailsList[i][j]['Resources__c'];
            //         }
            //     }
            // }
            const apptDatesssss = [];
            for (let k = 0; k < this.workersWithServiceDuration.length; k++) {
                apptDatesssss.push(moment(this.workersWithServiceDuration[k][0].startTime).format('YYYY-MM-DD'));
            }

            const appointmentBookingData = {
                'clientId': this.clientId,
                'Worker__c': workerIds,
                'Appt_Date_Time__c': this.bsValue,
                'Appt_Start': this.commonService.getDBDatTmStr(startdate).split(' ')[0],
                'Appt_End': this.commonService.getDBDatTmStr(endDate).split(' ')[0],
                'ApptDates': apptDatesssss,
                // 'serviceId': this.serviceId,
                'starttime': this.bookStandingTime,
                'numberOfBooks': this.numberOfBookStandings,
                'bookEvery': this.bookEvery,
                'bookEvery1': this.bookEvery1,
                'services': this.rows,
                'page': 'bookStanding'

            };
            this.bookStandingApptService.searchForAppointmentAction(appointmentBookingData).subscribe(data => {
                let length = 0;
                this.apptBookoutData = [];
                this.appointmentData = [];
                const serviceResourceMap = new Map();
                if (data['result']) {
                    this.resourceSlot = data['result']['resourceSlot'];
                    this.workerHours = data['result']['companyhours'];
                    const datesArray = [];
                    const apptData = [];
                    if (data['result']['result'].length > 0) {
                        for (let i = 0; i < data['result']['result'].length; i++) {
                            const modifiedDate: any = {};
                            const apptDuration = data['result']['result'][i]['Service_Duration'];
                            modifiedDate.startDate = this.commonService.getDateTmFrmDBDateStr(data['result']['result'][i]['Booking_Date_Time']);
                            modifiedDate.endDate = new Date(modifiedDate.startDate.getTime() + parseInt(apptDuration, 10) * 60000);
                            modifiedDate.apptDuration1 = data['result']['result'][i]['Duration_1__c'];
                            modifiedDate.apptDuration2 = data['result']['result'][i]['Duration_2__c'];
                            modifiedDate.apptDuration3 = data['result']['result'][i]['Duration_3__c'];
                            modifiedDate.Duration_1_Available_for_Other_Work__c = data['result']['result'][i]['Duration_1_Available_for_Other_Work__c'];
                            modifiedDate.Duration_2_Available_for_Other_Work__c = data['result']['result'][i]['Duration_2_Available_for_Other_Work__c'];
                            modifiedDate.Duration_3_Available_for_Other_Work__c = data['result']['result'][i]['Duration_3_Available_for_Other_Work__c'];
                            datesArray.push(modifiedDate);
                        }
                        length = data['result']['result'].length;
                    } else {
                        length = 1;
                    }
                    for (let j = 0; j < length; j++) {
                        for (let i = 0; i < parseInt(this.numberOfBookStandings, 10); i++) {
                            if (j === 0) {
                                apptData.push([]);
                            }
                            for (let k = 0; k < this.workersWithServiceDuration[i].length; k++) {
                                let status = 'Booked';
                                let FullName = this.setName(this.rows[k].workerName, k, this.getWorker);
                                const workerTime: any = this.workersWithServiceDuration[i][k];
                                const workerId = this.workersWithServiceDuration[i][k].workerId;

                                if (j === 0) {
                                    if (this.commonService.checkWorkerServiceStatus(workerTime.startTime, workerTime.endTime, workerId, this.workerHours)) {
                                        status = 'Closed';
                                    }
                                }
                                if (data['result']['result'].length > 0) {
                                    if (this.commonService.compareDatesForAppointment(datesArray[j].startDate, datesArray[j].endDate, workerTime.startTime, workerTime.endTime)) {
                                        if (data['result']['result'][j]['Resources__c']) {
                                            const resources = data['result']['result'][j]['Resources__c'].split(',');
                                            resources.forEach(element => {
                                                const namess = this.commonService.getDBDatTmStr(workerTime.startTime) + element;

                                                if (serviceResourceMap.has(namess)) {
                                                    const resourceCount = serviceResourceMap.get(namess);
                                                    serviceResourceMap.set(namess, resourceCount + 1);
                                                } else {
                                                    serviceResourceMap.set(namess, 1);
                                                }
                                            });
                                        }
                                    }
                                    if (j === 0) {
                                        if (status !== 'Closed') {
                                            if (data['result']['result'][j]['workerId'] === this.workersWithServiceDuration[i][k].workerId) {
                                                if (this.commonService.compareDatesForAppointment(datesArray[j].startDate, datesArray[j].endDate, workerTime.startTime, workerTime.endTime)) {
                                                    const diffMs = (workerTime.endTime - workerTime.startTime);
                                                    const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                                                    if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 0 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 0 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 0) {
                                                        status = 'Conflicting';
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() + parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                        const apptStart1 = workerTime.startTime;
                                                        const apptEnd1 = workerTime.endTime;
                                                        const apptRedStart1 = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        const apptRedEnd1 = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (status === 'Conflicting') {
                                                            if (apptRedStart1.getTime() <= apptStart1.getTime() && apptEnd1.getTime() <= apptRedEnd1.getTime()) {
                                                                status = 'Booked';
                                                            } else {
                                                                status = 'Conflicting';
                                                            }
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() + parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            status = 'Booked';
                                                        } else {
                                                            status = 'Conflicting';
                                                        }
                                                    }
                                                    // status = 'Conflicting';
                                                    FullName = data['result']['result'][j].FullName;
                                                }
                                            }
                                        }
                                    } else {
                                        if (apptData[i][k].Status__c === 'Booked') {
                                            if (data['result']['result'][j]['workerId'] === this.workersWithServiceDuration[i][k].workerId) {
                                                if (this.commonService.compareDatesForAppointment(datesArray[j].startDate, datesArray[j].endDate, workerTime.startTime, workerTime.endTime)) {
                                                    const diffMs = (workerTime.endTime - workerTime.startTime);
                                                    const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                                                    if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 0 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 0 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 0) {
                                                        apptData[i][k].Status__c = 'Conflicting';
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() + parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                        const apptStart1 = workerTime.startTime;
                                                        const apptEnd1 = workerTime.endTime;
                                                        const apptRedStart1 = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        const apptRedEnd1 = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (status === 'Conflicting') {
                                                            if (apptRedStart1.getTime() <= apptStart1.getTime() && apptEnd1.getTime() <= apptRedEnd1.getTime()) {
                                                                apptData[i][k].Status__c = 'Booked';
                                                            } else {
                                                                apptData[i][k].Status__c = 'Conflicting';
                                                            }
                                                        }
                                                    } else if (datesArray[j]['Duration_1_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = datesArray[j].startDate;
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() + parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1 &&
                                                        datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_2_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt(datesArray[j]['apptDuration1'], 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    } else if (datesArray[j]['Duration_3_Available_for_Other_Work__c'] === 1) {
                                                        const apptStart = workerTime.startTime;
                                                        const apptEnd = workerTime.endTime;
                                                        const apptRedStart = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2']), 10) * 60000);
                                                        const apptRedEnd = new Date(datesArray[j].startDate.getTime() +
                                                            parseInt((datesArray[j]['apptDuration1'] + datesArray[j]['apptDuration2'] + datesArray[j]['apptDuration3']), 10) * 60000);
                                                        if (apptRedStart.getTime() <= apptStart.getTime() && apptEnd.getTime() <= apptRedEnd.getTime()) {
                                                            apptData[i][k].Status__c = 'Booked';
                                                        } else {
                                                            apptData[i][k].Status__c = 'Conflicting';
                                                        }
                                                    }
                                                    // status = 'Conflicting';
                                                    apptData[i][k].FullName = data['result']['result'][j].FullName;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (j === 0) {
                                    // const workerObj = this.modifyObject(this.rows, k);
                                    // const workerObj = this.rows[k];
                                    apptData[i].push(Object.assign({}, this.rows[k], {
                                        'bsValue': this.commonService.getDBDatTmStr(this.workersWithServiceDuration[i][k].startTime),
                                        'Status__c': status,
                                        'bookOutStartTime': this.bookStandingTime,
                                        'FullName': FullName,
                                        'workerId': workerId
                                    }));
                                } else {
                                    // apptData[i][k].bsValue = this.commonService.getDBDatTmStr(workerTime.startTime);
                                }
                            }
                        }
                    }
                    // Loop1:
                    for (let n = 0; n < this.rows.length; n++) {
                        this.rows[n]['Resources__c'] = '';
                        let filterdServiceForRes = this.resourceSlot.filter((resource) => resource.serviceId === this.rows[n].Id);
                        if (filterdServiceForRes.length > 0) {
                            const filterType = filterdServiceForRes[0]['filters'];
                            if (filterType === 'Any') {
                                filterdServiceForRes = filterdServiceForRes.sort((a, b) => {
                                    return a.priority - b.priority;
                                });
                                for (let l = 0; l < this.workersWithServiceDuration.length; l++) {
                                    const matchDate: any = this.workersWithServiceDuration[l];
                                    for (let p = 0; p < matchDate.length; p++) {
                                        const dateTime = matchDate[p];
                                        const Date = this.commonService.getDBDatTmStr(dateTime.startTime);
                                        this.resStatus[Date.split(' ')[0]] = '';
                                        for (let m = 0; m < filterdServiceForRes.length; m++) {
                                            const element = filterdServiceForRes[m];
                                            //  if (element.priority === 1) {
                                            const resourceName = Date + element.resName + ' #1';
                                            if (serviceResourceMap.has(resourceName)) {
                                                if (serviceResourceMap.get(resourceName) >= element.slots) {
                                                    if (filterdServiceForRes.length - 1 === m) {
                                                        this.resStatus[Date.split(' ')[0]] = ' Required Resource Unavailable.';
                                                    }
                                                } else {
                                                    this.rows[n]['Resources__c'] = element.resName;
                                                    break;
                                                }
                                            } else {
                                                this.rows[n]['Resources__c'] = element.resName;
                                                break;
                                            }
                                            //     }
                                        }
                                        if (this.resStatus[Date.split(' ')[0]] !== '') {
                                            this.rows[n]['Resources__c'] = filterdServiceForRes[0].resName;
                                        }
                                    }
                                }
                            } else if (filterType === 'All') {
                                for (let l = 0; l < this.workersWithServiceDuration.length; l++) {
                                    const matchDate: any = this.workersWithServiceDuration[l];
                                    for (let p = 0; p < matchDate.length; p++) {
                                        const dateTime = matchDate[p];
                                        const Date = this.commonService.getDBDatTmStr(dateTime.startTime);
                                        this.resStatus[Date.split(' ')[0]] = '';
                                        filterdServiceForRes.forEach((element, index) => {
                                            if (filterdServiceForRes.length - 1 !== index) {
                                                this.rows[n]['Resources__c'] += element.resName + ',';
                                            } else {
                                                this.rows[n]['Resources__c'] += element.resName;
                                            }
                                            const resourceName = Date + element.resName + ' #1';
                                            if (serviceResourceMap.has(resourceName)) {
                                                if (serviceResourceMap.get(resourceName) >= element.slots) {
                                                    this.resStatus[Date.split(' ')[0]] = ' Required Resource Unavailable.';
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                    this.appointmentData = apptData;
                    // let loopLength: any = 0;
                    // if (this.bookEvery1 === 'Weeks') {
                    //     loopLength = (this.appointmentData.length) / (this.bookEvery * 7);
                    // } else if (this.bookEvery1 === 'Months') {
                    //     loopLength = (this.appointmentData.length) / (this.bookEvery * 30);
                    // } else {
                    //     loopLength = (this.appointmentData.length) / (this.bookEvery * 1);
                    // }
                    // this.appointmentData = this.appointmentData.slice(0, parseInt(loopLength, 10));
                    for (let i = 0; i < this.appointmentData.length; i++) {
                        const compdate = this.commonService.getDateTmFrmDBDateStr(this.appointmentData[i][0]['bsValue']);
                        if (compdate < this.bsValue1) {
                            this.appointmentData[i][0]['displayDate'] = this.commonService.getUsrDtStrFrmDBStr(this.appointmentData[i][0]['bsValue']);
                        } else {
                            this.appointmentData = this.appointmentData.slice(0, i);
                            break;
                        }
                    }
                    apptData.forEach((obj, i) => {
                        obj.forEach((obj1) => {
                            this.apptBookoutData.push(obj1);
                        });
                    });
                    // const availableLength = this.apptBookoutData.filter((obj) => obj['Status__c'].toLowerCase() === 'booked').length;
                    // if (availableLength === this.apptBookoutData.length) {
                    //     this.scheduleAvailableButton = true;
                    // } else {
                    //     this.scheduleAvailableButton = false;
                    // }
                }
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                }
            );
        } // else end
    }// validations end

    // Method to schedule appoitnment
    scheduleAvailable(apptType) {
        // if (this.bookStandingVisitType === '' || this.bookStandingVisitType === 'undefined' || this.bookStandingVisitType === undefined) {
        //     this.bookStandErr = 'Select a Visit Type';
        //     window.scrollTo(0, 0);
        // } else {scheduleAvailableButton
        const year = this.bsValue.getFullYear();
        const month = this.bsValue.getMonth();
        const day = this.bsValue.getDate();
        //  const workerIds = this.rows.map((obj) => obj.workerName.split('$')[0]);
        this.startDateWithTime = new Date(year, month, day,
            this.timeConversion(this.bookStandingTime),
            parseInt(this.bookStandingTime.split(' ')[0].split(':')[1], 10), 0, 0);
        const apptDates: any = [];
        let isConflicting: any = false;
        const ApptServiceTax = [];
        const ApptNetPrice = [];
        if (apptType === 'scheduleAvailable') {
            let i = 0;
            this.appointmentData.filter((obj1: Array<any>) => {
                let totalServiceTax = 0;
                let netPrice = 0;
                if (obj1.length > 0) {

                    const filterStatusList = obj1.filter((obj) => obj['Status__c'].toLowerCase() === 'booked');
                    const IsPackageLength = filterStatusList.filter((obj) => obj['IsPackage'] === 1).length;
                    const length = filterStatusList.length;
                    if (length > 0) {
                        apptDates.push([]);
                        const filterOBj = filterStatusList.filter((obj) => {
                            if (IsPackageLength > 0) {
                                obj['IsPackage'] = 1;
                            }

                            const changedObj = Object.assign({ 'Appt_Status__c': 'Booked' }, obj);
                            changedObj['Service_Tax__c'] = this.calculateServiceTax(changedObj);
                            totalServiceTax += changedObj['Service_Tax__c'];
                            netPrice += changedObj.service['Price__c'];
                            apptDates[i].push(changedObj);
                        });
                        i++;
                    }
                }
                ApptServiceTax.push(totalServiceTax);
                ApptNetPrice.push(netPrice);
            });
        } else {
            let i = 0;
            this.appointmentData.filter((obj1: Array<any>) => {
                let totalServiceTax = 0;
                let netPrice = 0;
                if (obj1.length > 0) {
                    const IsPackageLength = obj1.filter((obj) => obj['IsPackage'] === 1).length;
                    const length = obj1.filter((obj) => obj['Status__c'].toLowerCase() !== 'booked').length;
                    apptDates.push([]);
                    const filterOBj = obj1.filter((obj) => {
                        if (IsPackageLength > 0) {
                            obj['IsPackage'] = 1;
                        }
                        const changedObj = Object.assign({ 'Appt_Status__c': 'Booked' }, obj);
                        if (obj['Status__c'].toLowerCase() === 'closed') {
                            changedObj['Status__c'] = 'Booked';
                        }
                        if (length > 0) {
                            changedObj['Appt_Status__c'] = 'Conflicting';
                            isConflicting = true;
                        }
                        changedObj['Service_Tax__c'] = this.calculateServiceTax(changedObj);
                        totalServiceTax += changedObj['Service_Tax__c'];
                        netPrice += +changedObj.service['Price__c'];
                        apptDates[i].push(changedObj);
                    });
                    i++;
                }
                ApptServiceTax.push(totalServiceTax);

                ApptNetPrice.push(netPrice);
            });
        }
        // let modifyData: any;
        // modifyData = this.apptCalculateServiceTax(this.rows);
        const dates = {
            'Client__c': this.clientId,
            'serviceId': this.serviceId,
            'AppDates': apptDates,
            'apptime': this.bookStandingTime,
            'Client_Type__c': this.bookStandingVisitType,
            'apptBookDate': this.startDateWithTime,
            'apptNote': this.bookStandingText,
            'serviceGroupColour': this.serviceGroupColour,
            // 'serviceData': modifyData,
            'Duration__c': this.serviceDurations,
            'apptId': '',
            'ApptTax': ApptServiceTax,
            'NetPrice': ApptNetPrice,
            'apptCreatedDate': this.commonService.getDBDatTmStr(new Date())
        };
        if (this.bookAllType === 'true') {
            dates['AppDates'] = apptDates.filter((obj) => obj[0]['Appt_Status__c'] === 'Booked');
        }
        // if (isConflicting) {
        //     // if (confirm('There are conflicts with booking the standing appointment, which will directly affect the booking calendar. Schedule anyway?')) {
        //     // }
        //     this.scheduleAppointments(dates);
        //     // else {
        //         // this.bookStandErr = 'New appointment conflicts were found';
        //         // this.checkConflictError = 'New appointment conflicts were found';
        //         // window.scrollTo(0, 0);
        //         // this.appointmentData = [];
        //         // return;
        //     // }
        // } else {
        //     this.scheduleAppointments(dates);
        // }
        // }
        this.scheduleAppointments(dates);
    }
    calculateServiceTax(taxableObj): number {
        return this.commonService.calculatePercentage(this.serviceTax, taxableObj.service['Price__c'], taxableObj.service['Taxable__c']);
    }
    scheduleAppointments(apptData) {
        this.passdate = apptData.apptBookDate.getFullYear() + '-' + (apptData.apptBookDate.getMonth() + 1) + '-' + apptData.apptBookDate.getDate();
        this.bookStandingApptService.scheduleAvailable(apptData).subscribe(data => {
            if (this.sentNotify === true && apptData['AppDates'].length > 0) {  // if sentNotify  true send button functionality
                this.bookStandingApptService.sendApptNotifs(data['result']['apptIds'], this.sentNotify, this.cliEmail).subscribe(data2 => { }, error => { });
                this.workerList = [];
                // this.workerList = data['result'];
                this.router.navigate(['/appointments'], { queryParams: { date: this.passdate } }).then(() => {
                    const toastermessage: any = this.translateService.get('LOGIN.APPT_SUCESSFULLY_CREATED');
                    this.toastr.success(toastermessage.value, null, { timeOut: 2000 });
                });
            } else if (apptData['AppDates'].length > 0) {  // if sentNotify  false go to appt book  button functionality
                this.bookStandingApptService.sendApptNotifs(data['result']['apptIds'], this.sentNotify, this.cliEmail).subscribe(data2 => { }, error => { });
                this.router.navigate(['/appointments'], { queryParams: { date: this.passdate } }).then(() => {
                    const toastermessage: any = this.translateService.get('LOGIN.APPT_SUCESSFULLY_CREATED');
                    this.toastr.success(toastermessage.value, null, { timeOut: 2000 });
                });
            } else {
                this.router.navigate(['/appointments'], { queryParams: { date: this.passdate } }).then(() => {
                    const toastermessage: any = this.translateService.get('LOGIN.APPT_SUCESSFULLY_CREATED');
                    this.toastr.success(toastermessage.value, null, { timeOut: 2000 });
                });
            }
        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                } else if (statuscode === '2091') {
                    const bookingError = JSON.parse(error['_body']).message;
                    // Warning Don't Delete This alert Code//
                    alert(bookingError);
                }
            });
    }
    //   checkForServices() and checkForServiceObject() are written to check whether the services added contains
    //   service name ,service worker and (package  or service) if not it returns true to show validation
    //   written by Ravi Kanth
    checkForServices(services: Array<any>, property1, property2, property3): boolean {
        const properties = [property1, property2, property3];
        if (properties.map((property) => this.checkForServiceObject(services, property)).indexOf(false) !== -1) {
            return true;
        }
        return false;
    }

    checkForServiceObject(services: Array<any>, propertyName: string): boolean {
        const isProperty = services.map((obj) => obj.hasOwnProperty(propertyName)).indexOf(false) !== -1 ? false : true;
        if (isProperty) {
            if (services.filter((obj) => obj[propertyName] === '').length !== 0) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }

    }

    timeConversion(time: string): number {
        let hours: any;
        if (time.split(' ')[1] === 'AM') {
            hours = time.split(' ')[0].split(':')[0];
            if (+hours === 12) {
                hours = 0;
            }
        } else if (time.split(' ')[1] === 'PM') {
            hours = time.split(' ')[0].split(':')[0];
            if (parseInt(hours, 10) !== 12) {
                hours = parseInt(hours, 10) + 12;
            }
        }
        return hours;
    }


    // UTCstringToUserDate(utcstring: string): Date {
    //     return new Date(this.commonService.UTCStrToUsrTmzStr(utcstring));
    // }

    // comparing the dates whether they are in booked appointment list.written  by ravi

    setApptDates(apptDate: { 'startTime': Date, 'endTime': Date }, dateType: string, setDatebyNumber: number): { 'startTime': Date, 'endTime': Date } {
        const apptTimings: any = {};
        for (const key in apptDate) {
            if (apptDate.hasOwnProperty(key)) {
                let appDate = new Date(apptDate[key].getTime());
                if (dateType.toLocaleLowerCase() === 'days') {
                    appDate.setDate(apptDate[key].getDate() + setDatebyNumber * this.bookEvery);
                } else if (dateType.toLocaleLowerCase() === 'weeks') {
                    appDate.setDate(apptDate[key].getDate() + setDatebyNumber * 7 * this.bookEvery);
                } else {
                    appDate = this.addMonths(appDate, setDatebyNumber * this.bookEvery);
                }
                apptTimings[key] = appDate;
            }
        }

        return apptTimings;
    }



    modifyObject(listToModify: Array<any>, index) {
        const workersInfo = listToModify.map((obj) => {
            return {
                Duration_1__c: obj.Duration_1__c,
                Duration_2__c: obj.Duration_2__c,
                Duration_3__c: obj.Duration_3__c,
                Buffer_After__c: obj.Buffer_After__c,
                Duration__c: obj.Duration__c,
                Guest_Charge__c: obj.Guest_Charge__c,
                Id: obj.Id,
                // Name: obj.Name,   serviceName
                Net_Price__c: obj.Net_Price__c,
                //  name: obj.name,   workerName
                serviceGroupColour: obj.serviceGroupColour,
                //  serviceGroupName: obj.serviceGroupName.split('$')[0],
                serviceName: obj.Id,
                workerName: obj.workerName.split('$')[0],
                IsPackage: obj.IsPackage,
                Booked_Package__c: obj.Booked_Package__c
            };
        });
        return workersInfo[index];
    }

    setName(id, i, getWorker): any {
        let name: any;
        for (i = 0; i < getWorker.length; i++) {
            if (getWorker[i].workerName === id) {
                name = getWorker[i].names;
            }
        }
        // const name = getWorker[index].filter((obj) => obj.workerName === id);
        return name;
    }

    addMonths(startDate: Date, addNoOfMonths): Date {
        const monthcal = (startDate.getMonth() + addNoOfMonths) % 12;
        const modifiedDate = new Date(startDate.getFullYear(), startDate.getMonth() + addNoOfMonths, startDate.getDate(), startDate.getHours(),
            startDate.getMinutes(), startDate.getSeconds());
        if (monthcal !== modifiedDate.getMonth()) {
            modifiedDate.setDate(0);
        }
        return modifiedDate;
    }
    clearAppts() {
        this.appointmentData = [];
        this.clearErrorMsg();
    }
    getWorkersFromDate() {
        const serviceIds = [];
        const selectedIds = [];
        this.getNumberofBookOuts();
        this.calculateEndDate();
        this.rows.filter((data) => {
            if (data['Id'] !== '' || !isNullOrUndefined(data['Id'])) {
                serviceIds.push(data['Id']);
                selectedIds.push(data['Id']);
            } else {
                serviceIds.push(data['']);
            }
        });
        if (selectedIds.length > 0) {
            const bookingdata = {
                bookingdate: this.commonService.getDBDatStr(this.bsValue),
                serviceIds: selectedIds
            };
            this.bookStandingApptService.getUsers(bookingdata).subscribe(data => {
                const workerservices = data['result'];
                serviceIds.forEach((id, i) => {
                    if (id !== '' && !isNullOrUndefined(id)) {
                        this.workerList[i] = workerservices.filter((worker) => worker.sId === id);
                        const isExsists = this.workerList[i].findIndex((worker) => worker.workername === this.rows[i]['workerName']) !== -1 ? true : false;
                        if (!isExsists) {
                            this.rows[i]['workerName'] = this.workerList[i].length > 0 ? this.workerList[i][0]['workername'] : '';
                            this.workerList[i].filter((worker) => worker.workername === this.workerName).map((worker) => {
                                Object.assign(this.rows[i], this.commonService.getServiceDurations(worker, this.clientId, this.cliData, this.rows[i]));
                            });
                            this.calculateServiceDurations1();
                        }
                    }
                });
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    getClientAppointments(id) {
        const client = {
            'clientId': id,
            'apptViewValue': 'All'
        };
        this.newClientService.getClientAppointmentsData(client).subscribe(
            data => {
                this.cliData = data['result'].AppointmenServices.filter(filterList => filterList.PrefDur === 1);
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    showpopUp(type) {
        this.bookAllType = type;
        this.notificationModal.show();
    }
    closeNotifyModal() {
        this.notificationModal.hide();
    }
    sendNotification(type) {
        if (type === true) {
            const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (this.cliEmail === '') {
                this.cliEmailErr = 'Email should not be blank';
            } else if (this.cliEmail !== '' && !EMAIL_REGEXP.test(this.cliEmail)) {
                this.cliEmailErr = 'Email id is invalid';
            } else {
                this.cliEmailErr = '';
                this.sentNotify = true;
                if (this.bookAllType === 'false') {
                    this.scheduleAvailable('scheduleAllDates');
                } else {
                    this.scheduleAvailable('scheduleAvailable');
                }
                this.notificationModal.hide();
            }
        } else {
            this.sentNotify = false;
            if (this.bookAllType === 'false') {
                this.scheduleAvailable('scheduleAllDates');
            } else {
                this.scheduleAvailable('scheduleAvailable');
            }
            this.notificationModal.hide();
        }
    }
    getWorkerList() {
        // this.apptSearchData = [];
        const CalendatDate = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
        this.bookStandingApptService.getWorkerLists(CalendatDate).subscribe(
            data => {
                this.getWorker = data['result'];
                this.addServices();
                // if (this.isRebooking !== 1 && !this.bookingType) {
                //     this.addInput();
                // }
                const dat1 = this.getWorker[0];
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    expressService(value, i, param) {
        this.searchBtn = false;
        //  this.apptSearchData = [];
        if (value) {
            let workerId;
            let workerName;
            workerId = value.split('$')[0];
            workerName = value.split('$')[1];
            const CalendatDate = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
            if (!this.rows[i]) {
                this.rows[i] = {};
            }
            this.bookStandingApptService.expressBookingServices(workerId, CalendatDate).subscribe(
                data => {
                    this.bookingExpress[i] = data['result']['serviceList'];

                    if (this.bookingExpress[i][0] && param) {
                        for (let k = 0; k < this.getWorker.length; k++) {
                            if (this.getWorker[k]['workerId'] === workerId) {
                                this.rows[i]['StartDay'] = this.getWorker[k]['StartDay'];
                                this.rows[i]['wrkrName'] = this.getWorker[k]['wrkrName'];
                            }
                        }
                        this.rows[i]['service'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0] : {};
                        // if (this.isRebooking !== 1) {
                        //     this.rows[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
                        // }
                        this.rows[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
                        this.rows[i]['worker'] = workerId;
                        this.rows[i]['workerName'] = workerName;
                        this.rows[i]['fullWorker'] = workerId + '$' + workerName;
                    }
                    if (this.bookingExpress[i]) {
                        this.listServices(this.rows[i]['serviceId'], i);
                    }
                    if (!this.clientId) {
                        this.isPreferredDuration = 0;
                        this.preferredDur = 0;
                    } else {
                        if (this.cliData) {
                            this.isPreferredDuration = 0;
                            this.preferredDur = 0;
                            this.isPreferredDuration = this.getPreferredDuration(this.cliData, this.bookingExpress[i], this.rows[i]);
                            if (this.isPreferredDuration.length > 0) {
                                this.preferredDur = 0;
                                for (let j = 0; j < this.bookingExpress[i].length; j++) {
                                    if (this.isPreferredDuration[0].Service__c === this.bookingExpress[i][j].serviceId) {
                                        this.bookingExpress[i][j]['Duration_1__c'] = this.isPreferredDuration[0].Duration_1__c;
                                        this.bookingExpress[i][j]['Duration_2__c'] = this.isPreferredDuration[0].Duration_2__c;
                                        this.bookingExpress[i][j]['Duration_3__c'] = this.isPreferredDuration[0].Duration_3__c;
                                        this.bookingExpress[i][j]['Buffer_after__c'] = this.isPreferredDuration[0].Buffer_after__c;
                                        this.bookingExpress[i][j]['sumDurationBuffer'] = this.isPreferredDuration[0].Duration__c;
                                        this.bookingExpress[i][j]['PrefDur'] = this.preferredDur;
                                        // this.bookingExpress[i][j]['PrefDur'] = this.preferredDur;
                                        this.prefService = this.isPreferredDuration[0].Duration__c;
                                    }
                                }
                            }
                        }
                        this.calculateServiceDurations1();
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    calculateServiceDurations1() {
        this.servicePrice = 0;
        this.serviceDurations = 0;
        if (this.rows && this.rows.length > 0) {
            for (let j = 0; j < this.rows.length; j++) {
                const serviceVal = this.rows[j].service;
                if (serviceVal.sumDurationBuffer !== null || serviceVal.sumDurationBuffer !== '') {
                    // this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer) ? parseFloat(serviceVal.sumDurationBuffer) : parseFloat(serviceVal.dursergrp);
                    this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer);
                } else {
                    this.serviceDurations += parseFloat(serviceVal.dursergrp) ? parseFloat(serviceVal.dursergrp) : 0;
                }
            }
        }
    }
    listServices(value, i) {
        // this.apptSearchData = [];
        const obj = this.bookingExpress[i].filter((data) => data.serviceId === value);
        if (obj.length > 0) {
            this.rows[i]['service'] = obj[0];
            this.rows[i]['serviceId'] = obj[0]['serviceId'];
            this.rows[i]['skipRecalculate'] = 'false';
            this.searchBtn = true;
            this.calculateServiceDurations1();
        } else {
            this.bookingExpress[i].push({ Name: this.rows[i]['service']['Name'], serviceId: this.rows[i]['serviceId'] });
            this.calculateServiceDurations1();
        }
    }
    getPreferredDuration(cliData, servicelist, inputs) {
        for (let i = 0; i < this.cliData.length; i++) {
            for (let t = 0; t < servicelist.length; t++) {
                if ((servicelist[t].serviceId === this.cliData[i].Service__c) && (inputs.worker === this.cliData[i].Worker__c)) {
                    if (this.cliData[i].PrefDur === 1) {
                        const obj = [];
                        obj.push({
                            'Service__c': cliData[i].Service__c,
                            'Duration__c': cliData[i].Duration__c,
                            'Duration_1__c': cliData[i].Duration_1__c,
                            'Duration_2__c': cliData[i].Duration_2__c,
                            'Duration_3__c': cliData[i].Duration_3__c,
                            'Buffer_after__c': cliData[i].Buffer_After__c
                        });
                        return obj;
                    }
                }
            }
        }
        return 0;
    }
    moreAppointments() {
        this.showMore = true;
    }
    // apptCalculateServiceTax(apptBookingData: Array<any>) {
    //     const serviceTaxs = [];
    //     const servicePrice = [];
    //     // let totalTax = 0;
    //     // let totalPrice = 0;
    //     apptBookingData.forEach(element => {
    //         if (element.service.Taxable__c === 0) {
    //             serviceTaxs.push(0);
    //         } else if (element.service.Taxable__c === 1) {
    //             // let lknlk: any;
    //             const lknlk = JSON.parse(element.service.serviceTax)['serviceTax'];
    //             serviceTaxs.push(lknlk);
    //         }
    //         servicePrice.push(element.service.Price__c);
    //     });
    // return {
    //     serviceTaxs;
    //     servicePrice
    // }
    // const data = apptBookingData.map((bookedData) => {
    //   bookedData.service['Service_Tax__c'] = 0;
    //   if (!isNullOrUndefined(bookedData.service['serviceTax'])) {
    //     const serviceTax = +JSON.parse(bookedData.service['serviceTax']).serviceTax;
    //     bookedData.service['Service_Tax__c'] = this.commonService.calculatePercentage(serviceTax, bookedData.service['Price__c'], 1);
    //   }
    //   totalPrice += +bookedData.service['Price__c'];
    //   totalTax += +bookedData.service['Service_Tax__c'];
    //   return bookedData;
    // });
    // return {
    //   bookingData: data,
    //   serviceTax: totalTax,
    //   sales: totalPrice
    // };
    //  }
} // main Method end here
