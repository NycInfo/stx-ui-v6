/*
ngOnInit() : Method to loading athe page..
searchClients() : Method for searching clients
showData() : Method for loading All clients Data.
clearmessage() : Method for Clearing  error messages.
*/
import {
    Component, Renderer2, ViewContainerRef, ViewEncapsulation, OnInit, ViewChild, OnDestroy,
    AfterViewInit, Inject, Output, EventEmitter, Directive, HostListener, ElementRef, NgZone
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { validateConfig } from '@angular/router/src/config';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { WaitingListService } from './waitinglist.service';
import { AppointmentsService } from '../../../app/appointments/main/appointments.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { JwtHelper } from 'angular2-jwt';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { parse } from 'url';
import { DecimalPipe } from '@angular/common';
import * as config from '../../app.config';
import { CommonService } from '../../common/common.service';
import { worker } from 'cluster';
declare let $: any;
declare var swal: any;



@Component({
    selector: 'app-appointments-popup',
    templateUrl: './waitinglist.html',
    styleUrls: ['./waitinglist.css'],
    providers: [WaitingListService, CommonService, AppointmentsService]
})
export class WaitingListComponent implements OnInit {
    searchKey: any;
    DataObj: any;
    error: any;
    checkOutList: any;
    rowsPerPage: any;
    autoList = [];
    cSearch: any;
    apiEndPoints = config.S3_URL;
    apptId: any;
    decodeUserToken: any;
    hideClientInfo: any;
    decodedToken: any;
    noResult: any;
    toDate: any;
    date = new Date();
    startDate: any = new Date();
    endDate: any;
    datePickerConfig: any;
    minDate = new Date();
    sendDate = new Date();
    public searchField = new FormControl();
    paramsId: any;
    actionmethod: any;
    dropdownList = [];
    selectedWorkers = [];
    dropdownSettings = {};
    workerList = [];
    inputs = [];
    firstName = '';
    lastName = '';
    mobileNumber = '';
    countrycode = '01';
    primaryEmail = '';
    textArea = '';
    sms_checkbox = 0;
    expressClientIds: any;
    allowQuickAddAccess: any;
    clientfieldMobilePhone: any;
    clientfieldPrimaryEmail: any;
    public getWorker: any = [];
    chooseDate = new Date();
    bookingExpress: any = [];
    servicePrice: any;
    serviceDurations: any = [];
    sumDuration: any;
    isNewClient: any = false;
    clientId: any;
    waitingList: any = [];
    FilterData: any = [];
    filterBy: any;
    disField: Boolean = false;
    waitingListId = '';
    actionPopup = 'add';
    fullName: any;
    noEmail: any;
    btnBook = 0;
    acctCharge: any = '';
    depReq: any = '';
    clientShow: any = '';
    other: any = '';
    note: any = '';
    Booking_Restriction_Type__c: any = '';
    paramDt: any;
    workrArr: any = [];
    cliData = [];
    toastermessage: any;
    expiry: any = false;
    isPreferredDuration: any;
    preferredDur: any = 0;
    prefService: 0;
    @ViewChild('clientModal') public clientModal: ModalDirective;
    @ViewChild('waitingListModal') public waitingListModal: ModalDirective;
    @ViewChild('serviceNotesModal') public serviceNotesModal: ModalDirective;
    constructor(

        @Inject(DOCUMENT) document, r: Renderer2,
        private appointmentsServices: AppointmentsService,
        private activatedRoute: ActivatedRoute,
        private waitingListService: WaitingListService,
        private toastr: ToastrService,
        private commonService: CommonService,
        private translateService: TranslateService,
        private router: Router) {
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
        this.activatedRoute.queryParams.subscribe(params => {
            this.paramsId = activatedRoute.snapshot.params['Id'];
            this.actionmethod = params.actionMethod;
        });
        this.activatedRoute.queryParamMap.subscribe((params) => {
            if (params.has('date')) {
                this.toDate = this.commonService.getDateFrmDBDateStr(params.get('date'));
            } else {
                const currentDate = new Date();
                const dtStr = currentDate.getFullYear()
                    + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2)
                    + '-' + ('0' + currentDate.getDate()).slice(-2)
                    + ' ' + ('0' + currentDate.getHours()).slice(-2)
                    + ':' + ('0' + currentDate.getMinutes()).slice(-2)
                    + ':' + ('0' + currentDate.getSeconds()).slice(-2);
                this.toDate = this.commonService.getDateFrmDBDateStr(dtStr.split(' ')[0]);
            }
            this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
        });
    }
    /*Method for page Load */
    ngOnInit() {
        this.selectedWorkers.forEach(element => {
            this.workrArr.push(element['id']);
        });
        this.getList(false, 'Created Date', this.paramDt, this.workrArr);
        this.getWorkerList();
        this.getFilters();
        this.boGetWorkerList();
        this.endDate = new Date(this.date.setMonth(this.date.getMonth() + 1));
        // ---Start of code for Permissions Implementation--- //
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedToken = {};
            this.decodeUserToken = {};
        }
        if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
        } else {
            this.decodedToken = {};
        }

        this.getHideClientContactInfo();
        this.listClientFields();
        this.addInput();
        // ---End of code for permissions Implementation--- //
        this.searchField.valueChanges
            .debounceTime(400)
            .switchMap(term => this.waitingListService.getData(term)
            ).subscribe(
                data => {
                    this.autoList = [];
                    this.autoList = data['result'];
                    if (this.cSearch && this.cSearch.trim()) {
                        if (this.autoList.length === 0) {
                            this.toastr.warning('No record found. ', null, { timeOut: 3000 });
                        }
                    } else {
                        this.autoList = [];
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };

    }
    getFilters() {
        this.waitingListService.getfilterData().subscribe(
            data => {
                this.FilterData = data['sortByFilters'];
                this.filterBy = this.FilterData[0].option;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }

    showCliModal() {
        this.autoList = [];
        this.clientModal.show();
        this.getHideClientContactInfo();
        this.cSearch = '';
        setTimeout(() => {
            const cliModal = <HTMLInputElement>document.getElementById('waitingSearch');
            cliModal.focus();
        }, 1000);
    }
    closeCliModal() {
        this.clientModal.hide();
    }
    addInput() {
        this.inputs.push({});
        if (this.workerList.length > 0) {
            this.expressService(this.getWorker[0].workerId + '$' + this.getWorker[0].workerName, this.inputs.length - 1, true);
            // this.expressService(this.workerList[0].id + '$' + this.workerList[0].name, this.inputs.length - 1, true);
        }
    }

    deleteFieldValue(index) {
        this.inputs.splice(index, 1);
        this.calculateServiceDurations();
    }

    displayWaitPopUp(DataObj) {
        this.getWorkerList();
        this.boGetWorkerList();
        this.clientModal.hide();
        this.getClientAppointmemts(DataObj.Id);
        // this.waitingListModal.show();
        this.disField = true;
        this.firstName = DataObj.FirstName;
        this.lastName = DataObj.LastName;
        this.clientId = DataObj.Id;
        if (DataObj.MobilePhone) {
            this.mobileNumber = DataObj.MobilePhone.split('01-')[1];
        }
        this.primaryEmail = DataObj.Email;
        this.expressClientIds = DataObj.Id;
        this.sms_checkbox = DataObj.Sms_Consent__c;
        this.expressService(this.getWorker[0].workerId + '$' + this.getWorker[0].workerName, 0, true);
        // this.expressService(this.workerList[0].Id + '$' + this.workerList[0].name, 0, true);

        if (DataObj.Booking_Restriction_Type__c === 'Do Not Book') {
            this.Booking_Restriction_Type__c = 'Do Not Book';
            this.fullName = DataObj.FirstName + ' ' + DataObj.LastName;
            this.expressClientIds = DataObj.Id;
            const noEmail = DataObj.Email === 1 ? this.noEmail = 'No Email.' : '';
            const acctCharge = DataObj.BR_Reason_Account_Charge_Balance__c === 1 ? this.acctCharge = 'Account Charge Balance.' : '';
            const depReq = DataObj.BR_Reason_Deposit_Required__c === 1 ? this.depReq = 'Deposit Required.' : '';
            const noShow = DataObj.BR_Reason_No_Show__c === 1 ? this.clientShow = 'Persistant No Show.' : '';
            const other = DataObj.BR_Reason_Other__c === 1 ? this.other = 'Other.' : '';
            this.note = DataObj.BR_Reason_Other_Note__c;
            this.btnBook = 0;
            this.serviceNotesModal.show();
        } else if (DataObj.Booking_Restriction_Type__c === 'Alert Only') {
            this.Booking_Restriction_Type__c = 'Alert Only';
            this.fullName = DataObj.FirstName + ' ' + DataObj.LastName;
            this.expressClientIds = DataObj.Id;
            const noEmail = DataObj.Email === 1 ? this.noEmail = 'No Email.' : '';
            const acctCharge = DataObj.BR_Reason_Account_Charge_Balance__c === 1 ? this.acctCharge = 'Account Charge Balance.' : '';
            const depReq = DataObj.BR_Reason_Deposit_Required__c === 1 ? this.depReq = 'Deposit Required.' : '';
            const noShow = DataObj.BR_Reason_No_Show__c === 1 ? this.clientShow = 'Persistant No Show.' : '';
            const other = DataObj.BR_Reason_Other__c === 1 ? this.other = 'Other.' : '';
            this.note = DataObj.BR_Reason_Other_Note__c;
            this.btnBook = 1;
            this.serviceNotesModal.show();
        } else if (DataObj.Booking_Restriction_Type__c === 'None') {
            this.waitingListModal.show();
            this.actionPopup = 'add';
        } else {
            this.waitingListModal.show();
            this.actionPopup = 'add';
        }
    }

    bookanywayPopup() {
        this.serviceNotesModal.hide();
        this.waitingListModal.show();
    }
    pasteNumPhone(value) {
        let temp = '';
        if (value.indexOf('(') !== 0) {
            for (let i = 0; i < value.length; i++) {
                if (i === 0) {
                    temp += '(' + value[i];
                } else if (i === 2) {
                    temp += value[i] + ')';
                } else if (i === 5) {
                    temp += value[i] + '-';
                } else {
                    temp += value[i];
                }
                this.mobileNumber = temp.substr(0, 13);
            }
        }
    }
    mobileSms(val) {
        if (!val) {
            this.sms_checkbox = 0;
        }
    }
    SmscheckOrUnCheck(che) {
        let value;
        if (che) {
            value = 1;
        } else {
            value = 0;
        }
        this.sms_checkbox = value;
    }
    closePopup() {
        this.waitingListModal.hide();
        this.clear();
    }
    closeWaitingListModal() {
        this.waitingListModal.hide();
        this.clear();
    }
    closeOpenModel() {
        this.clientModal.hide();
        this.waitingListModal.show();
        this.firstName = '';
        this.lastName = '';
        this.mobileNumber = '';
        this.primaryEmail = '';
        this.textArea = '';
        this.clientId = '';
        this.isNewClient = true;
        this.expressService(this.getWorker[0].workerId + '$' + this.getWorker[0].workerName, 0, true);
        // this.expressService(this.workerList[0].id + '$' + this.workerList[0].name, 0, true);
    }
    hyphen_generate_mobile(value,tagid) {
        if (value === undefined) {
            value = '';
        }
        if (value.length === 0) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat('(');
        }
        if (value.length === 1) {
            (<HTMLInputElement>document.getElementById(tagid)).value = '('+value;
          }
        if (value.length === 4) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat(')');
        } if (value.length === 8) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat('-');
        }
    }
    listClientFields() {
        this.appointmentsServices.getClientFields().subscribe(
            data => {
                const clientFeilds = JSON.parse(data['result'][1].JSON__c);
                this.allowQuickAddAccess = clientFeilds.allowQuickAdd;
                if (this.allowQuickAddAccess === true) {
                    this.clientfieldMobilePhone = clientFeilds.mobilePhone;
                    this.clientfieldPrimaryEmail = clientFeilds.primaryEmail;
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }

    getHideClientContactInfo() {
        this.waitingListService.getHideCliContactInfo(this.decodeUserToken.data.id).subscribe(data => {
            this.hideClientInfo = data['result'][0].Hide_Client_Contact_Info__c;
        }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    errorHandler(event, i) {
        this.autoList[i]['image'] = '';
    }
    /* book out page methods starts */
    boGetWorkerList() {
        this.appointmentsServices.boGetWorkerList().subscribe(data => {
            this.workerList = [];
            this.workerList = data['result']
                .filter(filterList => filterList.IsActive);
            this.selectedWorkers = [];
            this.workerList.forEach((obj, index) => {
                this.selectedWorkers.push(
                    {
                        'id': obj.Id,
                        'name': obj.FirstName + ' ' + obj.LastName
                    }
                );
                obj.name = obj.FirstName + ' ' + obj.LastName;
                obj.id = obj.Id;
            });
        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }

    getWorkerList() {
        const dt1 = this.commonService.getDBDatStr(this.sendDate);
        this.appointmentsServices.getWorkerLists(dt1).subscribe(
            data => {
                this.getWorker = data['result'];
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    getClientAppointmemts(id) {
        const client = {
            'clientId': id,
            'apptViewValue': 'All'
        };
        this.waitingListService.getClientAppointmentsData(client).subscribe(
            data => {
                this.cliData = data['result'].AppointmenServices.filter(filterList => filterList.PrefDur === 1);
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    expressService(value, i, param) {
        if (value) {
            let workerId;
            let workerName;
            workerId = value.split('$')[0];
            workerName = value.split('$')[1];
            const dt1 = this.commonService.getDBDatStr(this.sendDate);
            if (!this.inputs[i]) {
                this.inputs[i] = {};
            }
            this.appointmentsServices.expressBookingServices(workerId, dt1).subscribe(
                data => {
                    this.bookingExpress[i] = data['result']['serviceList'];
                    if (this.bookingExpress[i][0] && param) {
                        for (let k = 0; k < this.getWorker.length; k++) {
                            if (this.getWorker[k]['workerId'] === workerId) {
                                this.inputs[i]['StartDay'] = this.getWorker[k]['StartDay'];
                                this.inputs[i]['wrkrName'] = this.getWorker[k]['wrkrName'];
                            }
                        }
                        this.inputs[i]['service'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0] : {};
                        this.inputs[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
                        this.inputs[i]['worker'] = workerId;
                        this.inputs[i]['workerName'] = workerName;
                        this.inputs[i]['fullWorker'] = workerId + '$' + workerName;
                    }
                    if (this.bookingExpress[i][0]) {
                        this.listServices(this.inputs[i]['serviceId'], i);
                    }
                    if (!this.clientId) {
                        this.isPreferredDuration = 0;
                        this.preferredDur = 0;
                    } else {
                        if (this.cliData) {
                            this.isPreferredDuration = 0;
                            this.preferredDur = 0;
                            this.isPreferredDuration = this.getPreferredDuration(this.cliData, this.bookingExpress[i], this.inputs[i]);

                            if (this.isPreferredDuration.length > 0) {
                                this.preferredDur = 0;
                                for (let j = 0; j < this.bookingExpress[i].length; j++) {
                                    if (this.isPreferredDuration[0].Service__c === this.bookingExpress[i][j].serviceId) {
                                        this.bookingExpress[i][j]['Duration_1__c'] = this.isPreferredDuration[0].Duration_1__c;
                                        this.bookingExpress[i][j]['Duration_2__c'] = this.isPreferredDuration[0].Duration_2__c;
                                        this.bookingExpress[i][j]['Duration_3__c'] = this.isPreferredDuration[0].Duration_3__c;
                                        this.bookingExpress[i][j]['Buffer_after__c'] = this.isPreferredDuration[0].Buffer_after__c;
                                        this.bookingExpress[i][j]['sumDurationBuffer'] = this.isPreferredDuration[0].Duration__c;
                                        this.bookingExpress[i][j]['PrefDur'] = this.preferredDur;
                                        this.prefService = this.isPreferredDuration[0].Duration__c;
                                    }
                                }
                            }
                        }
                        this.calculateServiceDurations();
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    listServices(value, i) {
        const obj = this.bookingExpress[i].filter((data) => data.serviceId === value);
        if (obj.length > 0) {
            this.inputs[i]['service'] = obj[0];
            this.inputs[i]['serviceId'] = obj[0]['serviceId'];
            this.inputs[i]['skipRecalculate'] = 'false';
            this.calculateServiceDurations();
        } else {
            this.bookingExpress[i].push({ Name: '(' + this.inputs[i]['service']['Name'] + ')', serviceId: this.inputs[i]['serviceId'] });
            this.calculateServiceDurations();
        }
    }
    getPreferredDuration(cliData, servicelist, inputs) {
        for (let i = 0; i < this.cliData.length; i++) {
            for (let t = 0; t < servicelist.length; t++) {
                if ((servicelist[t].serviceId === this.cliData[i].Service__c) && (inputs.worker === this.cliData[i].Worker__c)) {
                    if (this.cliData[i].PrefDur === 1) {
                        const obj = [];
                        obj.push({
                            'Service__c': cliData[i].Service__c,
                            'Duration__c': cliData[i].Duration__c,
                            'Duration_1__c': cliData[i].Duration_1__c,
                            'Duration_2__c': cliData[i].Duration_2__c,
                            'Duration_3__c': cliData[i].Duration_3__c,
                            'Buffer_after__c': cliData[i].Buffer_After__c
                        });
                        return obj;
                    }
                }
            }
        }
        return 0;
    }
    calculateServiceDurations() {
        this.servicePrice = 0;
        this.serviceDurations = 0;
        if (this.inputs && this.inputs.length > 0) {
            for (let j = 0; j < this.inputs.length; j++) {
                const serviceVal = this.inputs[j].service;
                if (serviceVal.Price__c !== null) {
                    this.servicePrice += parseFloat(serviceVal.Price__c);
                } else {
                    this.servicePrice += parseFloat(serviceVal.pcsergrp);
                }
                if (serviceVal.sumDurationBuffer !== null || serviceVal.sumDurationBuffer !== '') {
                    // this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer) ? parseFloat(serviceVal.sumDurationBuffer) : parseFloat(serviceVal.dursergrp);
                    this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer);
                } else {
                    this.serviceDurations += parseFloat(serviceVal.dursergrp) ? parseFloat(serviceVal.dursergrp) : 0;
                }
            }
        }
    }

    saveData(id) {
        const dt = new Date();
        const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.firstName === '' || this.firstName === undefined) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.FIRST_NAME_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.lastName === '' || this.lastName === undefined) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.LAST_NAME_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.clientfieldMobilePhone === true && this.mobileNumber === '' && (this.hideClientInfo === 0 && this.mobileNumber === '')) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.MOBILE_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.mobileNumber && this.mobileNumber.length !== 13) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.MOBILE_INVALID');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.clientfieldPrimaryEmail === true && this.primaryEmail === '' && (this.hideClientInfo === 0 && this.primaryEmail === '')) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.EMAIL_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.primaryEmail && !EMAIL_REGEXP.test(this.primaryEmail) && (this.hideClientInfo === 0 && this.primaryEmail === '')) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.EMAIL_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.startDate === null) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.START_DATE_MANDATORY');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.startDate.setHours(0, 0, 0, 0) < dt.setHours(0, 0, 0, 0)) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.VALID_START_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (isNaN(this.startDate.getTime())) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.START_DATE_INVALID');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.endDate && isNaN(this.endDate.getTime())) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.END_DATE_INVALID');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else if (this.endDate && this.endDate.setHours(0, 0, 0, 0) < this.startDate.setHours(0, 0, 0, 0)) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.VALID_END_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        } else {
            if (this.endDate === null) {
                this.endDate = this.startDate;
            }
            const dataObj = {
                'clientFstName': this.firstName.trim(),
                'clientLstName': this.lastName.trim(),
                'clientMobile': this.mobileNumber !== '' && this.mobileNumber ? this.countrycode + '-' + this.mobileNumber : '',
                'clientEmail': this.primaryEmail,
                'startDate': this.commonService.getDBDatTmStr(this.startDate),
                'endDate': this.commonService.getDBDatTmStr(this.endDate),
                'services': this.inputs,
                'Sms_Consent__c': this.sms_checkbox,
                'servicePrice': this.servicePrice,
                'serviceDuration': this.serviceDurations,
                'notes': this.textArea,
                'isNewClient': this.isNewClient,
                'IsPackage': '',
            };
            if (this.clientId) {
                dataObj['clientId'] = this.clientId;
            } else {
                dataObj['clientId'] = '';
            }
            if (id) {
                dataObj['waitingListId'] = id;
            } else {
                dataObj['waitingListId'] = '';
            }
            this.waitingListService.saveData(dataObj).subscribe(
                data => {
                    const dataa = data['result'];
                    if (dataa) {
                        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                        this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                        this.toDate = this.startDate;
                        this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
                        this.workrArr = [];
                        this.selectedWorkers.forEach(element => {
                            this.workrArr.push(element['id']);
                        });
                        this.getList(this.expiry, this.filterBy, this.paramDt, this.workrArr);
                        this.waitingListModal.hide();
                        this.clear();
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (statuscode) {
                        case '2033':
                            this.toastr.error('Record with the same name already exists', null, { timeOut: 2500 });
                            break;
                    }
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    clear() {
        this.firstName = '';
        this.lastName = '';
        this.mobileNumber = '';
        this.inputs = [];
        this.textArea = '';
        this.clientId = '';
        this.sms_checkbox = 0;
        this.servicePrice = 0;
        this.sumDuration = 0;
        this.disField = false;
        this.isNewClient = false;
        this.waitingListId = '';
        this.startDate = new Date();
        this.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
    }
    getList(value, filterBy, toDate, selectedWorkers) {
        if (isNaN(this.toDate.getTime())) {
            this.toastermessage = this.translateService.get('WAITINGLIST_APPOINTMENT.SEARCH_DATE_INVALID');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
            window.scrollTo(0, 0);
            this.waitingList = [];
        } else {
            this.workrArr = [];
            this.selectedWorkers.forEach(element => {
                this.workrArr.push(element['id']);
            });
            if (this.workerList.length === this.workrArr.length) {
                this.workrArr = 'All';
            }
            this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
            this.waitingListService.getWaitingList(value, filterBy, this.paramDt, this.workrArr).subscribe(
                data => {
                    this.waitingList = data['result'];
                    if (this.waitingList.length === 0) {
                        this.toastr.error('No records found', null, { timeOut: 1500 });
                    } else {
                        let arr = '';
                        for (let i = 0; i < this.waitingList.length; i++) {

                            if (this.waitingList[i]['MobilePhone']) {
                                this.waitingList[i]['MobilePhone'] = this.waitingList[i]['MobilePhone'].split('-')[1] + '-' + this.waitingList[i]['MobilePhone'].split('-')[2];
                            } else {
                                this.waitingList[i]['MobilePhone'] = '';
                            }
                            if (this.waitingList[i]['Phone']) {
                                this.waitingList[i]['Phone'] = this.waitingList[i]['Phone'].split('-')[1] + '-' + this.waitingList[i]['Phone'].split('-')[2];
                            } else {
                                this.waitingList[i]['Phone'] = '';
                            }
                            this.waitingList[i]['createdDt'] = this.commonService.getUsrDtStrFrmDBStr(this.waitingList[i]['CreatedDate']);
                            this.waitingList[i]['stdate'] = this.commonService.getUsrDtStrFrmDBStr(this.waitingList[i]['Earliest_Date_c']);
                            this.waitingList[i]['endate'] = this.commonService.getUsrDtStrFrmDBStr(this.waitingList[i]['Latest_Date__c']);
                            for (let j = 0; j < JSON.parse(this.waitingList[i].Services_and_Workers__c).length; j++) {
                                arr += JSON.parse(this.waitingList[i].Services_and_Workers__c)[j]['service']['Name'] + ' ';
                                arr += '(' + JSON.parse(this.waitingList[i].Services_and_Workers__c)[j]['workerName'] + '),';
                            }
                            arr = arr.slice(0, -1);
                            this.waitingList[i]['services'] = arr;
                            arr = '';
                        }
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    deleterecord(id) {
        this.waitingListService.deleteWaitingListRecrd(id).subscribe(
            data => {
                const dataa = data['result'];
                if (dataa['affectedRows'] > 0) {
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_DELETE_SUCCESS');
                    this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
                    this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
                    this.workrArr = [];
                    this.selectedWorkers.forEach(element => {
                        this.workrArr.push(element['id']);
                    });
                    this.getList(this.expiry, 'Created Date', this.paramDt, this.workrArr);
                } else {
                    this.toastr.warning('There is no expired records in the list', null, { timeOut: 1500 });
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                const statuscode = JSON.parse(error['_body']).status;
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    showExpRcrds(value) {
        this.expiry = value;
        this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
        this.workrArr = [];
        this.selectedWorkers.forEach(element => {
            this.workrArr.push(element['id']);
        });
        if (this.expiry === true) {
            this.getList(true, 'Created Date', this.paramDt, this.workrArr);
        } else {
            this.getList(false, 'Created Date', this.paramDt, this.workrArr);
        }

    }
    changeFiltersBy(value) {
        if (value === 'Date Range') {
            this.filterBy = value;
        } else if (value === 'Client First Name') {
            this.filterBy = value;
        } else if (value === 'Client Last Name') {
            this.filterBy = value;
        } else if (value === 'Created Date') {
            this.filterBy = value;
        }
        this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
        this.workrArr = [];
        this.selectedWorkers.forEach(element => {
            this.workrArr.push(element['id']);
        });
        this.getList(this.expiry, this.filterBy, this.paramDt, this.workrArr);
    }
    editWaitingListModal(waitingData) {
        this.getClientAppointmemts(waitingData.Client__c);
        this.actionPopup = 'edit';
        this.waitingListId = waitingData.Id;
        this.inputs = [];
        this.waitingListService.editWaitListingData(waitingData.Id).subscribe(
            data => {
                const tempList = data['result'][0];
                this.disField = true;
                this.clientId = tempList['Client__c'];
                this.firstName = tempList['FirstName'];
                this.lastName = tempList['LastName'];
                if (tempList['MobilePhone']) {
                    this.mobileNumber = tempList['MobilePhone'].split('-')[1] + '-' + tempList['MobilePhone'].split('-')[2];
                }
                this.sms_checkbox = tempList['Sms_Consent__c'];
                this.primaryEmail = tempList['Email'];
                this.startDate = this.commonService.getDateTmFrmDBDateStr(tempList['Earliest_Date_c']);
                this.endDate = this.commonService.getDateTmFrmDBDateStr(tempList['Latest_Date__c']);
                this.textArea = tempList['Note__c'];
                const jsonServWor = JSON.parse(tempList['Services_and_Workers__c']);
                for (let i = 0; i < jsonServWor.length; i++) {
                    this.inputs.push({
                        'service': jsonServWor[i]['service'],
                        'serviceId': jsonServWor[i]['serviceId'],
                        'worker': jsonServWor[i]['worker'],
                        'workerName': jsonServWor[i]['workerName'],
                        'fullWorker': jsonServWor[i]['fullWorker'],
                        'wrkrName': jsonServWor[i]['wrkrName'], // this is dependency in find appt slots workername pls dont make change.//
                        'StartDay': jsonServWor[i]['StartDay']
                    });
                    this.expressService(jsonServWor[i]['fullWorker'], i, false);
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                const statuscode = JSON.parse(error['_body']).status;
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
        this.waitingListModal.show();
    }
    notePopup() {
        this.serviceNotesModal.show();
    }
    changedata() {
        this.workrArr = [];
        this.selectedWorkers.forEach(element => {
            this.workrArr.push(element['id']);
        });
        this.paramDt = this.commonService.getDBDatTmStr(this.toDate);
        this.getList(this.expiry, 'Created Date', this.paramDt, this.workrArr);
    }


}

