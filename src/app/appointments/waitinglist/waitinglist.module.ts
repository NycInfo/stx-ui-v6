import { WaitingListComponent } from './waitinglist.component';
import { WaitingListRoutingModule } from './waitinglist.routing';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DataTableModule } from '../../../custommodules/primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModule } from '../../common/share.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from 'ng2-translate';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
    imports: [
        CommonModule,
        WaitingListRoutingModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        DataTableModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
        NgMultiSelectDropDownModule.forRoot(),
        PerfectScrollbarModule
    ],
    declarations: [
        WaitingListComponent,
    ]
})
export class WaitingListModule {
}
