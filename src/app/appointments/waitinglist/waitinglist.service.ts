/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClient } from '../../common/http-client';

@Injectable()
export class WaitingListService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getWorkerList() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
      .map(this.extractData);
  }
  getHideCliContactInfo(id) {
    return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
      .map(this.extractData);
  }
  getData(searchKey) {
    return this.http.get(this.apiEndPoint + '/api/clientsearch/' + searchKey)
      .map(this.extractData);
  }
  saveData(obj) {
    return this.http.post(this.apiEndPoint + '/api/waitinglist', obj)
      .map(this.extractData);
  }
  getWaitingList(value, filterBy, paramDt, selectedWorkers) {

    if (selectedWorkers.length === 0) {
      selectedWorkers = 'All';
    }
    const headers = new Headers();
    headers.append('listtype', value);
    headers.append('filterby', filterBy);
    headers.append('wldate', paramDt);
    headers.append('workers', selectedWorkers);
    return this.http.getHeader(this.apiEndPoint + '/api/waitinglist', headers)
      .map(this.extractData);
  }
  editWaitListingData(id) {
    return this.http.get(this.apiEndPoint + '/api/waitinglistdata/' + id)
      .map(this.extractData);
  }
  deleteWaitingListRecrd(id) {
    return this.http.delete(this.apiEndPoint + '/api/waitinglist/' + id)
      .map(this.extractData);
  }
  getfilterData() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .map(this.extractData);
  }
  getDateWorkerFilter(dt, worker) {
    if (worker.length === 0){
      worker = 'all';
    }
    const headers = new Headers();
    headers.append('filterDate', dt);
    headers.append('filterWrkr', JSON.stringify(worker));
    return this.http.getHeader(this.apiEndPoint + '/api/waitinglist/filters', headers)
      .map(this.extractData);
  }
  getClientAppointmentsData(client) {
    return this.http.post(this.apiEndPoint + '/api/clientSearch/bookappointments', client)
        .map(this.extractData);
}

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
