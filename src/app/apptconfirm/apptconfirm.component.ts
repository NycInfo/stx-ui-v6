import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApptConfirmService } from './apptconfirm.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../common/common.service';
import { DatePipe } from '@angular/common';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './apptconfirm.html',
  styleUrls: ['./apptconfirm.component.css'],
  providers: [ApptConfirmService, CommonService],
})
export class ApptConfirmComponent implements OnInit {

  datePickerConfig: any;
  dbName = '';
  appointmentId: any;
  displayMsg = '';
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private apptConfirmService: ApptConfirmService,
    private commonService: CommonService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.dbName = activatedRoute.snapshot.params['schema'];
      this.appointmentId = activatedRoute.snapshot.params['appointmentId'];
    });
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    const currentDate = new Date();
    const dtStr = currentDate.getFullYear()
      + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2)
      + '-' + ('0' + currentDate.getDate()).slice(-2)
      + ' ' + ('0' + currentDate.getHours()).slice(-2)
      + ':' + ('0' + currentDate.getMinutes()).slice(-2)
      + ':' + ('0' + currentDate.getSeconds()).slice(-2);
    this.apptConfirmService.confirmAppt(this.dbName, this.appointmentId, dtStr).subscribe(data => {
      if (data['result']) {
        data['result']['Service_Date_Time__c'] = this.commonService.getUsrDtStrFrmDBStrFormat(data['result']['Service_Date_Time__c']);
        this.displayMsg = 'Hi ' + data['result']['clinetName']
          + ', your appointment has been confirmed for ' + data['result']['Service_Date_Time__c'][0] + ' at ' + data['result']['Service_Date_Time__c'][1]
          + ' for a ' + data['result']['workerName'] + '';
      } else {
        this.displayMsg = ' Your appointment has been completed';
      }
    },
      error => {
        this.displayMsg = 'Unable to confirm your Appointment'
      });
  }

}
