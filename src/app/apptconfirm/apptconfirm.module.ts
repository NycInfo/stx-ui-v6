import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApptConfirmComponent } from './apptconfirm.component';
import { ApptConfirmRoutingModule } from './apptconfirm.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ApptConfirmRoutingModule,
        BsDatepickerModule.forRoot(),
        ShareModule,
    ],
    declarations: [
        ApptConfirmComponent
    ]
})
export class ApptConfirmModule {
}
