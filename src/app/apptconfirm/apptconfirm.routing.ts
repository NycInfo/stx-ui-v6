import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ApptConfirmComponent } from './apptconfirm.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ApptConfirmComponent,
                children: [
                    {
                        path: '',
                        component: ApptConfirmComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ApptConfirmRoutingModule {
}
