/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClient } from '../common/http-client';

@Injectable()
export class ApptConfirmService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  /*To extract json data*/
  confirmAppt(dbName, appointmentId, dtStr) {
    return this.http.post(this.apiEndPoint + '/api/appointments/confirm', {
      'schema': dbName,
      'apptId': appointmentId,
      'dt': dtStr
    })
      .map(this.extractData);
  }
  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }
}
