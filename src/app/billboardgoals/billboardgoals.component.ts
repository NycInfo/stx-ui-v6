import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { BillboardgoalsService } from './billboardgoals.service';
import * as config from '../app.config';
import { SetupgoalsvirtualcoachingService } from '../../../src/app/setup/setupworkers/setupgoalsvirtualcoaching/setupgoalsvirtualcoaching.service'
@Component({
    selector: 'app-billboardgoals-popup',
    templateUrl: './billboardgoals.html',
    styleUrls: ['./billboardgoals.css'],
    providers: [BillboardgoalsService, SetupgoalsvirtualcoachingService]
})
export class BillboardgoalsComponent implements OnInit {
    tabsArray = [true, false, false, false, false, false, false];
    activeTabClass = ['active', '', '', '', '', '', ''];
    monthsData: any;
    startmonth = '';
    yearsDataList: any = [];
    year: any;
    billGoal = 'servicesales';
    slideDown = false;
    selectedItem = -1;
    virtualData: any = {};
    toastermessage: any;
    serviceSalesData: any = [];
    data: any = [];
    workerName = '';
    workerImage: any = {};
    billBoardGoalData: any = [];
    displayBillboardGoals: any = [];
    setupBillboardGoalsData: any = [];
    optionsData: any;
    displayBillboardGoalType : any;
    constructor(private toastr: ToastrService,
        private billboardgoalsService: BillboardgoalsService,
        private setupgoalsvirtualcoachingService: SetupgoalsvirtualcoachingService,
        private router: Router,
        private translateService: TranslateService) {
    }
    ngOnInit() {
        this.getVirtualData();
        this.getMonths();
        this.getYear();
    }
    upateTabs(index) {
        for (let i = 0; i < this.tabsArray.length; i++) {
            if (i === index) {
                this.tabsArray[i] = true;
                this.activeTabClass[i] = 'active';
            } else {
                this.tabsArray[i] = false;
                this.activeTabClass[i] = '';
            }
        }
    }
    getVirtualData() {
        this.setupgoalsvirtualcoachingService.getData()
            .subscribe(data => {
                this.virtualData = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });

    }
    getMonths() {
        this.billboardgoalsService.getMonthsTypes().subscribe(
            data => {
                this.monthsData = data['workerGoalMonths'];
                this.startmonth = this.monthsData[0].value;
                setTimeout(() => {
                    this.getGoalDetails(this.billGoal, this.startmonth, this.year);
                }, 2000);

            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getYear() {
        const years = new Date().getFullYear();
        for (let i = 0; i <= 10; i++) {
            this.yearsDataList.push(years - i);
            this.year = this.yearsDataList[0];
        }
        this.getGoalDetails(this.billGoal, this.startmonth, this.year);

    }

    changeMonth(value) {
        this.startmonth = value;
        this.getGoalDetails(this.billGoal, this.startmonth, this.year);

    }
    changeYear(value) {
        this.year = value;
        this.getGoalDetails(this.billGoal, this.startmonth, this.year);

    }
    changeType(val) {
        this.billGoal = val;
        this.getGoalDetails(this.billGoal, this.startmonth, this.year);
    }
    getGoalDetails(billgoal, month, year) {
        this.displayBillboardGoals = [];
        billgoal = this.billGoal;
        month = this.startmonth;
        year = this.year;
        setTimeout(() => {
            this.billboardgoalsService.getBillboardGoals(month, year).subscribe(
                data => {
                    this.billBoardGoalData = data['result'];
                    console.log(this.billBoardGoalData )
                    if (billgoal === this.billBoardGoalData['srvcSalesData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['srvcSalesData']['result'];
                        this.displayBillboardGoalType = 'Service Sales';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['rebookData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['rebookData']['result'];
                        this.displayBillboardGoalType = 'Rebook %';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['averageTicketData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['averageTicketData']['result'];
                        this.displayBillboardGoalType = 'Average Ticket';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['retailServiceData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['retailServiceData']['result'];
                        this.displayBillboardGoalType = 'Retail To Service %';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['productSalesData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['productSalesData']['result'];
                        this.displayBillboardGoalType = 'Product Sales';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['retailperTicketData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['retailperTicketData']['result'];
                        this.displayBillboardGoalType = 'Retail Per Ticket';
                        this.showBillGoals(this.displayBillboardGoals);
                    } else if (billgoal === this.billBoardGoalData['retailperGuestData']['type']) {
                        this.displayBillboardGoals = this.billBoardGoalData['retailperGuestData']['result'];
                        this.displayBillboardGoalType = '# Retail Per Guest';
                        this.showBillGoals(this.displayBillboardGoals);
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
            // this.billboardgoalsService.getSetupBillboardGoals(month, year).subscribe(
            //     data => {
            //         this.setupBillboardGoalsData = data['result'];
            //         console.log(this.setupBillboardGoalsData)
            //     },
            //     error => {

            //     });
        }, 2000);
    }
    showBillGoals(displaygoals) {
        for (let i = 0; i < displaygoals.length; i++) {
            if (displaygoals[i]['billBoardData'] >= 100) {
                displaygoals[i]['message'] = this.virtualData['excellent'];
                displaygoals[i]['color'] = '#7ed957';
            } else if (displaygoals[i]['billBoardData'] <= 99 && displaygoals[i]['billBoardData'] > 89) {
                displaygoals[i]['message'] = this.virtualData['verygood'];
                displaygoals[i]['color'] = '#7ed957';
            } else if (displaygoals[i]['billBoardData'] <= 89 && displaygoals[i]['billBoardData'] > 74) {
                displaygoals[i]['message'] = this.virtualData['good'];
                displaygoals[i]['color'] = '#ffde59';
            } else if (displaygoals[i]['billBoardData'] <= 74 && displaygoals[i]['billBoardData'] > 59) {
                displaygoals[i]['message'] = this.virtualData['fair'];
                displaygoals[i]['color'] = '#ff914d';
            } else if (displaygoals[i]['billBoardData'] <= 59 && displaygoals[i]['billBoardData'] > 0) {
                displaygoals[i]['message'] = this.virtualData['poor'];
                displaygoals[i]['color'] = '#ff5757';
            } else if (displaygoals[i]['billBoardData'] === 0) {
                displaygoals[i]['message'] = this.virtualData['verypoor'];
                displaygoals[i]['color'] = '#d9d9d8';
            }
        }
        this.data = displaygoals;
    }
    showGoalDetails(index) {
        if (index === this.selectedItem) {
            this.selectedItem = -1;
        } else {
            this.selectedItem = index;
        }
    }
}
