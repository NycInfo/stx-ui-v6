import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillboardgoalsComponent } from './billboardgoals.component';
import { BillboardgoalsRoutingModule } from './billboardgoals.routing';
import { TranslateModule } from 'ng2-translate';
import { ShareModule } from '../common/share.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        BillboardgoalsRoutingModule,
        TranslateModule,
        FormsModule,
        ShareModule
    ],
    declarations: [
        BillboardgoalsComponent
    ]
})
export class BillboardgoalsModule {
}
