import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BillboardgoalsComponent } from './billboardgoals.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BillboardgoalsComponent,
                children: [
                    {
                        path: '',
                        component: BillboardgoalsComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class BillboardgoalsRoutingModule {
}
