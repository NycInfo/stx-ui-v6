
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClient } from '../common/http-client';
import { Headers } from '@angular/http';

@Injectable()
export class BillboardgoalsService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getMonthsTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .map(this.extractData);
  }
   getBillboardGoals(month, year) {
    const headers = new Headers();
    headers.append('month', month);
    headers.append('year', year);
    return this.http.getHeader(this.apiEndPoint + '/api/billboardgoals', headers)
      .map(this.extractData);
  }
  getSetupBillboardGoals(month, year) {
    const headers = new Headers();
    headers.append('month', month);
    headers.append('year', year);
    return this.http.getHeader(this.apiEndPoint + '/api/setup/billboardgoals', headers)
      .map(this.extractData);
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
