import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { CheckOutService } from './checkout.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as config from '../../app.config';
import { CommonService } from '../../common/common.service';
import { JwtHelper } from 'angular2-jwt';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { parse } from 'url';
import { DecimalPipe } from '@angular/common';
import { Md5 } from 'ts-md5/dist/md5';
import { datepickerLocale } from 'fullcalendar';
import { getFullYear } from 'ngx-bootstrap/chronos/utils/date-getters';
declare let $: any;
declare var swal: any;
@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './checkout.html',
  providers: [CheckOutService, CommonService],
  styleUrls: ['./checkout.css']
})
export class CheckOutComponent implements OnInit {
  searchKey: any;
  DataObj: any;
  error: any;
  checkOutList: any;
  rowsPerPage: any;
  autoList = [];
  apiEndPoints = config.S3_URL;
  apptId: any;
  decodeUserToken: any;
  hideClientInfo: any;
  decodedToken: any;
  noResult: any;

  newdate: any = new Date();
  public bsValue: any;
  public bsValue1: any = new Date();
  firstName: any = '';
  lastName: any = '';
  birthday: any = '';
  gender: any = '';
  primaryEmail: any = '';
  memUniId: any = '';
  mobileNumber: any = '';
  sms_checkbox: any = '';
  MemberShipType: any = '';
  MemberShipTypeError: any = '';
  paymentTypesError: any = '';
  MembersipsList = [];
  refPaymentId: any = '';
  charge: any = '';
  chargeError: any = '';
  cardNumber: any = '';
  NameOnCard: any = '';
  checkBox = false;
  cvv: any = '';
  monthList = ['01 - January', '02 - February', '03 - March', '04 - April', '05 - May', '06 - June',
    '07 - July', '08 - August', '09 - September', '10 - October', '11 - November', '12 - December'];
  yearList = [];
  expYear = 0;
  expMonth = 1;
  zipCode: any = '';
  clientName: any = '';
  autoBill = false;
  nextBillDate: any = '';
  nextBillDateError: any = '';
  paymentList = [];
  paymentTypes: any = '';
  hidePaymentTab = true;
  setPayment: any;
  firstNameError: any = '';
  lastNameError: any = '';
  primaryEmailError: any = '';
  NameOnCardError: any = '';
  memUniIdError: any = '';
  expiryError: any = '';
  cvvError: any = '';
  cardNumberError: any = '';
  toastermessage: any;
  maxDate = new Date();
  saveButon = true;
  showClientInfo: any = [];
  dummyClientName = 'STX';
  tokenbody: any;
  cardTokenId1 = '';
  deleteClientId: any;
  datePickerConfig: any;
  table = false;
  amount: any;
  countrycode = '01';
  total = 0;
  endDate = new Date();
  minDate = new Date();
  datedate: any = new Date();

  public searchField = new FormControl();
  @ViewChild('membershipsModal') public membershipsModal: ModalDirective;
  @ViewChild('refundsModal') public refundsModal: ModalDirective;
  @ViewChild('newMemberModal') public newMemberModal: ModalDirective;
  ecommerceData: any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private checkOutService: CheckOutService) {
    this.route.queryParams.subscribe(params => {
      this.apptId = route.snapshot.params['Id'];
    });

  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }

    // ---End of code for permissions Implementation--- //
    this.searchField.valueChanges
      .debounceTime(400)
      // .distinctUntilChanged()
      .switchMap(term => this.checkOutService.getData(term)
      ).subscribe(
        data => {
          this.DataObj = data['result'];
          if (this.searchKey && this.searchKey.trim()) {
            if (this.searchKey.length !== 0 && this.DataObj.length === 0) {
              this.noResult = 'No Results';
            } else {
              this.noResult = '';
            }
          } else {
            this.DataObj = [];
          }
        },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    this.getHideClientContactInfo();
    this.getPaymentTypes();
    this.getSetupMemberships();
    this.createYearsList();
    this.maxDate = new Date();
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });

    this.datedate.setDate(this.datedate.getDate() - 90);
    if (this.datedate && this.endDate) {
      this.generateReport();
    }
  }
  /*client search data */
  searchClients() {
    if (this.searchKey === '' || this.searchKey === undefined || (this.searchKey.trim()).length <= 1) {
      this.DataObj = [];
      this.noResult = '';
    }
  }
  /**
   * Memberships modal code starts
   */
  showMembershipsModal() {
    this.membershipsModal.show();
    this.getHideClientContactInfo();
  }

  populateFromDate() {
    if (this.bsValue > this.bsValue1) {
      this.bsValue1 = this.bsValue;
    }
  }

  createYearsList() {
    const curtYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.yearList.push(curtYear + i);
    }
    this.expYear = this.yearList[0];
  }

  getSetupMemberships() {
    const inActive = 1;
    this.checkOutService.getSetupMemberships(inActive)
      .subscribe(data => {
        this.MembersipsList = data['result'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }

  hyphen_generate_mobile(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat('-');
    }
  }

  payType(val) {
    if ('cash' === (val.split('$')[1]).toLowerCase()) {
      this.autoBill = false;
      this.hidePaymentTab = false;
    } else {
      this.hidePaymentTab = true;
    }
  }

  cancelMembershipsModal() {
    this.membershipsModal.hide();
    this.searchKey = '';
    this.DataObj = [];
  }

  closeMemberShip() {
    this.clearErrorMsg();
    this.newMemberModal.hide();
  }

  cancelIncludeTicket() {
    this.router.navigate(['/checkout/' + this.apptId]);
  }
  /**
   * Memberships modal code ends
   */
  /**
  * Memberships modal code starts
  */
  showRefundsModal() {
    this.refundsModal.show();
    this.getHideClientContactInfo();
    setTimeout(() => {
      const refundModal = <HTMLInputElement>document.getElementById('refundByName');
      refundModal.focus();
    }, 1000);
  }
  cancelRefundsModal() {
    this.refundsModal.hide();
  }
  /**
   * Memberships modal code ends
   */
  lookupCloseModal() {

  }

  addTickets() {
    const includedTicketList = this.checkOutList.filter((ticket) => ticket['include']);
    const addTicket = {
      includeTickets: includedTicketList,
      apptId: this.apptId
    };
    this.checkOutService.addTickets(addTicket).subscribe((result) => {
      this.router.navigate(['/checkout/' + this.apptId]);
    }, (error) => {
      const status = JSON.parse(error['status']);
      const statuscode = JSON.parse(error['_body']).status;
      switch (JSON.parse(error['_body']).status) {
        case '2033':
          break;
      }
      if (statuscode === '2085' || statuscode === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }
  cancelModal() {
    this.refundsModal.hide();
    this.DataObj = [];
    this.searchKey = '';
  }
  getHideClientContactInfo() {
    this.checkOutService.getHideCliContactInfo(this.decodeUserToken.data.id).subscribe(data => {
      this.hideClientInfo = data['result'][0].Hide_Client_Contact_Info__c;
    }, error => {
      const errStatus = JSON.parse(error['_body'])['status'];
      if (errStatus === '2085' || errStatus === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }
  errorHandler(event, i) {
    this.DataObj[i]['Client_Pic__c'] = '';
  }
  showNewMemberModal() {
    this.membershipsModal.hide();
    this.newMemberModal.show();
  }
  cancelNewMemberModal() {
    this.newMemberModal.hide();
  }

  getPaymentTypes() {
    this.checkOutService.getPaymentTypesData().subscribe(
      data => {
        this.refPaymentId = data['result'].Id;
        const payment = data['result']['paymentResult'];
        for (let i = 0; i < payment.length; i++) {
          if (payment[i].Name.toLowerCase() === 'credit card' || payment[i].Name.toLowerCase() === 'clover') {
            this.paymentList.push(payment[i]);
          }
        }
      });
  }

  generateToken(obj, dataObj) {
    let expmonth;
    if (this.expMonth.toString().length <= 1) {
      expmonth = '0' + this.expMonth;
    } else {
      expmonth = this.expMonth;
    }
    const d = new Date();
    const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
      (d.getFullYear() + '').slice(-2) + ':' +
      ('00' + d.getHours()).slice(-2) + ':' +
      ('00' + d.getMinutes()).slice(-2) + ':' +
      ('00' + d.getSeconds()).slice(-2) + ':000';

    const clientId = Math.floor((Math.random() * 123456789) + 26);
    this.commonService.ecommerceDetails().subscribe(
      resData => {
        this.ecommerceData = JSON.parse(resData.result[0].JSON__c);
        if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
          const hash = Md5.hashStr(this.ecommerceData.merchantId + clientId + dateTime + this.cardNumber +
            expmonth + this.expYear.toString().substring(2) + this.commonService.getCardType(this.cardNumber) + this.NameOnCard + this.ecommerceData.accessToken);

          const clientData1 = {
            merchantref: clientId,
            terminalid: this.ecommerceData.merchantId,
            cardType: this.commonService.getCardType(this.cardNumber),
            cardHolName: this.NameOnCard,
            dateTime: dateTime,
            cardNum: this.cardNumber,
            cardExp: expmonth + this.expYear.toString().substring(2),
            hash: hash,
            cvv: this.cvv
          };
          this.tokenbody = this.commonService.createToken(clientData1);

          const url = config.ANYWHERECOMMERCE_PAYMENT_API;

          const reqObj = {
            'url': url,
            'xml': this.tokenbody
          };
          this.checkOutService.xmlPayment(reqObj).subscribe(
            data => {
              let cardTokenId: any = '';
              const parseString = require('xml2js').parseString;
              parseString(data['result'], function (err, result) {
                cardTokenId = result;
              });
              if (cardTokenId.ERROR && cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID MERCHANTREF') {
                this.delClientMember(dataObj);
                this.toastr.warning('INVALID MERCHANTREF', null, { timeOut: 3000 });
              } else if ((cardTokenId.ERROR) && (!cardTokenId.SECURECARDUPDATERESPONSE || !cardTokenId.SECURECARDREGISTRATIONRESPONSE)) {
                if ((cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] === cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] || this.cardNumber.toString() === '0')
                  && (cardTokenId.ERROR.ERRORSTRING[0] !== 'INVALID CARDEXPIRY') &&
                  (cardTokenId.ERROR.ERRORSTRING[0] !== 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12')) {
                  this.delClientMember(dataObj);
                  this.toastr.warning('Credit Card Processing Error: INVALID CARDNUMBER field', null, { timeOut: 3000 });
                } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID CARDEXPIRY') {
                  this.delClientMember(dataObj);
                  this.toastr.warning('Invalid card expiry', null, { timeOut: 3000 });
                } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12') {
                  this.delClientMember(dataObj);
                  this.toastr.warning('Card number must be 12 digits ', null, { timeOut: 3000 });
                }
              }

              this.cardTokenId1 = cardTokenId.SECURECARDREGISTRATIONRESPONSE.CARDREFERENCE[0];
              if (this.cardTokenId1 !== '') {
                this.paymentTicket(obj, dataObj);
              } else {
                this.delClientMember(dataObj);
                this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
              }
            },
            error => {
              const status = JSON.parse(error['status']);
              const statuscode = JSON.parse(error['_body']).status;
              switch (status) {
                case 500:
                  break;
                case 400:
                  if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                      localStorage.setItem('page', this.router.url);
                      this.router.navigate(['/']).then(() => { });
                    }
                  } break;
              }
            });
        } else {
          this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
        }
      });
  }

  paymentTicket(obj, clientId) {
    const d = new Date();
    let paymentData;
    const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
      (d.getFullYear() + '').slice(-2) + ':' +
      ('00' + d.getHours()).slice(-2) + ':' +
      ('00' + d.getMinutes()).slice(-2) + ':' +
      ('00' + d.getSeconds()).slice(-2) + ':000';
    this.commonService.ecommerceDetails().subscribe(
      resData => {
        this.ecommerceData = JSON.parse(resData.result[0].JSON__c);
        if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
          const hash = Md5.hashStr(this.ecommerceData.merchantId + this.refPaymentId + this.charge + dateTime + this.ecommerceData.accessToken);
          const url = config.ANYWHERECOMMERCE_PAYMENT_API;
          const clientData = {
            ticketPaymntId: this.refPaymentId,
            terminalid: this.ecommerceData.merchantId,
            dateTime: dateTime,
            cardNum: this.cardNumber,
            cardType: this.commonService.getCardType(this.cardNumber),
            currency: 'USD',
            terminalType: '1',
            transactionType: '4',
            hash: hash,
            amountDue: this.charge,
            cardExp: ('0' + this.expMonth).slice(-2) + this.expYear.toString().slice(-2)
          };
          const tokenbody = this.commonService.createPaymentToken(clientData);
          const reqObj = {
            'url': url,
            'xml': tokenbody
          };
          this.checkOutService.xmlPayment(reqObj).subscribe(
            data => {
              const parseString = require('xml2js').parseString;
              parseString(data['result'], function (err, result) {
                paymentData = result;
              });
              if (paymentData && paymentData.PAYMENTRESPONSE.RESPONSECODE[0] === 'A') {
                this.savePaymentsData(paymentData, obj, clientId);
                this.toastr.info('payment done successfully ,please wait for remaining process', null, { timeOut: 4000 });
              } else {
                this.newMemberModal.hide();
                this.toastr.warning('Error Occured, Invalid Details', null, { timeOut: 4000 });
                this.clearErrorFileds();
                this.delClientMember(clientId);
              }
            },
            error => {
              const status = JSON.parse(error['status']);
              const statuscode = JSON.parse(error['_body']).status;
              switch (status) {
                case 500:
                  break;
                case 400:
                  if (statuscode === '2040') {
                    this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                    window.scrollTo(0, 0);
                  } else if (statuscode === '9998') {
                    this.toastr.error('FirstName, LastName,EmailId allready exists', null, { timeOut: 3000 });
                  } else if (statuscode === '2083') {
                    const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                    this.toastr.error(toastermessage.value, null, { timeOut: 3500 });
                  } else if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                      localStorage.setItem('page', this.router.url);
                      this.router.navigate(['/']).then(() => { });
                    }
                  } break;
              }
            });
        } else {
          this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
        }
      });
  }

  membershipPlanchange(value) {
    if (value) {
      const planChange = value;
      this.amount = planChange.split('$')[1];
      this.charge = this.amount;
    }
  }

  delClientMember(dataObj) {
    if (dataObj && dataObj.length > 0) {
      this.checkOutService.detClientId(dataObj).subscribe(
        data => {
          const delClientId = data['result'];
        });
    }
  }

  savePaymentsData(paymentData, obj, clientId) {
    let approvalCode = '';
    let refCode = '';
    if (paymentData === null) {
      approvalCode = '';
      refCode = '';
    } else {
      approvalCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      refCode = paymentData.PAYMENTRESPONSE.UNIQUEREF[0];
    }
    const paymentObj = {
      'Id': this.refPaymentId,
      'amountToPay': this.charge,
      'merchantAccnt': this.ecommerceData.merchantId,
      'cardHolderName': this.NameOnCard,
      'cardNumber': this.cardNumber,
      'approvalCode': approvalCode,
      'refCode': refCode,
      'token': this.cardTokenId1 ? this.cardTokenId1 : '',
      'clientId': clientId,
    };

    this.checkOutService.addToPaymentsTicket(paymentObj, obj)
      .subscribe(data1 => {
        const dataObj = data1['result'];
        this.newMemberModal.hide();
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
        this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
        this.clearErrorFileds();
        setTimeout(() => {
          // window.location.reload();
        }, 3000);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }

  saveMember() {
    let countryCode: any;
    let mobileNumber: any;
    if (this.countrycode === undefined || this.countrycode === '') {
      countryCode = '';
    } else {
      countryCode = this.countrycode;
    }
    if (this.mobileNumber === undefined || this.mobileNumber === '') {
      mobileNumber = '';
    } else {
      mobileNumber = countryCode + '-' + this.mobileNumber;
    }
    this.saveButon = false;
    const birthday = moment(this.bsValue).format('MM-DD-YYYY');
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (this.paymentTypes) {
      const payAlert = this.paymentTypes.split('$')[1].toLowerCase() === 'cash';
      this.setPayment = payAlert;               // true or false
    }
    if (this.firstName === '' || this.lastName === ''
      || this.memUniId === '' || this.MemberShipType === ''
      || this.paymentTypes === '' || this.nextBillDate === ''
      || this.charge === '' && (this.setPayment === true || this.setPayment === false)
    ) {
      if (this.firstName === '') {
        this.firstNameError = 'please enter first name';
      }
      if (this.lastName === '') {
        this.lastNameError = 'please enter last name';
      }
      if (this.memUniId === '') {
        this.memUniIdError = 'please enter Member Id';
      }
      if (this.MemberShipType === '') {
        this.MemberShipTypeError = 'please select membership type';
      }
      if (this.paymentTypes === '') {
        this.paymentTypesError = 'please select payment type';
      }
      if (this.nextBillDate === '') {
        this.nextBillDateError = 'please select plan';
      }
      if (this.charge === '') {
        this.chargeError = 'please enter amount';
      }
      if (this.setPayment === false) {
        if (this.cardNumber === '') {
          this.cardNumberError = 'Credit Card Processing Error: Invalid Card Number';
        }
        if (this.NameOnCard === '') {
          this.NameOnCardError = 'please enter person name';
        }
        if (this.cvv === '') {
          this.cvvError = 'Invalid Cvv.';
        }
      }
    } else {
      const token = Math.floor((Math.random() * 123456789) + 26);

      const CurrentDate = new Date();
      CurrentDate.setMonth(CurrentDate.getMonth() + (+this.nextBillDate));

      const t = new Date(CurrentDate);
      const membershipPlains = ('00' + (t.getMonth() + 1)).slice(-2) + '-' + ('00' + t.getDate()).slice(-2) + '-' +
        (t.getFullYear() + '').slice(-2) + ':' +
        ('00' + t.getHours()).slice(-2) + ':' +
        ('00' + t.getMinutes()).slice(-2) + ':' +
        ('00' + t.getSeconds()).slice(-2) + ':000';

      const obj = {
        'firstName': this.firstName,
        'lastName': this.lastName,
        'birthday': birthday,
        'mobileNumber': mobileNumber,
        'sms_checkbox': this.sms_checkbox,
        'gender': this.gender,
        'email': this.primaryEmail,
        'memUniId': this.memUniId,
        'MemberShipType': this.MemberShipType,
        'paymentTypes': this.paymentTypes,
        'charge': this.charge,
        'Auto_Bill__c': this.autoBill,
        'zipcode': this.zipCode,
        'plan': this.nextBillDate,
        'Next_Bill_Date__c': moment(membershipPlains, 'MM-DD-YYYY:HH:mm:sss').format('YYYY-MM-DD HH:mm:ss'),
      };
      this.checkOutService.addToClient(obj)
        .subscribe(data1 => {
          const dataObj = data1['result'];
          if (dataObj && dataObj.length > 0) {
            this.newMemberModal.hide();
            if (this.setPayment) {                                         // only cash
              this.saveNonCashpayment(obj, dataObj);
            } else if (!this.setPayment) {
              if (this.autoBill) {
                this.generateToken(obj, dataObj);                              // for token and auto billing
              } else if (!this.autoBill) {
                this.paymentTicket(obj, dataObj);                              // only payment
              }
            }

          } else {
            this.toastr.error('Record inserted, but payment not done', null, { timeOut: 3500 });
          }
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                if (statuscode === '2040') {
                  this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                  window.scrollTo(0, 0);
                } else if (statuscode === '9998') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                  this.toastr.info(toastermessage.value, null, { timeOut: 4000 });
                } else if (statuscode === '2083') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_MEMBERSHIP_ID');
                  this.toastr.error(toastermessage.value, null, { timeOut: 3600 });
                } else if (statuscode === '2085' || statuscode === '2071') {
                  if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                  }
                } break;
            }
          });
    }
  }
  saveNonCashpayment(obj, clientId) {
    const paymentObjs = {
      'Id': this.refPaymentId,
      'amountToPay': this.charge,
      'merchantAccnt': this.ecommerceData.merchantId,
      'cardHolderName': '',
      'cardNumber': '',
      'approvalCode': '',
      'refCode': '',
      'clientId': clientId,
    };
    this.checkOutService.addToPaymentsTicket(paymentObjs, obj)
      .subscribe(data1 => {
        const dataObj = data1['result'];
        this.newMemberModal.hide();
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
        this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
        this.clearErrorFileds();
        setTimeout(() => {
          // window.location.reload();
        }, 3000);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }

  clearErrorMsg() {
    this.firstNameError = '';
    this.lastNameError = '';
    this.primaryEmailError = '';
    this.cardNumberError = '';
    this.NameOnCardError = '';
    this.memUniIdError = '';
    this.MemberShipTypeError = '';
    this.nextBillDateError = '';
    this.chargeError = '';
    this.paymentTypesError = '';
    this.expiryError = '';
    this.cvvError = '';
    this.zipCode = '';
  }
  clearErrorFileds() {
    this.firstName = '';
    this.lastName = '';
    this.NameOnCard = '';
    this.mobileNumber = '';
    this.sms_checkbox = '';
    this.gender = '';
    this.primaryEmail = '';
    this.memUniId = '';
    this.MemberShipType = '';
    this.paymentTypes = '';
    this.charge = '';
    this.cardNumber = '';
    this.cvv = '';
    this.zipCode = '';
    this.nextBillDate = '';
    this.charge = '';
  }

  input() {
    this.table = false;
  }

  generateReport() {
    this.total = 0;
    this.table = true;
    if (this.datedate > this.endDate) {
      this.toastr.error('Begin Date must be before the End Date', null, { timeOut: 3000 });
      this.table = false;
    }
    this.checkOutService.getCheckOutList(this.datedate, this.endDate)
      .subscribe(data => {
        this.checkOutList = data['result'];
        for (let i = 0; i < this.checkOutList.length; i++) {
          this.checkOutList[i].disaplayDate = this.commonService.getUsrDtStrFrmDBStr(this.checkOutList[i].Date);
          this.total += this.checkOutList[i]['balancedue'];

        }
        if (this.apptId) {
          for (let i = 0; i < this.checkOutList.length; i++) {
            this.checkOutList[i]['include'] = false;
            // this.checkOutList[i]['balancedue'] = undefined;
          }
        }

      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

}
