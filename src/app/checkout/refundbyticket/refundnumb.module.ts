import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  RefundNumbComponent } from './refundnumb.component';
import {  RefundNumbRoutingModule } from './refundnumb.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        RefundNumbRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        RefundNumbComponent
    ]
})
export class  RefundNumbModule {
}
