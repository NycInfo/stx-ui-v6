import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RefundNewComponent } from './refundnew.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: RefundNewComponent,
                children: [
                    {
                        path: '',
                        component: RefundNewComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class RefundNewRoutingModule {
}
