import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClient } from '../../common/http-client';

@Injectable()
export class RefundNewService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  /*-- Method to get memberships list --*/
  getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .map(this.extractData);
  }

  /*-- Method to get refund list --*/
  getRefund(refunddata) {
    return this.http.post(this.apiEndPoint + '/api/checkout/refund', refunddata)
      .map(this.extractData);
  }

  getRefundTO(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/refund/' + apptId)
      .map(this.extractData);
  }

  postRefundData(refunddata) {
    return this.http.post(this.apiEndPoint + '/api/checkout/refund/payment/new', refunddata)
      .map(this.extractData);
  }
  postRefundData1(refunddata) {
    return this.http.post(this.apiEndPoint + '/api/checkout/refund/payment1', refunddata)
      .map(this.extractData);
  }
  getCloverDevices() {
    return this.http.get(this.apiEndPoint + '/api/clover/device/list')
      .map(this.extractData);
  }
  getCloverInfo() {
    return this.http.get(this.apiEndPoint + '/api/clover/info')
      .map(this.extractData);
  }
  xmlPayment(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
      .map(this.extractData);
  }
  getWorkerDetails() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
    .map(this.extractData);
  }
  getRefundData(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/refundbyaptid/' + apptId)
    .map(this.extractData);
  }
  getPaymentTypesData() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/paymenttypes')
      .map(this.extractData);
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
