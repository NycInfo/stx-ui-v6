import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { RefundsNoclientService } from './refundnoclient.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Md5 } from 'ts-md5/dist/md5';
import * as config from '../../app.config';
import * as clover from 'remote-pay-cloud';
@Component({
  selector: 'app-refundnoclient',
  templateUrl: './refundnoclient.component.html',
  styleUrls: ['./refundnoclient.component.css'],
  providers: [RefundsNoclientService, CommonService]
})
export class RefundnoclientComponent implements OnInit {
  clientName = 'NO CLIENT';
  refundtype: any = 'Service Refund';
  ElectronicPayment: any;
  Electronicdisabled = false;
  startDate: any;
  date = new Date();
  refundedBy: any;
  endDate: any;
  refundByError: any;
  refundSaveBut = false;
  refundTOData: any;
  dateError: any;
  totaltaxAmt: any;
  totalfixAmt: any;
  refundData: any;
  refundPostData: any;
  local: any;
  taxvalue = 0;
  amountMatcherror: any;
  totalAmt = 0;
  refund = [];
  refundto: any = false;
  searchList = false;
  refundSaveData = [];
  checkedData = [];
  servicelist = false;
  productlist = false;
  electroniclist = false;
  maxdate = new Date();
  datePickerConfig: any;
  cloverDevice = '';
  cloverDeviceList = [];
  cloverConnector: any;
  cloverConnectorListener: any;
  cloverIndex: any;
  cloverInfo: any = [];
  toastermessage: any;
  accountChanrgeId: any;
  accountChanrgeName: any;
  ecommerceData: any;
  @ViewChild('cloverModal') public cloverModal: ModalDirective;
  constructor(private commonService: CommonService, private refundsNoclientService: RefundsNoclientService,
    private router: Router, private toastr: ToastrService, private translateService: TranslateService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    this.endDate = new Date(this.date.getTime());
    this.endDate.setDate(this.date.getDate());
    this.startDate = new Date(this.date.setMonth(this.date.getMonth() - 1));
    const local = JSON.parse(localStorage.getItem('browserObject'));
    if (localStorage.getItem('browserObject') === '' || localStorage.getItem('browserObject') === null) {
      this.local = 'N/A';
    } else {
      this.local = local.CashDrawer.split(' ')[0];
    }
  }
  refundtypeOnChange(value) {
    this.clear();
    this.refundto = false;
    this.refundSaveBut = false;
    this.checkedData = [];
    this.refundTOData = [];
    this.totalfixAmt = 0;
    this.totaltaxAmt = 0;
    this.totalAmt = 0;
    this.taxvalue = 0;
    this.searchList = false;
    const today = new Date();
    const newdate = new Date();
    if (value === 'Payment Overcharge') {
      this.ElectronicPayment = 'Electronic payments may only be refunded within 90 days.';
      this.Electronicdisabled = true;
      this.startDate = new Date(newdate.setDate(today.getDate() - 90));
    } else {
      this.ElectronicPayment = '';
      this.Electronicdisabled = false;
      this.startDate = new Date(newdate.setDate(today.getDate() - 30));
    }
  }
  hide() {
    this.totalfixAmt = 0;
    this.totaltaxAmt = 0;
    this.searchList = false;
    this.refundData = [];
    this.electroniclist = true;
    this.refundTOData = [];
    this.refundto = false;
    this.refundSaveBut = false;
  }
  refundSearch() {
    if (this.refundedBy === '' || this.refundedBy === null || this.refundedBy === undefined || this.refundedBy === 'undefined') {
      this.refundByError = 'CHECK_OUTS.REFUND.REFUND_BY';
      this.hide();
    } else if ((this.Electronicdisabled === false) && (![this.startDate].every(Boolean) || this.startDate === null || isNaN(this.startDate))) {
      this.dateError = 'CHECK_OUTS.REFUND.BEGIN_DATE';
      this.startDate = '';
      this.hide();
    } else if ((this.Electronicdisabled === false) && (![this.endDate].every(Boolean) || this.endDate === null || isNaN(this.endDate))) {
      this.dateError = 'CHECK_OUTS.REFUND.END_DATE';
      this.endDate = '';
      this.hide();
    } else if ((this.Electronicdisabled === false) &&
      (this.startDate !== '' && this.startDate !== null && this.startDate !== undefined && !isNaN(this.startDate)) &&
      (this.endDate !== '' && this.endDate !== null && this.endDate !== undefined && !isNaN(this.endDate)) &&
      (this.startDate > this.endDate)) {
      this.dateError = 'CHECK_OUTS.REFUND.BEGIN_DATE_A_E_D';
      this.hide();
    } else {
      const refunddata = {
        id: 'no client',
        type: this.refundtype,
        refundedBy: this.refundedBy,
        startDate: this.commonService.getDBDatTmStr(this.startDate).split(' ')[0],
        endDate: this.commonService.getDBDatTmStr(this.endDate).split(' ')[0]
      };
      this.refundsNoclientService.getRefund(refunddata)
        .subscribe(data => {
          this.refundData = data['result'];
          for (let i = 0; i < this.refundData.length; i++) {
            if (this.refundData[i].Service_Date_Time__c) {
              this.refundData[i].disaplayDate = this.commonService.getUsrDtStrFrmDBStr(this.refundData[i].Service_Date_Time__c);
            }
            if (this.refundData[i].Appt_Date_Time__c) {
              this.refundData[i].apptTime = this.commonService.getUsrDtStrFrmDBStr(this.refundData[i].Appt_Date_Time__c);
            }
          }
          this.refundData.forEach(element => {
            element.Net_Price__c = element.Net_Price__c ? element.Net_Price__c.toFixed(2) : element.Net_Price__c;
            element.oldAmt = element.Net_Price__c;
            element.newPrice = element.Net_Price__c * element.Qty_Sold__c;
          });
          this.searchList = true;
          if (this.refundtype === 'Service Refund') {
            this.servicelist = true;
            this.productlist = false;
            this.electroniclist = false;
          } else if (this.refundtype === 'Product Refund') {
            this.productlist = true;
            this.servicelist = false;
            this.electroniclist = false;
          } else if (this.refundtype === 'Payment Overcharge') {
            this.electroniclist = true;
            this.productlist = false;
            this.servicelist = false;
          }
        }, error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              window.scrollTo(0, 0);
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });

    }
  }

  calculateRefundAmount(value, item, i) {
    if (value === true) {
      this.taxvalue = 0;
      this.totalAmt = 0;
      this.refundsNoclientService.getRefundTO(item)
        .subscribe(data => {
          this.refund = data['result'][0];
          this.accountChanrgeId = data['result'][1][1]['Id'];
          this.accountChanrgeName = data['result'][1][1]['Name'];
          this.refund.forEach(element => {
            element.OriginalPaymentAmount = element.Amount_Paid__c;
          });
          const Cindex = this.refund.findIndex(el => el.Name === 'Cash');
          const Aindex = this.refund.findIndex(el => el.Name === 'Account Charge');
          if (Cindex === -1) {
            this.refund.push({
              'Id': data['result'][1][1]['Id'],
              'ptId': data['result'][1][1]['Id'],
              'Name': data['result'][1][1]['Name'],
              'Amount_Paid__c': '0.00',
              'OriginalPaymentAmount': '0.00'
            });
          }
          if (Aindex === -1) {
            this.refund.push({
              'Id': data['result'][1][0]['Id'],
              'ptId': data['result'][1][0]['Id'],
              'Ineligible': 'Ineligible',
              'Name': data['result'][1][0]['Name']
            });
          }
          if (this.refund.length > 0) {
            this.refundTOData = this.refund.filter(function (obj) { return obj.Id; });
            this.refundSaveBut = true;
          }
        }, error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              window.scrollTo(0, 0);
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });

      this.refundto = true;
      this.refundData.forEach((element, index) => {
        if (element.apptName === item.apptName) {
          element.seleCheckBox = false;
        } else {
          element.seleCheckBox = true;
        }
        if (element.selectVal === true) {
          element.selectVal = true;
        } else {
          element.selectVal = false;
        }
        if ((element.selectVal === true) && (element.apptName !== item.apptName)) {
          element.selectVal = false;
          this.totalAmt = 0;
          this.taxvalue = 0;
          this.totalfixAmt = 0;
          this.totaltaxAmt = 0;
          this.checkedData = [];
          this.refundSaveData = [];
        }
      });
      if (this.refundtype === 'Service Refund') {
        for (let t = 0; t < this.refundData.length; t++) {
          if (this.refundData[t].Service_Tax__c === null) {
            this.refundData[t].Service_Tax__c = 0;
          }
          if (this.refundData[t].selectVal === true && this.refundData[t].Taxable__c === 1) {
            this.taxvalue += (this.refundData[t].Net_Price__c / this.refundData[t].oldAmt) * this.refundData[t].Service_Tax__c;
          }
          if (this.refundData[t].selectVal === true && this.refundData[t].oldAmt !== 0) {
            let ser_tax = 0;
            if (this.refundData[t].Taxable__c === 1) {
              ser_tax = this.refundData[t].Service_Tax__c;
            } else { ser_tax = 0; }
            this.totalAmt += parseFloat(this.refundData[t].Net_Price__c) + (parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt)) * ser_tax;
          }
        }
      }
      if (this.refundtype === 'Product Refund') {
        for (let t = 0; t < this.refundData.length; t++) {
          if (this.refundData[t].Product_Tax__c === null) {
            this.refundData[t].Product_Tax__c = 0;
          }
          if (this.refundData[t].selectVal === true && this.refundData[t].Taxable__c === 1) {
            this.taxvalue += (this.refundData[t].newPrice / (+this.refundData[t].oldAmt * this.refundData[t].Qty_Sold__c)) * this.refundData[t].Product_Tax__c;
          }
          if (this.refundData[t].selectVal === true && this.refundData[t].oldAmt !== 0) {
            let ser_tax = 0;
            if (this.refundData[t].Taxable__c === 1) {
              ser_tax = this.refundData[t].Product_Tax__c;
            } else { ser_tax = 0; }
            this.totalAmt += (this.refundData[t].newPrice) + (this.refundData[t].newPrice / (+this.refundData[t].oldAmt * this.refundData[t].Qty_Sold__c)) * ser_tax;
          }
        }
      }
      this.totaltaxAmt = this.taxvalue.toFixed(2);
      this.totalfixAmt = this.totalAmt.toFixed(2);

    } else if (value === false) {
      this.amountMatcherror = '';
      if (this.refundtype === 'Service Refund') {
        for (let t = 0; t < this.refundData.length; t++) {
          if (i === t && this.refundData[t].oldAmt !== 0) {
            let ser_tax = 0;
            if (this.refundData[t].Taxable__c === 1) {
              ser_tax = this.refundData[t].Service_Tax__c;
              this.taxvalue -= (this.refundData[t].Net_Price__c / this.refundData[t].oldAmt) * this.refundData[t].Service_Tax__c;
            } else { ser_tax = 0; }
            this.totalAmt -= parseFloat(this.refundData[t].Net_Price__c) + (parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt)) * ser_tax;
          }
        }
      } else if (this.refundtype === 'Product Refund') {
        for (let t = 0; t < this.refundData.length; t++) {
          if (i === t && this.refundData[t].oldAmt !== 0) {
            let ser_tax = 0;
            if (this.refundData[t].Taxable__c === 1) {
              ser_tax = this.refundData[t].Product_Tax__c;
              this.taxvalue -= (this.refundData[t].newPrice / this.refundData[t].oldAmt) * this.refundData[t].Product_Tax__c;
            } else { ser_tax = 0; }
            this.totalAmt -= (this.refundData[t].newPrice * this.refundData[t].Qty_Sold__c) + (this.refundData[t].newPrice / this.refundData[t].oldAmt) * ser_tax;
          }
        }
      }
      this.totaltaxAmt = this.taxvalue.toFixed(2);
      this.totalfixAmt = this.totalAmt.toFixed(2);
      if (this.totaltaxAmt === '-0.00' || this.totaltaxAmt === '0.00') {
        this.totaltaxAmt = 0;
      }
      if (this.totalfixAmt === '-0.00' || this.totalfixAmt === '0.00') {
        this.totalfixAmt = 0;
      }

      const uncheck = this.refundData.filter(function (obj) {
        return obj.selectVal === true;
      });
      if (uncheck.length === 0) {
        this.refundto = false;
        this.refundSaveBut = false;
        this.refundTOData = [];
        this.refundData.forEach((element, index) => {
          element.seleCheckBox = false;
        });
      } else {
        this.refundto = true;
        this.refundData.forEach((element, index) => {
          if (element.apptName === item.apptName) {
            element.seleCheckBox = false;
          } else {
            element.seleCheckBox = true;
          }
        });
      }
    }
  }
  clear() {
    this.refundByError = '';
    this.dateError = '';
    this.amountMatcherror = '';
  }
  save() {
    const cloverRefAry = [];
    let toref = 0;
    this.refundTOData.forEach((element, index) => {
      if (index === 0) {
        toref = element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      } else {
        toref += element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      }
      if (element['Name'] === 'Clover') {
        cloverRefAry.push({
          'payId': element['Approval_Code__c'],
          'refId': element['Reference_Number__c'],
          'amt': element['Amount_Paid__c'] * 100,
          'index': index
        });
        this.cloverIndex = index;
      }
    });
    if (this.refundtype === 'Service Refund' || this.refundtype === 'Product Refund') {
      if (this.totalfixAmt === toref.toFixed(2).toString()) {
        const refId = <HTMLInputElement>document.getElementById('refundId');
        if (cloverRefAry.length > 0 && cloverRefAry[0]['amt'] > 0 && !refId.value) {
          const paymId = <HTMLInputElement>document.getElementById('paymentId');
          paymId.value = cloverRefAry[0]['payId'];
          const ordId = <HTMLInputElement>document.getElementById('orderId');
          ordId.value = cloverRefAry[0]['refId'];
          const amt = <HTMLInputElement>document.getElementById('amtId');
          amt.value = cloverRefAry[0]['amt'];
          this.refundsNoclientService.getCloverDevices().subscribe(data => {
            this.cloverDeviceList = data['result'];
            if (this.cloverDeviceList.length === 0) {
              this.toastr.warning('No Clover Device found to refund', null, { timeOut: 4000 });
            } else if (this.cloverDeviceList.length === 1) {
              this.cloverDevice = this.cloverDeviceList[0]['id'];
              this.connectCloverDevice();
            } else {
              this.cloverDevice = this.cloverDeviceList[0]['id'];
              this.cloverModal.show();
            }
          }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            switch (JSON.parse(error['_body'])['status']) {
              case '2102':
                this.toastermessage = this.translateService.get(JSON.parse(error['_body']).message);
                this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
                break;
              case '2103':
                this.toastermessage = this.translateService.get(JSON.parse(error['_body']).result);
                this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
                break;
              case 500:
                break;
            }
            if (errStatus === '2085' || errStatus === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
        } else {
          this.RefundToSave();
        }
      } else {
        this.amountMatcherror = 'CHECK_OUTS.REFUND.AMOUNT_NOT_MATCH';
      }
    } else {
      this.RefundToSave();
    }
  }
  connectCloverDevice() {
    this.toastr.info('Please wait, connecting to Clover device', null, { timeOut: 5000 });
    const args = [this, config.cloverCfg.remoteAppId,
      clover.BrowserWebSocketImpl.createInstance,
      new clover.ImageUtil(),
      config.cloverCfg.server,
      this.cloverInfo['accessToken'],
      new clover.HttpSupport(XMLHttpRequest),
      this.cloverInfo['merchantId'],
      this.cloverDevice,
      'guestId'];
    const cloverConnectorFactoryConfiguration = {};
    cloverConnectorFactoryConfiguration[clover.CloverConnectorFactoryBuilder.FACTORY_VERSION] = clover.CloverConnectorFactoryBuilder.VERSION_12;
    const cloverConnectorFactory = clover.CloverConnectorFactoryBuilder.createICloverConnectorFactory(cloverConnectorFactoryConfiguration);
    if (this.cloverConnector) {
      this.cloverConnector.dispose();
    }
    this.cloverConnector = cloverConnectorFactory.createICloverConnector(new (Function.prototype.bind.apply(clover.WebSocketCloudCloverDeviceConfiguration, args)));
    this.setCloverConnectorListener(this.cloverConnector);
    this.setDisposalHandler();
    this.cloverConnector.initializeConnection();
    this.cloverModal.hide();
  }
  setCloverConnectorListener(cloverConnector) {
    const CloverConnectorListener = function (connector) {
      const clvObj = new clover.remotepay.ICloverConnectorListener();
      this.cloverConnector = connector;
    };
    CloverConnectorListener.prototype = Object.create(clover.remotepay.ICloverConnectorListener.prototype);
    CloverConnectorListener.prototype.constructor = CloverConnectorListener;
    CloverConnectorListener.prototype.onDeviceConnected = function () {
    };
    CloverConnectorListener.prototype.onDeviceReady = function (merchInfo) {
      const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
      toastEle.value = 'Connectd to device ' + merchInfo.deviceInfo.name + '---' + 'success';
      const evObj = document.createEvent('Events');
      evObj.initEvent('click', true, false);
      toastEle.dispatchEvent(evObj);
      const rpr = new clover.remotepay.RefundPaymentRequest();
      const paymId = <HTMLInputElement>document.getElementById('paymentId');
      const ordId = <HTMLInputElement>document.getElementById('orderId');
      const amt = <HTMLInputElement>document.getElementById('amtId');
      rpr.setPaymentId(paymId.value);
      rpr.setOrderId(ordId.value);
      rpr.setAmount(parseInt(amt.value, 10));
      // rpr.setFullRefund(true);
      cloverConnector.refundPayment(rpr);
    };
    CloverConnectorListener.prototype.onDeviceError = function (deviceErrorEvent) {
      if (deviceErrorEvent.message.indexOf('java.lang.IllegalArgumentException') === -1) {
        const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
        toastEle.value = deviceErrorEvent.getMessage() + '---' + 'error';
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        toastEle.dispatchEvent(evObj);
      }
    };
    CloverConnectorListener.prototype.onDeviceDisconnected = function () {
      const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
      toastEle.value = 'Clover device disconnected' + '---' + 'error';
      const evObj = document.createEvent('Events');
      evObj.initEvent('click', true, false);
      toastEle.dispatchEvent(evObj);
    };
    CloverConnectorListener.prototype.onRefundPaymentResponse = function (onRefundPaymentResponse) {
      if (onRefundPaymentResponse.result === 'SUCCESS') {
        const refId = <HTMLInputElement>document.getElementById('refundId');
        refId.value = onRefundPaymentResponse.refund.id;
        const completeTrs: any = document.getElementById('completeTrsId');
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        completeTrs.dispatchEvent(evObj);
      } else {
        const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
        toastEle.value = 'Failed to refund' + '---' + 'error';
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        toastEle.dispatchEvent(evObj);
      }
    };
    this.cloverConnectorListener = new CloverConnectorListener(this.cloverConnector);
    this.cloverConnector.addCloverConnectorListener(this.cloverConnectorListener);
  }
  setDisposalHandler() {
    window.onbeforeunload = function (event) {
      try {
        this.cloverConnector.dispose();
      } catch (e) {
        // console.error(e);
      }
    }.bind(this);
  }
  RefundToSave() {
    let toref = 0;
    this.checkedData = [];
    this.refundSaveData = [];
    this.refundTOData.forEach((element, index) => {
      if (index === 0) {
        toref = element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      } else {
        toref += element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      }
      if (element.Amount_Paid__c !== 0 && element.Amount_Paid__c !== 0.00 && element.Amount_Paid__c !== null) {
        this.refundSaveData.push({
          'PaymentType': element.Name,
          'AmountToRefund': element.Amount_Paid__c ? element.Amount_Paid__c : 0,
          'OriginalPaymentAmount': element.OriginalPaymentAmount,
          'MerchantAccountName': element.Merchant_Account_Name__c,
          'ReferenceNumber': element.Reference_Number__c === null ? '' : element.Reference_Number__c,
          'ApprovalCode': element.Approval_Code__c === null ? '' : element.Approval_Code__c,
          'giftNumber': element.Gift_Number__c,
          'Id': element.Id,
          'Process_Electronically__c': element.Process_Electronically__c,
          'ptId': element.ptId
        });
      }
    });
    for (let t = 0; t < this.refundData.length; t++) {
      if (this.refundData[t].selectVal === true) {
        if (this.refundtype === 'Product Refund') {
          if (this.refundData[t].deductFromWorker === true) {
            this.refundData[t].deductFromWorker = 1;
          } else if (this.refundData[t].deductFromWorker === false) {
            this.refundData[t].deductFromWorker = 0;
          }
          if (this.refundData[t].returnToInventory === true) {
            this.refundData[t].returnToInventory = 1;
          } else if (this.refundData[t].returnToInventory === false) {
            this.refundData[t].returnToInventory = 0;
          }
          this.checkedData.push({
            'ProductId': this.refundData[t].Product__c, 'WorkerId': this.refundData[t].Worker__c, 'Taxable': this.refundData[t].Taxable__c,
            'deductFromWorker': this.refundData[t].deductFromWorker, 'Amount': this.refundData[t].newPrice, 'Ticket#': this.refundData[t].apptName,
            'Product': this.refundData[t].Name, 'Quantity': this.refundData[t].Qty_Sold__c, 'OriginalAmount': this.refundData[t].Net_Price__c,
            'Product_Tax__c': this.refundData[t].Product_Tax__c === null ? 0 :
              (this.refundData[t].Net_Price__c / (this.refundData[t].oldAmt) * this.refundData[t].Product_Tax__c).toFixed(2),
            'ReturntoInventory': this.refundData[t].returnToInventory, 'Date': this.refundData[t].Service_Date_Time__c, 'id': this.refundData[t].Id
          });
        } else if (this.refundtype === 'Service Refund') {
          if (this.refundData[t].deductFromWorker === true) {
            this.refundData[t].deductFromWorker = 1;
          } else if (this.refundData[t].deductFromWorker === false) {
            this.refundData[t].deductFromWorker = 0;
          }
          this.checkedData.push({
            'ServiceId': this.refundData[t].Service__c, 'WorkerId': this.refundData[t].Worker__c, 'Taxable': this.refundData[t].Taxable__c,
            'deductFromWorker': this.refundData[t].deductFromWorker, 'Amount': this.refundData[t].Net_Price__c,
            'OriginalAmount': this.refundData[t].oldAmt,
            'Service_Tax': this.refundData[t].Service_Tax__c === null ? 0 :
              ((this.refundData[t].Net_Price__c / this.refundData[t].oldAmt) * this.refundData[t].Service_Tax__c).toFixed(2),
            'Date': this.refundData[t].Service_Date_Time__c, 'id': this.refundData[t].Id, 'Service_Group_Color__c': this.refundData[t].Service_Group_Color__c
          });
        } else if (this.refundtype === 'Payment Overcharge') {
          this.checkedData.push({
            'AmountPaid': this.refundData[t].Amount_Paid__c, 'PaymentType': this.refundData[t].Name,
            'Service_Tax': this.refundData[t].Service_Tax__c === null ? 0 : this.refundData[t].Service_Tax__c,
            'Date': this.refundData[t].Appt_Date_Time__c, 'Ticket': this.refundData[t].apptName, 'Status_c': this.refundData[t].Status__c,
            'Payments_c': this.refundData[t].Payments__c, 'Current_Balance_c': this.refundData[t].Current_Balance__c, 'id': this.refundData[t].Id
          });
        }
      }
    }
    const saveRec = {
      clientId: 'no client',
      clientname: this.clientName,
      refundType: this.refundtype,
      totalAmt: this.totalfixAmt,
      selectList: this.checkedData,
      Drawer_Number__c: this.local !== 'N/A' ? this.local : '',
      refundToList: this.refundSaveData.filter(function (obj) { return obj.AmountToRefund && Number(obj.AmountToRefund) !== 0; }),
      Appt_Date_Time__c: this.commonService.getDBDatTmStr(new Date())
    };
    this.amountMatcherror = '';
    let refundCardOnFile = saveRec.refundToList.filter((obj) => obj.Process_Electronically__c == 1);
    if (refundCardOnFile.length > 0) {
      for (let i = 0; i < refundCardOnFile.length; i++) {
        this.checkRefund(refundCardOnFile[i], saveRec);
      }
    } else {
      this.savePaymentsData(null, saveRec);
    }
  }
  checkRefund(obj, saveRec) {
    const d = new Date();
    let paymentDatas;
    const url = config.ANYWHERECOMMERCE_PAYMENT_API;

    const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
      (d.getFullYear() + '').slice(-2) + ':' +
      ('00' + d.getHours()).slice(-2) + ':' +
      ('00' + d.getMinutes()).slice(-2) + ':' +
      ('00' + d.getSeconds()).slice(-2) + ':000';

    const refNumber = obj['ReferenceNumber'];
    const refAmount = +obj['AmountToRefund'].toFixed(2);
    this.commonService.ecommerceDetails().subscribe(data => {
      this.ecommerceData = JSON.parse(data.result[0].JSON__c);
      const hashs = Md5.hashStr(this.ecommerceData.merchantId + refNumber + refAmount
        + dateTime + this.ecommerceData.accessToken);
      const paymentObject = {
        uniqueref: refNumber,
        terminalid: this.ecommerceData.merchantId,
        amountRefund: refAmount,
        dateTime: dateTime,
        hash: hashs,
        clientName: this.clientName,
        refund: 'Refund'
      };
      const tokenBody = this.commonService.refundPayment(paymentObject);
      const reqObj = {
        'url': url,
        'xml': tokenBody,
        'saveRec': saveRec,
        'reference': paymentObject.uniqueref
      };
      if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
        this.refundsNoclientService.xmlPayment(reqObj).subscribe(
          data1 => {
            const parseString = require('xml2js').parseString;
            parseString(data1['result'], function (err, result) {
              paymentDatas = result;
            });
            if (paymentDatas.ERROR && paymentDatas.ERROR.ERRORSTRING[0] === 'Invalid UNIQUEREF field') {
              // saveRec.refundToList.forEach(element => {
              //   if (element.ReferenceNumber === reqObj.reference) {
              //     element.ptId = this.accountChanrgeId;
              //     element.Id = this.accountChanrgeId;
              //     element.PaymentType = this.accountChanrgeName;
              //     element.Process_Electronically__c = 0;
              //   }
              // });
              this.toastr.warning('Payment was failed please select another payment mode', null, { timeOut: 5000 });
              // this.savePaymentsData(paymentDatas, saveRec);
            } else if (paymentDatas.REFUNDRESPONSE && paymentDatas.REFUNDRESPONSE.RESPONSECODE[0] === 'A') {
              this.savePaymentsData(paymentDatas, saveRec);
              this.toastr.success('Your payment was successfully completed', null, { timeOut: 3000 });
              localStorage.removeItem('client_token');
            } else {
              this.toastermessage = this.translateService.get('Payment Failed');
              this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
            }
          });
      } else {
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      }
    });
  }
  savePaymentsData(paymentDatas, saveRec) {
    if (paymentDatas) {
      saveRec.refundToList[0]['ReferenceNumber'] = paymentDatas.REFUNDRESPONSE.UNIQUEREF[0];
    }
    this.refundsNoclientService.postRefundData(saveRec)
      .subscribe(data => {
        this.refundPostData = data;
        this.router.navigate(['/checkout']);
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
      );
  }
  displayToast() {
    let toastMsg: any = <HTMLInputElement>document.getElementById('toastMsgId');
    toastMsg = toastMsg.value.split('---');
    if (toastMsg[1] === 'error') {
      this.toastr.error(toastMsg[0], null, { timeOut: 4000 });
    } else if (toastMsg[1] === 'info') {
      this.toastr.info(toastMsg[0], null, { timeOut: 2000 });
    } else {
      this.toastr.success(toastMsg[0], null, { timeOut: 1000 });
    }
  }

  cloverRefund() {
    this.toastr.success('Completed refund through clover', null, { timeOut: 4000 });
    const refId = <HTMLInputElement>document.getElementById('refundId');
    this.refundTOData[this.cloverIndex]['Reference_Number__c'] = refId.value;
    this.RefundToSave();
  }
}
