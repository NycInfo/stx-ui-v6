import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import * as moment from 'moment/moment';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ClientappointmentsService } from './clientappointments.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as config from '../app.config';
import { parse } from 'querystring';
import { Conditional } from '@angular/compiler';
import { CommonService } from '../common/common.service';
import { isNullOrUndefined } from 'util';
import { NewClientService } from '../../app/clients/newclient/newclient.service';
import { WaitingListService } from '../../app/appointments/waitinglist/waitinglist.service';
@Component({
    selector: 'app-home-popup',
    templateUrl: './clientappointments.html',
    styleUrls: ['./clientappointments.component.css'],
    providers: [ClientappointmentsService, NewClientService, CommonService, WaitingListService]
})
export class ClientappointmentsComponent implements OnInit {
    minDate: Date;
    public bsValue: any = new Date();
    visitTypesList: any;
    visitTypeValue: any = '';
    selectedServiceGroup: any;
    addServiceGroupName: any;
    serviceGroupList: any = [];
    packagesList: any;
    serviceDetailsList = [];
    rows = [];
    serviceId: any;
    serviceName: any;
    serviceDuration1: any;
    serviceDuration2: any;
    serviceDuration3: any;
    sumOfServiceDurations: any = 0;
    serviceGroupName: any;
    workerList = [];
    worker: any;
    apptSearchData: any;
    book = [];
    bookingDataList: any;
    bookingIntervalMinutes: any = 0;
    bookingIntervalMinutes1: any;
    maximumofAvailabilities: any;
    maximumofAvailabilities1 = 0;
    IntervalMinutes: any;
    workerName: any;
    calendarList: any;
    dt: any;
    dt1: any;
    booking: any;
    booking1: any;
    day: any;
    workerTimes: any;
    startTimeHour: any = [];
    startTimeMins: any = [];
    endTimeHour: any = [];
    endTimeMins: any = [];
    daleteArray: any = [];
    times = [];
    finalTimes = [];
    finalDate = [];
    error8: any;
    error9: any = '';
    error10: any;
    textBoldlist: any;
    selectedIndex = -1;
    apptDate: any;
    apptNotes: any;
    showScheduleButton = false;
    reScheduleButton = false;
    showWaitinglist = false;
    clientId: any;
    appointmentId: any;
    finalDate1: any;
    type: any;
    date1: any;
    error: any;
    apptmentId: any;
    serviceGroupColour: any;
    apptData: any = [];
    cliData: any = [];
    datePickerConfig: any;
    isRebooking: number;
    bookApptErr: any = '';
    clientData: any;
    showCliName: any;
    serviceDetailKeys = ['Duration_1__c', 'Duration_2__c', 'Duration_3__c',
        'Buffer_After__c', 'Guest_Charge__c', 'Net_Price__c'];
    serviceTax: any;
    firstName = '';
    lastName = '';
    cliMobile = '';
    cliEmail = '';
    cliSms_Consent__c = 0;
    waitingId = '';
    bookingType = '';
    durDate: any = [];
    isNewClient: any = false; /* it is not using in add to waiting list method */
    styleOnClickRating: any = 'fa fa-star checked';
    Arr = Array;
    getWorker = [];
    bookingExpress: any = [];
    waitingListWorkerName: any = [];
    clearResourceErr: any;
    serviceDurations: any;
    prefService: 0;
    servicePrice = 0;
    inputs = [];
    listingName: any;
    clientNotes: '';
    isPreferredDuration: any;
    preferredDur: any = 0;
    searchBtn = false;
    apptId: any;
    @ViewChild('serviceNotesModal') public serviceNotesModal: ModalDirective;
    constructor(private clientappointmentsService: ClientappointmentsService,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private translateService: TranslateService,
        private router: Router,
        private newClientService: NewClientService,
        private commonService: CommonService,
        private waitingListService: WaitingListService,
        private sanitizer: DomSanitizer) {
        this.minDate = new Date();
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
        this.route.paramMap.subscribe((params) => {
            this.clientId = params.get('clientId');
            this.appointmentId = params.get('appointmentId');
        });
        this.route.queryParamMap.subscribe(params => {
            if (params.has('bookingType')) {
                if (params.get('bookingType') === 'rebook') {
                    this.isRebooking = 1;
                } else if (params.get('bookingType') === 'waitingList') {
                    this.bookingType = params.get('bookingType');
                    this.waitingId = this.appointmentId;
                    this.appointmentId = '';
                } else if (params.get('bookingType') === 'cancelAppointment') {
                    this.bookingType = params.get('bookingType');
                    this.apptId = this.appointmentId;
                }
            } else {
                this.isRebooking = 0;
            }
        });
    }

    ngOnInit() {
        this.searchBtn = false;
        this.getClientData(this.clientId);
        this.listVisitTypes();
        // this.getServiceGroupsList();

        // this.addServices(0);
        this.getAllActivePackages();
        if (this.appointmentId) {
            this.getApptServiceDetails(this.clientId, this.appointmentId);
        } else {
            this.getServiceGroups(this.appointmentId);
        }
        this.getServRetTaxs();
        this.getClientAppointmemts(this.clientId);
        this.getWorkerList();
        // this.addInput();
    }
    /**
* Method to get service tax  and retail tax calculation
*/
    getServRetTaxs() {
        this.clientappointmentsService.getServProdTax().subscribe(
            data => {
                console.log
                const taxData = JSON.parse(data['result'][2].JSON__c);
                this.serviceTax = taxData.serviceTax;
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /**
     * Common methods starts
     */

    updateBookedRecords() {
        if (this.serviceGroupList.length > 0 && this.inputs.length > 0) {
            if (this.inputs && this.inputs.length > 0) {
                for (let i = 0; i < this.inputs.length; i++) {
                    this.inputs[i]['IsPackage'] = 0;
                    this.inputs[i]['Booked_Package__c'] = '';
                    if (this.isRebooking === 1) {
                        this.inputs[i]['Rebooked__c'] = this.isRebooking;
                    }
                    this.serviceDetailsList[i] = this.inputs[i].servList;
                    this.workerList[i] = this.inputs[i].workerList;
                    this.assaignServiceDurations(this.workerList[i], this.inputs[i].workerName, i);
                    this.inputs[i]['serviceName'] = this.inputs[i]['Id'];
                    const pckgId = this.inputs[i]['pckgId'];
                    if (!isNullOrUndefined(pckgId) && pckgId !== '') {
                        this.inputs[i]['serviceGroup'] = pckgId;
                        this.inputs[i]['IsPackage'] = 1;
                        this.inputs[i]['Booked_Package__c'] = pckgId.split(':')[1];
                    } else {
                        const serviceGroup = this.inputs[i]['serviceGroupName'];
                        this.serviceGroupList.filter((service) => service.serviceGroupName === serviceGroup).map((service) => {
                            this.inputs[i]['serviceGroup'] = service.serviceGroupName + '$' + service.serviceGroupColor;
                        });
                    }
                    this.calculateServiceDurations(i);
                }
            }
        }
    }
    getApptServiceDetails(clientId, apptId) {
        const reqDate = this.commonService.getDBDatStr(new Date());
        this.clientappointmentsService.getApptServices(clientId, apptId, reqDate).subscribe(data => {
            const resData = data['result'];
            const rebookLists = resData.srvcresult;

            const service = [];
            this.inputs = [];
            for (let i = 0; i < rebookLists.length; i++) {
                const tempObj = {};
                tempObj['StartDay'] = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
                tempObj['fullWorker'] = rebookLists[i]['workerName'] + '$' + rebookLists[i]['name'];
                tempObj['service'] = {};
                tempObj['service']['Book_Every__c'] = rebookLists[i]['Book_Every__c'];
                tempObj['service']['Buffer_after__c'] = rebookLists[i]['Buffer_After__c'];
                tempObj['service']['Description__c'] = 'desc';
                tempObj['service']['Duration_1__c'] = rebookLists[i]['Duration_1__c'];
                tempObj['service']['Duration_2__c'] = rebookLists[i]['Duration_2__c'];
                tempObj['service']['Duration_3__c'] = rebookLists[i]['Duration_3__c'];
                tempObj['service']['Guest_Charge__c'] = rebookLists[i]['Guest_Charge__c'];
                tempObj['service']['Name'] = rebookLists[i]['Name'];
                tempObj['service']['Price__c'] = rebookLists[i]['Net_Price__c'];
                tempObj['service']['Service_Group__c'] = rebookLists[i]['serviceGroupName'];
                tempObj['service']['Taxable__c'] = rebookLists[i]['Taxable__c'];
                tempObj['service']['avaiable1'] = rebookLists[i]['Duration_1_Available_for_Other_Work__c'];
                tempObj['service']['avaiable2'] = rebookLists[i]['Duration_2_Available_for_Other_Work__c'];
                tempObj['service']['avaiable3'] = rebookLists[i]['Duration_3_Available_for_Other_Work__c'];
                tempObj['service']['color'] = rebookLists[i]['serviceGroupColour'];
                tempObj['service']['dursergrp'] = rebookLists[i]['Duration__c'];
                tempObj['service']['pcsergrp'] = rebookLists[i]['Net_Price__c'];
                tempObj['service']['resourceName'] = rebookLists[i]['Resources__c'];
                tempObj['service']['serviceId'] = rebookLists[i]['Id'];
                tempObj['service']['serviceTax'] = rebookLists[i]['Service_Tax__c'];
                tempObj['service']['sumDurationBuffer'] = rebookLists[i]['Duration__c'];
                tempObj['serviceId'] = rebookLists[i]['Id'];
                tempObj['worker'] = rebookLists[i]['workerName'];
                tempObj['workerName'] = rebookLists[i]['name'];
                tempObj['wrkrName'] = rebookLists[i]['name'];
                // tempObj['service']['Levels__c'] = rebookLists[i][''];
                // tempObj['skipRecalculate'] = rebookLists[i][''];
                // tempObj['service']['slots'] = rebookLists[i][''];
                // tempObj['service']['priority'] = rebookLists[i][''];
                // tempObj['service']['filters'] = rebookLists[i][''];
                // tempObj['service']['Service_Level__c'] = rebookLists[i][''];
                this.inputs.push(tempObj);
                this.expressService(tempObj['fullWorker'], i, false);
            }
            /* below visit type is dispalyed when reebook time */
            const appData = resData.apptrst[0];
            if (appData.Client_Type__c && appData.Client_Type__c !== 'null' && appData.Client_Type__c !== 'undefined') {
                this.visitTypeValue = appData.Client_Type__c;
            } else {
                this.visitTypeValue = '';
            }

            // for (let i = 0; i < rebookLists.length; i++) {
            //     this.inputs = [];

            //     this.inputs['serviceId'] = rebookLists[i]['Id'];
            //     this.inputs['fullWorker'] = rebookLists[i]['workerName'] + '$' + rebookLists[i]['name'];
            //     this.inputs['StartDay'] = rebookLists[i]['Service_Date_Time__c'].split(' ')[0];
            //     // this.inputs['sumDurationBuffer'] = rebookLists[i]['Duration__c'];
            //     service.push({
            //         'worker': rebookLists[i]['workerName'],
            //         'workerName': rebookLists[i]['name'],
            //         'wrkrName': rebookLists[i]['name'],
            //         'serviceId': rebookLists[i]['Id']
            //     });
            //     this.inputs.push(service);

            // }

            // const serviceList = [];
            // const workerList = [];
            // 30-7-2018
            // this.serviceGroupList = resData.srvgResult;
            // this.serviceGroupName = this.serviceGroupList.length > 0 ? this.serviceGroupList[0].serviceGroupName + '$' + this.serviceGroupList[0].serviceGroupColor : undefined;
            // 30-7-2018
            // this.updateBookedRecords();
            // for (let j = 0; j < serviceList.length; j++) {
            //     // this.serviceDetailsList[j] = serviceList[j];
            //     // this.workerList[j] = workerList[j];
            //     this.rows[j]['Name'] = this.rows[j]['Id'] + this.rows[j]['Duration_1__c'] + this.rows[j]['Duration_2__c'] + this.rows[j]['Duration_3__c']
            //         + this.rows[j]['Buffer_After__c'] + this.rows[j]['Guest_Charge__c'];
            //     this.rows[j]['serviceGroupName'] = resData.srvcresult[j]['serviceGroupName'] + resData.srvcresult[j]['serviceGroupColour'];
            // }

            if (this.inputs.length <= 0) {
                // this.addServices(0);
            } else {
                // const appData = resData.apptrst[0];
                // this.bsValue = new Date(appData.Appt_Date_Time__c);
                // if (appData.Client_Type__c && appData.Client_Type__c !== 'null' && appData.Client_Type__c !== 'undefined') {
                //     this.visitTypeValue = appData.Client_Type__c;
                // } else {
                //     this.visitTypeValue = '';
                // }
                // if (this.isRebooking === 1) {
                //     this.apptNotes = '';
                // } else {
                //     this.apptNotes = appData.Notes__c === 'null' || appData.Notes__c === 'undefined' ? null : appData.Notes__c;
                // }
                // this.sumOfServiceDurations = appData.Duration__c;

            }
        },
            error => {
                // this.rows = [{}];
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getClientData(clientId) {
        this.clientappointmentsService.getClient(clientId)
            .subscribe(data => {
                this.clientNotes = data['result'].results[0].Notes__c;
                this.clientData = data['result'].results[0];
                this.showCliName = data['result'].results[0].FirstName + ' ' + data['result'].results[0].LastName;
                this.firstName = data['result'].results[0].FirstName;
                this.lastName = data['result'].results[0].LastName;
                this.cliMobile = data['result'].results[0].MobilePhone;
                this.cliEmail = data['result'].results[0].Email;
                this.cliSms_Consent__c = data['result'].results[0].Sms_Consent__c;
                // document.getElementById('displayNameId').innerHTML = 'Appointments: Booking - ' + this.clientData.FirstName + ' ' + this.clientData.LastName;
                // document.getElementById('displayNameId').innerHTML = '- ' + this.clientData.FirstName + ' ' + this.clientData.LastName;
                if (!this.appointmentId) {
                    if (this.clientData.Booking_Restriction_Type__c === 'Do Not Book' || this.clientData.Booking_Restriction_Type__c === 'Alert Only') {
                        this.serviceNotesModal.show();
                    }
                }

            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    listVisitTypes() {
        this.clientappointmentsService.getVisitTypes().subscribe(
            data => {
                this.visitTypesList = data['result'];
                // this.visitTypeValue = this.visitTypesList[0].visitType;
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }
    onVisitTypeChange(value) {
        if (value !== '') {
            this.visitTypeValue = value;
        }

    }
    bookAnyWay() {
        this.serviceNotesModal.hide();
    }
    getServiceGroups(apptId) {
        const reqDate = this.commonService.getDBDatStr(this.bsValue);
        this.clientappointmentsService.getServiceGroups('Service', reqDate).subscribe(data => {
            this.serviceGroupList = [];
            this.serviceGroupList = data['result']
                .filter(filterList => filterList.active && !filterList.isSystem);
            this.serviceGroupName = this.serviceGroupList[0].serviceGroupName + '$' + this.serviceGroupList[0].serviceGroupColor;
            if (!apptId) {
                // added to sync waiting list data
                if (this.waitingId) {
                    this.getFiltData();
                } else {
                    this.addServices(0);
                    this.categoryOfService(this.serviceGroupName, 0);
                }
                // added to sync waiting list data
            } else {
                this.updateBookedRecords();
            }

        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    categoryOfService(value, i) {
        this.finalTimes = [];
        if (value.indexOf('scale') === 0) {
            this.type = 'Package';
        } else {
            this.type = 'ApptService';
        }
        const serviceGroupName = value.split('$')[0];

        // this.rows[i].serviceGroupColour = value.split('$')[1];
        this.serviceDetailsList[i] = [];
        // if (this.workerList[i]) {
        //     delete this.workerList[i];
        // }
        this.workerList[i] = [];
        this.rows[i].Id = '';
        this.rows[i].serviceName = '';
        this.rows[i].workerName = '';
        this.removeServiceDetails(i);
        this.calculateServiceDurations(i);
        this.clientappointmentsService.getServices(serviceGroupName, this.type, this.commonService.getDBDatStr(this.bsValue)).subscribe(data => {
            if (this.type === 'Package') {
                const services: Array<any> = data['result']['serviceresultJson'];
                const serviceRelatedWorkers: Array<any> = data['result']['results'];
                const DupserviceGroupName = serviceGroupName;
                const packageId = serviceGroupName.split(':')[1];
                if (this.serviceDetailsList[i]) {
                    this.serviceDetailsList.splice(i, 1);
                }
                if (this.workerList[i]) {
                    this.workerList.splice(i, 1);
                }
                let updateRow;
                if (this.rows[i]) {
                    if (this.rows[i].tsId) {
                        updateRow = this.rows[i];
                    }
                    this.rows.splice(i, 1);
                }
                const length = this.rows.length;
                if (services) {
                    services.filter((service, index) => {
                        this.rows.push({ Id: '', serviceGroup: DupserviceGroupName, Rebooked__c: 0, apptId: '' });
                        this.rows[length + 0] = updateRow ? updateRow : this.rows[length + 0];
                        this.serviceDetailsList[length + index] = services;
                        const workers = [];
                        serviceRelatedWorkers.filter((worker) => {

                            if (worker.sId === service.Id) {
                                workers.push(worker);
                            }
                        });
                        this.workerList[length + index] = workers;
                        this.rows[length + index]['IsPackage'] = 1;
                        this.rows[length + index]['Booked_Package__c'] = packageId;
                        this.rows[length + index]['serviceGroup'] = this.rows[length]['serviceGroup'];
                        this.rows[length + index]['wName'] = workers.length > 0 ? workers[0]['name'] : ''; /*for waiting list*/
                        this.rows[length + index]['workerName'] = workers.length > 0 ? workers[0].workername : '';
                        this.rows[length + index]['serviceName'] = service.Id;
                        this.rows[length + index]['Id'] = service.Id;
                        this.rows[length + index]['serviceGroupColour'] = service.serviceGroupColor;
                        // this.serviceDetailKeys.map((key) => {
                        //     this.rows[length + index][key] = workers[0][key] ? +workers[0][key] : 0;
                        // });
                        if (this.rows[length + index]['workerName']) {
                            Object.assign(this.rows[length + index], this.commonService.getServiceDurations(workers[0], this.clientId, this.cliData, this.rows[length + index]));
                        }
                        // this.rows[length + index].serviceGroupColour = '';
                        this.calculateServiceDurations(length + index);
                    });
                }
                if (this.rows) {
                    this.rows = this.rows.filter((obj) => obj.workerName !== '');
                }
            } else {
                this.serviceDetailsList[i] = data['result']['result'];
                this.rows[i]['IsPackage'] = 0;
                this.rows[i]['Booked_Package__c'] = '';
                this.rows[i].serviceGroupColour = value.split('$')[1];
            }

        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }

    categoryOfService2(value, i) {
        this.finalTimes = [];
        if (value.indexOf('scale') === 0) {
            this.type = 'Package';
        } else {
            this.type = 'ApptService';
        }
        const tempAry = value.split('$');
        const serviceGroupName = tempAry[0];
        if (tempAry[1]) {
            this.rows[i].serviceGroupColour = value.split('$')[1];
        }
        this.serviceDetailsList[i] = [];
        // if (this.workerList[i]) {
        //     delete this.workerList[i];
        // }
        this.workerList[i] = [];
        this.rows[i].Id = '';
        this.rows[i].serviceName = '';
        this.rows[i].workerName = '';
        this.removeServiceDetails(i);
        this.calculateServiceDurations(i);
        this.clientappointmentsService.getServices(serviceGroupName, this.type, this.commonService.getDBDatStr(this.bsValue)).subscribe(
            data => {
                if (data['result']['type'] === 'Package') {
                    this.rows[i]['IsPackage'] = 1;
                    this.rows[i]['serviceGroup'] = value.split('$')[0];
                    const services: Array<any> = data['result']['serviceresultJson'];
                    this.serviceDetailsList[i] = services;
                } else {
                    this.serviceDetailsList[i] = data['result']['result'];
                    this.rows[i]['IsPackage'] = 0;
                    this.rows[i]['Booked_Package__c'] = '';
                    this.rows[i].serviceGroupColour = value.split('$')[1];
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }

    // calculateServiceDurations(i) {
    //     if (this.rows && this.rows.length > 0) {
    //         this.sumOfServiceDurations = 0;
    //         for (let j = 0; j < this.rows.length; j++) {
    //             let totalDuration = 0;
    //             if (this.rows[j]['Levels__c'] !== undefined) {
    //                 const temp1 = JSON.parse(this.rows[j]['Levels__c']);
    //                 const temp = temp1.filter((obj) => obj.levelNumber === this.rows[j]['Service_Level__c']);
    //                 if (temp.length > 0) {
    //                     for (let k = 0; k < temp.length; k++) {
    //                         if (temp[k]['levelNumber'] === this.rows[j]['Service_Level__c']) {
    //                             if (!isNullOrUndefined(this.rows[j]['workerName']) && this.rows[j]['workerName'] !== '') {
    //                                 totalDuration += +this.rows[j]['Duration_1__c'];
    //                                 totalDuration += +this.rows[j]['Duration_2__c'];
    //                                 totalDuration += +this.rows[j]['Duration_3__c'];
    //                                 totalDuration += +this.rows[j]['Buffer_After__c'];
    //                                 this.rows[j].Duration__c = totalDuration;
    //                                 this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
    //                             }
    //                         } else {
    //                             if (!isNullOrUndefined(this.rows[j]['workerName']) && this.rows[j]['workerName'] !== '') {
    //                                 totalDuration += +temp[k]['Duration_1__c'];
    //                                 totalDuration += +temp[k]['Duration_2__c'];
    //                                 totalDuration += +temp[k]['Duration_3__c'];
    //                                 totalDuration += +temp[k]['Buffer_After__c'];
    //                                 this.rows[j].Duration__c = totalDuration;
    //                                 this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
    //                             }
    //                         }
    //                     }
    //                 } else {
    //                     if (!isNullOrUndefined(this.rows[j]['workerName']) && this.rows[j]['workerName'] !== '') {
    //                         totalDuration += +this.rows[j]['Duration_1__c'];
    //                         totalDuration += +this.rows[j]['Duration_2__c'];
    //                         totalDuration += +this.rows[j]['Duration_3__c'];
    //                         totalDuration += +this.rows[j]['Buffer_After__c'];
    //                         this.rows[j].Duration__c = totalDuration;
    //                         this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
    calculateServiceDurations(i) {
        if (this.rows && this.rows.length > 0) {
            this.sumOfServiceDurations = 0;
            for (let j = 0; j < this.rows.length; j++) {
                let totalDuration = 0;
                if (!isNullOrUndefined(this.rows[j]['workerName']) && this.rows[j]['workerName'] !== '') {
                    totalDuration += +this.rows[j]['Duration_1__c'];
                    totalDuration += +this.rows[j]['Duration_2__c'];
                    totalDuration += +this.rows[j]['Duration_3__c'];
                    totalDuration += +this.rows[j]['Buffer_After__c'];
                    this.rows[j].Duration__c = totalDuration;
                    this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
                }

            }
        }
    }
    getWorkersFromDate() {
        this.clear();
        const serviceIds = [];
        const selectedIds = [];
        this.rows.filter((data) => {
            if (data['Id'] !== '' || !isNullOrUndefined(data['Id'])) {
                serviceIds.push(data['Id']);
                selectedIds.push(data['Id']);
            } else {
                serviceIds.push(data['']);
            }
        });
        if (selectedIds.length > 0) {
            const bookingdata = {
                bookingdate: this.commonService.getDBDatStr(this.bsValue),
                serviceIds: selectedIds
            };
            this.clientappointmentsService.getUsers(bookingdata).subscribe(data => {
                const workerservices = data['result'];
                serviceIds.forEach((id, i) => {
                    if (id !== '' && !isNullOrUndefined(id)) {
                        this.workerList[i] = workerservices.filter((worker) => worker.sId === id);
                        const isExsists = this.workerList[i].findIndex((worker) => worker.workername === this.rows[i]['workerName']) !== -1 ? true : false;
                        if (!isExsists) {
                            this.rows[i]['workerName'] = this.workerList[i].length > 0 ? this.workerList[i][0]['workername'] : '';
                            this.assaignServiceDurations(this.workerList[i], this.rows[i]['workerName'], i);
                            this.calculateServiceDurations(i);
                        }
                    }
                });
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }

    }


    // Method to chane worker

    workerListOnChange(value, i) {
        this.finalTimes = [];
        this.workerName = value;
        if (this.bookingType !== 'waitingList') {
            this.inputs[i]['wName'] = this.workerList[i].filter(obj => obj['workername'] === value)[0]['name'];
        }
        this.assaignServiceDurations(this.workerList[i], this.workerName, i);
        this.calculateServiceDurations(i);
        // this.clientdata = JSON.parse(localStorage.getItem('bookstanding'));
    }

    assaignServiceDurations(workers: Array<any>, workerId: string, index: number) {
        const selectedWorker = workers.filter((worker) => worker.workername === workerId).map((worker) => {
            Object.assign(this.rows[index], this.commonService.getServiceDurations(worker, this.clientId, this.cliData, this.rows[index]));
        });
        if (selectedWorker.length === 0) {
            const serviceDetails = this.rows[index].serviceName.split('$');
            if (this.bookingType !== 'waitingList') {
                this.rows[index]['Duration_1__c'] = +serviceDetails[1];
                this.rows[index]['Duration_2__c'] = +serviceDetails[2];
                this.rows[index]['Duration_3__c'] = +serviceDetails[3];
                this.rows[index]['Buffer_After__c'] = +serviceDetails[4];
                this.rows[index]['Guest_Charge__c'] = +serviceDetails[5];
                this.rows[index]['Net_Price__c'] = +serviceDetails[6];
                this.rows[index]['Duration_1_Available_for_Other_Work__c'] = this.rows[index]['sDuration1Available'];
                this.rows[index]['Duration_2_Available_for_Other_Work__c'] = this.rows[index]['sDuration2Available'];
                this.rows[index]['Duration_3_Available_for_Other_Work__c'] = this.rows[index]['sDuration3Available'];
                this.workerList[index].push({ workername: workerId, name: '(' + this.rows[index].name + ')' });
            } else {
                for (let i = 0; i < this.durDate.length; i++) {
                    if (!this.rows[i]) {
                        this.rows[i] = {};
                    }
                    if (workerId === this.durDate[i]['worker']) {
                        this.rows[index]['Duration_1__c'] = +this.durDate[i]['service']['Duration_1__c'];
                        this.rows[index]['Duration_2__c'] = +this.durDate[i]['service']['Duration_2__c'];
                        this.rows[index]['Duration_3__c'] = +this.durDate[i]['service']['Duration_3__c'];
                        this.rows[index]['Buffer_After__c'] = +this.durDate[i]['service']['Buffer_after__c'];
                        this.rows[index]['Guest_Charge__c'] = +0;
                        this.rows[index]['Net_Price__c'] = +this.durDate[i]['service']['Price__c'];
                        this.rows[index]['Duration_1_Available_for_Other_Work__c'] = this.rows[index]['serviceAvail1'];
                        this.rows[index]['Duration_2_Available_for_Other_Work__c'] = this.rows[index]['serviceAvail2'];
                        this.rows[index]['Duration_3_Available_for_Other_Work__c'] = this.rows[index]['serviceAvail3'];
                        this.workerList[index].push({ workername: workerId, name: '(' + this.rows[index].wName + ')' });
                    }
                }
            }
        }
    }

    removeServiceDetails(index) {
        this.serviceDetailKeys.map((key) => {
            delete this.rows[index][key];
        });
    }
    clearErrMsg() {
        this.error8 = '';
        this.error9 = '';
        this.error10 = '';
        this.finalTimes = [];
        this.bookApptErr = '';
    }
    getBookingData() {

        // for (let i = 0; i < this.rows.length; i++) {
        //     if (this.rows[i].serviceName === undefined || this.rows[i].serviceName === 'undefined'
        //         || this.rows[i].serviceName === '') {
        //         this.error9 = 'A service must be selected for each line(s)';
        //     }
        // }
        if (this.error9 !== '') {
            this.error9 = 'A service must be selected for each line(s)';
        } else {
            this.clientappointmentsService.getBookingData().subscribe(
                data => {
                    this.bookingDataList = data['result'];
                    this.bookingIntervalMinutes = this.bookingDataList.bookingIntervalMinutes;
                    this.maximumofAvailabilities = this.bookingDataList.maximumAvailableToShow;
                    this.IntervalMinutes = this.bookingDataList.bookingIntervalMinutes;
                    this.activeMemberCalendar();
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    activeMemberCalendar() {

        let serviceIds = '';
        let workerIds = '';
        for (let i = 0; i < this.rows.length; i++) {
            workerIds += '\'' + this.rows[i].workerName + '\',';
            serviceIds += '\'' + this.rows[i].Id + '\',';
        }
        workerIds = '(' + workerIds.slice(0, -1) + ')';
        serviceIds = '(' + serviceIds.slice(0, -1) + ')';
        const dataObj = {
            'serviceIds': serviceIds,
            'workerIds': workerIds,
            'clientId': this.clientId,
            'searchDay': moment(this.bsValue).format('dddd')
        };
        this.clientappointmentsService.fetchingActiveMembers(dataObj).subscribe(
            data => {
                this.workerTimes = data['result'];
                for (let i = 0; i < this.workerTimes.length; i++) {
                    if (this.workerTimes[i].starttime.split(' ')[1] === 'AM') {
                        this.startTimeHour[i] = this.workerTimes[i].starttime.split(' ')[0].split(':')[0];
                        this.startTimeMins[i] = this.workerTimes[i].starttime.split(' ')[0].split(':')[1];
                    } else if (this.workerTimes[i].starttime.split(' ')[1] === 'PM') {
                        this.startTimeHour[i] = this.workerTimes[i].starttime.split(' ')[0].split(':')[0];
                        this.startTimeHour[i] = parseInt(this.startTimeHour, 10) + 12;
                        this.startTimeMins[i] = this.workerTimes[i].starttime.split(' ')[0].split(':')[1];
                    }
                    if (this.workerTimes[i].endtime.split(' ')[1] === 'AM') {
                        this.endTimeHour[i] = this.workerTimes[i].endtime.split(' ')[0].split(':')[0];
                        this.endTimeMins[i] = this.workerTimes[i].endtime.split(' ')[0].split(':')[1];
                    } else if (this.workerTimes[i].endtime.split(' ')[1] === 'PM') {
                        this.endTimeHour[i] = this.workerTimes[i].endtime.split(' ')[0].split(':')[0];
                        if (parseInt(this.endTimeHour[i], 10) === 12) {
                            this.endTimeHour[i] = parseInt(this.endTimeHour[i], 10);
                        } else {
                            this.endTimeHour[i] = parseInt(this.endTimeHour[i], 10) + 12;
                        }
                        this.endTimeMins[i] = this.workerTimes[i].endtime.split(' ')[0].split(':')[1];
                    }
                }
                this.method();
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    addInput() {
        this.inputs.push({});
        if (this.getWorker.length > 0) {
            this.expressService(this.getWorker[0].workerId + '$' + this.getWorker[0].workerName, this.inputs.length - 1, true);
        }
    }

    getWorkerList() {
        this.apptSearchData = [];
        const CalendatDate = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
        this.clientappointmentsService.getWorkerLists(CalendatDate).subscribe(
            data => {
                this.getWorker = data['result'];
                if (this.isRebooking !== 1 && !this.bookingType) {
                    this.addInput();
                }
                const dat1 = this.getWorker[0];
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }

    expressService(value, i, param) {
        this.searchBtn = false;
        this.apptSearchData = [];
        if (value) {
            let workerId;
            let workerName;
            workerId = value.split('$')[0];
            workerName = value.split('$')[1];
            const CalendatDate = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
            if (!this.inputs[i]) {
                this.inputs[i] = {};
            }
            this.clientappointmentsService.expressBookingServices(workerId, CalendatDate).subscribe(
                data => {
                    this.bookingExpress[i] = data['result']['serviceList'];

                    if (this.bookingExpress[i][0] && param) {
                        for (let k = 0; k < this.getWorker.length; k++) {
                            if (this.getWorker[k]['workerId'] === workerId) {
                                this.inputs[i]['StartDay'] = this.getWorker[k]['StartDay'];
                                this.inputs[i]['wrkrName'] = this.getWorker[k]['wrkrName']; // this is dependency with waiting list worker name in search results don't make change //
                            }
                        }
                        if (this.isRebooking !== 1) {
                            this.inputs[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
                        } else {
                            this.inputs[i]['service'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0] : '';
                        }
                        this.inputs[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
                        this.inputs[i]['worker'] = workerId;
                        this.inputs[i]['workerName'] = workerName;
                        this.inputs[i]['fullWorker'] = workerId + '$' + workerName;
                    }
                    if (this.bookingExpress[i]) {
                        this.listServices(this.inputs[i]['serviceId'], i);
                    }

                    if (!this.clientId) {
                        this.isPreferredDuration = 0;
                        this.preferredDur = 0;
                    } else {
                        if (this.cliData) {
                            this.isPreferredDuration = 0;
                            this.preferredDur = 0;
                            this.isPreferredDuration = this.getPreferredDuration(this.cliData, this.bookingExpress[i], this.inputs[i]);

                            if (this.isPreferredDuration.length > 0) {
                                this.preferredDur = 0;
                                for (let j = 0; j < this.bookingExpress[i].length; j++) {
                                    if (this.isPreferredDuration[0].Service__c === this.bookingExpress[i][j].serviceId) {
                                        this.bookingExpress[i][j]['Duration_1__c'] = this.isPreferredDuration[0].Duration_1__c;
                                        this.bookingExpress[i][j]['Duration_2__c'] = this.isPreferredDuration[0].Duration_2__c;
                                        this.bookingExpress[i][j]['Duration_3__c'] = this.isPreferredDuration[0].Duration_3__c;
                                        this.bookingExpress[i][j]['Buffer_after__c'] = this.isPreferredDuration[0].Buffer_after__c;
                                        this.bookingExpress[i][j]['sumDurationBuffer'] = this.isPreferredDuration[0].Duration__c;
                                        this.bookingExpress[i][j]['PrefDur'] = this.preferredDur;
                                        this.prefService = this.isPreferredDuration[0].Duration__c;
                                    }
                                }
                            }
                        }
                        this.calculateServiceDurations1();
                    }
                },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    getPreferredDuration(cliData, servicelist, inputs) {
        for (let i = 0; i < this.cliData.length; i++) {
            for (let t = 0; t < servicelist.length; t++) {
                if ((servicelist[t].serviceId === this.cliData[i].Service__c) && (inputs.worker === this.cliData[i].Worker__c)) {
                    if (this.cliData[i].PrefDur === 1) {
                        const obj = [];
                        obj.push({
                            'Service__c': cliData[i].Service__c,
                            'Duration__c': cliData[i].Duration__c,
                            'Duration_1__c': cliData[i].Duration_1__c,
                            'Duration_2__c': cliData[i].Duration_2__c,
                            'Duration_3__c': cliData[i].Duration_3__c,
                            'Buffer_after__c': cliData[i].Buffer_After__c
                        });
                        return obj;
                    }
                }
            }
        }
        return 0;
    }

    // expressService2(value, i) {
    //     if (value) {
    //         const CalendatDate = moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD');
    //         this.clientappointmentsService.expressBookingServices(value, CalendatDate).subscribe(
    //             data => {
    //                 this.waitingListWorkerName = data['result']['workerNamess'];
    //                 this.bookingExpress[i] = data['result']['serviceList'];
    //                 const appointmentList = data['result']['appList'];
    //                 if (this.bookingExpress[i][0]) {
    //                     this.inputs[i]['service'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0] : {};
    //                     this.inputs[i]['serviceId'] = this.bookingExpress[i].length > 0 ? this.bookingExpress[i][0]['serviceId'] : '';
    //                     this.inputs[i]['worker'] = value;
    //                     this.listServices(this.inputs[i]['serviceId'], i);
    //                 }
    //             },
    //             error => {
    //                 const errStatus = JSON.parse(error['_body'])['status'];
    //                 if (errStatus === '2085' || errStatus === '2071') {
    //                     if (this.router.url !== '/') {
    //                         localStorage.setItem('page', this.router.url);
    //                         this.router.navigate(['/']).then(() => { });
    //                     }
    //                 }
    //             });
    //     }
    // }

    deleteFieldValue(index) {
        this.inputs.splice(index, 1);
        this.calculateServiceDurations1();
        this.apptSearchData = [];
        this.searchBtn = true;
    }

    listServices(value, i) {
        this.apptSearchData = [];
        const obj = this.bookingExpress[i].filter((data) => data.serviceId === value);
        if (obj.length > 0) {
            this.inputs[i]['service'] = obj[0];
            this.inputs[i]['serviceId'] = obj[0]['serviceId'];
            this.inputs[i]['skipRecalculate'] = 'false';
            this.searchBtn = true;
            this.calculateServiceDurations1();
        } else {
            this.bookingExpress[i].push({
                Name: '(' + this.inputs[i]['service']['Name'] + ')',
                serviceId: this.inputs[i]['serviceId'],
            });
            this.inputs[i]['service']['Service_Tax__c'] = this.inputs[i]['service']['serviceTax'];
            this.calculateServiceDurations1();
            this.searchBtn = true;
        }
    }

    calculateServiceDurations1() {
        this.servicePrice = 0;
        this.serviceDurations = 0;
        if (this.inputs && this.inputs.length > 0) {
            for (let j = 0; j < this.inputs.length; j++) {
                const serviceVal = this.inputs[j].service;
                if (this.inputs[j].service) {
                    if (serviceVal.sumDurationBuffer !== null || serviceVal.sumDurationBuffer !== '' || serviceVal.sumDurationBuffer !== 0) {
                        // this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer) ? parseFloat(serviceVal.sumDurationBuffer) : parseFloat(serviceVal.dursergrp);
                        this.serviceDurations += parseFloat(serviceVal.sumDurationBuffer);
                    } else {
                        this.serviceDurations += parseFloat(serviceVal.dursergrp) ? parseFloat(serviceVal.dursergrp) : 0;
                    }
                }
            }
        }
    }

    searchForAppointment() {
        this.selectedIndex = undefined;
        const workerIds = [];
        const durations = [];
        const bookEvery = [];
        const resources = [];
        const resFilter = [];
        this.listingName = [];
        // let tempSrcDate = this.bsValue;
        for (let i = 0; i < this.inputs.length; i++) {
            this.listingName.push({
                'name': this.inputs[i]['wrkrName'],
                'serviceName': this.inputs[i]['service']['Name']
            });
            workerIds.push(this.inputs[i].worker);
            if (this.inputs[i]['service']['sumDurationBuffer'] === null || this.inputs[i]['service']['sumDurationBuffer'] === '' || this.inputs[i]['service']['sumDurationBuffer'] === 0) {
                durations.push(this.inputs[i]['service']['dursergrp']);
            } else {
                durations.push(this.inputs[i]['service']['sumDurationBuffer']);
            }
            bookEvery.push(this.inputs[i]['service']['Book_Every__c']);
            if (this.inputs[i]['service']['resourceName']) {
                resources.push(this.inputs[i]['service']['resourceName'].replace(/#1/g, ''));
            }
            resFilter.push(this.inputs[i]['service']['filters']);
        }
        const dataObj = {
            'date': moment(this.bsValue, 'ddd MMM DD YYYY HH:mm:ss').format('YYYY-MM-DD'),
            'id': workerIds,
            'dateformat': 'MM/DD/YYYY hh:mm:ss a',
            'durations': durations,
            'mindate': this.commonService.getDBDatTmStr(new Date()),
            'bookEvery': bookEvery,
            'resources': resources,
            'resFilter': resFilter
        };
        const tempSrvData = [];
        this.inputs.forEach(obj => {
            tempSrvData.push({
                'duration1': obj['service']['Duration_1__c'],
                'duration2': obj['service']['Duration_2__c'],
                'duration3': obj['service']['Duration_3__c'],
                'bufferafter': obj['service']['Buffer_after__c'],
                'available1': obj['service']['avaiable1'],
                'available2': obj['service']['avaiable2'],
                'available3': obj['service']['avaiable3']
            });
        });
        dataObj['serviceData'] = tempSrvData;
        this.clientappointmentsService.searchForAppts(dataObj).subscribe(data => {
            this.apptSearchData = data['result'];
        },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                } else if (errStatus === 2107) {
                    this.toastr.error('No slots available for this day')
                }
            });
    }

    //   checkForServices() and checkForServiceObject() are written to check whether the services added contains
    //   service name ,service worker and (package  or service) if not it returns true to show validation
    //   written by Ravi Kanth
    checkForServices(services: Array<any>, property1, property2, property3): boolean {
        const properties = [property1, property2, property3];
        if (properties.map((property) => this.checkForServiceObject(services, property)).indexOf(false) !== -1) {
            return true;
        }
        return false;
    }

    clear() {
        this.apptSearchData = [];
        this.showScheduleButton = false;
        this.reScheduleButton = false;
        this.showWaitinglist = false;
    }

    checkForServiceObject(services: Array<any>, propertyName: string): boolean {
        const isProperty = services.map((obj) => obj.hasOwnProperty(propertyName)).indexOf(false) !== -1 ? false : true;
        if (isProperty) {
            if (services.filter((obj) => obj[propertyName] === '').length !== 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    // searchForAppointment() {
    //     this.finalTimes = [];
    //     this.finalDate = [];
    //     // const clientdata = JSON.parse(localStorage.getItem('clientData'));
    //     const dataObj = {
    //         'date': this.bsValue,
    //         // 'worker': this.rows[0].workerName,
    //         // 'visitType': this.visitTypeValue,
    //         // 'clientId': this.clientId,
    //         'workerData': this.rows
    //     };
    //     this.clientappointmentsService.searchAppt(dataObj).subscribe(data => {
    //         this.apptSearchData = data['result'];
    //         this.getBookingData();
    //     },
    //         error => {
    //             const status = JSON.parse(error['status']);
    //             const statuscode = JSON.parse(error['_body']).status;
    //             switch (JSON.parse(error['_body']).status) {
    //                 case '2033':
    //                     break;
    //             }
    //         });
    // }
    method() {
        for (let j = 0; j < this.startTimeMins.length; j++) {
            for (let i = 0; i < this.maximumofAvailabilities; i++) {
                let startSlot;
                let endSlot;
                if (this.apptSearchData.length > 0) {
                    startSlot = new Date(this.apptSearchData[j].Appt_Date_Time__c);
                    endSlot = new Date(startSlot.getTime() + this.apptSearchData[j].Duration__c * 60000);
                }
                const year = this.bsValue.getFullYear();
                const month = this.bsValue.getMonth();
                const day = this.bsValue.getDate();
                const date = [];
                const date1 = [];
                date[i] = new Date(year, month, day, this.startTimeHour[j], (parseInt(this.startTimeMins[j], 10) + (i * parseInt(this.bookingIntervalMinutes, 10))), 0, 0);
                date1[i] = new Date(year, month, day, this.endTimeHour[j], parseInt(this.endTimeMins[j], 10), 0, 0);
                const finalDate = moment(date[i]).format('MM/DD/YYYY hh:mm A');
                this.finalDate1 = moment(date1[i]).format('MM/DD/YYYY hh:mm A');
                if (startSlot && endSlot) {
                    if ((date[i] <= date1[i]) && this.maximumofAvailabilities >= this.finalTimes.length + 1) {
                        if (!((date[i] >= startSlot && date[i] <= endSlot) || (date1[i] >= startSlot && date1[i] <= endSlot))) {
                            this.finalTimes.push(finalDate);
                            this.finalDate.push(date[i]);
                        }
                    }
                } else if (this.maximumofAvailabilities >= this.finalTimes.length + 1) {
                    this.finalTimes.push(finalDate);
                    this.finalDate.push(date[i]);
                }
            }
        }
        const currentDate = new Date();
        if (currentDate > this.date1) {
            this.finalTimes = [];
            this.finalDate = [];
            this.error10 = 'The Company Hours for this record is in past';
        }
    }
    scheduleButtonShow(searchData, i) {
        this.apptDate = searchData;
        this.showScheduleButton = true;
        if (this.clientId !== '' && this.appointmentId) {
            this.showScheduleButton = false;
            this.reScheduleButton = true;
        } else {
            this.showScheduleButton = true;
            this.reScheduleButton = false;
        }
        this.textBoldlist = true;
        this.selectedIndex = i;
    }
    getAllActivePackages() {
        const currentDate = this.commonService.getDBDatStr(new Date()).split(' ')[0];
        this.clientappointmentsService.getAllActivePackages(currentDate)
            .subscribe(data => {
                this.packagesList = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    bookAppointment() {
        if (this.inputs[0].apptId === undefined) {
            this.apptmentId = '';
        } else {
            this.apptmentId = this.inputs[0].apptId;
        }
        const appointmentDate = this.commonService.getDBDatTmStr2(this.apptDate, 'MM/DD/YYYY hh:mm:ss a');
        // const clientdata = JSON.parse(localStorage.getItem('clientData'));
        const IsPackage = this.inputs.filter((obj) => obj['IsPackage'] === 1).length > 0 ? true : false;
        const serviceTaxResult = this.commonService.calculateServiceTax(+this.serviceTax, this.inputs, IsPackage);
        const servicesData = [];
        const servicesDatas = [];

        let modifyData: any;
        modifyData = this.apptCalculateServiceTax(this.inputs);
        for (let i = 0; i < this.inputs.length; i++) {
            servicesData.push(this.inputs[i]);
        }
        for (let t = 0; t < servicesData.length; t++) {
            const tempObj = {};

            // if (servicesData[t]['service']['workerDuration1'] !== 0 ||
            //     servicesData[t]['service']['workerDuration2'] !== 0 ||
            //     servicesData[t]['service']['workerDuration3'] !== 0 ||
            //     servicesData[t]['service']['workerBuffer'] !== 0) {
            //     tempObj['Buffer_After__c'] = servicesData[t]['service']['workerBuffer'];
            //     tempObj['Duration_1__c'] = servicesData[t]['service']['workerDuration1'];
            //     tempObj['Duration_2__c'] = servicesData[t]['service']['workerDuration2'];
            //     tempObj['Duration_3__c'] = servicesData[t]['service']['workerDuration3'];
            //     tempObj['Duration__c'] = servicesData[t]['service']['sumDurationBuffer'];
            // } else {
            tempObj['Buffer_After__c'] = servicesData[t]['service']['Buffer_after__c'];
            tempObj['Duration_1__c'] = servicesData[t]['service']['Duration_1__c'];
            tempObj['Duration_2__c'] = servicesData[t]['service']['Duration_2__c'];
            tempObj['Duration_3__c'] = servicesData[t]['service']['Duration_3__c'];
            //tempObj['Duration__c'] = servicesData[t]['service']['sumDurationBuffer'] ? servicesData[t]['service']['sumDurationBuffer'] : servicesData[t]['service']['dursergrp'];
            tempObj['Duration__c'] = servicesData[t]['service']['sumDurationBuffer']
            // }

            tempObj['Duration_1_Available_for_Other_Work__c'] = servicesData[t]['service']['avaiable1'];
            tempObj['Duration_2_Available_for_Other_Work__c'] = servicesData[t]['service']['avaiable2'];
            tempObj['Duration_3_Available_for_Other_Work__c'] = servicesData[t]['service']['avaiable3'];
            tempObj['PrefDur'] = this.preferredDur;
            tempObj['Rebooked__c'] = this.isRebooking;
            tempObj['Name'] = servicesData[t]['service']['Name'];
            tempObj['Booked_Package__c'] = '';
            tempObj['wName'] = '';
            tempObj['Guest_Charge__c'] = servicesData[t]['service']['Guest_Charge__c'];
            tempObj['Id'] = servicesData[t]['service']['serviceId'];
            tempObj['Levels__c'] = servicesData[t]['service']['Levels__c'];
            tempObj['IsPackage'] = servicesData[t]['IsPackage'];
            tempObj['workerName'] = servicesData[t]['worker'];
            tempObj['Book_Every__c'] = servicesData[t]['service']['Book_Every__c'];

            tempObj['Net_Price__c'] = servicesData[t]['service']['Price__c'] ? servicesData[t]['service']['Price__c'] : servicesData[t]['service']['pcsergrp'];
            tempObj['Resources__c'] = servicesData[t]['service']['resourceName'] ? servicesData[t]['service']['resourceName'].replace(/#1/g, '') : null;
            tempObj['Service_Level__c'] = servicesData[t]['service']['Service_Level__c'];
            tempObj['Service_Tax__c'] = servicesData[t]['service']['Service_Tax__c'] ? servicesData[t]['service']['Service_Tax__c'] : 0;
            tempObj['Taxable__c'] = servicesData[t]['service']['Taxable__c'];
            tempObj['bookEvery'] = servicesData[t]['service']['Book_Every__c'];
            tempObj['desc'] = 'desc';
            tempObj['serviceGroup'] = servicesData[t]['service']['Service_Group__c'] + '$' + servicesData[t]['service']['color'];
            tempObj['serviceGroupColour'] = servicesData[t]['service']['color'];
            tempObj['serviceName'] = servicesData[t]['serviceId'];
            servicesDatas.push(tempObj);
        }
        const appointBookingData = {
            // 'Worker__c': this.rows[0].workerName,
            //   'Rebooked__c': this.isRebooking,
            'Client_Type__c': this.visitTypeValue,
            'Client__c': this.clientId,
            'Duration__c': this.serviceDurations,
            'Appt_Date_Time__c': appointmentDate,
            'apptCreatedDate': this.commonService.getDBDatTmStr(new Date()),
            'Notes__c': this.apptNotes ? this.apptNotes : null,
            'apptId': this.isRebooking ? '' : this.apptmentId,
            'bookingType': 'findappt',
            'Service_Tax__c': modifyData.serviceTax,
            'Service_Sales__c': modifyData.sales,
            'servicesData': servicesDatas,
            'serviceGroupColour': this.serviceGroupColour,
            'daleteArray': this.daleteArray,
            'IsPackage': IsPackage ? 1 : 0,
        };
        if (this.waitingId) {
            appointBookingData['waitingId'] = this.waitingId;
        }
        this.clientappointmentsService.appointmentBooking(appointBookingData).subscribe(data => {
            const apptId = data['result']['apptId'];
            this.router.navigate(['/appointments'], { queryParams: { date: appointmentDate.split(' ')[0] } }).then(() => {
                const toastermessage: any = this.translateService.get('LOGIN.APPT_SUCESSFULLY_CREATED');
                this.toastr.success(toastermessage.value, null, { timeOut: 2000 });
            });
            this.clientappointmentsService.sendApptNotifs([apptId]).subscribe(data2 => { }, error => { });
        },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                } else if (statuscode === '2091') {
                    const bookingError = JSON.parse(error['_body']).message;
                    // Warning Don't Delete This alert Code//
                    alert(bookingError);
                }
            });
    }


    addServices(i) {
        this.finalTimes = [];
        this.rows.push({
            serviceGroup: this.serviceGroupName, Name: '', Net_Price__c: 0,
            Id: '', serviceGroupColour: '', serviceName: '', IsPackage: 0, Booked_Package__c: '', Rebooked__c: 0
        });
        const index = this.rows.length - 1;
        this.workerList[i] = [];
        this.serviceDetailsList[i] = [];
        if (index !== 0) {
            this.categoryOfService(this.serviceGroupName, index);
        }

    }
    removeServices(row, index) {
        if (this.rows[index].tsId) {
            this.rows[index]['delete'] = true;
        }
        // this.rows[index].push(this.rows[index]);
        this.daleteArray.push(this.rows[index]);
        this.rows.splice(index, 1);
        this.workerList.splice(index, 1);
        this.serviceDetailsList.splice(index, 1);
        this.calculateServiceDurations(index);
        this.finalTimes = [];
    }
    // getTodayDate() {
    //     this.bsValue = new Date();
    // }
    // calculationForOneWeek() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 7));
    // }
    // calculationForTwoWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 14));
    // }
    // calculationForThreeWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 21));
    // }
    // calculationForFourWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 28));
    // }
    // calculationForFiveWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 35));
    // }
    // calculationForSixWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 42));
    // }
    // calculationForSevenWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 49));
    // }
    // calculationForEightWeeks() {
    //     this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 56));
    // }
    updateDatepicker(wkNum: number) {
        this.clear();
        if (wkNum === 0) {
            this.bsValue = new Date();
        } else {
            this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 7 * wkNum));
        }
    }

    getClientAppointmemts(id) {
        const client = {
            'clientId': id,
            'apptViewValue': 'All'
        };
        this.newClientService.getClientAppointmentsData(client).subscribe(
            data => {
                this.cliData = data['result'].AppointmenServices.filter(filterList => filterList.PrefDur === 1);
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    addToWaitingList(id) {
        const IsPackage = this.inputs.filter((obj) => obj['IsPackage'] === 1).length > 0 ? true : false;
        const serviceTaxResult = this.commonService.calculateServiceTax(+this.serviceTax, this.inputs, IsPackage);
        // const servicesData = [];
        // const tempsrvAry = [];
        // const servicesDatas = [];
        // const modifyData = this.apptCalculateServiceTax(this.inputs);
        // for (let i = 0; i < this.inputs.length; i++) {
        //     servicesData.push(this.inputs[i]);
        // }
        // for (let i = 0; i < servicesData.length; i++) {
        //     const tempObj = {};
        //     tempObj['service'] = {};
        //     tempObj['service']['pcsergrp'] = servicesData[i]['service']['pcsergrp'];
        //     tempObj['service']['Name'] = servicesData[i]['service']['Name'];
        //     tempObj['service']['priority'] = null; /**doubt to ask */
        //     tempObj['service']['filters'] = servicesData[i]['service']['filters'];
        //     tempObj['service']['slots'] = servicesData[i]['service']['priority'];
        //     tempObj['service']['dursergrp'] = servicesData[i]['service']['dursergrp'];
        //     tempObj['service']['Service_Group__c'] = servicesData[i]['service']['Service_Group__c'];
        //     tempObj['service']['serviceId'] = servicesData[i]['service']['serviceId'];
        //     tempObj['service']['resourceName'] = servicesData[i]['service']['resourceName'].replace(/#1/g, '');
        //     tempObj['service']['Taxable__c'] = servicesData[i]['service']['Taxable__c'];
        //     tempObj['service']['Price__c'] = servicesData[i]['service']['Price__c'];
        //     tempObj['service']['workerBuffer'] = servicesData[i]['service']['workerBuffer'];
        //     tempObj['service']['serviceBuffer'] = 0;
        //     tempObj['service']['workerDuration1'] = 0;
        //     tempObj['service']['serviceDuration1'] = servicesData[i]['service']['serviceDuration1'];
        //     tempObj['service']['workerDuration2'] = 0;
        //     tempObj['service']['serviceDuration2'] = servicesData[i]['service']['serviceDuration2'];
        //     tempObj['service']['workerDuration3'] = 0;
        //     tempObj['service']['serviceDuration3'] = servicesData[i]['service']['serviceDuration3'];
        //     tempObj['service']['workerAvail1'] = 0;
        //     tempObj['service']['serviceAvail1'] = servicesData[i]['service']['serviceAvail1'];
        //     tempObj['service']['workerAvail2'] = 0;
        //     tempObj['service']['serviceAvail2'] = servicesData[i]['service']['serviceAvail2'];
        //     tempObj['service']['workerAvail3'] = 0;
        //     tempObj['service']['serviceAvail3'] = servicesData[i]['service']['serviceAvail3'];
        //     tempObj['service']['color'] = servicesData[i]['service']['color'];
        //     tempObj['service']['Buffer_after__c'] = servicesData[i]['service']['Buffer_After__c'];
        //     tempObj['service']['Duration_1__c'] = servicesData[i]['service']['Duration_1__c'];
        //     tempObj['service']['Duration_2__c'] = servicesData[i]['service']['Duration_2__c'];
        //     tempObj['service']['Duration_3__c'] = servicesData[i]['service']['Duration_3__c'];
        //     tempObj['service']['avaiable1'] = servicesData[i]['service']['avaiable1'];
        //     tempObj['service']['avaiable2'] = servicesData[i]['service']['avaiable2'];
        //     tempObj['service']['avaiable3'] = servicesData[i]['service']['avaiable3'];
        //     tempObj['service']['sumDurationBuffer'] = servicesData[i]['service']['sumDurationBuffer'];
        //     tempObj['service']['Description__c'] = servicesData[i]['service']['Description__c'];
        //     tempObj['serviceId'] = servicesData[i]['serviceId'];
        //     tempObj['worker'] = servicesData[i]['worker'];
        //     tempObj['workerName'] = this.waitingListWorkerName;
        //     tempObj['fullWorker'] = servicesData[i]['worker'] + '$' + this.waitingListWorkerName;
        //     tempObj['skipRecalculate'] = false;
        //     tempsrvAry.push(tempObj);
        // }

        const dataObj = {
            'clientFstName': this.firstName,
            'clientLstName': this.lastName,
            'clientMobile': this.cliMobile,
            'clientEmail': this.cliEmail,
            'startDate': this.commonService.getDBDatTmStr(this.bsValue),
            'endDate': this.commonService.getDBDatTmStr(new Date(this.bsValue.setMonth(this.bsValue.getMonth() + 1))),
            'services': this.inputs,
            'Sms_Consent__c': this.cliSms_Consent__c,
            'servicePrice': this.servicePrice,
            'serviceDuration': this.serviceDurations,
            'notes': this.apptNotes ? this.apptNotes : '',
            'isNewClient': this.isNewClient,
            'IsPackage': '',
        };
        if (this.clientId) {
            dataObj['clientId'] = this.clientId;
        } else {
            dataObj['clientId'] = '';
        }
        if (id) {
            dataObj['waitingListId'] = id;
        } else {
            dataObj['waitingListId'] = '';
        }
        const wtDate = dataObj['startDate'].split(' ')[0]; /* wtDate is used to pass to waiting list */

        this.waitingListService.saveData(dataObj).subscribe(
            data => {
                const dataa = data['result'];
                if (dataa) {
                    this.toastr.success('Record saved Successfully', null, { timeOut: 1500 });
                    this.router.navigate(['/appointment/waitinglist'], { queryParams: { date: wtDate } }).then(() => { });
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                const statuscode = JSON.parse(error['_body']).status;
                switch (statuscode) {
                    case '2033':
                        this.toastr.error('Record with the same name already exists', null, { timeOut: 2500 });
                        break;
                }
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    getFiltData() {
        this.inputs = [];
        this.waitingListService.editWaitListingData(this.waitingId).subscribe(
            data => {
                const tempList = data['result'][0];
                this.clientId = tempList['Client__c'];
                this.firstName = tempList['FirstName'];
                this.lastName = tempList['LastName'];
                const date1 = new Date(tempList['Earliest_Time_c']);
                if (this.bsValue.setHours(0, 0, 0, 0) < date1.setHours(0, 0, 0, 0)) {
                    this.bsValue = this.commonService.getDateFrmDBDateStr(tempList['Earliest_Time_c']);
                }
                if (tempList['Note__c']) {
                    this.apptNotes = tempList['Note__c'];
                } else {
                    this.apptNotes = '';
                }
                const srvWor = JSON.parse(tempList['Services_and_Workers__c']);
                this.durDate = JSON.parse(tempList['Services_and_Workers__c']);

                const jsonServWor = JSON.parse(tempList['Services_and_Workers__c']);
                for (let i = 0; i < jsonServWor.length; i++) {
                    const sasd = this.getWorker.filter((obj) => obj.workerId === jsonServWor[i]['fullWorker'].split('$')[0]);
                    if (sasd && sasd.length === 0) {
                        this.getWorker.push({
                            'names': '(' + jsonServWor[i]['fullWorker'].split('$')[1] + ')',
                            'workerId': jsonServWor[i]['fullWorker'].split('$')[0],
                            'workerName': jsonServWor[i]['fullWorker'].split('$')[1],
                            'wrkrName': jsonServWor[i]['wrkrName']
                        });
                    }
                    this.inputs.push({
                        'service': jsonServWor[i]['service'],
                        'serviceId': jsonServWor[i]['serviceId'],
                        'worker': jsonServWor[i]['worker'],
                        'workerName': jsonServWor[i]['workerName'],
                        'fullWorker': jsonServWor[i]['fullWorker'],
                        'StartDay': jsonServWor[i]['StartDay'],
                        'wrkrName': jsonServWor[i]['wrkrName']
                    });
                    this.expressService(jsonServWor[i]['fullWorker'], i, false);
                }

                // for (let i = 0; i < srvWor.length; i++) {
                //     if (!this.inputs[i]) {
                //         this.inputs[i] = {};
                //     }
                //     // this.rows[i]['serviceGroup'] = srvWor[i]['service']['Service_Group__c'] + '$' + srvWor[i]['service']['color'];
                //     // this.rows[i]['serviceGroupColour'] = srvWor[i]['service']['color'];
                //     const wkId2 = srvWor[i]['worker'];
                //     // this.expressService(wkId2, 0);

                //     this.inputs[i]['serviceId'] = srvWor[i]['serviceId'];

                //     this.listServices(this.inputs[i]['serviceId'], i);

                //     // this.categoryOfService2(srvWor[i]['service']['Service_Group__c'] + '$' + srvWor[i]['service']['color'], i);
                //     // this.rows[i]['Id'] = srvWor[i]['serviceId'];
                //     // this.rows[i]['desc'] = srvWor[i]['service']['Description__c'];
                //     // const bookingdata = {
                //     //     bookingdate: this.bsValue,
                //     //     serviceIds: [this.inputs[i].serviceName]
                //     // };
                //     // this.clientappointmentsService.getUsers(bookingdata).subscribe(
                //     //     data2 => {
                //     //         this.workerList[i] = data2['result'];
                //     //         if (data2['result'] && data2['result'].length > 0) {
                //     //             this.rows[i].workerName = srvWor[i]['worker'];
                //     //             this.rows[i].wName = srvWor[i]['workerName']; /* for waitinglist */
                //     //             this.workerListOnChange(this.rows[i].workerName, i);
                //     //         }
                //     //     },
                //     //     error => {
                //     //         const status = JSON.parse(error['status']);
                //     //         const statuscode = JSON.parse(error['_body']).status;
                //     //         switch (JSON.parse(error['_body']).status) {
                //     //             case '2033':
                //     //                 break;
                //     //         }
                //     //         if (statuscode === '2085' || statuscode === '2071') {
                //     //             if (this.router.url !== '/') {
                //     //                 localStorage.setItem('page', this.router.url);
                //     //                 this.router.navigate(['/']).then(() => { });
                //     //             }
                //     //         }
                //     //     }
                //     // );
                // }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                const statuscode = JSON.parse(error['_body']).status;
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }

    // calculate servcie tax for appointments
    apptCalculateServiceTax(apptBookingData: Array<any>): { bookingData: Array<any>, serviceTax: number, sales: number } {
        let totalTax = 0;
        let totalPrice = 0;
        const data = apptBookingData.map((bookedData) => {
            bookedData.service['Service_Tax__c'] = 0;
            if (!isNullOrUndefined(bookedData.service['serviceTax'])) {
                const serviceTax = +JSON.parse(bookedData.service['serviceTax']).serviceTax;
                bookedData.service['Service_Tax__c'] = this.commonService.calculatePercentage(serviceTax, bookedData.service['Price__c'], 1);
            }
            totalPrice += +bookedData.service['Price__c'];
            totalTax += bookedData.service['Service_Tax__c'];
            return bookedData;
        });
        return {
            bookingData: data,
            serviceTax: totalTax,
            sales: totalPrice
        };
    }

}
