import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { NewClientFormService } from './newclientform.service';
import * as config from '../../app.config';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined } from 'util';
import { CommonService } from '../../common/common.service';
import { TranslateService } from 'ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { JwtHelper } from 'angular2-jwt';
@Component({
    selector: 'app-newclient',
    templateUrl: './newclientform.html',
    styleUrls: ['./newclientform.css'],
    providers: [NewClientFormService, CommonService]
})
export class NewClientFormComponent implements OnInit {
    clientPicture: any;
    clientEditObj: any;
    ClientProError: any = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    allowQuickAddAccess: any;
    birthDate: any;
    gender: any;
    mailingAddress: any;
    mobilePhone: any;
    primaryEmail: any;
    primaryPhone: any;
    secondaryEmail: any;
    /* client fields end */
    /*clientcared fileds */
    clientCardPrimaryPhone: any;
    clientCardMobilePhone: any;
    clientCardBirthDate: any;
    clientCardMailingAddress: any;
    clientCardPrimaryEmail: any;
    clientCardSecondaryEmail: any;
    clientCardGender: any;
    occupationData: any = [];
    errorMessage: any;
    statesList: any;
    mobileCountryCode = '01';
    primaryCountryCode = '01';
    mailingCountriesList = [{ 'NAME': 'Canada' }, { 'NAME': 'United States' }];
    noEmailBool = false;
    errorValidDate = '';
    hideClientInfo: any;
    decodedToken: any;
    decodeUserToken: any;
    day: any;
    month: any;
    year: any;
    birthdate = new Date();
    formQuestionsList: any = [];
    formApplyList: any = [];
    message = '';
    newClientId: any;
    clientDataObj: any;
    clientId = '';
    error = '';
    StartingBalanceDisable = false;
    accoutChargeBalance: any = false;
    depositRequired: any = false;
    persistanceNoShow: any = false;
    other: any = false;
    apptNotes: any;
    pin: any;
    noEmailAppt: any = false;
    /* message tab variable */
    messageBox = false;
    actionmethod: any;
    clienProfile = { 'fName': '', 'lName': '', 'id': '', 'FullName': '', 'email': '', 'phone': '', 'name': '', 'pic': '', 'note': '', 'client_since': '', 'index': '' };
    AppointmentsTab = {
        'noEmailAppt': '',
        'accoutChargeBalance': '',
        'depositRequired': '',
        'persistanceNoShow': '',
        'apptNotes': '',
        'pin': '',
        'standingAppt': '',
        'Other': ''
    };
    constructor(private activatedRoute: ActivatedRoute,
        private newClientformService: NewClientFormService,
        private sanitizer: DomSanitizer, private commonservice: CommonService, private http: HttpClient,
        private location: Location,
        private toastr: ToastrService, private translateService: TranslateService, private router: Router) {
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['actionMethod']) {
                this.actionmethod = params.actionMethod;
                this.clientId = params.id;
            } else {
                this.clientId = '';
                this.StartingBalanceDisable = false;
            }
        });
    }
    ngOnInit() {
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedToken = {};
            this.decodeUserToken = {};
        }
        if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
        } else {
            this.decodedToken = {};
        }
        this.clientObj();
        this.listClientFields();
        this.getOccupation();
        this.getApplyData();
        this.getformQuestions();
        if (this.clientId) {
            this.getClientData(this.clientId);
        } else {
            this.getApplyData();
            this.getformQuestions();
        }
        this.getCountry('United States');
        this.getHideClientContactInfo();

        // this.getClientProfile(this.clientId, )
    }
    getHideClientContactInfo() {
        this.newClientformService.getHideCliContactInfo(this.decodeUserToken.data.id).subscribe(data => {
            this.hideClientInfo = data['result'][0].Hide_Client_Contact_Info__c;
        }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    getClientProfile(pro, i) {
        this.clear();
        /*  consultation data */
        if (pro.Form_Checkboxes__c && pro.Form_Checkboxes__c !== 'undefined') {
            setTimeout(() => {
                const existFormCheckboxes = JSON.parse(pro.Form_Checkboxes__c);
                for (let m = 0; m < existFormCheckboxes.length; m++) {
                    for (let n = 0; n < this.formApplyList.length; n++) {
                        if ((existFormCheckboxes[m]['checkboxName'] === this.formApplyList[n]['checkboxName']) &&
                            existFormCheckboxes[m]['active1'] === true) {
                            this.formApplyList[n]['active1'] = existFormCheckboxes[m]['active1'];
                        }
                    }
                }
            }, 1000);
        } else {
            this.getApplyData();
        }
        if (pro.Form_Questions__c && pro.Form_Questions__c !== 'undefined') {
            setTimeout(() => {
                const existFormQuestions = JSON.parse(pro.Form_Questions__c);
                for (let m = 0; m < existFormQuestions.length; m++) {
                    for (let n = 0; n < this.formQuestionsList.length; n++) {
                        if ((existFormQuestions[m]['question'] === this.formQuestionsList[n]['question']) &&
                            existFormQuestions[m]['answer']) {
                            this.formQuestionsList[n]['answer'] = existFormQuestions[m]['answer'].replace(/<br>/g, '\n');
                        }
                    }
                }
            }, 1000);
        } else {
            this.getformQuestions();
        }
        // get top left profile data
        this.clienProfile = {
            'fName': pro.FirstName,
            'lName': pro.LastName,
            'name': pro.FirstName + ' ' + pro.LastName,
            'id': pro.Id,
            'FullName': pro.FullName,
            'email': pro.Email === 'null' ? '' : pro.Email,
            'phone': pro.MobilePhone === 'null' ? '' : pro.MobilePhone,
            'pic': pro.Client_Pic__c,
            'note': pro.Notes__c === 'null' ? '' : pro.Notes__c,
            'client_since': pro.CreatedDate,
            'index': i
        };
        // get appointments data
        this.AppointmentsTab = {
            'noEmailAppt': pro.BR_Reason_No_Email__c,
            'accoutChargeBalance': pro.BR_Reason_Account_Charge_Balance__c,
            'depositRequired': pro.BR_Reason_Deposit_Required__c,
            'persistanceNoShow': pro.BR_Reason_No_Show__c,
            'Other': pro.BR_Reason_Other__c,
            'apptNotes': pro.BR_Reason_Other_Note__c,
            'pin': pro.Pin__c,
            'standingAppt': ''
        };
        this.other = this.AppointmentsTab.Other;
        this.pin = this.AppointmentsTab.pin;
        this.noEmailAppt = this.AppointmentsTab.noEmailAppt;
        this.accoutChargeBalance = this.AppointmentsTab.accoutChargeBalance;
        this.depositRequired = this.AppointmentsTab.depositRequired;
        this.persistanceNoShow = this.AppointmentsTab.persistanceNoShow;
        this.apptNotes = this.AppointmentsTab.apptNotes;
        window.scrollTo(0, 0);
    }
    /*--- This Method lists Client Fields ---*/
    listClientFields() {
        this.newClientformService.getClientFields().subscribe(
            data => {
                const clientFeilds = JSON.parse(data['result'][1].JSON__c);
                const clientCardFeilds = JSON.parse(data['result'][0].JSON__c);
                this.birthDate = clientFeilds.birthDate;
                this.gender = clientFeilds.gender;
                this.mailingAddress = clientFeilds.mailingAddress;
                this.mobilePhone = clientFeilds.mobilePhone;
                this.primaryEmail = clientFeilds.primaryEmail;
                this.primaryPhone = clientFeilds.primaryPhone;
                this.secondaryEmail = clientFeilds.secondaryEmail;
                this.clientCardPrimaryPhone = clientCardFeilds.primaryPhone;
                this.clientCardMobilePhone = clientCardFeilds.mobilePhone;
                this.clientCardBirthDate = clientCardFeilds.birthDate;
                this.clientCardMailingAddress = clientCardFeilds.mailingAddress;
                this.clientCardPrimaryEmail = clientCardFeilds.primaryEmail;
                this.clientCardSecondaryEmail = clientCardFeilds.secondaryEmail;
                this.clientCardGender = clientCardFeilds.gender;
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }
    clientObj() {
        this.clientEditObj = {
            FirstName: '',
            LastName: '',
            MailingStreet: '',
            MailingCity: '',
            MailingState: '',
            MailingCountry: 'United States',
            Pin__c: '',
            Email: '',
            MobilePhone: '',
            HomePhone: '',
            Phone: '',
            Title: '',
            Birthdate: '',
            Emergency_Name__c: '',
            Relationship: '',
            Emergency_Primary_Phone__c: '',
            Emergency_Secondary_Phone__c: '',
            Gender__c: '',
            Active__c: 1,
            ReferredClient: '',
            BirthDateNumber__c: '',
            BirthMonthNumber__c: '',
            BirthYearNumber__c: '',
            Marketing_Mobile_Phone__c: false,
            Marketing_Primary_Email__c: false,
            Marketing_Opt_Out__c: '',
            Notification_Mobile_Phone__c: false,
            Notification_Opt_Out__c: '',
            Notification_Primary_Email__c: false,
            Reminder_Mobile_Phone__c: false,
            Reminder_Opt_Out__c: '',
            Reminder_Primary_Email__c: false,
            Active_Rewards__c: '1',
            Membership_ID__c: '',
            Starting_Balance__c: '',
            Payment_Type_Token__c: '',
            Token_Expiration_Date__c: '',
            Token_Present__c: '',
            Credit_Card_Token__c: '',
            Secondary_Email__c: '',
            MiddleName: '',
            MailingPostalCode: '',
            Responsible_Party__c: '',
            Client_Flag__c: '',
            Refer_A_Friend_Prospect__c: '',
            Referred_On_Date__c: '',
            No_Email__c: 0,
            Marketing_Secondary_Email__c: '',
            ReferredClientPic: '',
            ResponsibleClient: '',
            ResponsibleClientPic: '',
            Notification_Secondary_Email__c: '',
            Reminder_Secondary_Email__c: '',
            Booking_Restriction_Note__c: '',
            Notes__c: '',
            Booking_Frequency__c: '',
            Allow_Online_Booking__c: 1,
            Referred_By__c: '',
            selectedFlags: [],
            Client_Pic__c: '',
            Id: '',
            CreatedDate: '',
            Current_Balance__c: '',
            Sms_Consent__c: 0
        };
    }
    hyphen_generate_mobile1(value,tagid) {
        if (value === undefined || value === null) {
            value = '';
        }
        if (value.length === 0) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat('(');
        }
        if (value.length === 1) {
            (<HTMLInputElement>document.getElementById(tagid)).value = '('+value;
          }
        if (value.length === 4) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat(')');
        } if (value.length === 8) {
            (<HTMLInputElement>document.getElementById(tagid)).value = value.concat('-');
        }
    }
    pasteNum(value) {
        let temp = '';
        if (value.indexOf('(') !== 0) {
            for (let i = 0; i < value.length; i++) {
                if (i === 0) {
                    temp += '(' + value[i];
                } else if (i === 2) {
                    temp += value[i] + ')';
                } else if (i === 5) {
                    temp += value[i] + '-';
                } else {
                    temp += value[i];
                }
                this.clientEditObj.MobilePhone = temp.substr(0, 13);
            }
        }
    }
    numOnly(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    pasteNumPhone(value) {
        let temp = '';
        if (value.indexOf('(') !== 0) {
            for (let i = 0; i < value.length; i++) {
                if (i === 0) {
                    temp += '(' + value[i];
                } else if (i === 2) {
                    temp += value[i] + ')';
                } else if (i === 5) {
                    temp += value[i] + '-';
                } else {
                    temp += value[i];
                }
                this.clientEditObj.Phone = temp.substr(0, 13);
            }
        }
    }
    hyphen_generate_home(value) {
        if (value === undefined || value === null) {
            value = '';
        }
        if (value.length === 0) {
            (<HTMLInputElement>document.getElementById('homePhone')).value = value.concat('(');
        }
        if (value.length === 4) {
            (<HTMLInputElement>document.getElementById('homePhone')).value = value.concat(')');
        } if (value.length === 8) {
            (<HTMLInputElement>document.getElementById('homePhone')).value = value.concat('-');
        }
    }
    hyphen_generate_Wphone(value) {
        if (value === undefined || value === null) {
            value = '';
        }
        if (value.length === 0) {
            (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('(');
        }
        if (value.length === 4) {
            (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat(')');
        } if (value.length === 8) {
            (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('-');
        }
    }
    mobileSms(val) {
        if (!val) {
            this.clientEditObj['Sms_Consent__c'] = 0;
        }
    }
    SmscheckOrUnCheck(che) {
        let value;
        if (che) {
            value = 1;
        } else {
            value = 0;
        }
        this.clientEditObj['Sms_Consent__c'] = value;
        // if (che) {
        //   this.clientEditObj['Marketing_Mobile_Phone__c'] = value;
        //   this.clientEditObj['Notification_Mobile_Phone__c'] = value;
        //   this.clientEditObj['Reminder_Mobile_Phone__c'] = value;
        // }
    }
    validDate() {
        this.errorValidDate = '';
    }
    getOccupation() {
        this.newClientformService.getOccupations().subscribe(
            data => {
                this.occupationData = data['result'].filter(
                    filterList => filterList.active === true || filterList.active === 'true');
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    updateErrMsg(index: number) {
        this.ClientProError[index] = '';
    }
    getLocation() {
        if (this.clientEditObj.MailingPostalCode.length > 4) {
            this.http.get('https://ziptasticapi.com/' + this.clientEditObj.MailingPostalCode).subscribe(
                result => {
                    if (result['error']) {
                        const toastermessage: any = this.translateService.get('SETUPCOMPANY.ZIP_CODE_NOT_FOUND');
                        this.toastr.error(toastermessage.value, null, { timeOut: 1500 });
                    } else {
                        if (result['country'] === 'US') {
                            this.clientEditObj.MailingCountry = 'United States';
                            this.getCountry(this.clientEditObj.MailingCountry);
                            config.states.forEach(state => {
                                if (state.abbrev === result['state']) {
                                    this.clientEditObj.MailingState = state.name;
                                }
                            });
                        }
                        const cityArray = result['city'].split(' ');
                        for (let i = 0; i < cityArray.length; i++) {
                            if (i === 0) {
                                this.clientEditObj.MailingCity = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
                            } else {
                                this.clientEditObj.MailingCity += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
                            }
                        }
                    }
                },
                error => {
                }
            );
        }
    }
    getCountry(coun) {
        this.newClientformService.getStates(coun)
            .subscribe(statesValues => {
                this.statesList = statesValues['result'];
            },
                error => {
                    this.errorMessage = <any>error;
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    clear() {
        this.ClientProError[0] = '';
        this.ClientProError[1] = '';
        this.ClientProError[2] = '';
        this.ClientProError[3] = '';
        this.ClientProError[4] = '';
        this.ClientProError[5] = '';
        this.ClientProError[6] = '';
        this.ClientProError[7] = '';
        this.ClientProError[8] = '';
        this.ClientProError[9] = '';
        this.ClientProError[10] = '';
        this.ClientProError[11] = '';
        this.ClientProError[12] = '';
    }
    /* Method to get questions from setup client preferences questions tab */
    getformQuestions() {
        this.formQuestionsList = [];
        this.newClientformService.getformQuestions().subscribe(
            data => {
                if (data['result']) {
                    this.formQuestionsList = data['result'].filter(
                        filterList => filterList.active === true);
                    for (let i = 0; i < this.formQuestionsList.length; i++) {
                        if ((this.formQuestionsList[i].active === false || this.formQuestionsList[i].active === undefined) &&
                            (this.formQuestionsList[i].question === '' || this.formQuestionsList[i].question === undefined)) {
                            this.formQuestionsList.splice(i, 1);
                        }
                        this.formQuestionsList[i]['answer'] = '';
                    }
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    /* Method to get apply list from setup client preferences apply checkbox tab */
    getApplyData() {
        this.formApplyList = [];
        this.newClientformService.getApplyData().subscribe(
            data => {
                if (data['result']) {
                    this.formApplyList = data['result']['formcheckboxes'].filter(
                        filterList => filterList.active === true);
                    for (let i = 0; i < this.formApplyList.length; i++) {
                        this.formApplyList[i]['active1'] = false;
                    }
                    this.message = data['result']['message'];
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    commonSave(bol) {
        if (this.noEmailAppt === true) {
            this.noEmailAppt = 1;
        } else if (this.noEmailAppt === false) {
            this.noEmailAppt = 0;
        }
        if (this.accoutChargeBalance === true) {
            this.accoutChargeBalance = 1;
        } else if (this.accoutChargeBalance === false) {
            this.accoutChargeBalance = 0;
        }
        if (this.depositRequired === true) {
            this.depositRequired = 1;
        } else if (this.depositRequired === false) {
            this.depositRequired = 0;
        }
        if (this.persistanceNoShow === true) {
            this.persistanceNoShow = 1;
        } else if (this.persistanceNoShow === false) {
            this.persistanceNoShow = 0;
        }
        if (this.other === true) {
            this.other = 1;
        } else if (this.other === false) {
            this.other = 0;
        }
        // Appointment tab end
        if (this.clientEditObj.Active__c === true) {
            this.clientEditObj.Active__c = 1;
        } else if (this.clientEditObj.Active__c === false) {
            this.clientEditObj.Active__c = 0;
        }

        if (this.clientEditObj.No_Email__c === true) {
            this.clientEditObj.No_Email__c = 1;
        } else if (this.clientEditObj.No_Email__c === false) {
            this.clientEditObj.No_Email__c = 0;
        }
        this.clear();
        const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.clientEditObj.FirstName.trim() === 'undefined' || this.clientEditObj.FirstName.trim() === undefined ||
            this.clientEditObj.FirstName.trim() === '') {
            this.ClientProError[0] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_FIRSTNAME');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.FirstName === 'null') {
            this.ClientProError[0] = this.translateService.get('Enter Valid Name.');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.LastName.trim() === 'undefined' || this.clientEditObj.LastName.trim() === undefined ||
            this.clientEditObj.LastName.trim() === '') {
            this.ClientProError[1] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_LASTNAME');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.LastName === 'null') {
            this.ClientProError[1] = this.translateService.get('Enter Valid Name.');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.Phone === '' && this.clientCardPrimaryPhone === true && this.hideClientInfo === 0) {
            this.ClientProError[2] = this.translateService.get('primary Phone cannot be blank.');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.Phone && this.clientEditObj.Phone.length !== 13) {
            this.ClientProError[2] = this.translateService.get('Invalid primary Phone number.');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.Phone !== '' && this.clientCardPrimaryPhone === true && this.hideClientInfo === 0 && this.primaryCountryCode === '') {
            this.ClientProError[2] = this.translateService.get('primary Phone country code cannot be blank.');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.MobilePhone === 'undefined' || this.clientEditObj.MobilePhone === undefined ||
            this.clientEditObj.MobilePhone === '' || this.clientEditObj.MobilePhone === null || this.clientEditObj.MobilePhone === 'null') && this.clientCardMobilePhone === true &&
            this.hideClientInfo === 0) {
            this.ClientProError[3] = this.translateService.get('Mobile phone cannot be blank.');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MobilePhone && this.clientEditObj.MobilePhone.length !== 13) {
            this.ClientProError[3] = this.translateService.get('Invalid mobile phone number.');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.MobilePhone === 'undefined' || this.clientEditObj.MobilePhone === undefined ||
            this.clientEditObj.MobilePhone !== '' || this.clientEditObj.MobilePhone !== null || this.clientEditObj.MobilePhone !== 'null') && this.clientCardMobilePhone === true &&
            this.hideClientInfo === 0 && this.mobileCountryCode === '') {
            this.ClientProError[3] = this.translateService.get('Mobile phone country code cannot be blank.');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.No_Email__c === 0) && (this.clientEditObj.Email === '' && this.clientCardPrimaryEmail === true) && (this.hideClientInfo === 0)) {
            this.ClientProError[4] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_PRIMARY_EMAIL');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.No_Email__c === 0) && (this.hideClientInfo === 0) && (this.clientEditObj.Email !== '' && !EMAIL_REGEXP.test(this.clientEditObj.Email))) {
            this.ClientProError[4] = this.translateService.get('CLIENTS.INVALID_PRIMARY_EMAIL');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.No_Email__c === 0) && (this.hideClientInfo === 0) && (this.clientEditObj.Secondary_Email__c === '' && this.clientCardSecondaryEmail === true)) {
            this.ClientProError[5] = this.translateService.get('CLIENTS.SEC_NOBLANK_EMAIL');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.No_Email__c === 0) && (this.hideClientInfo === 0) && (this.clientEditObj.Secondary_Email__c !== '' &&
            !EMAIL_REGEXP.test(this.clientEditObj.Secondary_Email__c))) {
            this.ClientProError[5] = this.translateService.get('CLIENTS.INVALID_SEC_EMAIL');
            window.scrollTo(0, 0);
        } else if ((this.clientEditObj.No_Email__c === 0) && (this.hideClientInfo === 0) && (this.clientEditObj.Secondary_Email__c !== '' &&
            !EMAIL_REGEXP.test(this.clientEditObj.Secondary_Email__c))) {
            this.ClientProError[5] = this.translateService.get('CLIENTS.INVALID_CLIENT_INFO_SECONDARY_EMAIL');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MailingStreet === '' && this.clientCardMailingAddress === true && this.hideClientInfo === 0) {
            this.ClientProError[6] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_ADDRESS');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MailingPostalCode === '' && this.clientCardMailingAddress === true && this.hideClientInfo === 0) {
            this.ClientProError[7] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_ZIP');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MailingCountry === '' && this.clientCardMailingAddress === true && this.hideClientInfo === 0) {
            this.ClientProError[8] = this.translateService.get('SETUPCOMPANY.VALID_NOBLANK_SETUPCOMPANY_COUNTRY_CODE');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MailingState === '' && this.clientCardMailingAddress === true && this.hideClientInfo === 0) {
            this.ClientProError[9] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_STATE');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.MailingCity === '' && this.clientCardMailingAddress === true && this.hideClientInfo === 0) {
            this.ClientProError[10] = this.translateService.get('CLIENTS.NOBLANK_CLIENT_INFO_CITY');
            window.scrollTo(0, 0);
        } else if (this.clientEditObj.Gender__c === '' && this.clientCardGender === true) {
            this.ClientProError[12] = this.translateService.get('CLIENTS.SELECT_GENDER');
            window.scrollTo(0, 500);
        } else if ((this.clientEditObj.Birthdate === '' || this.clientEditObj.Birthdate === null) && this.clientCardBirthDate === true) {
            this.ClientProError[11] = this.translateService.get('CLIENTS.SELECT_BIRTH_DATE');
            window.scrollTo(0, 500);
        } else {
            if (this.clientEditObj.Birthdate) {
                // First check for the pattern
                const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                if (!datePattern.test(this.clientEditObj.Birthdate)) {
                    this.errorValidDate = 'Invalid date format';
                    return false;
                }
                // Parse the date parts to integers
                const parts = this.clientEditObj.Birthdate.split('/');
                const day = parseInt(parts[1], 10);
                const month = parseInt(parts[0], 10);
                const year = parseInt(parts[2], 10);
                // Check the ranges of month and year
                if (year < 1000 || year > 3000 || month === 0 || month > 12) {
                    this.errorValidDate = 'Invalid date format';
                    return false;
                }
                const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Adjust for leap years
                if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
                    monthLength[1] = 29;
                }
                // Check the range of the day
                this.clientEditObj.Birthdate = month + '/' + day + '/' + year;
                this.day = day;
                this.month = month;
                this.year = year;
            } else {
                this.day = null;
                this.month = null;
                this.year = null;
            }
            if (this.clientEditObj.Notification_Primary_Email__c === true ||
                this.clientEditObj.Notification_Primary_Email__c === 1) {
                this.clientEditObj.Notification_Primary_Email__c = 1;
            } else {
                this.clientEditObj.Notification_Primary_Email__c = 0;
            }
            if (this.clientEditObj.Reminder_Primary_Email__c === true ||
                this.clientEditObj.Reminder_Primary_Email__c === 1) {
                this.clientEditObj.Reminder_Primary_Email__c = 1;
            } else {
                this.clientEditObj.Reminder_Primary_Email__c = 0;
            }
            if (this.clientEditObj.Marketing_Primary_Email__c === true ||
                this.clientEditObj.Marketing_Primary_Email__c === 1) {
                this.clientEditObj.Marketing_Primary_Email__c = 1;
            } else {
                this.clientEditObj.Marketing_Primary_Email__c = 0;
            }
            if (this.clientEditObj.Notification_Mobile_Phone__c === true ||
                this.clientEditObj.Notification_Mobile_Phone__c === 1) {
                this.clientEditObj.Notification_Mobile_Phone__c = 1;
            } else {
                this.clientEditObj.Notification_Mobile_Phone__c = 0;
            }
            if (this.clientEditObj.Reminder_Mobile_Phone__c === true ||
                this.clientEditObj.Reminder_Mobile_Phone__c === 1) {
                this.clientEditObj.Reminder_Mobile_Phone__c = 1;
            } else {
                this.clientEditObj.Reminder_Mobile_Phone__c = 0;
            }
            if (this.clientEditObj.Marketing_Mobile_Phone__c === true ||
                this.clientEditObj.Marketing_Mobile_Phone__c === 1) {
                this.clientEditObj.Marketing_Mobile_Phone__c = 1;
            } else {
                this.clientEditObj.Marketing_Mobile_Phone__c = 0;
            }
            this.clientDataObj = {
                // client profile data
                'clientInfoActive': this.clientEditObj.Active__c,
                'clientInfoFirstName': this.clientEditObj.FirstName.trim(),
                'clientInfoMiddleName': this.clientEditObj.MiddleName,
                'clientInfoLastName': this.clientEditObj.LastName.trim(),
                'clientInfoMailingStreet': this.clientEditObj.MailingStreet,
                'clientInfoMailingCountry': this.clientEditObj.MailingCountry,
                'clientInfoPostalCode': this.clientEditObj.MailingPostalCode,
                'clientInfoMailingCity': this.clientEditObj.MailingCity,
                'clientInfoMailingState': this.clientEditObj.MailingState,
                'clientInfoPrimaryPhone': this.clientEditObj.Phone !== '' ? this.primaryCountryCode + '-' + this.clientEditObj.Phone : '',
                'clientInfoMobilePhone': this.clientEditObj.MobilePhone !== '' ? this.mobileCountryCode + '-' + this.clientEditObj.MobilePhone : '',
                'homePhone': this.clientEditObj.HomePhone,
                'clientInfoPrimaryMail': this.clientEditObj.Email,
                'clientInfoSecondaryEmail': this.clientEditObj.Secondary_Email__c,
                'clientInfoEmergName': this.clientEditObj.Emergency_Name__c,
                'clientInfoEmergPrimaryPhone': this.clientEditObj.Emergency_Primary_Phone__c,
                'clientInfoEmergSecondaryPhone': this.clientEditObj.Emergency_Secondary_Phone__c,
                'clientInfoNoEmail': this.clientEditObj.No_Email__c,
                'responsibleParty': this.clientEditObj.Responsible_Party__c,
                'gender': this.clientEditObj.Gender__c,
                'birthDay': this.day,
                'birthMonth': this.month,
                'occupationvalue': this.clientEditObj.Title,
                'birthYear': this.year,
                'selectedFlags': this.clientEditObj.Client_Flag__c,
                'referredBy': this.clientEditObj.Referred_By__c,
                'clientPicture': this.clientPicture,
                'ReferedAFriendProspect': !this.clientId ? 0 : this.clientEditObj.Refer_A_Friend_Prospect__c,
                'referedOnDate': this.commonservice.getDBDatTmStr(this.clientEditObj.referedDate),
                'marketingOptOut': this.clientEditObj.Marketing_Opt_Out__c,
                'marketingMobilePhone': this.clientEditObj.Marketing_Mobile_Phone__c,
                'marketingPrimaryEmail': this.clientEditObj.Marketing_Primary_Email__c,
                'marketingSecondaryEmail': this.clientEditObj.Marketing_Secondary_Email__c,
                'notificationMobilePhone': this.clientEditObj.Notification_Mobile_Phone__c,
                'notificationOptOut': this.clientEditObj.Notification_Opt_Out__c,
                'notificationPrimaryEmail': this.clientEditObj.Notification_Primary_Email__c,
                'notificationSecondaryEmail': this.clientEditObj.Notification_Secondary_Email__c,
                'reminderOptOut': this.clientEditObj.Reminder_Opt_Out__c,
                'reminderMobilePhone': this.clientEditObj.Reminder_Mobile_Phone__c,
                'reminderPrimaryEmail': this.clientEditObj.Reminder_Primary_Email__c,
                'reminderSecondaryEmail': this.clientEditObj.Reminder_Secondary_Email__c,
                'notes': this.clientEditObj.Notes__c,

                // Appt data
                'noEmailAppt': this.noEmailAppt,
                'accoutChargeBalance': this.accoutChargeBalance,
                'depositRequired': this.depositRequired,
                'persistanceNoShow': this.persistanceNoShow,
                'other': this.other,
                'otherReason': this.clientEditObj.Booking_Restriction_Note__c,
                'apptNotes': this.apptNotes,
                'bookingFrequency': this.clientEditObj.Booking_Frequency__c,
                'allowOnlineBooking': this.clientEditObj.Allow_Online_Booking__c === true || this.clientEditObj.Allow_Online_Booking__c === 1 ? 1 : 0,
                'pin': this.pin,
                // Accounts
                'activeRewards': this.clientEditObj.Active_Rewards__c,
                'isNewClient': this.clientId === '' ? true : false,
                'startingBalance': this.clientEditObj.Starting_Balance__c,
                'clientMemberShipId': this.clientEditObj.Membership_ID__c,
                'creditCardToken': this.clientEditObj.Credit_Card_Token__c,
                'tokenExpirationDate': this.clientEditObj.Token_Expiration_Date__c,
                'PaymentType': this.clientEditObj.Payment_Type_Token__c,
                'tokenPresent': this.clientEditObj.Token_Present__c,
                'CurrentBalance': this.clientEditObj.Current_Balance__c,
                'startingBalDis': this.StartingBalanceDisable === false ? this.clientEditObj.Starting_Balance__c : 0,
                'Sms_Consent__c': this.clientEditObj.Sms_Consent__c,
                // consultation data
                'consultationQuestList': this.formQuestionsList,
                'consultationApplyList': this.formApplyList
            };
            if (this.clientEditObj.No_Email__c) {
                this.clientDataObj.reminderPrimaryEmail = 0;
                this.clientDataObj.notificationPrimaryEmail = 0;
                this.clientDataObj.marketingPrimaryEmail = 0;
            } else if (!this.clientEditObj.No_Email__c && !this.clientEditObj.Email) {
                this.clientDataObj.reminderPrimaryEmail = 0;
                this.clientDataObj.notificationPrimaryEmail = 0;
                this.clientDataObj.marketingPrimaryEmail = 0;
            }
            for (const key in this.clientDataObj) {
                if (this.clientDataObj[key] === 'null' || this.clientDataObj[key] === null || this.clientDataObj[key] === 'undefined'
                    || this.clientDataObj[key] === undefined || this.clientDataObj[key] === 'null-null-null') {
                    this.clientDataObj[key] = '';
                }
            }
            if (this.clienProfile.id === '') {
                this.clienProfile.id = undefined;
            }
            this.newClientformService.saveClient(this.clientId, this.clientDataObj).subscribe(
                data => {
                    const clientInfoDetails = data['result'];
                    this.newClientId = data['result'].clientId;
                    this.clear();
                    this.clienProfile.email = this.clientEditObj.Email;
                    this.messageBox = true;
                }, error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2083':
                            this.ClientProError[10] = this.translateService.get('CLIENTS.DUPLICATE_MEMBERSHIP_ID');
                            // this.updateTabs(7);
                            // window.scrollTo(0, 600);
                            break;
                        case '2088':
                            this.ClientProError[18] = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                            // this.updateTabs(0);
                            break;
                        case '2092':
                            this.ClientProError[18] = this.translateService.get(JSON.parse(error['_body']).message);
                            // this.updateTabs(0);
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    leftProfileUpdate(pro) {
        this.clienProfile = {
            'fName': pro.FirstName,
            'lName': pro.LastName,
            'name': pro.FirstName + ' ' + pro.LastName,
            'id': pro.Id,
            'FullName': pro.FullName,
            'email': pro.Email === 'null' ? '' : pro.Email,
            'phone': pro.MobilePhone === 'null' ? '' : pro.MobilePhone,
            'pic': pro.Client_Pic__c,
            'note': pro.Notes__c === 'null' ? '' : pro.Notes__c,
            'client_since': pro.CreatedDate,
            'index': null
        };
    }
    getClientData(clientId) {
        this.newClientformService.getClient(clientId)
            .subscribe(data => {
                this.clientEditObj = data['result']['results'][0];
                if (!this.clientEditObj['Email']) {
                    this.clientEditObj['Notification_Primary_Email__c'] = 0;
                    this.clientEditObj['Reminder_Primary_Email__c'] = 0;
                    this.clientEditObj['Marketing_Primary_Email__c'] = 0;
                }
                if (!this.clientEditObj['MobilePhone']) {
                    this.clientEditObj['Sms_Consent__c'] = 0;
                    this.clientEditObj['Notification_Mobile_Phone__c'] = 0;
                    this.clientEditObj['Reminder_Mobile_Phone__c'] = 0;
                    this.clientEditObj['Marketing_Mobile_Phone__c'] = 0;
                }
                this.getClientProfile(this.clientEditObj, null);
                this.noEmailBool = this.clientEditObj.No_Email__c === 1 ? true : false;
                if (this.clientEditObj.Birthdate === '' || this.clientEditObj.Birthdate === 'undefined' || this.clientEditObj.Birthdate === 'null-null-null') {
                    this.clientEditObj.Birthdate = '';
                } else if (this.clientEditObj.Birthdate) {
                    this.clientEditObj.Birthdate =
                        this.clientEditObj.Birthdate.split('-')[0] + '/' + this.clientEditObj.Birthdate.split('-')[1] + '/' + this.clientEditObj.Birthdate.split('-')[2];
                }
                // flag
                if (this.clientEditObj.MobilePhone) {
                    this.clientEditObj.MobilePhone = this.clientEditObj.MobilePhone.split('-')[1] + '-' + this.clientEditObj.MobilePhone.split('-')[2];
                }
                if (this.clientEditObj.Phone) {
                    this.clientEditObj.Phone = this.clientEditObj.Phone.split('-')[1] + '-' + this.clientEditObj.Phone.split('-')[2];
                }
                for (const key in this.clientEditObj) {
                    if (!this.clientEditObj || this.clientEditObj[key] === 'null' || this.clientEditObj[key] === 'undefined'
                        || this.clientEditObj[key] === 'null-null-null') {
                        this.clientEditObj[key] = '';
                    }
                }
            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    cancel() {
        this.router.navigate(['/client']);
    }
    Cli_No_Email(val) {
        this.noEmailBool = val;
        this.clear();
        if (val) {
            this.clientEditObj.Marketing_Primary_Email__c = 0;
            this.clientEditObj.Notification_Primary_Email__c = 0;
            this.clientEditObj.Reminder_Primary_Email__c = 0;
            this.clientEditObj.Marketing_Secondary_Email__c = 0;
            this.clientEditObj.Notification_Secondary_Email__c = 0;
            this.clientEditObj.Reminder_Secondary_Email__c = 0;
        }
    }
}
