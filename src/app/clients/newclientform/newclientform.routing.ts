import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewClientFormComponent } from './newclientform.component';
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild([
    {
        path: '',
        component: NewClientFormComponent,
        children: [
            {
                path: '',
                component: NewClientFormComponent
            }
        ]
    }
])],
  exports: [RouterModule]
})
export class NewClientFormRoutingModule { }
