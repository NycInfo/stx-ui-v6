import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashBoardnewService } from './dashboardnew.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { Chart } from 'angular-highcharts';
import * as config from '../app.config';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-dashboardnew-app',
  templateUrl: './dashboardnew.html',
  styleUrls: ['./dashboardnew.css'],
  providers: [DashBoardnewService]
})
export class DashBoardnewComponent implements OnInit {
  decodedToken: any;
  decodedUserToken: any;
  worker = '';
  dashboardData: any = {};

  bookedOnlinePer = 0;
  commonPer = 0;
  tpWrkrPrdImg = false;
  tpWrkrSrvImg = false;
  topProductWorkerName = '';
  topServiceWorkerName = '';
  topProductWorkerImage: any = {};
  topServiceWorkerImage: any = {};
  tpWrkrPrdWkImg = false;
  tpWrkrSrvWkImg = false;
  topProductWkWorkerName = '';
  topServiceWkWorkerName = '';
  topProductWkWorkerImage: any = {};
  topServiceWkWorkerImage: any = {};
  tpWrkrPrdMnthImg = false;
  tpWrkrSrvMnthImg = false;
  topProductMnthWorkerName = '';
  topServiceMnthWorkerName = '';
  topProductMnthWorkerImage: any = {};
  topServiceMnthWorkerImage: any = {};
  tpWrkrPrdYrImg = false;
  tpWrkrSrvYrImg = false;
  topProductYrWorkerName = '';
  topServiceYrWorkerName = '';
  topProductYrWorkerImage: any = {};
  topServiceYrWorkerImage: any = {};
  type = '';
  updateFlag = true;
  bookedDay: any = [];
  bookedWeek: any = [];
  bookedMonth: any = [];
  bookedYear: any = [];
  bookedDayPer = 0;
  bookedWeekPer = 0;
  bookedMonthPer = 0;
  bookedYearPer = 0;
  bookedOnlineDay: any = [];
  bookedOnlineWeek: any = [];
  bookedOnlineMonth: any = [];
  bookedOnlineYear: any = [];
  bookedOnlineDayPer = 0;
  bookedOnlineWeekPer = 0;
  bookedOnlineMonthPer = 0;
  bookedOnlineYearPer = 0;
  newClientsDay: any = [];
  newClientsWeek: any = [];
  newClientsMonth: any = [];
  newClientsYear: any = [];
  newClientDayPer = 0;
  newClientWeekPer = 0;
  newClientMonthPer = 0;
  newClientYearPer = 0;
  productSalesDay = 0;
  productSaleWeek = 0;
  productSalesMonth = 0;
  productSalesYear = 0;
  averageProductDay = 0;
  averageProductWeek = 0;
  averageProductMonth = 0;
  averageProductYear = 0;
  averageServiceDay = 0;
  averageServiceWeek = 0;
  averageServiceMonth = 0;
  averageServiceYear = 0;
  averageTicketDay = 0;
  averageTicketWeek = 0;
  averageTicketMonth = 0;
  averageTicketYear = 0;
  rebookedDay = 0;
  rebookedWeek = 0;
  rebookedMonth = 0;
  rebookedYear = 0;
  rebookDayPer = 0;
  rebookWeekPer = 0;
  rebookMonthPer = 0;
  rebookYearPer = 0;
  retailPerGuestDay = 0;
  retailPerGuestWeek = 0;
  retailPerGuestMonth = 0;
  retailPerGuestYear = 0;
  retailToServiceDay: any = [];
  retailToServiceWeek: any = [];
  retailToServiceMonth: any = [];
  retailToServiceYear: any = [];
  serviceSalesDay = 0;
  serviceSalesMonth = 0;
  serviceSalesYear = 0;
  bookedTicketedBooked = 0;
  bookedTicketedCheckedIn = 0;
  bookedTicketed = 0;
  bookedTicketedBookedWeek = 0;
  bookedTicketedCheckedInWeek = 0;
  bookedTicketedWeek = 0;
  bookedTicketedBookedMonth = 0;
  bookedTicketedCheckedInMonth = 0;
  bookedTicketedMonth = 0;
  bookedTicketedBookedYear = 0;
  bookedTicketedCheckedInYear = 0;
  bookedTicketedYear = 0;
  onlinebookeddata: any = [];
  productSaleWeekDate: any = [];
  productSaleWeekTotal: any = [];
  serviceSaleWeekDate: any = [];
  serviceSaleWeekTotal: any = [];
  sumOfServiceSalesWk = 0;
  sumOfProductSalesWk = 0;
  showWorkers = false;
  title = false;
  data3: any;
  workerList = [];
  chart: Chart;
  bookedDayChart: Chart;
  bookedWeekChart: Chart;
  bookedMonthChart: Chart;
  bookedYearChart: Chart;
  rebookDayChart: Chart;
  rebookWeekChart: Chart;
  rebookMonthChart: Chart;
  rebookYearChart: Chart;
  bookOnlineDayChart: Chart;
  bookedOnlineWeekChart: Chart;
  bookedOnlineMonthChart: Chart;
  bookedOnlineYearChart: Chart;
  newClientDayChart: Chart;
  newClientWeekChart: Chart;
  newClientMonthChart: Chart;
  newClientYearChart: Chart;
  bookedTicketedDayChart: Chart;
  bookedTicketedWeekChart: Chart;
  bookedTicketedMonthChart: Chart;
  bookedTicketedYearChart: Chart;
  retailToServiceDayChart: Chart;
  retailToServiceWeekChart: Chart;
  retailToServiceMonthChart: Chart;
  retailToServiceYearChart: Chart;
  productSaleWeekChart: Chart;
  serviceSaleWeekChart: Chart;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private dashboardnewService: DashBoardnewService,
    @Inject('apiEndPoint') private apiEndPoint: string
  ) {
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for permissions Implementation--- //
    this.getWorkerList();
    // this.init(this.type);
  }
  getWorkerList() {
    this.dashboardnewService.getWorkerList().subscribe(data => {
      this.workerList = data['result'];
      this.worker = '';
      if (!this.decodedToken['Home'][2]['allowAcces']) {
        this.worker = this.workerList[0]['Id'];
      }
      this.getDashboardNewDetails();
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getDashboardNewDetails() {
    this.dashboardData = {};
    this.serviceSaleWeekDate = [];
    this.serviceSaleWeekTotal = [];
    this.productSaleWeekDate = [];
    this.productSaleWeekTotal = [];
    this.dashboardnewService.getDashboardNewDetails(this.worker).subscribe(data => {
      this.dashboardData = data['result'];
      if (this.dashboardData.Booked['day'].length > 0) {
        this.bookedDay['count'] = this.dashboardData.Booked['day'][0]['count'];
        this.bookedDay['percentage'] = +parseFloat(this.dashboardData.Booked['day'][0]['percentage']).toFixed(2);
        this.bookedDayPer = this.bookedDay['percentage'];
        this.bookDay();
      } else {
        this.bookedDay['count'] = 0;
        this.bookedDay['percentage'] = 0;
        this.bookedDayPer = 0;
        this.bookDay();
      }
      if (this.dashboardData.Booked['week'].length > 0) {
        this.bookedWeek['count'] = this.dashboardData.Booked['week'][0]['count'];
        this.bookedWeek['percentage'] = +parseFloat(this.dashboardData.Booked['week'][0]['percentage']).toFixed(2);
        this.bookedWeekPer = this.bookedWeek['percentage'];
        this.bookWeek();
      } else {
        this.bookedWeek['count'] = 0;
        this.bookedWeek['percentage'] = 0;
        this.bookedWeekPer = 0;
        this.bookWeek();
      }
      if (this.dashboardData.Booked['month'].length > 0) {
        this.bookedMonth['count'] = this.dashboardData.Booked['month'][0]['count'];
        this.bookedMonth['percentage'] = +parseFloat(this.dashboardData.Booked['month'][0]['percentage']).toFixed(2);
        this.bookedMonthPer = this.bookedMonth['percentage'];
        this.bookMonth();
      } else {
        this.bookedMonth['count'] = 0;
        this.bookedMonth['percentage'] = 0;
        this.bookedMonthPer = 0;
        this.bookMonth();
      }
      if (this.dashboardData.Booked['year'].length > 0) {
        this.bookedYear['count'] = this.dashboardData.Booked['year'][0]['count'];
        this.bookedYear['percentage'] = +parseFloat(this.dashboardData.Booked['year'][0]['percentage']).toFixed(2);
        this.bookedYearPer = this.bookedYear['percentage'];
        this.bookYear();
      } else {
        this.bookedYear['count'] = 0;
        this.bookedYear['percentage'] = 0;
        this.bookedYearPer = 0;
        this.bookYear();
      }
      if (this.dashboardData.BookedOnline['day'].length > 0) {
        this.bookedOnlineDay['count'] = this.dashboardData.BookedOnline['day'][0]['count'];
        this.bookedOnlineDay['percentage'] = +parseFloat(this.dashboardData.BookedOnline['day'][0]['percentage']).toFixed(2);
        this.bookedOnlineDayPer = this.bookedOnlineDay['percentage'];
        this.bookOnlineDay();
      } else {
        this.bookedOnlineDay['count'] = 0;
        this.bookedOnlineDay['percentage'] = 0;
        this.bookedOnlineDayPer = 0;
        this.bookOnlineDay();
      }
      if (this.dashboardData.BookedOnline['week'].length > 0) {
        this.bookedOnlineWeek['count'] = this.dashboardData.BookedOnline['week'][0]['count'];
        this.bookedOnlineWeek['percentage'] = +parseFloat(this.dashboardData.BookedOnline['week'][0]['percentage']).toFixed(2);
        this.bookedOnlineWeekPer = this.bookedOnlineWeek['percentage'];
        this.bookOnlineWeek();
      } else {
        this.bookedOnlineWeek['count'] = 0;
        this.bookedOnlineWeek['percentage'] = 0;
        this.bookedOnlineWeekPer = 0;
        this.bookOnlineWeek();
      }
      if (this.dashboardData.BookedOnline['month'].length > 0) {
        this.bookedOnlineMonth['count'] = this.dashboardData.BookedOnline['month'][0]['count'];
        this.bookedOnlineMonth['percentage'] = +parseFloat(this.dashboardData.BookedOnline['month'][0]['percentage']).toFixed(2);
        this.bookedOnlineMonthPer = this.bookedOnlineMonth['percentage'];
        this.bookOnlineMonth();
      } else {
        this.bookedOnlineMonth['count'] = 0;
        this.bookedOnlineMonth['percentage'] = 0;
        this.bookedOnlineMonthPer = 0;
        this.bookOnlineMonth();
      }
      if (this.dashboardData.BookedOnline['year'].length > 0) {
        this.bookedOnlineYear['count'] = this.dashboardData.BookedOnline['year'][0]['count'];
        this.bookedOnlineYear['percentage'] = +parseFloat(this.dashboardData.BookedOnline['year'][0]['percentage']).toFixed(2);
        this.bookedOnlinePer = this.bookedOnlineYear['percentage'];
        this.bookOnlineYear();
      } else {
        this.bookedOnlineYear['count'] = 0;
        this.bookedOnlineYear['percentage'] = 0;
        this.bookedOnlinePer = 0;
        this.bookOnlineYear();
      }
      if (this.dashboardData.NewClients['day'].length > 0) {
        this.newClientsDay['count'] = this.dashboardData.NewClients['day'][0]['count'];
        this.newClientsDay['percentage'] = +parseFloat(this.dashboardData.NewClients['day'][0]['percentage']).toFixed(2);
        this.newClientDayPer = this.newClientsDay['percentage'];
        this.newClientDay();
      } else {
        this.newClientsDay['count'] = 0;
        this.newClientsDay['percentage'] = 0;
        this.newClientDayPer = 0;
        this.newClientDay();
      }
      if (this.dashboardData.NewClients['week'].length > 0) {
        this.newClientsWeek['count'] = this.dashboardData.NewClients['week'][0]['count'];
        this.newClientsWeek['percentage'] = +parseFloat(this.dashboardData.NewClients['week'][0]['percentage']).toFixed(2);
        this.newClientWeekPer = this.newClientsWeek['percentage'];
        this.newClientWeek();
      } else {
        this.newClientsWeek['count'] = 0;
        this.newClientsWeek['percentage'] = 0;
        this.newClientWeekPer = 0;
        this.newClientWeek();
      }
      if (this.dashboardData.NewClients['month'].length > 0) {
        this.newClientsMonth['count'] = this.dashboardData.NewClients['month'][0]['count'];
        this.newClientsMonth['percentage'] = +parseFloat(this.dashboardData.NewClients['month'][0]['percentage']).toFixed(2);
        this.newClientMonthPer = this.newClientsMonth['percentage'];
        this.newClientMonth();
      } else {
        this.newClientsMonth['count'] = 0;
        this.newClientsMonth['percentage'] = 0;
        this.newClientMonthPer = 0;
        this.newClientMonth();
      }
      if (this.dashboardData.NewClients['year'].length > 0) {
        this.newClientsYear['count'] = this.dashboardData.NewClients['year'][0]['count'];
        this.newClientsYear['percentage'] = +parseFloat(this.dashboardData.NewClients['year'][0]['percentage']).toFixed(2);
        this.newClientYearPer = this.newClientsYear['percentage'];
        this.newClientYear();
      } else {
        this.newClientsYear['count'] = 0;
        this.newClientsYear['percentage'] = 0;
        this.newClientYearPer = 0;
        this.newClientYear();
      }
      if (this.dashboardData.ProductSales['day'].length > 0) {
        this.productSalesDay = this.dashboardData.ProductSales['day'][0]['productSales'];
      } else {
        this.productSalesDay = 0;
      }
      if (this.dashboardData.ProductSales['week'].length > 0) {
        this.productSaleWeek = this.dashboardData.ProductSales['week'][0]['productTotal'];
        for (let i = 0; i < this.dashboardData.ProductSales['week'].length; i++) {
          this.productSaleWeekDate.push(data['result'].ProductSales['week'][i]['productDate']);
          this.productSaleWeekTotal.push(data['result'].ProductSales['week'][i]['productTotal']);
          this.productSalesWeek();
        }
        this.sumOfProductSalesWk = this.productSaleWeekTotal.reduce((a, b) => a + b, 0);
      } else {
        this.sumOfProductSalesWk = 0;
        this.productSalesWeek();
      }
      if (this.dashboardData.ProductSales['month'].length > 0) {
        this.productSalesMonth = this.dashboardData.ProductSales['month'][0]['productSales'];
      } else {
        this.productSalesMonth = 0;
      }
      if (this.dashboardData.ProductSales['year'].length > 0) {
        this.productSalesYear = this.dashboardData.ProductSales['year'][0]['productSales'];
      } else {
        this.productSalesYear = 0;
      }
      if (this.dashboardData.AverageProduct['day'].length > 0) {
        this.averageProductDay = this.dashboardData.AverageProduct['day'][0]['percentage'];
      } else {
        this.averageProductDay = 0;
      }
      if (this.dashboardData.AverageProduct['week'].length > 0) {
        this.averageProductWeek = this.dashboardData.AverageProduct['week'][0]['percentage'];
      } else {
        this.averageProductWeek = 0;
      }
      if (this.dashboardData.AverageProduct['month'].length > 0) {
        this.averageProductMonth = this.dashboardData.AverageProduct['month'][0]['percentage'];
      } else {
        this.averageProductMonth = 0;
      }
      if (this.dashboardData.AverageProduct['year'].length > 0) {
        this.averageProductYear = this.dashboardData.AverageProduct['year'][0]['percentage'];
      } else {
        this.averageProductYear = 0;
      }
      if (this.dashboardData.AverageService['day'].length > 0) {
        this.averageServiceDay = this.dashboardData.AverageService['day'][0]['percentage'];
      } else {
        this.averageServiceDay = 0;
      }
      if (this.dashboardData.AverageService['week'].length > 0) {
        this.averageServiceWeek = this.dashboardData.AverageService['week'][0]['percentage'];
      } else {
        this.averageServiceWeek = 0;
      }
      if (this.dashboardData.AverageService['month'].length > 0) {
        this.averageServiceMonth = this.dashboardData.AverageService['month'][0]['percentage'];
      } else {
        this.averageServiceMonth = 0;
      }
      if (this.dashboardData.AverageService['year'].length > 0) {
        this.averageServiceYear = this.dashboardData.AverageService['year'][0]['percentage'];
      } else {
        this.averageServiceYear = 0;
      }
      if (this.dashboardData.AverageTicket['day'].length > 0) {
        this.averageTicketDay = this.dashboardData.AverageTicket['day'][0]['percentage'];
      } else {
        this.averageTicketDay = 0;
      }
      if (this.dashboardData.AverageTicket['week'].length > 0) {
        this.averageTicketWeek = this.dashboardData.AverageTicket['week'][0]['percentage'];
      } else {
        this.averageTicketWeek = 0;
      }
      if (this.dashboardData.AverageTicket['month'].length > 0) {
        this.averageTicketMonth = this.dashboardData.AverageTicket['month'][0]['percentage'];
      } else {
        this.averageTicketMonth = 0;
      }
      if (this.dashboardData.AverageTicket['year'].length > 0) {
        this.averageTicketYear = this.dashboardData.AverageTicket['year'][0]['percentage'];
      } else {
        this.averageTicketYear = 0;
      }
      if (this.dashboardData.Rebooked['day'].length > 0) {
        this.rebookedDay = +parseFloat(this.dashboardData.Rebooked['day'][0]['percentage']).toFixed(2);
        this.rebookDay();
      } else {
        this.rebookedDay = 0;
        this.rebookDay();
      }
      if (this.dashboardData.Rebooked['week'].length > 0) {
        this.rebookedWeek = +parseFloat(this.dashboardData.Rebooked['week'][0]['percentage']).toFixed(2);
        this.rebookWeek();
      } else {
        this.rebookedWeek = 0;
        this.rebookWeek();
      }
      if (this.dashboardData.Rebooked['month'].length > 0) {
        this.rebookedMonth = +parseFloat(this.dashboardData.Rebooked['month'][0]['percentage']).toFixed(2);
        this.rebookMonth();
      } else {
        this.rebookedMonth = 0;
        this.rebookMonth();
      }
      if (this.dashboardData.Rebooked['year'].length > 0) {
        this.rebookedYear = +parseFloat(this.dashboardData.Rebooked['year'][0]['percentage']).toFixed(2);
        this.rebookYear();
      } else {
        this.rebookedYear = 0;
        this.rebookYear();
      }
      if (this.dashboardData.RetailPerGuest['day'].length > 0) {
        this.retailPerGuestDay = this.dashboardData.RetailPerGuest['day'][0]['percentage'];
      } else {
        this.retailPerGuestDay = 0;
      }
      if (this.dashboardData.RetailPerGuest['week'].length > 0) {
        this.retailPerGuestWeek = this.dashboardData.RetailPerGuest['week'][0]['percentage'];
      } else {
        this.retailPerGuestWeek = 0;
      }
      if (this.dashboardData.RetailPerGuest['month'].length > 0) {
        this.retailPerGuestMonth = this.dashboardData.RetailPerGuest['month'][0]['percentage'];
      } else {
        this.retailPerGuestMonth = 0;
      }
      if (this.dashboardData.RetailPerGuest['year'].length > 0) {
        this.retailPerGuestYear = this.dashboardData.RetailPerGuest['year'][0]['percentage'];
      } else {
        this.retailPerGuestYear = 0;
      }
      if (this.dashboardData.RetailToService['day'].length > 0) {
        this.retailToServiceDay['serviceTotal'] = +parseFloat(this.dashboardData.RetailToService['day'][0]['serviceTotal']).toFixed(2);
        this.retailToServiceDay['productTotal'] = +parseFloat(this.dashboardData.RetailToService['day'][0]['productTotal']).toFixed(2);
        this.retailToServiceDay['othersTotal'] = +parseFloat(this.dashboardData.RetailToService['day'][0]['othersTotal']).toFixed(2);
        this.retailServiceDay();
      } else {
        this.retailToServiceDay['serviceTotal'] = 0;
        this.retailToServiceDay['productTotal'] = 0;
        this.retailToServiceDay['othersTotal'] = 0;
        this.retailServiceDay();
      }
      if (this.dashboardData.RetailToService['week'].length > 0) {
        this.retailToServiceWeek['serviceTotal'] = +parseFloat(this.dashboardData.RetailToService['week'][0]['serviceTotal']).toFixed(2);
        this.retailToServiceWeek['productTotal'] = +parseFloat(this.dashboardData.RetailToService['week'][0]['productTotal']).toFixed(2);
        this.retailToServiceWeek['othersTotal'] = +parseFloat(this.dashboardData.RetailToService['week'][0]['othersTotal']).toFixed(2);
        this.retailServiceWeek();
      } else {
        this.retailToServiceWeek['serviceTotal'] = 0;
        this.retailToServiceWeek['productTotal'] = 0;
        this.retailToServiceWeek['othersTotal'] = 0;
        this.retailServiceWeek();
      }
      if (this.dashboardData.RetailToService['month'].length > 0) {
        this.retailToServiceMonth['serviceTotal'] = +parseFloat(this.dashboardData.RetailToService['month'][0]['serviceTotal']).toFixed(2);
        this.retailToServiceMonth['productTotal'] = +parseFloat(this.dashboardData.RetailToService['month'][0]['productTotal']).toFixed(2);
        this.retailToServiceMonth['othersTotal'] = +parseFloat(this.dashboardData.RetailToService['month'][0]['othersTotal']).toFixed(2);
        this.retailServiceMonth();
      } else {
        this.retailToServiceMonth['serviceTotal'] = 0;
        this.retailToServiceMonth['productTotal'] = 0;
        this.retailToServiceMonth['othersTotal'] = 0;
        this.retailServiceMonth();
      }
      if (this.dashboardData.RetailToService['year'].length > 0) {
        this.retailToServiceYear['serviceTotal'] = +parseFloat(this.dashboardData.RetailToService['year'][0]['serviceTotal']).toFixed(2);
        this.retailToServiceYear['productTotal'] = +parseFloat(this.dashboardData.RetailToService['year'][0]['productTotal']).toFixed(2);
        this.retailToServiceYear['othersTotal'] = +parseFloat(this.dashboardData.RetailToService['year'][0]['othersTotal']).toFixed(2);
        this.retailServiceYear();
      } else {
        this.retailToServiceYear['serviceTotal'] = 0;
        this.retailToServiceYear['productTotal'] = 0;
        this.retailToServiceYear['othersTotal'] = 0;
        this.retailServiceYear();
      }
      if (this.dashboardData.ServiceSales['day'].length > 0) {
        this.serviceSalesDay = +parseFloat(this.dashboardData.ServiceSales['day'][0]['serviceSales']).toFixed(2);
      } else {
        this.serviceSalesDay = 0;
      }
      if (this.dashboardData.ServiceSales['week'].length > 0) {
        for (let i = 0; i < this.dashboardData.ServiceSales['week'].length; i++) {
          this.serviceSaleWeekDate.push(data['result'].ServiceSales['week'][i]['serviceDate']);
          this.serviceSaleWeekTotal.push(data['result'].ServiceSales['week'][i]['serviceTotal']);
          this.serviceSalesWeek();
        }
        this.sumOfServiceSalesWk = this.serviceSaleWeekTotal.reduce((p, q) => p + q, 0);
      } else {
        this.sumOfServiceSalesWk = 0;
        this.serviceSalesWeek();
      }
      if (this.dashboardData.ServiceSales['month'].length > 0) {
        this.serviceSalesMonth = +parseFloat(this.dashboardData.ServiceSales['month'][0]['serviceSales']).toFixed(2);
      } else {
        this.serviceSalesMonth = 0;
      }
      if (this.dashboardData.ServiceSales['year'].length > 0) {
        this.serviceSalesYear = +parseFloat(this.dashboardData.ServiceSales['year'][0]['serviceSales']).toFixed(2);
      } else {
        this.serviceSalesYear = 0;
      }
      if (this.dashboardData.TopWorkerProductSales['day'].length > 0) {
        this.tpWrkrPrdImg = this.dashboardData.TopWorkerProductSales['day'][0]['image'] ? true : false;
        this.topProductWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerProductSales['day'][0]['image'] + '?time=' + new Date().getTime();
        this.topProductWorkerName = this.dashboardData.TopWorkerProductSales['day'][0]['workerName'];
      } else {
        this.topProductWorkerImage = {};
        this.topProductWorkerName = '';
      }
      if (this.dashboardData.TopWorkerServiceSales['day'].length > 0) {
        this.tpWrkrSrvImg = this.dashboardData.TopWorkerServiceSales['day'][0]['image'] ? true : false;
        this.topServiceWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerServiceSales['day'][0]['image'] + '?time=' + new Date().getTime();
        this.topServiceWorkerName = this.dashboardData.TopWorkerServiceSales['day'][0]['workerName'];
      } else {
        this.topServiceWorkerImage = {};
        this.topServiceWorkerName = '';
      }
      if (this.dashboardData.TopWorkerProductSales['week'].length > 0) {
        this.tpWrkrPrdWkImg = this.dashboardData.TopWorkerProductSales['week'][0]['image'] ? true : false;
        this.topProductWkWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerProductSales['week'][0]['image'] + '?time=' + new Date().getTime();
        this.topProductWkWorkerName = this.dashboardData.TopWorkerProductSales['week'][0]['workerName'];
      } else {
        this.topProductWkWorkerImage = {};
        this.topProductWkWorkerName = '';
      }
      if (this.dashboardData.TopWorkerServiceSales['week'].length > 0) {
        this.tpWrkrSrvWkImg = this.dashboardData.TopWorkerServiceSales['week'][0]['image'] ? true : false;
        this.topServiceWkWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerServiceSales['week'][0]['image'] + '?time=' + new Date().getTime();
        this.topServiceWkWorkerName = this.dashboardData.TopWorkerServiceSales['week'][0]['workerName'];
      } else {
        this.topServiceWkWorkerImage = {};
        this.topServiceWkWorkerName = '';
      }
      if (this.dashboardData.TopWorkerProductSales['month'].length > 0) {
        this.tpWrkrPrdMnthImg = this.dashboardData.TopWorkerProductSales['month'][0]['image'] ? true : false;
        this.topProductMnthWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerProductSales['month'][0]['image'] + '?time=' + new Date().getTime();
        this.topProductMnthWorkerName = this.dashboardData.TopWorkerProductSales['month'][0]['workerName'];
      } else {
        this.topProductMnthWorkerImage = {};
        this.topProductMnthWorkerName = '';
      }
      if (this.dashboardData.TopWorkerServiceSales['month'].length > 0) {
        this.tpWrkrSrvMnthImg = this.dashboardData.TopWorkerServiceSales['month'][0]['image'] ? true : false;
        this.topServiceMnthWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerServiceSales['month'][0]['image'] + '?time=' + new Date().getTime();
        this.topServiceMnthWorkerName = this.dashboardData.TopWorkerServiceSales['month'][0]['workerName'];
      } else {
        this.topServiceMnthWorkerImage = {};
        this.topServiceMnthWorkerName = '';
      }
      if (this.dashboardData.TopWorkerProductSales['year'].length > 0) {
        this.tpWrkrPrdYrImg = this.dashboardData.TopWorkerProductSales['year'][0]['image'] ? true : false;
        this.topProductYrWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerProductSales['year'][0]['image'] + '?time=' + new Date().getTime();
        this.topProductYrWorkerName = this.dashboardData.TopWorkerProductSales['year'][0]['workerName'];
      } else {
        this.topProductYrWorkerImage = {};
        this.topProductYrWorkerName = '';
      }
      if (this.dashboardData.TopWorkerServiceSales['year'].length > 0) {
        this.tpWrkrSrvYrImg = this.dashboardData.TopWorkerServiceSales['year'][0]['image'] ? true : false;
        this.topServiceYrWorkerImage.srcURL = config.S3_URL + this.dashboardData.TopWorkerServiceSales['year'][0]['image'] + '?time=' + new Date().getTime();
        this.topServiceYrWorkerName = this.dashboardData.TopWorkerServiceSales['year'][0]['workerName'];
      } else {
        this.topServiceYrWorkerImage = {};
        this.topServiceYrWorkerName = '';
      }
      if (this.dashboardData.BookedTicketd['day'].length > 0) {
        this.bookedTicketedBooked = +parseFloat(this.dashboardData.BookedTicketd['day'][0]['Booked']).toFixed(2);
        this.bookedTicketedCheckedIn = +parseFloat(this.dashboardData.BookedTicketd['day'][0]['CheckedIn']).toFixed(2);
        this.bookedTicketed = +parseFloat(this.dashboardData.BookedTicketd['day'][0]['Ticketed']).toFixed(2);
        this.bookTicketedDay();
      } else {
        this.bookedTicketed = 0;
        this.bookedTicketedCheckedIn = 0;
        this.bookedTicketedBooked = 0;
        this.bookTicketedDay();
      }
      if (this.dashboardData.BookedTicketd['week'].length > 0) {
        this.bookedTicketedBookedWeek = +parseFloat(this.dashboardData.BookedTicketd['week'][0]['Booked']).toFixed(2);
        this.bookedTicketedCheckedInWeek = +parseFloat(this.dashboardData.BookedTicketd['week'][0]['CheckedIn']).toFixed(2);
        this.bookedTicketedWeek = +parseFloat(this.dashboardData.BookedTicketd['week'][0]['Ticketed']).toFixed(2);
        this.bookTicketedWeek();
      } else {
        this.bookedTicketedBookedWeek = 0;
        this.bookedTicketedCheckedInWeek = 0;
        this.bookedTicketedWeek = 0;
        this.bookTicketedWeek();
      }
      if (this.dashboardData.BookedTicketd['month'].length > 0) {
        this.bookedTicketedBookedMonth = +parseFloat(this.dashboardData.BookedTicketd['month'][0]['Booked']).toFixed(2);
        this.bookedTicketedCheckedInMonth = +parseFloat(this.dashboardData.BookedTicketd['month'][0]['CheckedIn']).toFixed(2);
        this.bookedTicketedMonth = +parseFloat(this.dashboardData.BookedTicketd['month'][0]['Ticketed']).toFixed(2);
        this.bookTicketedMonth();
      } else {
        this.bookedTicketedBookedMonth = 0;
        this.bookedTicketedCheckedInMonth = 0;
        this.bookedTicketedBookedMonth = 0;
        this.bookTicketedMonth();
      }
      if (this.dashboardData.BookedTicketd['year'].length > 0) {
        this.bookedTicketedBookedYear = +parseFloat(this.dashboardData.BookedTicketd['year'][0]['Booked']).toFixed(2);
        this.bookedTicketedCheckedInYear = +parseFloat(this.dashboardData.BookedTicketd['year'][0]['CheckedIn']).toFixed(2);
        this.bookedTicketedYear = +parseFloat(this.dashboardData.BookedTicketd['year'][0]['Ticketed']).toFixed(2);
        this.bookTicketedYear();
      } else {
        this.bookedTicketedBookedYear = 0;
        this.bookedTicketedCheckedInYear = 0;
        this.bookedTicketedYear = 0;
        this.bookTicketedYear();
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  bookDay() {
    const bookedDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#faff00', // exploded plot from pie
        '#d3d3d3'],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: { fontWeight: 'bold', color: 'white' }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedDayPer
          },
          {
            name: '',
            y: 100 - this.bookedDayPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedDayChart = bookedDayChart;

  }
  bookWeek() {
    const bookedWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#faff00', // exploded plot from pie
        '#d3d3d3'],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: { fontWeight: 'bold', color: 'white' }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedWeekPer
          },
          {
            name: '',
            y: 100 - this.bookedWeekPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedWeekChart = bookedWeekChart;
  }
  bookMonth() {
    const bookedMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#faff00', // exploded plot from pie
        '#d3d3d3'],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: { fontWeight: 'bold', color: 'white' }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedMonthPer
          },
          {
            name: '',
            y: 100 - this.bookedMonthPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedMonthChart = bookedMonthChart;
  }
  bookYear() {
    const bookedYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#faff00', // exploded plot from pie
        '#d3d3d3'],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: { fontWeight: 'bold', color: 'white' }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedYearPer
          },
          {
            name: '',
            y: 100 - this.bookedYearPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedYearChart = bookedYearChart;
  }
  bookOnlineDay() {
    const bookOnlineDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#70faff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedOnlineDayPer
          },
          {
            name: '',
            y: 100 - this.bookedOnlineDayPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookOnlineDayChart = bookOnlineDayChart;
  }
  bookOnlineWeek() {
    const bookedOnlineWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#70faff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedOnlineWeekPer
          },
          {
            name: '',
            y: 100 - this.bookedOnlineWeekPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedOnlineWeekChart = bookedOnlineWeekChart;
  }
  bookOnlineMonth() {
    const bookedOnlineMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#70faff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedOnlineMonthPer
          },
          {
            name: '',
            y: 100 - this.bookedOnlineMonthPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedOnlineMonthChart = bookedOnlineMonthChart;
  }
  bookOnlineYear() {
    const bookedOnlineYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#70faff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.bookedOnlinePer
          },
          {
            name: '',
            y: 100 - this.bookedOnlinePer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.bookedOnlineYearChart = bookedOnlineYearChart;
  }
  rebookDay() {
    const rebookDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: ['#ff1ddb', // exploded plot from pie
        '#d3d3d3'],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: { fontWeight: 'bold', color: 'white' }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.rebookedDay
          },
          {
            name: '',
            y: 100 - this.rebookedDay,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.rebookDayChart = rebookDayChart;

  }
  rebookWeek() {
    const rebookWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: [
        '#ff1ddb', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: {
              fontWeight: 'bold', color: 'white',
            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.rebookedWeek
          },
          {
            name: '',
            y: 100 - this.rebookedWeek,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.rebookWeekChart = rebookWeekChart;
  }
  rebookMonth() {
    const rebookMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: [
        '#ff1ddb', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: {
              fontWeight: 'bold', color: 'white',
            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.rebookedMonth
          },
          {
            name: '',
            y: 100 - this.rebookedMonth,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.rebookMonthChart = rebookMonthChart;
  }
  rebookYear() {
    const rebookYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: { text: '', style: { display: 'none', background: 'none' } },
      credits: { enabled: false },
      colors: [
        '#ff1ddb', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true, cursor: 'pointer',
          dataLabels: {
            enabled: true, distance: -50,
            style: {
              fontWeight: 'bold', color: 'white',
            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.rebookedYear
          },
          {
            name: '',
            y: 100 - this.rebookedYear,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.rebookYearChart = rebookYearChart;
  }
  newClientDay() {
    const newClientDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#c488ff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.newClientDayPer
          },
          {
            name: '',
            y: 100 - this.newClientDayPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.newClientDayChart = newClientDayChart;
  }
  newClientWeek() {
    const newClientWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie', plotBorderWidth: 0 },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#c488ff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.newClientWeekPer
          },
          {
            name: '',
            y: 100 - this.newClientWeekPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.newClientWeekChart = newClientWeekChart;
  }
  newClientMonth() {
    const newClientMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie', plotBorderWidth: 0 },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#c488ff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          borderWidth: 0,
          size: '100%'
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.newClientMonthPer
          },
          {
            name: '',
            y: 100 - this.newClientMonthPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.newClientMonthChart = newClientMonthChart;
  }
  newClientYear() {
    const newclientYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: {
        enabled: false
      },
      colors: [
        '#c488ff', // exploded plot from pie
        '#d3d3d3'
      ],
      tooltip: {
        pointFormat: '{series.name}: <br>{point.percentage:.2f}%'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',

            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      series: [{
        name: 'Percentage',
        data: [
          {
            name: '',
            y: this.newClientYearPer
          },
          {
            name: '',
            y: 100 - this.newClientYearPer,
            sliced: true,
            selected: false
          }
        ],
      }]
    });
    this.newClientYearChart = newclientYearChart;
  }
  bookTicketedDay() {
    const bookedTicketedDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#00ff3f', '#38b6ff'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.bookedTicketed
          },
          {
            name: '',
            y: this.bookedTicketedCheckedIn
          },
          {
            name: '',
            y: this.bookedTicketedBooked
          }],
      }]
    });
    this.bookedTicketedDayChart = bookedTicketedDayChart;
  }
  bookTicketedWeek() {
    const bookedTicketedWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#00ff3f', '#38b6ff'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.bookedTicketedBookedWeek
          },
          {
            name: '',
            y: this.bookedTicketedCheckedInWeek
          },
          {
            name: '',
            y: this.bookedTicketedWeek
          }],
      }]
    });
    this.bookedTicketedWeekChart = bookedTicketedWeekChart;
  }
  bookTicketedMonth() {
    const bookedTicketedMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#00ff3f', '#38b6ff'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.bookedTicketedMonth
          },
          {
            name: '',
            y: this.bookedTicketedCheckedInMonth
          },
          {
            name: '',
            y: this.bookedTicketedBookedMonth
          }],
      }]
    });
    this.bookedTicketedMonthChart = bookedTicketedMonthChart;

  }
  bookTicketedYear() {
    const bookedTicketedYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#00ff3f', '#38b6ff'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.bookedTicketedYear
          },
          {
            name: '',
            y: this.bookedTicketedCheckedInYear
          },
          {
            name: '',
            y: this.bookedTicketedBookedYear
          }],
      }]
    });
    this.bookedTicketedYearChart = bookedTicketedYearChart;
  }
  retailServiceDay() {
    const retailToServiceDayChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#ffa51d', '#ff1ddb'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.retailToServiceDay.othersTotal
          },
          {
            name: '',
            y: this.retailToServiceDay.productTotal
          },
          {
            name: '',
            y: this.retailToServiceDay.serviceTotal
          }],
      }]
    });
    this.retailToServiceDayChart = retailToServiceDayChart;
  }
  retailServiceWeek() {
    const retailToServiceWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#ffa51d', '#ff1ddb'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.retailToServiceWeek.othersTotal
          },
          {
            name: '',
            y: this.retailToServiceWeek.productTotal
          },
          {
            name: '',
            y: this.retailToServiceWeek.serviceTotal
          }],
      }]
    });
    this.retailToServiceWeekChart = retailToServiceWeekChart;
  }
  retailServiceMonth() {
    const retailToServiceMonthChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#ffa51d', '#ff1ddb'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.retailToServiceMonth.othersTotal
          },
          {
            name: '',
            y: this.retailToServiceMonth.productTotal
          },
          {
            name: '',
            y: this.retailToServiceMonth.serviceTotal
          }],
      }]
    });
    this.retailToServiceMonthChart = retailToServiceMonthChart;
  }
  retailServiceYear() {
    const retailToServiceYearChart = new Chart({
      chart: { backgroundColor: 'none', type: 'pie' },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      tooltip: {
        pointFormat: '{series.name}: <br>{point.y:.2f}'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white',
            }
          },
          size: '100%',
          borderWidth: 0
        }
      },
      colors: ['#d9d9d8', '#ffa51d', '#ff1ddb'],
      series: [{
        name: 'Value',
        size: '100%',
        innerSize: '40%',
        data: [
          {
            name: '',
            y: this.retailToServiceYear.othersTotal
          },
          {
            name: '',
            y: this.retailToServiceYear.productTotal
          },
          {
            name: '',
            y: this.retailToServiceYear.serviceTotal
          }],
      }]
    });
    this.retailToServiceYearChart = retailToServiceYearChart;
  }
  productSalesWeek() {
    const productSaleWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'column', inverted: false },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      colors: [
        '#ffa51d', // exploded plot from pie
      ],
      xAxis: {
        labels: {
          style: {
            color: 'white'
          },
        },
        categories: this.productSaleWeekDate
      },
      yAxis: {
        gridLineColor: '#505050',
        labels: {
          format: '${value}',
          style: {
            color: 'white'
          },
        },
        title: {
          style: {
            color: 'white'
          },
          // text: 'Total Retail Sales'
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        column: {
          borderWidth: 0,
        }
      },
      series: [{
        pointWidth: 20,
        name: 'product sales',
        data: this.productSaleWeekTotal
      }],
    });
    this.productSaleWeekChart = productSaleWeekChart;
  }
  serviceSalesWeek() {
    const serviceSaleWeekChart = new Chart({
      chart: { backgroundColor: 'none', type: 'column', inverted: false },
      title: { text: '', style: { display: 'none' } },
      subtitle: {
        text: '',
        style: {
          display: 'none',
          background: 'none'
        }
      },
      credits: { enabled: false },
      colors: [
        '#ff1ddb', // exploded plot from pie
      ],
      xAxis: {
        labels: {
          style: {
            color: 'white'
          },
        },
        categories: this.serviceSaleWeekDate
      },
      yAxis: {
        gridLineColor: '#505050',
        labels: {
          format: '${value}',
          style: {
            color: 'white'
          },
        },
        title: {
          style: {
            color: 'white'
          },
          // text: 'Total Service Sales',
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: 'white'
          },
        }
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        column: {
          borderWidth: 0,
        }
      },
      series: [{
        pointWidth: 20,
        name: 'service sales',
        data: this.serviceSaleWeekTotal
      }],
    });
    this.serviceSaleWeekChart = serviceSaleWeekChart;
  }
  mainSelectChange(value) {
    this.worker = value;
    this.getDashboardNewDetails();
  }
}
