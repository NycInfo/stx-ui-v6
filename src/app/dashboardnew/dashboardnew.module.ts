import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashBoardnewComponent } from './dashboardnew.component';
import { DashBoardnewRoutingModule } from './dashboardnew.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { ChartModule } from 'angular-highcharts';
// import { ChartModule } from '../../custommodules/primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        DashBoardnewRoutingModule,
        ShareModule,
        ChartModule
    ],
    declarations: [
        DashBoardnewComponent
    ]
})
export class DashBoardnewModule {
}
