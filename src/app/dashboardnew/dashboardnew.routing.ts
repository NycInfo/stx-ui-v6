import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashBoardnewComponent } from './dashboardnew.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: DashBoardnewComponent,
                children: [
                    {
                        path: '',
                        component: DashBoardnewComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class DashBoardnewRoutingModule {
}
