/*
ngOnInit(): Method used for getting onload data.
getProductLines():Method used to get productline data.
onServiceGroupChange(value): Method to change inventory group value.
onProductLineChange(value): Method to change product line values.
InventorySortOptions(): Method for getting sort options.
getProductsBySearch(): Method for searching product data.
updateProducts(): Method to update the product data.
getInventoryGroups(): Method to get inventory group data.
getviewOption(): Method to get view options
 */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ManageInventoryService } from './manageinventory.service';
import { DecimalPipe } from '@angular/common';
@Component({
    selector: 'app-home-popup',
    templateUrl: './manageinventory.html',
    styleUrls: ['./manageinventory.css'],
    providers: [ManageInventoryService]
})
export class ManageInventoryComponent implements OnInit {
    inventoryGroupsData: any;
    productLinesData: any;
    inventorySortingdata: any;
    productsList = [];
    inventoryGroup = 'All';
    productLine = 'All';
    sortOption: any;
    viewOption = 'All';
    inActive = false;
    viewOptionData: any = [];
    error: any;
    noResultMsg: any;
    noResult: any = false;
    InventoryErr = '';
    showButton: any = false;
    list: any;
    constructor(private manageInventoryService: ManageInventoryService,
        private route: ActivatedRoute,
        private router: Router) {
    }
    /*method to get onload data */
    ngOnInit() {
        this.getInventoryGroups();
        this.getProductLines();
        this.InventorySortOptions();
        this.getviewOption();
        localStorage.removeItem('list');
    }
    /*Method used to get productline data */
    getProductLines() {
        this.manageInventoryService.getProductLinesData().subscribe(
            data => {
                this.productLinesData = data['result'].filter(
                    filterList => filterList.Active__c === 1);
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }
    /*Method to change inventorygroup value */
    onServiceGroupChange(value) {
        this.inventoryGroup = value;
    }
    /*Method to change product line value */
    onProductLineChange(value) {
        this.productLine = value;
    }
    /*Method to get sort options */
    InventorySortOptions() {
        this.manageInventoryService.getInventorySortOptions().subscribe(
            data => {
                this.inventorySortingdata = data['inventorySortOptions'];
                this.sortOption = this.inventorySortingdata[0].type;
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }
    /* Mehtod to search product data */
    getProductsBySearch() {
        const searchObj = {
            'inventoryGroup': this.inventoryGroup,
            'productLine': this.productLine,
            'viewOption': this.viewOption,
            'sortOption': this.sortOption,
            'inActive': this.inActive
        };
        this.productsList = [];
        this.manageInventoryService.productsSearch(searchObj).subscribe(
            data => {
                if (data.result.length === 0) {
                    this.noResultMsg = '** No Products Found with this Criteria **';
                    this.productsList = data['result'];
                    this.noResult = true;
                    this.showButton = false;
                } else {
                    this.productsList = data['result'];
                    localStorage.setItem('list', JSON.stringify(this.productsList));
                    this.list = this.productsList[0];
                    this.noResult = false;
                    this.showButton = true;
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        break;
                }
            }
        );
    }/*Method to update product data */
    qntyChangdValues() {
        const lists = JSON.parse(localStorage.getItem('list'));
        for (let j = 0; j < this.productsList.length; j++) {
            for (let k = 0; k < lists.length; k++) {
                if (this.productsList[j].Id === lists[k].Id) {
                    this.productsList[j].ondiff = Number(this.productsList[j].Quantity_On_Hand__c) - Number(lists[k].Quantity_On_Hand__c);
                    this.productsList[j]['isChanged'] = this.productsList[j].ondiff !== 0;
                }
            }
        }
    }
    updateProducts() {
        this.qntyChangdValues();
        const NUM_REGEXP = /^\d+(\.\d{1,2})?$/;
        for (let i = 0; i < this.productsList.length; i++) {
            if (this.productsList[i]['Product_Code__c'] === '') {
                this.InventoryErr = 'MANAGE_INVENTORY.SKU_REQUIRED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Name'] === '') {
                this.InventoryErr = 'MANAGE_INVENTORY.PRODUCT_NAME_REQUIRED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Size__c'] === '' || this.productsList[i]['Size__c'] < 0) {
                this.InventoryErr = 'MANAGE_INVENTORY.POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Size__c'] && !NUM_REGEXP.test(this.productsList[i]['Size__c'])) {
                this.InventoryErr = 'MANAGE_INVENTORY.POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Minimum_Quantity__c'] < 0) {
                this.InventoryErr = 'MANAGE_INVENTORY.MIN_QTY_POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Supplier_Minimum__c'] < 0) {
                this.InventoryErr = 'MANAGE_INVENTORY.SUPPLIER_MIN_POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Standard_Cost__c'] < 0) {
                this.InventoryErr = 'MANAGE_INVENTORY.STANDARD_COST_POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            } else if (this.productsList[i]['Price__c'] < 0) {
                this.InventoryErr = 'MANAGE_INVENTORY.PRICE_POSITIVE_NUMBERS_ALLOWED';
                window.scrollTo(0, 200);
            }
        }
        if (!this.InventoryErr) {
            const updateProducts = this.productsList;
            this.manageInventoryService.saveProductData(updateProducts).subscribe(
                data => {
                    const saveproducts = data['result'];
                    localStorage.removeItem('list');
                    this.router.navigate(['/inventory']);
                },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            if (statuscode === '2085' || statuscode === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                            /* Case:400 error is for checking sku */
                            this.error = 'MANAGE_INVENTORY.ERROR';
                            window.scrollTo(0, 200);
                            break;
                    }
                }
            );
        }

    }/* Method to get inventory groups data */
    getInventoryGroups() {
        this.manageInventoryService.getInventoryGroupsData().subscribe(
            data => {
                if (data['result'].length > 0) {
                    this.inventoryGroupsData = data['result'];
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        break;
                }
            }
        );
    }
    /* Method is used to getting the view options */
    getviewOption() {
        this.manageInventoryService.getViewOptions().subscribe(
            data => {
                this.viewOptionData = data['viewOptions'];

            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /* method to restrict charecters  */
    keyPress(event: any) {
        const pattern = /^[a-zA-Z0-9]*$/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
    keyPress1(event: any) {
        const pattern = /^[0-9-]*$/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
    numDecimal(event: any) {
        const pattern = /^[0-9.]*$/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
    clearErrorMsgs() {
        this.error = '';
        this.InventoryErr = '';
    }
    cancel() {
        this.clearErrorMsgs();
    }
}
