import { Component, ElementRef, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import * as config from '../../app.config';

import { MarketingEmailService } from './marketingemail.service';

@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './marketingemail.html',
  styleUrls: ['./marketingemail.component.css'],
  providers: [MarketingEmailService]
})
export class MarketingEmailComponent implements OnInit {

  constructor(private serv: MarketingEmailService,
    private route: Router,
    private hostElement: ElementRef, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    const iframe = this.hostElement.nativeElement.querySelector('iframe');
    this.serv.getEMURL().subscribe(data => {
      if (data['result']) {
        iframe.src = data['result'];
      } else {
        iframe.src = config.SALONCLOUDS_PLUS;
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.route.url !== '/') {
            localStorage.setItem('page', this.route.url);
            this.route.navigate(['/']).then(() => { });
          }
        }
      });
  }

  // salonPlusURL(): SafeResourceUrl {
  //   return this.sanitizer.bypassSecurityTrustResourceUrl(config.SALONCLOUDS_PLUS);
  // }

}
