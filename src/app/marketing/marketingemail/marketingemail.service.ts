import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '../../common/http-client';

@Injectable()
export class MarketingEmailService {
    constructor(private http: HttpClient,
        @Inject('apiEndPoint') private apiEndPoint: string,
    ) { }

    getEMURL() {
        return this.http.get(this.apiEndPoint + '/api/em/url')
            .map(this.extractData);
    }
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
          }
        const body = res.json();
        return body || {};
    }


}
