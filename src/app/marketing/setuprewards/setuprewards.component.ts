import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { SetupRewardsService } from './setuprewards.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { FormBuilder, FormControl, FormGroup, FormArray } from '@angular/forms';
import { CommonService } from '../../common/common.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './setuprewards.html',
  styleUrls: ['./setuprewards.css'],
  providers: [SetupRewardsService, CommonService],
})
export class SetupRewardsComponent implements OnInit {
  datePickerConfig: any;
  decodedUserToken: any;
  constructor(private route: ActivatedRoute,
    private router: Router, private builder: FormBuilder,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private setupPromotionsService: SetupRewardsService,
    private commonService: CommonService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });

  }
  rewardForm: FormGroup = this.builder.group({
    active_c: new FormControl(''),
    name: new FormControl(''),
    award_rules__c: new FormControl(''),
    createdbyid: new FormControl(''),
    createddate: new FormControl(''),
    id: new FormControl(''),
    isDeleted: new FormControl(''),
    lastmodifiedById: new FormControl(''),
    lastModifiedDate: new FormControl(''),
    ownerId: new FormControl(''),
    redeem_rules__c: new FormControl(''),
    systemmodstamp: new FormControl(''),
    awardpoint: new FormControl('')
  });
  rewardDataList: any;
  bsValue = new Date();
  awardArrayData = [];
  rewardData = [];
  AwardRulesLineerror = [];
  AwardRulesitemerror = [];
  datebeginitemerror = [];
  redeemArrayData = [];
  itemerror = [];
  isDuplicate: any;
  reedemnameerror = '';
  idx = 0;
  dateBeginError = [];
  redeemNameError = [];
  saverewardname = '';
  rewardnameerror: any;
  reedemnameerror1: any;
  reedemnameerror2: any;
  AwardRulesLineerror1: any;
  AwardRulesLineerror2: any;
  redeemNameError1: any;
  redeemNameError2: any;
  RedeemRulesLineerror1: any;
  RedeemRulesLineerror2: any;
  RedeemRulesLineerror3: any;
  RedeemRulesLineerror4: any;
  RedeemListError1: any;
  RedeemListError2: any;
  awardpointerror = [];
  goalListError1: any;
  goalListError2: any;
  toastermessage: any;
  rewardLength: any;
  awardList: any = [];
  forevery = [{ value: 'Amount Spent On', name: 'Amount Spent On' }, { value: 'Individual', name: 'Individual' }, { value: 'Completed Ticket', name: 'Completed Ticket' },
  { value: 'Referred Client', name: 'Referred Client' }];
  awardrulesitem = [{ value: 'Services', name: 'Services' }, { value: 'Products', name: 'Products' }, { value: 'Gifts', name: 'Gifts' }];
  discountList = [{ value: 'Amount', name: 'Amount' }, { value: 'Percent', name: 'Percent' }];
  OnOneItemList = [{ value: 'Services', name: 'Service' }, { value: 'Products', name: 'Product' }];
  ngOnInit() {
    try {
      this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedUserToken = {};
    }
    this.getRewardData();
    this.hideSelectDrpDwn(this.forevery[0]['name'], 0);
  }

  getRewardData() {
    this.setupPromotionsService.rewardData().subscribe(
      data => {
        this.rewardDataList = data['result'];
        this.rewardLength = this.rewardDataList.length;
        this.rewardDataList.forEach((element, index) => {
          element.Award_Rules__c = JSON.parse(this.rewardDataList[index].Award_Rules__c);
        });
        this.rewardDataList.forEach((element, index) => {
          element.Redeem_Rules__c = JSON.parse(this.rewardDataList[index].Redeem_Rules__c);
        });
        for (let i = 0; i < this.rewardDataList.length; i++) {
          for (let j = 0; j < this.rewardDataList[i].Award_Rules__c.length; j++) {
            this.rewardDataList[i].Award_Rules__c[j].dispStartDate = this.commonService.getDateFrmDBDateStr(this.rewardDataList[i].Award_Rules__c[j].startDate);
            this.rewardDataList[i].Award_Rules__c[j].dispEndDate = this.commonService.getDateFrmDBDateStr(this.rewardDataList[i].Award_Rules__c[j].endDate);
          }
          for (let j = 0; j < this.rewardDataList[i].Redeem_Rules__c.length; j++) {
            if ((this.rewardDataList[i].Redeem_Rules__c[j].dispStartDate !== null) || (this.rewardDataList[i].Redeem_Rules__c[j].endDate !== null)) {
              this.rewardDataList[i].Redeem_Rules__c[j].dispStartDate = this.commonService.getDateFrmDBDateStr(this.rewardDataList[i].Redeem_Rules__c[j].startDate);
              this.rewardDataList[i].Redeem_Rules__c[j].dispEndDate = this.commonService.getDateFrmDBDateStr(this.rewardDataList[i].Redeem_Rules__c[j].endDate);
            }
          }
        }
        this.rewardfullview(this.rewardDataList[0]);
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  addNew() {
    this.rewardForm.reset();
    this.awardArrayData.push({
      'awardPoints': '', 'item': '', 'dispStartDate': '', 'dispEndDate': '',
      'startDate': '', 'endDate': '', 'forEvery': 'Amount Spent On', 'awardConditionLines': '', 'conditionFilter': '', 'conditionType': '', 'lineNumber': ''
    });
    this.redeemArrayData.push({
      'conditionFilter': '', 'discount': '', 'dispStartDate': '', 'dispEndDate': '', 'startDate': '', 'endDate': '', 'discountType': 'Amount', 'onOneItem': 'Services', 'conditionType': '',
      'lineNumber': '', 'redeemConditionLines': '', 'redeemName': '', 'redeemPoints': ''
    });
    this.rewardForm.patchValue({
      active_c: 1
    });
  }
  hideSelectDrpDwn(award, i) {
    if (award === 'Completed Ticket') {
      this.awardList[i] = [];
    } else if (award === 'Referred Client') {
      this.awardList[i] = [];
    } else if (award === 'Amount Spent On') {
      this.awardList[i] = this.awardrulesitem;
    } else if (award === 'Individual') {
      this.awardList[i] = this.awardrulesitem;
    }
  }

  cancel() {
    this.rewardForm.reset();
    this.awardArrayData = [];
    this.redeemArrayData = [];
    this.rewardnameerror = '';
    this.awardpointerror = [];
    this.AwardRulesLineerror = [];
    this.datebeginitemerror = [];
    this.itemerror = [];
    this.redeemNameError = [];
    this.dateBeginError = [];
    this.reedemnameerror = '';
    this.router.navigate(['marketing']).then(() => { });
  }

  clearErrMsg() {
    this.rewardnameerror = '';
    this.reedemnameerror = '';
  }

  rewardfullview(data) {
    if (this.rewardLength !== 0) {
      // console.log('show data----')
      this.saverewardname = data.Name;
      data.Award_Rules__c.forEach(element => {
        this.awardArrayData.push(element);
      });
      for (let i = 0; i < this.awardArrayData.length; i++) {
        this.hideSelectDrpDwn(this.awardArrayData[i]['forEvery'], i);
      }
      data.Redeem_Rules__c.forEach(element => {
        this.redeemArrayData.push(element);
      });
      for (let i = 0; i < this.awardArrayData.length; i++) {
        if (isNaN(this.awardArrayData[i].dispStartDate) === true) {
          this.awardArrayData[i].dispStartDate = '';
        }
        if (isNaN(this.awardArrayData[i].dispEndDate) === true) {
          this.awardArrayData[i].dispEndDate = '';
        }
      }
      for (let i = 0; i < this.redeemArrayData.length; i++) {
        if (isNaN(this.redeemArrayData[i].dispStartDate) === true) {
          this.redeemArrayData[i].dispStartDate = '';
        }
        if (isNaN(this.redeemArrayData[i].dispEndDate) === true) {
          this.redeemArrayData[i].dispEndDate = '';
        }
      }
      this.rewardForm.patchValue({
        active_c: data.Active__c,
        award_rules__c: this.awardArrayData,
        createdbyid: data.CreatedById,
        createddate: data.CreatedDate,
        id: data.Id,
        isDeleted: data.IsDeleted,
        lastModifiedDate: data.LastModifiedDate,
        lastmodifiedById: data.LastModifiedById,
        name: data.Name,
        ownerId: data.OwnerId,
        redeem_rules__c: this.redeemArrayData,
        systemmodstamp: data.SystemModstamp
      });
    } else if (this.rewardLength === 0) {
      // console.log('fgh createeeeee')
      this.addNew();
    }
  }

  removeDuplicatesError(originalArray, prop) {
    const newArray = [];
    const lookupObject = {};
    for (let i = 0; i < originalArray.length; i++) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (const field of Object.keys(lookupObject)) {
      newArray.push(lookupObject[field]);
    }
    return newArray;
  }

  insertDuplicateData(name) {
    let res = false;
    this.rewardDataList.forEach(element => {
      if (element.Name.toUpperCase().trim() === name.toUpperCase().trim()) {
        res = true;
      }
    });
    return res;
  }
  checkIfNameExists(values) {
    let rownum;
    const valueArr = values.map(function (item) {
      return item.col;
    });
    this.isDuplicate = valueArr.some(function (item, idx) {
      rownum = idx;
      return valueArr.indexOf(item) !== idx;
    });
    this.idx = rownum;
  }
  rewardformsave() {
    this.AwardRulesLineerror = [];
    this.AwardRulesitemerror = [];
    this.datebeginitemerror = [];
    this.itemerror = [];
    this.dateBeginError = [];
    this.redeemNameError = [];
    this.awardpointerror = [];
    let awardpointBollean = false;

    for (let i = 0; i < this.awardArrayData.length; i++) {
      if (![this.awardArrayData[i].awardPoints].every(Boolean) || !this.awardArrayData[i].awardPoints === null) {
        this.awardArrayData[i].awardPoints = 0;
        awardpointBollean = true;
        this.AwardRulesLineerror1 = this.translateService.get('SETUP_REWARDS.AWARD_RULE_LINE');
        this.AwardRulesLineerror2 = this.translateService.get('SETUP_REWARDS.ONLY_POSITIVE_WHOLE_NUMBER_ENTERED');
        this.AwardRulesLineerror.push({ error: this.AwardRulesLineerror1.value + (i + 1) + ':' + this.AwardRulesLineerror2.value, errorBoll: awardpointBollean });
        this.awardpointerror = this.removeDuplicatesError(this.AwardRulesLineerror, 'error');
      } else if ((![this.awardArrayData[i].item].every(Boolean) || this.awardArrayData[i].item === null) &&
        this.awardArrayData[i].forEvery !== 'Referred Client' && this.awardArrayData[i].forEvery !== 'Completed Ticket') {
        this.AwardRulesitemerror.push({ error: 'Award Rules-Line ' + (i + 1) + ': Item is required' });
        this.itemerror = this.removeDuplicatesError(this.AwardRulesitemerror, 'error');
        awardpointBollean = true;
      } else {
        if ((this.awardArrayData[i].dispStartDate !== '' && this.awardArrayData[i].dispStartDate !== null && this.awardArrayData[i].dispEndDate !== ''
          && this.awardArrayData[i].dispEndDate !== null) && (this.awardArrayData[i].dispStartDate > this.awardArrayData[i].dispEndDate)) {
          this.goalListError1 = this.translateService.get('SETUP_REWARDS.AWARD_RULE_LINE');
          this.goalListError2 = this.translateService.get('SETUP_REWARDS.BEGIN_DATE_BEFORE_END_DATE');
          this.datebeginitemerror.push({ error: this.goalListError1.value + (i + 1) + ':' + this.goalListError2.value });
          this.dateBeginError = this.removeDuplicatesError(this.datebeginitemerror, 'error');
          awardpointBollean = true;
        }
        if (this.awardArrayData[i].dispStartDate === '' || this.awardArrayData[i].dispStartDate === null) {
          this.awardArrayData[i].dispStartDate = '';
        }
        if (this.awardArrayData[i].dispEndDate === '' || this.awardArrayData[i].dispEndDate === null) {
          this.awardArrayData[i].dispEndDate = '';
        }
      }
    }

    if (awardpointBollean === false) {
      for (let i = 0; i < this.redeemArrayData.length; i++) {
         this.redeemArrayData[i]['redeemName'] = this.redeemArrayData[i]['redeemName'] ? this.redeemArrayData[i]['redeemName'].trim() : this.redeemArrayData[i]['redeemName'];
         if (![this.redeemArrayData[i].redeemName].every(Boolean) || this.redeemArrayData[i].redeemName === null) {
          this.redeemNameError1 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
          this.redeemNameError2 = this.translateService.get('SETUP_REWARDS.NAME_REQUIRED');
          this.AwardRulesitemerror.push({ error: this.redeemNameError1.value + (i + 1) + ':' + this.redeemNameError2.value });
          this.redeemNameError = this.removeDuplicatesError(this.AwardRulesitemerror, 'error');
          awardpointBollean = true;
        } else {
          if (![this.redeemArrayData[i].redeemPoints].every(Boolean) || this.redeemArrayData[i].redeemPoints === null) {
            this.redeemArrayData[i].redeemPoints = 0;
            this.RedeemRulesLineerror1 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
            this.RedeemRulesLineerror2 = this.translateService.get('SETUP_REWARDS.REDEEM_ONLY_POSITIVE_WHOLE_NUMBER_ENTERED');
            this.AwardRulesitemerror.push({ error: this.RedeemRulesLineerror1.value + (i + 1) + ':' + this.RedeemRulesLineerror2.value });
            this.redeemNameError = this.removeDuplicatesError(this.AwardRulesitemerror, 'error');
            awardpointBollean = true;
            // }else if ((![this.redeemArrayData[i].redeemPoints].every(Boolean) || this.redeemArrayData[i].redeemPoints === null) ){

          } else if (![this.redeemArrayData[i].discount].every(Boolean) || this.redeemArrayData[i].discount === null) {
            this.redeemArrayData[i].discount = 0;
            this.RedeemRulesLineerror3 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
            this.RedeemRulesLineerror4 = this.translateService.get('SETUP_REWARDS.DISCOUNT_ONLY_POSITIVE_NUMBER');
            this.AwardRulesitemerror.push({ error: this.RedeemRulesLineerror3.value + (i + 1) + ':' + this.RedeemRulesLineerror4.value });
            this.redeemNameError = this.removeDuplicatesError(this.AwardRulesitemerror, 'error');
            awardpointBollean = true;
          } else {
            if ((this.redeemArrayData[i].dispStartDate !== '' && this.redeemArrayData[i].dispStartDate !== null && this.redeemArrayData[i].endDate !== ''
              && this.redeemArrayData[i].dispEndDate !== null) && (this.redeemArrayData[i].dispStartDate > this.redeemArrayData[i].dispEndDate)) {
              this.RedeemListError1 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
              this.RedeemListError2 = this.translateService.get('SETUP_REWARDS.BEGIN_DATE_BEFORE_END_DATE');
              this.AwardRulesitemerror.push({ error: this.RedeemListError1.value + (i + 1) + ':' + this.RedeemListError2.value });
              this.redeemNameError = this.removeDuplicatesError(this.AwardRulesitemerror, 'error');
              awardpointBollean = true;
            }
            if (this.redeemArrayData[i].dispStartDate === '' || this.redeemArrayData[i].dispStartDate === null) {
              this.redeemArrayData[i].dispStartDate = '';
            }
            if (this.redeemArrayData[i].dispEndDate === '' || this.redeemArrayData[i].dispEndDate === null) {
              this.redeemArrayData[i].dispEndDate = '';
            }
          }
        }
      }
    }
    this.rewardForm.patchValue({
      award_rules__c: this.awardArrayData,
      redeem_rules__c: this.redeemArrayData,
      active_c: this.rewardForm.value.active_c === null || this.rewardForm.value.active_c === undefined ? 0 : this.rewardForm.value.active_c
    });
    const dicscCheck = this.rewardForm['value']['redeem_rules__c'];
    for (let i = 0; i < dicscCheck.length; i++) {
      if (dicscCheck[i]['discountType'] === 'Percent' && (dicscCheck[i]['discount'] < 0 || dicscCheck[i]['discount'] > 100)) {
        this.reedemnameerror1 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
        this.reedemnameerror2 = this.translateService.get('SETUP_REWARDS.PERCENTAGE_BETWEEN_0_TO_100');
        this.reedemnameerror = this.reedemnameerror1.value + (this.idx + 1) + ':' + this.reedemnameerror2.value;
      }
    }
    const row = [];
    for (let r = 0; r < this.redeemArrayData.length; r++) {
      row.push({ 'col': this.redeemArrayData[r].redeemName, 'ind': r });
      this.checkIfNameExists(row);
    }
    if (this.rewardForm.value.name === '' || this.rewardForm.value.name === null || this.rewardForm.value.name === undefined) {
      this.rewardnameerror = 'VALIDATION_MSG.NAME_REQUIRED';
      window.scrollTo(0, 0);
    } else if (awardpointBollean) {
      awardpointBollean = true;
    } else if (this.isDuplicate) {
      this.reedemnameerror1 = this.translateService.get('SETUP_REWARDS.REDEEM_RULES_LINE');
      this.reedemnameerror2 = this.translateService.get('SETUP_REWARDS.DUPLICATE_NAME_FOUND');
      this.reedemnameerror = this.reedemnameerror1.value + (this.idx + 1) + ':' + this.reedemnameerror2.value;
      // this.reedemnameerror = 'Redeem Rules-Line ' + (this.idx + 1) + ': Duplicate Name found. All Names must be unique.';
    } else {
      if (this.rewardLength === 0 && this.reedemnameerror === '') {
        // console.log('neewwwwwwwwwwwwwwwwwwwww')
        if (this.insertDuplicateData(this.rewardForm.value.name.trim())) {
          this.rewardnameerror = 'SETUP_REWARDS.DUPLICATE_REWARD';
          window.scrollTo(0, 0);
        } else {
          this.rewardDateFormatChange();
          this.setupPromotionsService.postRewardsData(this.rewardForm.value)
            .subscribe(
              data => {
                this.rewardData = data;
                this.rewardForm.reset();
                this.awardArrayData = [];
                this.redeemArrayData = [];
                this.getRewardData();
                this.router.navigate(['marketing']).then(() => {
                  this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_REWARS_SUCCESS');
                  this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                });
              },
              error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                if (statuscode === '2076') {
                  this.rewardnameerror = 'SETUP_REWARDS.DUPLICATE_REWARD';
                  window.scrollTo(0, 0);
                } else if (statuscode === '2085' || statuscode === '2071') {
                  if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                  }
                }
              });
        }
      } else if (this.rewardLength.length !== 0 && this.reedemnameerror === '') {
        const rewardname = this.rewardForm.value.name.trim();
        if (this.saverewardname.toLocaleLowerCase() !== rewardname.toLocaleLowerCase()) {
          if (this.insertDuplicateData(this.rewardForm.value.name)) {
            this.rewardnameerror = 'SETUP_REWARDS.DUPLICATE_REWARD';
            window.scrollTo(0, 0);
          } else {
            this.updaterecord();
          }
        } else {
          this.updaterecord();
        }
      }
    }
  }
  validate() {
    this.reedemnameerror = '';
  }
  rewardDateFormatChange() {
    this.rewardForm.value['award_rules__c'].forEach(element => {
      if (element.dispStartDate) {
        element.startDate = this.commonService.getDBDatStr(element.dispStartDate);
      }
      if (element.dispEndDate) {
        element.endDate = this.commonService.getDBDatStr(element.dispEndDate);
      }
    });
    this.rewardForm.value['redeem_rules__c'].forEach(element => {
      if (element.dispStartDate) {
        element.startDate = this.commonService.getDBDatStr(element.dispStartDate);
      }
      if (element.dispEndDate) {
        element.endDate = this.commonService.getDBDatStr(element.dispEndDate);
      }
    });
  }
  updaterecord() {
    const temp = this.rewardForm.value['award_rules__c'];
    for (let i = 0; i < temp.length; i++) {
      if (temp[i]['forEvery'] === 'Completed Ticket' || temp[i]['forEvery'] === 'Referred Client') {
        temp[i]['item'] = '';
      }
    }
    this.rewardDateFormatChange();
    this.setupPromotionsService.updateRewardsData(this.rewardForm.value)
      .subscribe(
        data => {
          this.rewardData = data;
          this.getRewardData();
          this.rewardForm.reset();
          this.awardArrayData = [];
          this.redeemArrayData = [];
          this.reedemnameerror = '';
          this.router.navigate(['marketing']).then(() => {
            this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_REWARS_SUCCESS');
            this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
          });
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  addInput() {
    this.awardArrayData.push({
      'awardPoints': '', 'item': 'Services', 'startDate': '', 'endDate': '', 'forEvery': 'Amount Spent On',
       'awardConditionLines': '', 'conditionFilter': '', 'conditionType': '', 'lineNumber': ''});
    for (let i = 0; i < this.awardArrayData.length; i++) {
      this.hideSelectDrpDwn(this.awardArrayData[i]['forEvery'], i);
    }

  }
  addRedeemInput() {
    this.redeemArrayData.push({
      'conditionFilter': '', 'discount': '', 'startDate': '', 'endDate': '', 'discountType': 'Amount', 'onOneItem': 'Services',
      'conditionType': '', 'lineNumber': '', 'redeemConditionLines': '', 'redeemName': '', 'redeemPoints': ''
    });
  }
  deleteFieldValue(i) {
    this.awardArrayData.splice(i, 1);
  }
  deleteRedeemField(i) {
    this.redeemArrayData.splice(i, 1);
  }
  openUpgradeOption() {
    this.toastermessage = this.translateService.get('Please upgrade your package to perform this action');
    this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
  }
}
