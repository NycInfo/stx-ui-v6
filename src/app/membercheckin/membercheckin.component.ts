/**
 * this.saveMember()        - Membership save new client
 * this.existingMemebership - Membership existing clinet save
 * this.successAudio & this.badAudio    - For sound.
 * this.searchMemberId() & this.checkValidMember();   - search for memberid and give sound according to their membership plan whether plan expire or not.
 */


import {
  Component, ViewContainerRef, ViewEncapsulation, OnInit, ViewChild, OnDestroy,
  AfterViewInit, Inject, Output, EventEmitter, Directive, HostListener, ElementRef, NgZone
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { validateConfig } from '@angular/router/src/config';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { MembercheckinService } from './membercheckin.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../common/common.service';
// import { audios } from '../../assets/audio/success.mp3';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { JwtHelper } from 'angular2-jwt';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { parse } from 'url';
import { DecimalPipe } from '@angular/common';
import * as config from '../app.config';
import { Md5 } from 'ts-md5/dist/md5';
declare let $: any;
declare var swal: any;


@Component({
  selector: 'app-membercheckin',
  templateUrl: './membercheckin.html',
  styleUrls: ['./membercheckin.component.css'],
  providers: [MembercheckinService, CommonService],
})

export class MembercheckinComponent implements OnInit {
  public searchField = new FormControl();
  public datePickerDate: any;
  public getWorker: any = [];
  public bsValue: any;
  public bsValue1: any = new Date();
  existingBirthday = '';
  birthdayDate = '';
  autoList = [];
  cSearch: any;
  imgPath = config.S3_URL;
  hideTable = false;
  memberShipDetails: any;
  showIcon = 0;
  errMsg = '';
  memberSearch: any;
  datePickerConfig: any;
  newdate: any = new Date();
  firstName: any = '';
  lastName: any = '';
  birthday: any = '';
  gender: any = '';
  primaryEmail: any = '';
  memUniId: any = '';
  mobileNumber: any = '';
  sms_checkbox: any = '';
  existingSms_checkbox: any = '';
  MemberShipType: any = '';
  MemberShipTypeError: any = '';
  paymentTypesError: any = '';
  MembersipsList = [];
  refPaymentId: any = '';
  charge: any = '';
  chargeError: any = '';
  cardNumber: any = '';
  NameOnCard: any = '';
  checkBox = false;
  error: any = '';
  cvv: any = '';
  monthList = ['01 - January', '02 - February', '03 - March', '04 - April', '05 - May', '06 - June',
    '07 - July', '08 - August', '09 - September', '10 - October', '11 - November', '12 - December'];
  yearList = [];
  expYear = 0;
  expMonth = 1;
  zipCode: any = '';
  clientName: any = '';
  autoBill = false;
  nextBillDate: any = '';
  nextBillDateError: any = '';
  paymentList = [];
  paymentTypes: any = '';
  hidePaymentTab = true;
  setPayment: any;
  firstNameError: any = '';
  lastNameError: any = '';
  primaryEmailError: any = '';
  NameOnCardError: any = '';
  memUniIdError: any = '';
  expiryError: any = '';
  cvvError: any = '';
  cardNumberError: any = '';
  toastermessage: any;
  maxDate = new Date();
  minDate = new Date('1918,01,01');
  saveButon = true;
  showClientInfo: any = [];
  memberId: any = '';
  birthdayShow: any = '';
  dummyClientName = 'STX';
  tokenbody: any;
  cardTokenId1 = '';
  deleteClientId: any;
  countrycode = '01';

  showMobileNo: any;
  existingClientId: any;
  existingMemberShip: any = '';
  existingPaymentTypes: any = '';
  existingAutoBill = false;
  existingCardNumber: any = '';
  ExistingNameOnCard: any = '';
  existingExpMonth = 1;
  existingExpYear = 0;
  existingCvv: any = '';
  ExistingZipcode: any = '';
  existingCharge: any = '';
  existingNextBillDate: any = '';
  amount: any = '';
  existingGender: any = '';
  existingMobile: any = '';
  existingEmail: any;
  existingMemUniId = '';
  existingMemUniIdError = '';
  cashDrawer: any = '';
  autoBillingList: any;
  errorValidDate = '';
  day: any;
  month: any;
  year: any;
  hideClientInfo: any;
  decodeUserToken: any;
  decodedToken: any;
  cliId = '';
  tokenObj: any = {};
  existingTokenObj: any = {};
  clvrCrtTkn = false;
  paymentGateWay = '';
  @ViewChild('newMemberModal') public newMemberModal: ModalDirective;
  @ViewChild('searchPop') public searchPop: ModalDirective;
  @ViewChild('existingClient') public existingClient: ModalDirective;
  @ViewChild('searchMembership') public searchMembership: ModalDirective;
  ecommerceData: any;
  constructor(private membercheckinService: MembercheckinService,
    @Inject('defaultCountry') public defaultCountry: string,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private sanitizer: DomSanitizer,
    private elementRef: ElementRef,
    private zone: NgZone,
    private commonService: CommonService) { }

  public myForm: FormGroup;

  ngOnInit() {
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (localStorage.getItem('browserObject')) {
      const cashDrawrInfo = localStorage.getItem('browserObject');
      if (cashDrawrInfo) {
        this.cashDrawer = JSON.parse(cashDrawrInfo).CashDrawer ? JSON.parse(cashDrawrInfo).CashDrawer : null;
      }
    }
    this.getHideClientContactInfo();
    this.getPaymentTypes();
    // this.getListForAzutoBilling();
    this.createYearsList();
    this.getSetupMemberships();
    this.maxDate = new Date();
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });

    this.searchField.valueChanges
      .debounceTime(400)
      .switchMap(term => this.membercheckinService.getApptDetails(term)
      ).subscribe(
        data => {
          this.autoList = [];
          this.autoList = data['result'];
          if (this.cSearch && this.cSearch.trim()) {
            if (this.autoList.length === 0) {
              this.toastr.warning('No record found. ', null, { timeOut: 3000 });
            }
          } else {
            this.autoList = [];
          }
        },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    this.getHideClientContactInfo();
    const inpEle = <HTMLInputElement>document.getElementById('memberCheckin');
    inpEle.focus();
    this.commonService.ecommerceDetails().subscribe(
      resData => {
        this.ecommerceData = JSON.parse(resData.result[0].JSON__c);
      });
  }

  successAudio() {
    const audio = new Audio();
    audio.src = '../../assets/audio/BellDing.mp3';
    audio.load();
    audio.play();
  }
  badAudio() {
    const audio = new Audio();
    audio.src = '../../assets/audio/Buzzer.mp3';
    audio.load();
    audio.play();
  }
  getHideClientContactInfo() {
    this.membercheckinService.getHideCliContactInfo(this.decodeUserToken.data.id).subscribe(data => {
      if (data['result'] && data['result'].length > 0) {
        this.hideClientInfo = data['result'][0].Hide_Client_Contact_Info__c;
      }
    }, error => {
      const errStatus = JSON.parse(error['_body'])['status'];
      if (errStatus === '2085' || errStatus === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }

  hyphen_generate_mobile(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat('(');
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat('(');
    }
    if (value.length === 1) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = '(' + value;
      (<HTMLInputElement>document.getElementById('existingMobile')).value = '(' + value;
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat(')');
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobileNumber')).value = value.concat('-');
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat(')');
    }
  }

  hyphen_gen(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat('(');
    }
    if (value.length === 1) {
      (<HTMLInputElement>document.getElementById('existingMobile')).value = '(' + value;
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('existingMobile')).value = value.concat('-');
    }
  }

  // payType(val) {
  //   if ('cash' === (val.split('$')[1]).toLowerCase()) {
  //     this.autoBill = false;
  //     this.existingAutoBill = false;
  //     this.hidePaymentTab = false;
  //   } else {
  //     this.hidePaymentTab = true;
  //   }
  // }

  searchMemberId() {
    if (this.memberSearch) {
      this.searchPop.hide();
      const clientId = this.memberSearch;
      const todayDate = this.commonService.getDBDatTmStr(new Date()).split(' ')[0];
      this.membercheckinService.getmemberDetails(clientId, todayDate).subscribe(
        data => {
          this.memberShipDetails = data['result'];
          if (this.memberShipDetails && this.memberShipDetails.length > 0) {
            const dates = this.memberShipDetails[0].Next_Bill_Date__c;
            if ((data['result'][0].Birthdate === 'Invalid date' || data['result'][0].Birthdate === null) && (data['result'][0].Birthdate === 'null' ||
              data['result'][0].Birthdate === 'undefined') && data['result'][0].Birthdate === undefined) {
              this.birthdayShow = '';
            } else if (data['result'][0].Birthdate === 'undefined-undefined-undefined') {
              this.birthdayShow = '';
            } else if (data['result'][0].Birthdate === 'null-null-null') {
              this.birthdayShow = '';
            } else {
              this.birthdayShow = data['result'][0].Birthdate;
            }
            const expireTime = this.commonService.UTCStrToUsrTmzStr(dates).split(' ')[0];
            if (todayDate < expireTime) {
              this.showIcon = 1;
              this.successAudio();
              this.autoList = [];
              this.hideTable = true;
              setTimeout(() => {
                $('#showDiv').fadeOut();
              }, 7000);
              setTimeout(() => {
                this.clearData();
              }, 7000);
            } else {
              this.showIcon = 0;
              this.badAudio();
              this.autoList = [];
              this.hideTable = true;
              setTimeout(() => {
                $('#showDiv').fadeOut();
              }, 7000);
              setTimeout(() => {
                this.clearData();
              }, 7000);
            }
          } else {
            this.errMsg = 'MEMBER_CHECKIN.MEMBER_NOT_FOUND';
            this.toastermessage = this.translateService.get('Member not found.');
            this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
            setTimeout(() => {
              this.clearData();
            }, 2000);
          }
        },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }
  }

  checkValidMember(client) {
    this.searchPop.hide();
    const todayDate = this.commonService.getDBDatTmStr(new Date()).split(' ')[0];
    this.membercheckinService.getmemberDetails(client, todayDate).subscribe(
      data => {
        this.memberShipDetails = data['result'];
        if (this.memberShipDetails && this.memberShipDetails.length > 0) {
          const dates = this.memberShipDetails[0].Next_Bill_Date__c;
          const birth = data['result'][0].Birthdate;
          if ((birth === 'Invalid date' || birth === null) && (birth === 'null' || birth === 'undefined') && birth === undefined) {
            this.birthdayShow = '';
          } else if (birth === 'undefined-undefined-undefined') {
            this.birthdayShow = '';
          } else if (birth === 'null-null-null') {
            this.birthdayShow = '';
          } else {
            this.birthdayShow = birth;
          }
          const expireTime = this.commonService.UTCStrToUsrTmzStr(dates).split(' ')[0];
          if (todayDate < expireTime) {
            this.showIcon = 1;
            this.successAudio();
            this.autoList = [];
            this.hideTable = true;
            setTimeout(() => {
              $('#showDiv').fadeOut();
            }, 7000);
            setTimeout(() => {
              this.clearData();
            }, 7000);
          } else {
            this.showIcon = 0;
            this.badAudio();
            this.autoList = [];
            this.hideTable = true;
            setTimeout(() => {
              $('#showDiv').fadeOut();
            }, 7000);
            setTimeout(() => {
              this.clearData();
            }, 7000);
          }
        } else {
          this.errMsg = 'Member not found.';
          this.toastermessage = this.translateService.get('Member not found.');
          this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
          setTimeout(() => {
            this.clearData();
          }, 2000);
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });

  }
  getSetupMemberships() {
    const inActive = 1;
    this.membercheckinService.getSetupMemberships(inActive)
      .subscribe(data => {
        this.MembersipsList = data['result'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }

  newMember() {
    this.cSearch = '';
    this.autoList = [];
    this.clearErrorMsg();
    this.searchMembership.show();
    this.saveButon = true;
    setTimeout(() => {
      const newMember = <HTMLInputElement>document.getElementById('newMember');
      newMember.focus();
    }, 1000);
  }

  newClient() {
    this.cSearch = '';
    this.autoList = [];
    this.clearErrorMsg();
    this.searchMembership.hide();
    this.newMemberModal.show();
  }

  closeMemberShip() {
    this.firstName = '';
    this.lastName = '';
    this.mobileNumber = '';
    this.bsValue = '';
    this.primaryEmail = '';
    this.cardNumber = '';
    this.NameOnCard = '';
    this.memUniId = '';
    this.MemberShipType = '';
    this.nextBillDate = '';
    this.charge = '';
    this.paymentTypes = '';
    this.cvv = '';
    this.zipCode = '';
    this.newMemberModal.hide();
  }

  member() {
    this.clearData();
    this.searchPop.show();
    setTimeout(() => {
      const member = <HTMLInputElement>document.getElementById('memberSearch');
      member.focus();
    }, 1000);
  }
  // populateFromDate() {
  //   if (this.bsValue > this.bsValue1) {
  //     this.bsValue1 = this.bsValue;
  //   }
  // }
  createYearsList() {
    const curtYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.yearList.push(curtYear + i);
    }
    this.expYear = this.yearList[0];
    this.existingExpYear = this.yearList[0];
  }

  getPaymentTypes() {
    this.membercheckinService.getPaymentTypesData().subscribe(
      data => {
        this.refPaymentId = data['result'].Id;
        const payment = data['result']['paymentResult'];
        this.paymentList = data.result.paymentResult.filter(filterList => filterList.Name === 'Electronic Payment');
        if (this.paymentList.length === 0) {
          this.paymentList = [payment.filter(filterList => filterList.Process_Electronically__c === 1 && filterList.Active__c === 1)[0]];
        }
      });
  }

  membershipPlanchange(value) {
    if (value) {
      const planChange = value;
      this.amount = planChange.split('$')[1];
      this.charge = this.amount;
    }
  }

  existingMemPlanChange(value) {
    if (value) {
      const planChange = value;
      this.amount = planChange.split('$')[1];
      this.existingCharge = this.amount;
    }
  }

  generateToken(obj, dataObj) {
    let expmonth;
    if (this.expMonth.toString().length <= 1) {
      expmonth = '0' + this.expMonth;
    } else {
      expmonth = this.expMonth;
    }
    if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
      const clientData = {
        currency: 'USD',
        clientId: this.cliId,
        cardNumber: this.cardNumber.toString(),
        expMonth: expmonth,
        amount: 1,
        expYear: this.expYear.toString().substring(2),
        cvv: this.cvv,
        tokengeneration: false
      };
      this.membercheckinService.cloverApiPayment(clientData).subscribe(
        data => {
          if (data['result']['result'] === 'APPROVED') {
            this.tokenObj = data['result'];
            if (this.tokenObj['vaultedCard']['token']) {
              this.clvrCrtTkn = true;
              this.cardTokenId1 = this.tokenObj['vaultedCard'];
              this.paymentTicket(obj, dataObj);
            } else {
              this.delClientMember(dataObj);
              this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
            }
          } else {
            this.delClientMember(dataObj);
            this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        }
      );
    } else {
      this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    }
  }

  delClientMember(dataObj) {
    if (dataObj && dataObj.length > 0) {
      this.membercheckinService.detClientId(dataObj).subscribe(
        data => {
          const delClientId = data['result'];
        });
    }
  }

  paymentTicket(obj, clientId) {
    if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
      let clientData = {};
      if (this.clvrCrtTkn) { // payment after creation of new clienttoken, if this.clvrCrtTkn is true then sending clover token data
        clientData = this.tokenObj;
        clientData['token'] = this.tokenObj['vaultedCard']['token'];
        clientData['first6'] = this.tokenObj['vaultedCard']['first6'];
        clientData['last4'] = this.tokenObj['vaultedCard']['last4'];
        clientData['expirationDate'] = this.tokenObj['vaultedCard']['expirationDate'];
        clientData['amount'] = (+this.charge);
        clientData['transactionType'] = 'CardOnFile';
        clientData['currency'] = 'USD';
      } else {
        clientData = {
          terminalid: this.ecommerceData.merchantId,
          cardType: this.commonService.getCardType(this.cardNumber),
          amount: (+this.charge),
          cardExp: ('0' + this.expMonth).slice(-2) + this.expYear.toString().slice(-2),
          cardNumber: this.cardNumber,
          currency: 'USD',
          terminalType: '1',
          transactionType: '4',
          expMonth: this.expMonth,
          expYear: this.expYear,
          cvv: this.cvv,
        };
      }
      this.membercheckinService.cloverApiPayment(clientData).subscribe(
        data => {
          const paymentDatas = data['result'];
          if (paymentDatas && paymentDatas['result'] === 'APPROVED') {
            const paymentData1 = {
              'PAYMENTRESPONSE': {
                'APPROVALCODE': [''],
                'UNIQUEREF': ['']
              }
            };
            paymentData1.PAYMENTRESPONSE.APPROVALCODE[0] = paymentDatas['authCode'];
            paymentData1.PAYMENTRESPONSE.UNIQUEREF[0] = paymentDatas['token'];
            this.savePaymentsData(paymentData1, obj, clientId);
            this.toastr.info('payment done successfully ,please wait for remaining process', null, { timeOut: 4000 });
          } else {
            this.newMemberModal.hide();
            this.toastr.warning('Error Occured, Invalid Details', null, { timeOut: 4000 });
            this.clearErrorFileds();
            this.delClientMember(clientId);
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } else if (statuscode === '9998') {
                this.toastr.error('FirstName, LastName,EmailId allready exists', null, { timeOut: 3000 });
              } else if (statuscode === '2083') {
                const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                this.toastr.error(toastermessage.value, null, { timeOut: 3500 });
              } else if (statuscode === '2039') {
                this.error = JSON.parse(error['_body']).result;
                this.toastr.error(JSON.parse(error['_body']).result, null, { timeOut: 3000 });
                window.scrollTo(0, 0);
              } else if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
    } else {
      this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    }
  }
  validDate() {
    this.errorValidDate = '';
  }
  saveMember() {
    let countryCode: any;
    let mobileNumber: any;
    if (this.countrycode === undefined || this.countrycode === '') {
      countryCode = '';
    } else {
      countryCode = this.countrycode;
    }
    if (this.mobileNumber === undefined || this.mobileNumber === '') {
      mobileNumber = '';
    } else {
      mobileNumber = countryCode + '-' + this.mobileNumber;
    }

    this.saveButon = false;
    // const birthday = moment(this.birthdayDate).format('MM-DD-YYYY');
    // let birthdayDate = '';
    // if (birthday === 'Invalid date') {
    //   birthdayDate = '';
    // } else {
    //   birthdayDate = birthday;
    // }
    if (this.birthdayDate) {
      // First check for the pattern
      const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
      if (!datePattern.test(this.birthdayDate)) {
        this.errorValidDate = 'Invalid date format';
        return false;
      }
      // Parse the date parts to integers
      const parts = this.birthdayDate.split('/');
      const day = parseInt(parts[1], 10);
      const month = parseInt(parts[0], 10);
      const year = parseInt(parts[2], 10);
      // Check the ranges of month and year
      if (year < 1000 || year > 3000 || month === 0 || month > 12) {
        this.errorValidDate = 'Invalid date format';
        return false;
      }
      const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

      // Adjust for leap years
      if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
        monthLength[1] = 29;
      }
      // Check the range of the day
      this.birthdayDate = month + '/' + day + '/' + year;
      this.day = day;
      this.month = month;
      this.year = year;
    } else {
      this.day = null;
      this.month = null;
      this.year = null;
    }


    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (this.paymentTypes) {
      const payAlert = this.paymentTypes.split('$')[1].toLowerCase() === 'cash';
      this.setPayment = payAlert;               // true or false
    }
    if (this.firstName === '' || this.lastName === ''
      || this.memUniId === '' || this.MemberShipType === ''
      || this.paymentTypes === '' || this.nextBillDate === ''
      || this.charge === '' && (this.setPayment === true || this.setPayment === false)
    ) {
      if (this.firstName === '') {
        this.firstNameError = 'MEMBER_CHECKIN.ENTER_FIRST_NAME';
      }
      if (this.lastName === '') {
        this.lastNameError = 'MEMBER_CHECKIN.ENTER_LAST_NAME';
      }
      if (this.memUniId === '') {
        this.memUniIdError = 'MEMBER_CHECKIN.ENTER_MEMBER_ID';
      }
      if (this.MemberShipType === '') {
        this.MemberShipTypeError = 'MEMBER_CHECKIN.MEMBERSHIP_TYPE_SELECT';
      }
      if (this.paymentTypes === '') {
        this.paymentTypesError = 'MEMBER_CHECKIN.PLEASE_SELECT_PAYMENT_TYPE';
      }
      if (this.nextBillDate === '') {
        this.nextBillDateError = 'MEMBER_CHECKIN.PLEASE_SELECT_PLAN';
      }
      if (this.charge === '') {
        this.chargeError = 'MEMBER_CHECKIN.PLEASE_ENTER_AMOUNT';
      }
      if (this.setPayment === false) {
        if (this.cardNumber === '') {
          this.cardNumberError = 'MEMBER_CHECKIN.PLEASE_ENTER_CARD_NUMBER';
        }
        if (this.NameOnCard === '') {
          this.NameOnCardError = 'MEMBER_CHECKIN.PLEASE_ENTER_PERSON_NAME';
        }
        if (this.cvv === '') {
          this.cvvError = 'MEMBER_CHECKIN.PLEASE_ENTER_CVV';
        }
      }
    } else {
      const token = Math.floor((Math.random() * 123456789) + 26);

      const CurrentDate = new Date();
      CurrentDate.setMonth(CurrentDate.getMonth() + (+this.nextBillDate));

      const t = new Date(CurrentDate);
      const membershipPlains = ('00' + (t.getMonth() + 1)).slice(-2) + '-' + ('00' + t.getDate()).slice(-2) + '-' +
        (t.getFullYear() + '').slice(-2) + ':' +
        ('00' + t.getHours()).slice(-2) + ':' +
        ('00' + t.getMinutes()).slice(-2) + ':' +
        ('00' + t.getSeconds()).slice(-2) + ':000';

      const obj = {
        'firstName': this.firstName.trim(),
        'lastName': this.lastName.trim(),
        'birthday': this.birthdayDate ? this.birthdayDate : '',
        'mobileNumber': mobileNumber,
        'sms_checkbox': this.sms_checkbox === '' ? 0 : 1,
        'gender': this.gender,
        'email': this.primaryEmail,
        'memUniId': this.memUniId,
        'MemberShipType': this.MemberShipType,
        'paymentTypes': this.paymentTypes,
        'charge': (+this.charge).toFixed(2),
        'Auto_Bill__c': this.autoBill,
        'zipcode': this.zipCode,
        'plan': this.nextBillDate,
        'cashDrawer': this.cashDrawer ? this.cashDrawer : '',
        'Next_Bill_Date__c': moment(membershipPlains, 'MM-DD-YYYY:HH:mm:sss').format('YYYY-MM-DD HH:mm:ss'),
      };
      this.membercheckinService.addToClient(obj)
        .subscribe(data1 => {
          const dataObj = data1['result'];
          if (dataObj && dataObj.length > 0) {
            this.newMemberModal.hide();
            if (this.setPayment) {
              this.saveNonCashpayment(obj, dataObj);
            } else if (!this.setPayment) {
              if (this.autoBill) {
                this.generateToken(obj, dataObj);                              // for token and auto billing
              } else if (!this.autoBill) {
                this.paymentTicket(obj, dataObj);                              // only payment
              }
            }

          } else {
            this.toastr.error('Record inserted, but payment not done', null, { timeOut: 3500 });
          }
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                if (statuscode === '2040') {
                  this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                  window.scrollTo(0, 0);
                } else if (statuscode === '9998') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                  this.toastr.info(toastermessage.value, null, { timeOut: 4000 });
                } else if (statuscode === '2083') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_MEMBERSHIP_ID');
                  this.toastr.error(toastermessage.value, null, { timeOut: 3600 });
                } else if (statuscode === '2085' || statuscode === '2071') {
                  if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                  }
                } break;
            }
          });
    }
  }

  savePaymentsData(paymentData, obj, clientId) {
    let approvalCode = '';
    let refCode = '';
    if (paymentData === null) {
      approvalCode = '';
      refCode = '';
    } else {
      approvalCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      refCode = paymentData.PAYMENTRESPONSE.UNIQUEREF[0];
    }
    const paymentObj = {
      'Id': this.refPaymentId,
      'amountToPay': (+this.charge).toFixed(2),
      'merchantAccnt': this.ecommerceData.merchantId,
      'cardHolderName': this.NameOnCard,
      'cardNumber': this.cardNumber,
      'approvalCode': approvalCode,
      'refCode': refCode,
      'token': this.cardTokenId1 ? this.cardTokenId1 : null,
      'clientId': clientId,
    };
    this.membercheckinService.addToPaymentsTicket(paymentObj, obj)
      .subscribe(data1 => {
        const apptId = data1['result'].Id;
        const dataObj = data1['result'];
        this.newMemberModal.hide();

        this.clearErrorFileds();
        setTimeout(() => {
          this.router.navigate(['/completedticket/' + apptId]).then(() => {
            this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
            this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
          });
        }, 3000);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }

  saveNonCashpayment(obj, clientId) {
    const paymentObjs = {
      'Id': this.refPaymentId,
      'amountToPay': (+this.charge).toFixed(2) ? (+this.charge).toFixed(2) : (+this.existingCharge).toFixed(2),
      'merchantAccnt': this.ecommerceData.merchantId,
      'cardHolderName': '',
      'cardNumber': '',
      'approvalCode': '',
      'refCode': '',
      'clientId': clientId,
    };
    this.membercheckinService.addToPaymentsTicket(paymentObjs, obj)
      .subscribe(data1 => {
        const dataObj = data1['result'];
        this.newMemberModal.hide();
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
        this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
        this.clearErrorFileds();
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }

  getDataExistingClient(client) {
    this.existingClientId = client.Id;
    this.searchPop.hide();
    this.searchMembership.hide();
    let check = false;
    if (client.Sms_Consent__c === 0) {
      check = false;
    } else {
      check = true;
    }
    this.checkBox = check;
    this.showClientInfo = client;
    if (this.showClientInfo.Birthdate === 'Invalid date'
      || this.showClientInfo.Birthdate === null || this.showClientInfo.Birthdate === 'null'
      || this.showClientInfo.Birthdate === 'undefined' || this.showClientInfo.Birthdate === undefined) {
      this.birthdayShow = '';
    } else if (this.showClientInfo.Birthdate === 'undefined-undefined-undefined') {
      this.birthdayShow = '';
    } else if (this.showClientInfo.Birthdate === 'null-null-null') {
      this.birthdayShow = '';
    } else {
      this.birthdayShow = this.showClientInfo.Birthdate;
    }

    if (this.showClientInfo.Membership_ID__c === null || this.showClientInfo.Membership_ID__c === 'null'
      || this.showClientInfo.Membership_ID__c === undefined || this.showClientInfo.Membership_ID__c === 'undefined'
    ) {
      this.memberId = '0';
    } else {
      this.memberId = '1';
      this.existingMemUniId = this.showClientInfo.Membership_ID__c;
    }

    if (client.MobilePhone.indexOf('01') > -1) {
      this.showMobileNo = client.MobilePhone.slice(3, 16);
    } else {
      this.showMobileNo = client.MobilePhone;
    }
    this.existingClient.show();
  }

  existingGenerateToken(obj, dataObj) {
    // let expmonth;
    // if (this.existingExpMonth.toString().length <= 1) {
    //   expmonth = '0' + this.existingExpMonth;
    // } else {
    //   expmonth = this.existingExpMonth;
    // }
    // const d = new Date();
    // const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
    //   (d.getFullYear() + '').slice(-2) + ':' +
    //   ('00' + d.getHours()).slice(-2) + ':' +
    //   ('00' + d.getMinutes()).slice(-2) + ':' +
    //   ('00' + d.getSeconds()).slice(-2) + ':000';

    // const clientId = Math.floor((Math.random() * 123456789) + 26);
    // if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
    //   const hash = Md5.hashStr(this.ecommerceData.merchantId + clientId + dateTime + this.existingCardNumber + expmonth + this.existingExpYear.toString().substring(2) +
    //     this.commonService.getCardType(this.existingCardNumber) + this.ExistingNameOnCard + this.ecommerceData.accessToken);

    //   const clientData1 = {
    //     merchantref: clientId,
    //     terminalid: this.ecommerceData.merchantId,
    //     cardType: this.commonService.getCardType(this.existingCardNumber),
    //     cardHolName: this.ExistingNameOnCard,
    //     dateTime: dateTime,
    //     cardNum: this.existingCardNumber,
    //     cardExp: expmonth + this.existingExpYear.toString().substring(2),
    //     hash: hash,
    //     cvv: this.existingCvv
    //   };
    //   this.tokenbody = this.commonService.createToken(clientData1);

    //   const url = config.ANYWHERECOMMERCE_PAYMENT_API;

    //   const reqObj = {
    //     'url': url,
    //     'xml': this.tokenbody
    //   };
    //   this.membercheckinService.xmlPayment(reqObj).subscribe(
    //     data => {
    //       let cardTokenId: any = '';
    //       const parseString = require('xml2js').parseString;
    //       parseString(data['result'], function (err, result) {
    //         cardTokenId = result;
    //       });
    //       if (cardTokenId.ERROR && cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID MERCHANTREF') {
    //         this.toastr.warning('INVALID MERCHANTREF', null, { timeOut: 3000 });
    //       } else if ((cardTokenId.ERROR) && (!cardTokenId.SECURECARDUPDATERESPONSE || !cardTokenId.SECURECARDREGISTRATIONRESPONSE)) {
    //         if ((cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] === cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] || this.cardNumber.toString() === '0')
    //           && (cardTokenId.ERROR.ERRORSTRING[0] !== 'INVALID CARDEXPIRY') &&
    //           (cardTokenId.ERROR.ERRORSTRING[0] !== 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12')) {
    //           this.toastr.warning('Credit Card Processing Error: INVALID CARDNUMBER field', null, { timeOut: 3000 });
    //         } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID CARDEXPIRY') {
    //           this.toastr.warning('Invalid card expiry', null, { timeOut: 3000 });
    //         } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12') {
    //           this.toastr.warning('Card number must be 12 digits ', null, { timeOut: 3000 });
    //         }
    //       }

    //       this.cardTokenId1 = cardTokenId.SECURECARDREGISTRATIONRESPONSE.CARDREFERENCE[0];
    //       if (this.cardTokenId1 !== '') {
    //         this.existingPaymentTicket(obj, dataObj);
    //       } else {
    //         this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
    //       }
    //     },
    //     error => {
    //       const status = JSON.parse(error['status']);
    //       const statuscode = JSON.parse(error['_body']).status;
    //       switch (status) {
    //         case 500:
    //           break;
    //         case 400:
    //           if (statuscode === '2085' || statuscode === '2071') {
    //             if (this.router.url !== '/') {
    //               localStorage.setItem('page', this.router.url);
    //               this.router.navigate(['/']).then(() => { });
    //             }
    //           } break;
    //       }
    //     });
    // } else {
    //   this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
    //   this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    // }
    let expmonth;
    if (this.existingExpMonth.toString().length <= 1) {
      expmonth = '0' + this.existingExpMonth;
    } else {
      expmonth = this.existingExpMonth;
    }
    if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
      const clientData = {
        currency: 'USD',
        clientId: this.existingClientId,
        cardNumber: this.existingCardNumber.toString(),
        expMonth: expmonth,
        amount: 1,
        expYear: this.existingExpYear.toString().substring(2),
        cvv: this.existingCvv,
        tokengeneration: false
      };
      this.membercheckinService.cloverApiPayment(clientData).subscribe(
        data => {
          if (data['result']['result'] === 'APPROVED') {
            this.tokenObj = data['result'];
            if (this.tokenObj['vaultedCard']['token']) {
              this.clvrCrtTkn = true;
              this.cardTokenId1 = this.tokenObj['vaultedCard'];
              this.existingPaymentTicket(obj, dataObj);
            } else {
              this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
            }
          } else {
            this.toastr.warning('unable to generate token payment not done', null, { timeOut: 3000 });
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        }
      );
    } else {
      this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    }
  }

  // getListForAutoBilling() {
  //   const d = new Date();
  //   let paymentDatas;
  //   const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
  //     (d.getFullYear() + '').slice(-2) + ':' +
  //     ('00' + d.getHours()).slice(-2) + ':' +
  //     ('00' + d.getMinutes()).slice(-2) + ':' +
  //     ('00' + d.getSeconds()).slice(-2) + ':000';

  //   const url = config.ANYWHERECOMMERCE_PAYMENT_API;

  //   this.membercheckinService.getListOFAutoBilling().subscribe(data => {
  //     this.autoBillingList = data['result'];
  //     const orderData = [];
  //     const todayDate = moment().format('YYYYMMDD');

  //     for (let i = 0; i < this.autoBillingList.length; i++) {
  //       const orderId = this.autoBillingList[i].cmId + todayDate;
  //       const token = this.autoBillingList[i].Token__c;
  //       const price = this.autoBillingList[i].Price__c;
  //       const plan = this.autoBillingList[i].Plan__c ? this.autoBillingList[i].Plan__c : 1;

  //       const hash = Md5.hashStr(this.ecommerceData.merchantId + orderId + (+price)
  //         + dateTime + this.ecommerceData.accessToken);
  //       const clientData = {
  //         ticketPaymntId: orderId,
  //         terminalid: this.ecommerceData.merchantId,
  //         dateTime: dateTime,
  //         cardNum: token,
  //         cardType: 'SECURECARD',
  //         currency: 'USD',
  //         terminalType: '1',
  //         transactionType: '4',
  //         hash: hash,
  //         amountDue: (+price),
  //         cardExp: ('0' + this.existingExpMonth).slice(-2) + this.existingExpYear.toString().slice(-2)
  //       };
  //       const tokenbody = this.commonService.checkPaymentToken(clientData);
  //       const reqObj = {
  //         'url': url,
  //         'xml': tokenbody
  //       };
  //       this.membercheckinService.xmlPayment(reqObj).subscribe(
  //         data1 => {
  //           const parseString = require('xml2js').parseString;
  //           parseString(data1['result'], function (err, result) {
  //             paymentDatas = result;
  //           });
  //           if (paymentDatas && paymentDatas.PAYMENTRESPONSE) {
  //             this.saveAutoBillingToken(paymentDatas.PAYMENTRESPONSE, this.autoBillingList);
  //           }
  //         });
  //     }
  //   });
  // }

  // saveAutoBillingToken(payment, memberData) {
  //   this.membercheckinService.saveAutoBilling(payment, memberData)
  //     .subscribe(data1 => {
  //       // const dataObj = data1['result'];
  //     });
  // }



  existingPaymentTicket(obj, clientId) {
    if (this.ecommerceData.merchantId && this.ecommerceData.accessToken) {
      let clientData = {};
      if (this.clvrCrtTkn) { // payment after creation of new clienttoken, if this.clvrCrtTkn is true then sending clover token data
        clientData = this.tokenObj;
        clientData['token'] = this.tokenObj['vaultedCard']['token'];
        clientData['first6'] = this.tokenObj['vaultedCard']['first6'];
        clientData['last4'] = this.tokenObj['vaultedCard']['last4'];
        clientData['expirationDate'] = this.tokenObj['vaultedCard']['expirationDate'];
        clientData['amount'] = (+this.existingCharge);
        clientData['transactionType'] = 'CardOnFile';
        clientData['currency'] = 'USD';
      } else {
        clientData = {
          terminalid: this.ecommerceData.merchantId,
          cardType: this.commonService.getCardType(this.existingCardNumber),
          amount: (+this.existingCharge),
          cardExp: ('0' + this.expMonth).slice(-2) + this.expYear.toString().slice(-2),
          cardNumber: this.existingCardNumber,
          currency: 'USD',
          terminalType: '1',
          transactionType: '4',
          expMonth: this.existingExpMonth,
          expYear: this.existingExpYear,
          cvv: this.existingCvv,
        };
      }
      this.membercheckinService.cloverApiPayment(clientData).subscribe(
        data => {
          const paymentDatas = data['result'];
          if (paymentDatas && paymentDatas['result'] === 'APPROVED') {
            const paymentData1 = {
              'PAYMENTRESPONSE': {
                'APPROVALCODE': [''],
                'UNIQUEREF': ['']
              }
            };
            paymentData1.PAYMENTRESPONSE.APPROVALCODE[0] = paymentDatas['authCode'];
            paymentData1.PAYMENTRESPONSE.UNIQUEREF[0] = paymentDatas['token'];
            this.existingSavePaymentsData(paymentData1, obj, clientId);
            this.toastr.info('payment done successfully ,please wait for remaining process', null, { timeOut: 4000 });
          } else {
            this.newMemberModal.hide();
            this.toastr.warning('Error Occured, Invalid Details', null, { timeOut: 4000 });
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } else if (statuscode === '9998') {
                this.toastr.error('FirstName, LastName,EmailId allready exists', null, { timeOut: 3000 });
              } else if (statuscode === '2083') {
                const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                this.toastr.error(toastermessage.value, null, { timeOut: 3500 });
              } else if (statuscode === '2039') {
                this.error = JSON.parse(error['_body']).result;
                this.toastr.error(JSON.parse(error['_body']).result, null, { timeOut: 3000 });
                window.scrollTo(0, 0);
              } else if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
    } else {
      this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    }
  }

  existingSavePaymentsData(paymentData, obj, clientId) {
    let approvalCode = '';
    let refCode = '';
    if (paymentData === null) {
      approvalCode = '';
      refCode = '';
    } else {
      approvalCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      refCode = paymentData.PAYMENTRESPONSE.UNIQUEREF[0];
    }
    const paymentObj = {
      'Id': this.refPaymentId,
      'amountToPay': (+this.existingCharge).toFixed(2),
      'merchantAccnt': this.ecommerceData.merchantId,
      'cardHolderName': this.ExistingNameOnCard,
      'cardNumber': this.existingCardNumber,
      'approvalCode': approvalCode,
      'refCode': refCode,
      'token': this.cardTokenId1 ? this.cardTokenId1 : null,
      'clientId': clientId,
    };
    this.membercheckinService.addToPaymentsTicket(paymentObj, obj)
      .subscribe(data1 => {
        const apptId = data1['result'].Id;
        const dataObj = data1['result'];
        this.existingClient.hide();
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
        this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
        this.clearErrorFileds();
        setTimeout(() => {
          this.router.navigate(['/completedticket/' + apptId]).then(() => {
            this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.MEMBERSHIP_SAVED');
            this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
          });
        }, 3000);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }

  existingMemebership(showClientInfo) {
    if (this.existingBirthday) {
      // First check for the pattern
      const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
      if (!datePattern.test(this.existingBirthday)) {
        this.errorValidDate = 'Invalid date format';
        return false;
      }
      // Parse the date parts to integers
      const parts = this.existingBirthday.split('/');
      const day = parseInt(parts[1], 10);
      const month = parseInt(parts[0], 10);
      const year = parseInt(parts[2], 10);
      // Check the ranges of month and year
      if (year < 1000 || year > 3000 || month === 0 || month > 12) {
        this.errorValidDate = 'Invalid date format';
        return false;
      }
      const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      // Adjust for leap years
      if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
        monthLength[1] = 29;
      }
      // Check the range of the day
      this.existingBirthday = month + '/' + day + '/' + year;
      this.day = day;
      this.month = month;
      this.year = year;
    }
    if (this.existingPaymentTypes) {
      const payAlert = this.existingPaymentTypes.split('$')[1].toLowerCase() === 'cash';
      this.setPayment = payAlert;               // true or false
    }
    if (this.existingPaymentTypes === '' || this.existingNextBillDate === ''
      || this.existingMemberShip === '' && (this.setPayment === true || this.setPayment === false)) {

      if (this.existingPaymentTypes === '') {
        this.paymentTypesError = 'MEMBER_CHECKIN.PLEASE_SELECT_PAYMENT_TYPE';
      }
      if (this.existingMemberShip === '') {
        this.MemberShipTypeError = 'MEMBER_CHECKIN.MEMBERSHIP_TYPE_SELECT';
      }
      if (this.existingNextBillDate === '') {
        this.nextBillDateError = 'MEMBER_CHECKIN.PLEASE_SELECT_PLAN';
      }
      if (this.existingCharge === '') {
        this.chargeError = 'MEMBER_CHECKIN.PLEASE_ENTER_AMOUNT';
      }
      if (this.setPayment === false) {
        if (this.existingCardNumber === '') {
          this.cardNumberError = 'MEMBER_CHECKIN.PLEASE_ENTER_CARD_NUMBER';
        }
        if (this.ExistingNameOnCard === '') {
          this.NameOnCardError = 'MEMBER_CHECKIN.PLEASE_ENTER_PERSON_NAME';
        }
        if (this.existingCvv === '') {
          this.cvvError = 'MEMBER_CHECKIN.PLEASE_ENTER_CVV';
        }
      }
    } else if (this.existingMemUniId === '') {
      this.existingMemUniIdError = 'please enter member id';
    } else {
      let countryCode: any;
      let mobileNo: any;
      if (this.countrycode === undefined) {
        countryCode = '';
      } else {
        countryCode = this.countrycode;
      }

      if (this.existingMobile === undefined || this.existingMobile === '') {
        mobileNo = '';
      } else {
        mobileNo = countryCode + '-' + this.existingMobile;
      }

      const CurrentDate = new Date();
      CurrentDate.setMonth(CurrentDate.getMonth() + (+this.existingNextBillDate));

      const t = new Date(CurrentDate);
      const membershipPlains = ('00' + (t.getMonth() + 1)).slice(-2) + '-' + ('00' + t.getDate()).slice(-2) + '-' +
        (t.getFullYear() + '').slice(-2) + ':' +
        ('00' + t.getHours()).slice(-2) + ':' +
        ('00' + t.getMinutes()).slice(-2) + ':' +
        ('00' + t.getSeconds()).slice(-2) + ':000';
      let sms_checkbox = 0;
      if (this.existingSms_checkbox = true && mobileNo === '') {
        sms_checkbox = 0;
      } else {
        sms_checkbox = 1;
      }
      if (this.existingBirthday) {
        // First check for the pattern
        const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (!datePattern.test(this.existingBirthday)) {
          this.errorValidDate = 'Invalid date format';
          return false;
        }
        // Parse the date parts to integers
        const parts = this.existingBirthday.split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10);
        const year = parseInt(parts[2], 10);
        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month === 0 || month > 12) {
          this.errorValidDate = 'Invalid date format';
          return false;
        }
        const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        // Adjust for leap years
        if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
          monthLength[1] = 29;
        }
        // Check the range of the day
        this.existingBirthday = month + '/' + day + '/' + year;
        this.day = day;
        this.month = month;
        this.year = year;
      }
      const obj = {
        'clientId': this.existingClientId,
        'gender': this.existingGender ? this.existingGender : showClientInfo.Gender__c,
        'mobile': mobileNo,
        'birthday': this.existingBirthday ? this.existingBirthday : showClientInfo.Birthdate,
        'email': this.existingEmail ? this.existingEmail : showClientInfo.Email,
        'cashDrawer': this.cashDrawer ? this.cashDrawer : '',
        'memUniId': this.existingMemUniId ? this.existingMemUniId : showClientInfo.Membership_ID__c,
        'sms_checkbox': sms_checkbox,
        'MemberShipType': this.existingMemberShip,
        'paymentTypes': this.existingPaymentTypes,
        'charge': (+this.existingCharge).toFixed(2),
        'Auto_Bill__c': this.existingAutoBill,
        'zipcode': this.ExistingZipcode,
        'plan': this.existingNextBillDate,
        'Next_Bill_Date__c': moment(membershipPlains, 'MM-DD-YYYY:HH:mm:sss').format('YYYY-MM-DD HH:mm:ss'),
      };
      this.membercheckinService.updateClientInfo(obj)
        .subscribe(data1 => {
          const dataObj = data1['result'];
          if (dataObj && dataObj.affectedRows > 0) {
            this.newMemberModal.hide();
            if (this.setPayment) {                                         // only cash
              this.saveNonCashpayment(obj, this.existingClientId);
            } else if (!this.setPayment) {
              if (this.existingAutoBill) {
                this.existingGenerateToken(obj, this.existingClientId);                              // for token and auto billing
              } else if (!this.existingAutoBill) {
                this.existingPaymentTicket(obj, this.existingClientId);                              // only payment
              }
            }
          } else {
            this.toastr.error('Record updated, but payment not done', null, { timeOut: 3500 });
          }
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                if (statuscode === '2040') {
                  this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                  window.scrollTo(0, 0);
                } else if (statuscode === '9998') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_CLIENT');
                  this.toastr.info(toastermessage.value, null, { timeOut: 4000 });
                } else if (statuscode === '2083') {
                  const toastermessage: any = this.translateService.get('CLIENTS.DUPLICATE_MEMBERSHIP_ID');
                  this.toastr.error(toastermessage.value, null, { timeOut: 3600 });
                } else if (statuscode === '2085' || statuscode === '2071') {
                  if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                  }
                } break;
            }
          });
    }
  }

  closeExistingMemberShip() {
    this.existingMemberShip = '';
    this.existingPaymentTypes = '';
    this.existingAutoBill = false;
    this.existingCardNumber = '';
    this.ExistingNameOnCard = '';
    this.existingCvv = '';
    this.ExistingZipcode = '';
    this.existingCharge = '';
    this.existingNextBillDate = '';
    this.existingMemUniId = '';
    this.amount = '';
    this.existingClient.hide();
  }

  clearData() {
    this.memberShipDetails = [];
    this.autoList = [];
    this.cSearch = '';
    this.hideTable = false;
    this.memberSearch = '';
    this.errMsg = '';
  }
  clearErrorMsg() {
    this.firstNameError = '';
    this.lastNameError = '';
    this.primaryEmailError = '';
    this.cardNumberError = '';
    this.NameOnCardError = '';
    this.memUniIdError = '';
    this.MemberShipTypeError = '';
    this.existingMemUniIdError = '';
    this.nextBillDateError = '';
    this.chargeError = '';
    this.paymentTypesError = '';
    this.expiryError = '';
    this.cvvError = '';
  }
  clearExistingMember() {
    this.existingMemUniIdError = '';
    this.NameOnCardError = '';
    this.expiryError = '';
    this.cvvError = '';
    this.chargeError = '';
    this.nextBillDateError = '';
  }
  clearErrorFileds() {
    this.firstName = '';
    this.lastName = '';
    this.gender = '';
    this.autoBill = false;
    this.sms_checkbox = false;
    this.existingSms_checkbox = false;
    this.NameOnCard = '';
    this.mobileNumber = '';
    this.sms_checkbox = '';
    this.existingSms_checkbox = '';
    this.gender = '';
    this.primaryEmail = '';
    this.memUniId = '';
    this.MemberShipType = '';
    this.paymentTypes = '';
    this.charge = '';
    this.cardNumber = '';
    this.cvv = '';
    this.zipCode = '';
    this.nextBillDate = '';
    this.charge = '';
  }
  errorHandler(event, i) {
    this.autoList[i]['image'] = '';
  }

  test(keyCode) {
    if (keyCode === 13) {
      this.searchMemberId();
    }
  }

  // sting and number only, and avoid special charters
  numbersOnly(event: any) {
    const pattern = /^[a-z0-9]+$/i;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  // number and decimal only
  validateNum(event: any) {
    const pattern = /^[0-9.]*$/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}


