import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModule } from '../common/share.module';
import { MembercheckinRoutingModule } from './membercheckin-routing.module';
import { MembercheckinComponent } from './membercheckin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from 'ng2-translate';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MembercheckinRoutingModule,
    ShareModule,
    BsDatepickerModule.forRoot(),
    PerfectScrollbarModule,
    ModalModule.forRoot()
  ],
  declarations: [MembercheckinComponent]
})
export class MemberCheckInModule { }
