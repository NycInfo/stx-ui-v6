import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { HttpClient } from '../common/http-client';

@Injectable()
export class MembercheckinService {
    constructor(private http: HttpClient,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
    ) { }

    getApptDetails(id) {
        return this.http.get(this.apiEndPoint + '/api/membership/getMemberSearch/' + id)
            .map(this.extractData);
    }
    clientDetails(id) {
        return this.http.get(this.apiEndPoint + '/api/membership/clientDetails/' + id)
            .map(this.extractData);
    }
    getmemberDetails(id, date) {
        return this.http.get(this.apiEndPoint + '/api/membership/getmemberDetails/' + id + '/' + date)
            .map(this.extractData);
    }
    xmlPayment(reqObj) {
        return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
            .map(this.extractData);
    }
    cloverApiPayment(obj) {
        return this.http.post(this.apiEndPoint + '/api/clover/payment',  obj)
          .map(this.extractData);
    }
    /**
  * To get paymenttypes Data
  */
    getPaymentTypesData() {
        return this.http.get(this.apiEndPoint + '/api/setupmembership/getPaymentList')
            .map(this.extractData);
    }
    addToClient(obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/client', { 'obj': [obj] })
            .map(this.extractData);
    }
    updateClientInfo(obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/ticketpaymentsExist', { 'obj': [obj] })
            .map(this.extractData);
    }
    addToPaymentsTicket(paymentObj, obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/ticketpayments', { 'paymentObj': [paymentObj], 'obj': [obj] })
            .map(this.extractData);
    }
    getWorkerMerchantsData() {
        return this.http.get(this.apiEndPoint + '/api/checkout/ticketpayments/worker/merchant')
            .map(this.extractData);
    }
    getSetupMemberships(inActive) {
        return this.http.get(this.apiEndPoint + '/api/setupmemberships/' + inActive)
            .map(this.extractData);
    }
    detClientId(id) {
        return this.http.get(this.apiEndPoint + '/api/setupmemberships/delete/' + id)
            .map(this.extractData);
    }
    getListOFAutoBilling() {
        return this.http.get(this.apiEndPoint + '/api/setupmembership/getListAutoBilling')
            .map(this.extractData);
    }
    saveAutoBilling(payment, memberData) {
        return this.http.post(this.apiEndPoint + '/api/setupmembership/saveAutoBilling', { 'payment': [payment], 'memberData': [memberData] })
            .map(this.extractData);
    }
    getHideCliContactInfo(id) {
        return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
          .map(this.extractData);
      }
    /*To extract json data*/
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }
}
