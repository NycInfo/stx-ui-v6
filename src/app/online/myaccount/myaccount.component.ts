import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MyaccountService } from './myaccount.service';
import * as config from '../../app.config';
import { Password } from '../../../custommodules/primeng/primeng';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from 'ng2-translate';
import { Renderer } from '@angular/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css'],
  providers: [MyaccountService]
})
export class MyaccountComponent implements OnInit {
  dbName = '';
  clientId = '';
  companyName = '';
  companyLogo = 'assets/images/logo.png';
  apiEndPoints = config['S3_URL'];
  clientInfo: any = {
    firstname: '',
    lastname: '',
    email: '',
    Notification_Primary_Email__c: 0,
    Marketing_Primary_Email__c: 0,
    Reminder_Primary_Email__c: 0,
    mobilePhone: '',
    mailingPostalCode: '',
    mailingCountry: '',
    mailingCity: '',
    address: '',
    gender: '',
    birthDate: '',
    day: '',
    month: '',
    year: '',
    mailingState: '',
    cmpName: '',
    Password1: '',
    Password2: '',
    Notification_Mobile_Phone__c: 0,
    Marketing_Mobile_Phone__c: 0,
    Reminder_Mobile_Phone__c: 0,
    Sms_Consent__c: 0
  };
  toastermessage: any;
  mailingCountriesList = [{ 'NAME': 'Canada' }, { 'NAME': 'United States' }];
  primaryCountryCode = '01';
  statesList: any = [];
  clientErr = '';
  newClientPictureFile: File;
  newclientPictureFileView: SafeUrl = '';
  mobileReq: any = false;
  mainDiv = true;
  notifyDiv =  false;
  passwrdDiv = false;
  glyphiconClass1 = 'glyphicon-eye-open';
  passwordType1 = 'password';
  glyphiconClass2 = 'glyphicon-eye-open';
  passwordType2 = 'password';
  divClass = ['text-active', '', ''];
  passwrdErr = '';
  companyId = '';
  cliImage: any = {};
  type: any;
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private render: Renderer,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private myaccountService: MyaccountService,
  ) {
    if (localStorage.getItem('clienttoken')) {
      this.dbName = localStorage.getItem('param');
      this.clientId = localStorage.getItem('clientid');
      this.companyName = localStorage.getItem('compname');
      this.companyId = localStorage.getItem('compid');
      this.companyLogo = this.apiEndPoints + localStorage.getItem('complogo') + '?time=' + new Date().getTime();
    } else {
      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
    }
    this.activatedRoute.queryParams.subscribe(params => {
      this.type = params['type'];
      if (this.type === 'notify') {
        this.showDiv('notify');
      } else if (this.type === 'password') {
        this.showDiv('password');
      } else {
        this.showDiv('info');
      }
  });
  }

  ngOnInit() {
    this.getclientData(this.clientId);
    this.myaccountService.getClientInfo(this.dbName).subscribe(
      data => {
        if (data['result']['mobileRequired']) {
          this.mobileReq = data['result']['mobileRequired'];
        } else {
          this.mobileReq = false;
        }
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
            break;
        }
      }
    );
  }
  imageUpload(fileEvent) {
    // this.newClientPictureFile = fileEvent.target.files[0];
    // this.newclientPictureFileView = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.newClientPictureFile));
    // this.imgErr = '';
    if (this.cliImage.hasOwnProperty('error')) {
      delete this.cliImage.error;
    }
    const eventObj: MSInputMethodContext = <MSInputMethodContext>fileEvent;
    const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    const files: FileList = fileEvent.target.files;
    if (files.length > 0) {
      this.cliImage.file = files[0];
      this.cliImage.fileName = files[0].name;
      const fSize = this.cliImage.file.size;
      if (!this.cliImage.fileName.match(/.(jpg|jpeg|png|gif|svg|bmp)$/i)) {
        this.cliImage.error = 'COMMON_STATUS_CODES.2058';
        delete this.cliImage.file;
      } else if (fSize > 1000000) {
        this.cliImage.allowableSize = false;
        this.cliImage.error = 'SETUPCOMPANY.VALID_IMAGE_FILENAME';
        delete this.cliImage.file;
      } else {
        this.newClientPictureFile = fileEvent.target.files[0];
    this.newclientPictureFileView = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.newClientPictureFile));
        // this.cliImage.srcURL = this.apiEndPoints + this.image + '?time=' + new Date().getTime();
        // this.cliImage.allowableSize = true;
        // this.cliImage.srcURL = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.cliImage.file));
      }
    }
  }
  hyphen_generate_Wphone(value) {
    if (value === undefined || value === null) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('-');
    }
  }
  mobileSms(val) {
    if (!val) {
      this.clientInfo['Sms_Consent__c'] = 0;
      this.clientInfo['Notification_Mobile_Phone__c'] = 0;
      this.clientInfo['Reminder_Mobile_Phone__c'] = 0;
      this.clientInfo['Marketing_Mobile_Phone__c'] = 0;
    }
  }
  mailSend(val) {
    if (!val) {
      this.clientInfo['Notification_Primary_Email__c'] = 0;
      this.clientInfo['Reminder_Primary_Email__c'] = 0;
      this.clientInfo['Marketing_Primary_Email__c'] = 0;
    }
  }
  pasteNumPhone(value) {
    let temp = '';
    if (value.indexOf('(') !== 0) {
      for (let i = 0; i < value.length; i++) {
        if (i === 0) {
          temp += '(' + value[i];
        } else if (i === 2) {
          temp += value[i] + ')';
        } else if (i === 5) {
          temp += value[i] + '-';
        } else {
          temp += value[i];
        }
        this.clientInfo.mobilePhone = temp.substr(0, 13);
      }
    }
  }
  reDirectRoute() {
    this.router.navigate(['/onlinebook']);
  }
  showDiv(param) {
    if (param === 'info') {
      this.mainDiv = true;
      this.notifyDiv = false;
      this.passwrdDiv = false;
      this.divClass = ['text-active', '', ''];
    } else if (param === 'notify') {
      this.mainDiv = false;
      this.notifyDiv = true;
      this.passwrdDiv = false;
      this.divClass = ['', 'text-active', ''];
    } else {
      this.mainDiv = false;
      this.notifyDiv = false;
      this.passwrdDiv = true;
      this.divClass = ['', '', 'text-active'];
    }
  }

  assaignValues(key) {
    this.clientInfo[key] = !this.clientInfo[key];
    this.clientInfo[key] = this.clientInfo[key] ? 1 : 0;
  }

  clearError() {
    this.clientErr = '';
  }
  getclientData(clientId) {
    this.myaccountService.getClient(clientId).subscribe(
      data => {
        const cliData = data['result'];
        if (cliData['results'][0]['Client_Pic__c']) {
          this.newclientPictureFileView = config.S3_URL + cliData['results'][0]['Client_Pic__c'];
        }
        this.clientInfo['clientImagePath'] = cliData['results'][0]['Client_Pic__c'];
        this.clientInfo['clientId'] = cliData['results'][0]['Id'];
        this.clientInfo['firstname'] = cliData['results'][0]['FirstName'];
        this.clientInfo['lastname'] = cliData['results'][0]['LastName'];
        this.clientInfo['email'] = cliData['results'][0]['Email'];
        if (cliData['results'][0]['MobilePhone'] && cliData['results'][0]['MobilePhone'].length === 16) {
          this.clientInfo['mobilePhone'] = cliData['results'][0]['MobilePhone'].split('-')[1] + '-' + cliData['results'][0]['MobilePhone'].split('-')[2];
        } else {
          this.clientInfo['mobilePhone'] = '';
        }
        if (cliData['results'][0]['MailingPostalCode']) {
          this.clientInfo['mailingPostalCode'] = cliData['results'][0]['MailingPostalCode'];
          this.clientInfo['mailingCountry'] = cliData['results'][0]['MailingCountry'];
          this.getCountry(this.clientInfo['mailingCountry']);
          this.clientInfo['mailingState'] = cliData['results'][0]['MailingState'];
          this.clientInfo['mailingCity'] = cliData['results'][0]['MailingCity'];
        } else {
          this.clientInfo['mailingPostalCode'] = '';
          this.clientInfo['mailingCountry'] = '';
          this.clientInfo['mailingState'] = '';
          this.clientInfo['mailingCity'] = '';
        }
        this.clientInfo['address'] = cliData['results'][0]['MailingStreet'];
        if (cliData['results'][0]['Gender__c'] !== '') {
        this.clientInfo['gender'] = cliData['results'][0]['Gender__c'];
        } else {
          this.clientInfo['gender'] = '';
        }
        if (cliData['results'][0]['BirthMonthNumber__c']!== undefined && cliData['results'][0]['BirthMonthNumber__c'] && cliData['results'][0]['BirthMonthNumber__c']!== undefined  
        && cliData['results'][0]['BirthDateNumber__c'] && cliData['results'][0]['BirthYearNumber__c'] && cliData['results'][0]['BirthYearNumber__c']!== undefined) {
          this.clientInfo['birthDate'] = cliData['results'][0]['BirthMonthNumber__c'] + '/' + cliData['results'][0]['BirthDateNumber__c'] + '/' + cliData['results'][0]['BirthYearNumber__c'];
        } else {
          this.clientInfo['birthDate'] = '';
        }
        this.clientInfo['Sms_Consent__c'] = cliData['results'][0]['Sms_Consent__c'];
        this.clientInfo['Notification_Primary_Email__c'] = cliData['results'][0]['Notification_Primary_Email__c'];
        this.clientInfo['Reminder_Primary_Email__c'] = cliData['results'][0]['Reminder_Primary_Email__c'];
        this.clientInfo['Marketing_Primary_Email__c'] = cliData['results'][0]['Marketing_Primary_Email__c'];
        this.clientInfo['Notification_Mobile_Phone__c'] = cliData['results'][0]['Notification_Mobile_Phone__c'];
        this.clientInfo['Reminder_Mobile_Phone__c'] = cliData['results'][0]['Reminder_Mobile_Phone__c'];
        this.clientInfo['Marketing_Mobile_Phone__c'] = cliData['results'][0]['Marketing_Mobile_Phone__c'];
        this.clientInfo['oldPassword'] = cliData['results'][0]['Pin__c'] ? cliData['results'][0]['Pin__c'] : '';
        /* Below two  if conditions are checking for email and mobile notifications in onload. */
        if (!this.clientInfo['mobilePhone']) {
          this.removeMobNotify();
        }
        if (!this.clientInfo['email']) {
          this.removeEmailNotify();
        }
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
            break;
        }
      }
    );
  }
  removeMobNotify() {
    this.clientInfo['Sms_Consent__c'] = 0;
    this.clientInfo['Notification_Mobile_Phone__c'] = 0;
    this.clientInfo['Reminder_Mobile_Phone__c'] = 0;
    this.clientInfo['Marketing_Mobile_Phone__c'] = 0;
  }
  removeEmailNotify() {
    this.clientInfo['Notification_Primary_Email__c'] = 0;
    this.clientInfo['Reminder_Primary_Email__c'] = 0;
    this.clientInfo['Marketing_Primary_Email__c'] = 0;
  }
  togglePassword(index) {
    if (index === 1) {
      if (this.glyphiconClass1 === 'glyphicon-eye-open') {
        this.glyphiconClass1 = 'glyphicon-eye-close';
        this.passwordType1 = 'text';
      } else {
        this.glyphiconClass1 = 'glyphicon-eye-open';
        this.passwordType1 = 'password';
      }
    } else if (index === 2) {
      if (this.glyphiconClass2 === 'glyphicon-eye-open') {
        this.glyphiconClass2 = 'glyphicon-eye-close';
        this.passwordType2 = 'text';
      } else {
        this.glyphiconClass2 = 'glyphicon-eye-open';
        this.passwordType2 = 'password';
      }
    }
  }
  getLocation() {
    if (this.clientInfo['mailingPostalCode'].length > 4) {
      this.http.get('https://ziptasticapi.com/' + this.clientInfo['mailingPostalCode']).subscribe(
        result => {
          if (result['error']) {
            this.clientErr = 'SETUPCOMPANY.ZIP_CODE_NOT_FOUND';
            this.clientInfo['mailingCountry'] = '';
            this.clientInfo['mailingState'] = '';
            this.clientInfo['mailingCity'] = '';
          } else {
            if (result['country'] === 'US') {
              this.clientInfo['mailingCountry'] = 'United States';
              this.getCountry(this.clientInfo['mailingCountry']);
              config.states.forEach(state => {
                if (state.abbrev === result['state']) {
                  this.clientInfo['mailingState'] = state.name;
                }
              });
            }
            const cityArray = result['city'].split(' ');
            for (let i = 0; i < cityArray.length; i++) {
              if (i === 0) {
                this.clientInfo['mailingCity'] = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              } else {
                this.clientInfo['mailingCity'] += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              }
            }
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                }
              }
              break;
          }
        }
      );
    }
  }
  getCountry(coun) {
    this.myaccountService.getStates(coun)
      .subscribe(statesValues => {
        this.statesList = statesValues['result'];
      },
        error => {
          const errorMessage = <any>error;
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
  }
  saveProfile() {
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!this.clientInfo['firstname']) {
      this.clientErr = 'FirstName is required';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if (!this.clientInfo['lastname']) {
      this.clientErr = 'LastName is required';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if (!this.clientInfo['email']) {
      this.clientErr = 'Email is required';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if (!EMAIL_REGEXP.test(this.clientInfo['email'])) {
      this.clientErr = 'Invalid Email Address';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if ((this.clientInfo['Notification_Mobile_Phone__c'] === 1 || this.clientInfo['Marketing_Mobile_Phone__c'] === 1
      || this.clientInfo['Reminder_Mobile_Phone__c'] === 1 || this.mobileReq) && this.clientInfo['mobilePhone'] === '') {
      this.clientErr = 'Mobile Phone is required';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if (this.clientInfo['mobilePhone'] !== '' && (this.clientInfo['mobilePhone'].length < 13 || this.clientInfo['mobilePhone'].length > 13 )) {
      this.clientErr = 'Mobile Phone is invalid';
      this.showDiv('info');
      window.scrollTo(0, 0);
    } else if (this.clientInfo.Password1 !== this.clientInfo.Password2) {
      this.clientErr = 'Password should be same';
      this.showDiv('password');
    } else {
      if (this.clientInfo['birthDate']) {
        // First check for the patternp
        const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (!datePattern.test(this.clientInfo['birthDate'])) {
          this.clientErr = 'Invalid date format';
          window.scrollTo(0, 0);
          this.showDiv('info');
          return false;
        }
        // Parse the date parts to integers
        const parts = this.clientInfo['birthDate'].split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10);
        const year = parseInt(parts[2], 10);
        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month === 0 || month > 12) {
          this.clientErr = 'Invalid date format';
          this.showDiv('info');
          window.scrollTo(0, 0);
          return false;
        }
        const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
          monthLength[1] = 29;
        }
        // Check the range of the day
        this.clientInfo['birthDate'] = month + '/' + day + '/' + year;
        this.clientInfo['day'] = day;
        this.clientInfo['month'] = month;
        this.clientInfo['year'] = year;
      }
      if (!this.clientInfo['day']) {
        this.clientInfo['day'] = null;
      }
      if (!this.clientInfo['month']) {
        this.clientInfo['month'] = null;
      }
      if (!this.clientInfo['year']) {
        this.clientInfo['year'] = null;
      }
      const currentDate = new Date();
      const dtStr = currentDate.getFullYear()
        + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2)
        + '-' + ('0' + currentDate.getDate()).slice(-2)
        + ' ' + ('0' + currentDate.getHours()).slice(-2)
        + ':' + ('0' + currentDate.getMinutes()).slice(-2)
        + ':' + ('0' + currentDate.getSeconds()).slice(-2);
      this.clientInfo['dbName'] = this.dbName;
      this.clientInfo['cmpName'] = this.companyName;
      this.clientInfo['cmpId'] = this.companyId;
      this.clientInfo['clientInfoMailingCountry'] = 'United States';
      this.clientInfo['email_c'] = localStorage.getItem('compEmail__c');
      this.clientInfo['mobilePhone'] = this.clientInfo['mobilePhone'] ? this.primaryCountryCode + '-' + this.clientInfo['mobilePhone'] : '';
      this.clientInfo['dt'] = dtStr;
      this.myaccountService.saveClientProfile(this.clientInfo, this.newClientPictureFile, this.companyId).subscribe(
        data => {
          const cliientData = data['result'];
          this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
          this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
          this.router.navigate(['/onlinebook']);
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2088') {
                this.clientErr = 'CLIENTS.DUPLICATE_CLIENT';
                this.showDiv('info');
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                }
              } break;
          }
        }
      );
    }
  }

}
