import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyaccountComponent } from './myaccount.component';
import { MyaccountRoutingModule } from './myaccount.routing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
@NgModule({
    imports: [
        CommonModule,
        MyaccountRoutingModule,
        FormsModule,
        TranslateModule,
        //  ModalModule
       // BsDatepickerModule.forRoot()
    ],
    declarations: [
        MyaccountComponent
    ]
})
export class MyaccountModule {
}
