import { Injectable, Inject } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { HttpClient } from '../../common/http-client';

@Injectable()
export class OnlineClientAddService {

  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string
  ) { }
  saveClientProfile(clientObj, clientPictureFile: File, companyId) {
    const formData: any = new FormData();
    formData.append('clientPictureFile', clientPictureFile);
    formData.append('clientObj', JSON.stringify(clientObj));
    const headers = new Headers();
    headers.append('cid', companyId);
    return this.http.postHeader(this.apiEndPoint + '/api/onlineclientlogin', formData, headers)
      .map(this.extractData);
  }
  getClientInfo(dbName) {
    return this.http.get(this.apiEndPoint + '/api/client/info/' + dbName)
      .map(this.extractData);
  }
  getStates(countryName) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
      .map(this.extractData);
  }
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('clienttoken', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }

}
