import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as config from '../../app.config';
import { OnlineClientApptsService } from './onlineclientappts.service';
import { CommonService } from '../../common/common.service';
import { DatePipe } from '@angular/common';
import { ClientAppointmentsService } from '../../setup/setupclientappointments/clientappointments.service';

@Component({
  selector: 'app-onlineclientappts',
  templateUrl: './onlineclientappts.component.html',
  styleUrls: ['./onlineclientappts.component.css'],
  providers: [OnlineClientApptsService, CommonService]
})
export class OnlineClientApptsComponent implements OnInit {
  bsValue = new Date();
  timeStartTime: any;
  timeStartDate: any;
  companyName = '';
  companyLogo = '';
  companyPhone = '';
  apiEndPoints = config['S3_URL'];
  clientName: any;
  // clientName = localStorage.getItem('fname') + ' ' + localStorage.getItem('lname');
  apptType = 'future';
  pastApptsList = [];
  futureApptsList = [];
  displayList = [];
  deletdData = [];
  clientPic = '';
  clientid: any;
  allowApptChange: any;
  allowCancelChange: any;
  loadmore = 5;
  hideLoadMoreButt = false;
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private apiService: OnlineClientApptsService,
    private commonService: CommonService) {
    this.companyLogo = this.apiEndPoints + localStorage.getItem('complogo') + '?time=' + new Date().getTime();
  }

  ngOnInit() {
    this.clientid = localStorage.getItem('clientid');
    this.listClientFields();
    if (localStorage.getItem('clienttoken')) {
      this.companyName = localStorage.getItem('compname');
      this.companyPhone = localStorage.getItem('compphone');
      this.apiService.getAppointments(this.clientid).subscribe(
        data => {
          if (data['result']['clientData']) {
            this.clientName = data['result']['clientData'].clientName;
            this.clientPic = data['result']['clientData']['Client_Pic__c'] ? config.S3_URL + data['result']['clientData']['Client_Pic__c'] : '';
          } else if (data['result']['apptData']) {
            this.clientName = data['result']['apptData'][0].clientName;
            this.clientPic = data['result']['apptData'][0]['Client_Pic__c'] ? config.S3_URL + data['result']['apptData'][0]['Client_Pic__c'] : '';
          }
          const tempDatObj = new Date();
          const todayDate = new Date();
          todayDate.setHours(0, 0, 0, 0);
          // for (let i = 0; i < data['result']['apptData'].length; i++) {
          //   const tempAry = this.commonService.getAMPM(data['result']['apptData'][i]['Service_Date_Time__c']);
          //   const temp2 = tempAry.split('$');
          //   data['result']['apptData'][i]['apptDt'] = temp2[0];
          //   data['result']['apptData'][i]['apptTime'] = temp2[1];
          //   data['result']['apptData'][i]['past'] = this.validateDate(data['result']['apptData'][i]['Service_Date_Time__c'], tempDatObj);
          //   // data['result']['apptData'][i]['checked'] = false;
          //   for (let j = i + 1; j < data['result']['apptData'].length; j++) {
          //     if (data['result']['apptData'][i]['Id'] && data['result']['apptData'][j]['Id']
          //       && data['result']['apptData'][i]['Id'] === data['result']['apptData'][j]['Id']) {
          //       data['result']['apptData'][j]['Id'] = null;
          //       data['result']['apptData'][i]['past'] = this.validateDate(data['result']['apptData'][j]['Service_Date_Time__c'], tempDatObj);
          //     }
          //   }
          //   data['result']['apptData'][i]['rebookAllowed'] =
          //     this.isReebookAllowed(this.commonService.getDateTmFrmDBDateStr(data['result']['apptData'][i]['Appt_Date_Time__c']), todayDate, data['result']['apptData'][i]['Status__c']);
          // }
          // this.pastApptsList = data['result']['apptData'].filter(item => item['past']);
          // this.futureApptsList = data['result']['apptData'].filter(item => !item['past']);
          // this.updateDisplayList();
          // } else {
          //   this.displayList = [];
          // }
          if (data['result']['apptData']) {
            for (let i = 0; i < data['result']['apptData'].length; i++) {
              data['result']['apptData'][i]['rebookAllowed'] =
                this.isReebookAllowed(this.commonService.getDateTmFrmDBDateStr(data['result']['apptData'][i]['Appt_Date_Time__c']), todayDate, data['result']['apptData'][i]['Status__c']);
            }
            this.displayList = this.groupApptServices(data['result']['apptData']);
            this.displayList = this.addPastFutureTag(this.displayList);
            this.displayList = this.addDateTimeStrings(this.displayList);
            this.pastApptsList = this.displayList.filter(item => item['apptType'] === 'past');
            this.futureApptsList = this.displayList.filter(item => item['apptType'] === 'future');
            this.updateDisplayList();
          } else {
            this.displayList = [];
          }
        },
        error => {
          const statuscode = JSON.parse(error['_body']).status;
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        }
      );
    } else {
      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
    }
  }

  validateDate(dbDtStr: string, curDtTmObj: Date) {
    const dtObj = this.commonService.getDateTmFrmDBDateStr(dbDtStr);
    return (curDtTmObj > dtObj);
  }

  updateDisplayList() {
    if (this.apptType === 'future') {
      this.displayList = this.futureApptsList;
    } else {
      this.displayList = this.pastApptsList;
    }
    this.displayList.forEach(element => {
      this.bsValue = this.commonService.getDateTmFrmDBDateStr(element.apptDate);
      this.timeStartDate = new DatePipe('en-Us').transform(this.bsValue, 'MMMM d y');
      this.timeStartTime = new DatePipe('en-Us').transform(this.bsValue, 'hh:mm a');
      element.timeStartDate = this.timeStartDate;
      element.timeStartTime = this.timeStartTime;
    });
    this.loadmore = 5;
    if (this.loadmore > this.displayList.length || this.loadmore === this.displayList.length) {
      this.hideLoadMoreButt = true;
    } else {
      this.hideLoadMoreButt = false;
    }
  }
  moreAppts(app) {
    this.loadmore += 5;
    if (this.loadmore > app.length || this.loadmore === app.length) {
      this.hideLoadMoreButt = true;
    } else {
      this.hideLoadMoreButt = false;
    }
  }

  cancelAppts(i) {
    this.deletdData = [];
    for (let j = 0; j < this.futureApptsList.length; j++) {
      if (i === j) {
        this.deletdData.push(this.futureApptsList[j]);
      }
    }
    if (this.deletdData[0]['Status__c'] === 'Complete' || this.deletdData[0]['Status__c'] === 'Checked In') {
      this.toastr.error('You cannot cancel completed appointment', null, { timeOut: 3000 });
    } else {
      const dataObj = {
        'apptIds': this.deletdData
      };
      this.apiService.cancelAppts(dataObj).subscribe(
        data => {
          this.toastr.success('Selected Appointment canceled successfully', null, { timeOut: 3000 });
          this.apiService.sendCancelReminder(this.deletdData).subscribe(
            data1 => {
              const status = data1['result'];
            }, error1 => {
              const status = JSON.parse(error1['status']);
              const statuscode = JSON.parse(error1['_body']).status;
              switch (status) {
                case 500:
                  break;
                case 400:
                  break;
              }
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              }
            });
          this.ngOnInit();
        },
        error => {
          const statuscode = JSON.parse(error['_body']).status;
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        }
      );
    }
    this.updateDisplayList();
  }

  isReebookAllowed(apptDate: Date, todayDate: Date, status: string) {
    apptDate.setHours(0, 0, 0, 0);
    return (apptDate.getTime() === todayDate.getTime() && (status === 'Checked In' || status === 'Complete'));
  }
  listClientFields() {
    this.apiService.getOnlineData().subscribe(
      data => {
        this.allowCancelChange = data['result'].allowApptCancellations;
        this.allowApptChange = data['result'].allowApptChanges;
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
            break;
        }
      }
    );
  }

  groupApptServices(apptServices) {
    const apptIds = [];
    const appts = [];
    apptServices.forEach(element => {
      if (apptIds.indexOf(element['Id']) === -1) {
        apptIds.push(element['Id']);
      }
    });
    apptIds.forEach(apptId => {
      const apptsAry = apptServices.filter(obj => obj['Id'] === apptId);
      appts.push({
        'Id': apptId,
        'services': [],
        'Booked_Online__c': apptsAry[0]['Booked_Online__c'],
        'bookAgain': apptsAry[0]['bookAgain'],
        'rebookAllowed': apptsAry[0]['rebookAllowed'],
        'Status__c': apptsAry[0]['Status__c']
      });
      apptsAry.forEach(apt => {
        appts[appts.length - 1]['services'].push(apt['serviceName'] + ' (' + apt['names'] + ')');
        if (!appts[appts.length - 1]['apptDate']) {
          appts[appts.length - 1]['apptDate'] = apt['Appt_Date_Time__c'];
        } else {
          if (new Date(appts[appts.length - 1]['apptDate']) > new Date(apt['Appt_Date_Time__c'])) {
            appts[appts.length - 1]['apptDate'] = apt['Appt_Date_Time__c'];
          }
        }
      });
    });
    return appts;
  }

  addPastFutureTag(apptArray) {
    const curDate = new Date();
    let temp;
    apptArray.forEach(appt => {
      temp = this.commonService.getDateTmFrmDBDateStr(appt['apptDate']);
      if (temp > curDate) {
        appt['apptType'] = 'future';
      } else {
        appt['apptType'] = 'past';
      }
    });
    return apptArray;
  }

  addDateTimeStrings(dataArray) {
    const monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
      'September', 'October', 'November', 'December'];
    dataArray.forEach(element => {
      const apptDateObj = new Date(element['apptDate']);
      let hrs = ('0' + (apptDateObj.getHours() % 12)).slice(-2);
      if (apptDateObj.getHours() === 0 || apptDateObj.getHours() === 12) {
        hrs = '12';
      }
      element['apptDt'] = new DatePipe('en-Us').transform(this.bsValue, 'MMMM d y');
      element['apptTime'] = new DatePipe('en-Us').transform(this.bsValue, 'hh:mm a');
      // element['apptDt'] = monthsArray[apptDateObj.getMonth()] + ' ' + apptDateObj.getDate() + ', ' + apptDateObj.getFullYear();
      // element['apptTime'] = hrs + ':' + ('0' + apptDateObj.getMinutes()).slice(-2) + ' ';
      if (apptDateObj.getHours() / 12 >= 1) {
        element['apptTime'] += 'PM';
      } else {
        element['apptTime'] += 'AM';
      }
    });
    return dataArray;
  }
}
