/**
 getPaymentTypesDataGlobal() :in getpaymentTypesdata global method we are getting global company info and ticket reciept memo
 */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as config from '../../app.config';
import { OnlineGiftService } from './onlinegift.service';
import { OnlinePackagePurchaseService } from '../onlinepackagepurchase/onlinepackagepurchase.service';
import { CommonService } from '../../common/common.service';
import { CheckOutEditTicketService } from '../../checkout/editticket/checkouteditticket.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Md5 } from 'ts-md5/dist/md5';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-online-gift',
  templateUrl: './onlinegift.component.html',
  styleUrls: ['./onlinegift.component.css'],
  providers: [OnlineGiftService, CommonService]
})
export class OnlineGiftComponent implements OnInit {
  apptId: any;
  toastermessage: any;
  decodedToken: any;
  showPayment: any;
  companyName = '';
  clientBookingInfo: any;
  clientFstName = '';
  clientLstName = '';
  companyLogo = 'assets/images/logo.png';
  apiEndPoints = config['S3_URL'];
  mainDiv: any = true;
  amountList = ['25.00', '50.00', '75.00', '100.00', 'other'];
  // deliveryTypes = ['Email to Recipient', 'Email to Me'];
  dbName = '';
  giftErr = '';
  isTokenExsists;
  orderId: any;
  giftPurchaseObj: any = {
    Amount: 0,
    // deliveryType: '',
    RecipientName: '',
    RecipientEmail: '',
    firstname: '',
    lastname: '',
    PersonalMessage: '',
    // email: '',
    listedAmount: '',
    Transaction_Type__c: 'Gift'
  };
  giftAmount: any = '';
  amount = 0;
  cardNumber: any;
  cardHolderEmail = '';
  cardType: any = '';
  statesList: any;
  MailingPostalCode: any = '';
  mailingCountry = 'United States';
  mailingState: any;
  mailingCity: any;
  packageId: any;
  merchantWorkerList: any = [];
  paymentGateWay: any = '';
  merchantAccntName: any = '';
  bill: any;
  authCode = '';
  cardTypes = [];
  cardName = '';
  mailingCountriesList = [{ 'NAME': 'Canada' }, { 'NAME': 'United States' }];
  monthList = ['01 - January', '02 - February', '03 - March', '04 - April', '05 - May', '06 - June',
    '07 - July', '08 - August', '09 - September', '10 - October', '11 - November', '12 - December'];
  yearList = [];
  expYear = 0;
  cvv: any;
  errorMessage: any;
  expMonth = 1;
  mailingStreet = '';
  error: any;
  PckgError: any;
  withoutLogin: any;
  isNew = false;
  deletePaymnt = false;
  /*fro cardholder reciept fields starts */
  posLists: any;
  clientId = '';
  apptName = '';
  companyData: any = [];
  giftNumber = '';
  creationDate = ['', ''];
  lastModifiedDate = ['', ''];
  posDataObjList: any = [];
  merchantId = '';
  accessToken = '';
  addTest = false;
  electronicPaymentData: any;
  paymentCardId: any;
  constructor(
    private router: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private onlineGiftService: OnlineGiftService,
    private commonService: CommonService) {
    this.companyName = localStorage.getItem('compname');
    this.companyLogo = this.apiEndPoints + localStorage.getItem('complogo') + '?time=' + new Date().getTime();
    this.dbName = localStorage.getItem('param');
    this.isTokenExsists = localStorage.getItem('clienttoken') ? true : false;
    if (this.isTokenExsists) {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('clienttoken'));
      this.clientId = this.decodedToken['data']['Id'];
      this.clientFstName = this.decodedToken['data']['firstName'];
      this.clientLstName = this.decodedToken['data']['lastName'];
      this.cardHolderEmail = this.decodedToken['data']['userName'];
    } else {
      this.clientId = '';
      this.clientFstName = '';
      this.clientLstName = '';
      this.cardHolderEmail = '';
    }
    this.activatedRoute.queryParams.subscribe(params => {
      this.withoutLogin = activatedRoute.snapshot.params['param'];
    });
  }

  ngOnInit() {
    this.onlineGiftService.getClientInfo(this.dbName).subscribe(
      data => {
        if (data['result']['isNew']) {
          this.isNew = data['result']['isNew'];
        } else {
          this.isNew = false;
        }
      },
      error => {
        this.toastr.error('Invalid URL', null, { timeOut: 3000 });
      }
    );
    this.giftPurchaseObj.listedAmount = ''; /**this.amountList[0] */
    // this.giftPurchaseObj.deliveryType = ''; /**this.deliveryTypes[0] */
    this.createYearsList();
    this.getCountry('United States');
    if (this.withoutLogin === 'any') {
      this.getPaymentTypesDataGlobal();
    } else {
      this.companyInfo();
      this.getPaymentTypes();
      this.posDeviceList();
      this.onlinepos();
    }
  }
  onlinepos() {
    this.onlineGiftService.getPos().subscribe(data => {
      this.posDataObjList = data['result'];
      if (this.posDataObjList[0].JSON__c) {
        const merchantData1 = JSON.parse(this.posDataObjList[0].JSON__c);
        this.merchantId = merchantData1.merchantId;
        this.accessToken = merchantData1.accessToken;
        this.addTest = merchantData1.test;
      }
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/clientlogin/' + this.dbName]);
          }
        }
      });
  }
  reDirectRoute() {
    if (!this.isTokenExsists) {
      this.router.navigate(['/clientlogin/' + this.dbName]);
    } else {
      this.router.navigate(['/onlinebook']);
      // this.mainDiv = true;
      // this.showPayment = false;
    }
  }
  directRoute() {
    if (!this.isTokenExsists) {
      this.router.navigate(['/onlinegift/any']);
    } else {
      this.router.navigate(['/onlinegift']);
    }
    this.clear();
    this.mainDiv = true;
    this.showPayment = false;
    const element = document.getElementById('countdownId');
    element.style.display = 'none';
    const highestTimeoutId = setTimeout(';');
    for (let i = 0; i < highestTimeoutId; i++) {
      clearTimeout(i);
    }
  }
  clear() {
    this.giftAmount = '';
    this.giftPurchaseObj['RecipientName'] = '';
    this.giftPurchaseObj['RecipientEmail'] = '';
    this.giftPurchaseObj['PersonalMessage'] = '';
    this.clientFstName = '';
    this.clientLstName = '';
    this.cardNumber = '';
    this.cvv = '';
    this.cardHolderEmail = '';
    this.MailingPostalCode = '';
  }
  gotoRouting(type) {
    if (type === 'gift') {
      if (this.isTokenExsists) {
        this.mainDiv = true;
        this.showPayment = false;
        this.clear();
      } else {
        this.router.navigate(['/onlinegift/any']);
        this.mainDiv = true;
        this.showPayment = false;
        this.clear();
      }
    } else {
      if (this.isTokenExsists) {
        this.router.navigate(['/onlinebook']);
      } else {
        this.router.navigate(['/clientlogin/' + this.dbName]);
      }
    }
  }
  createYearsList() {
    const curtYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.yearList.push(curtYear + i);
    }
    this.expYear = this.yearList[0];
  }
  getCountry(coun) {
    this.onlineGiftService.getStates(coun)
      .subscribe(statesValues => {
        this.statesList = statesValues['result'];
      },
        error => {
          this.errorMessage = <any>error;
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + this.dbName]);
            }
          }
        });
  }
  getLocation() {
    if (this.MailingPostalCode && this.MailingPostalCode.length > 4) {
      this.http.get('https://ziptasticapi.com/' + this.MailingPostalCode).subscribe(
        result => {
          if (result['error']) {
            const toastermessage: any = this.translateService.get('SETUPCOMPANY.ZIP_CODE_NOT_FOUND');
            this.toastr.error(toastermessage.value, null, { timeOut: 1500 });
          } else {
            if (result['country'] === 'US') {
              this.mailingCountry = 'United States';
              this.getCountry(this.mailingCountry);
              config.states.forEach(state => {
                if (state.abbrev === result['state']) {
                  this.mailingState = state.name;
                }
              });
            }
            const cityArray = result['city'].split(' ');
            for (let i = 0; i < cityArray.length; i++) {
              if (i === 0) {
                this.mailingCity = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              } else {
                this.mailingCity += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              }
            }
          }
        },
        error => {
        }
      );
    }
  }
  getWorkerMerchants() {
    this.onlineGiftService.getWorkerMerchantsData()
      .subscribe(data => {
        this.merchantWorkerList.push({
          Payment_Gateway__c: 'AnywhereCommerce', FirstName: 'STX',
          LastName: 'QA 2017', Id: 'default_stx'
        });
        if (data['result'] && data['result'].length > 0) {
          this.merchantWorkerList = this.merchantWorkerList.concat(data['result']);
        }
        // for default values
        this.paymentGateWay = this.merchantWorkerList[0]['Payment_Gateway__c'];
        this.merchantAccntName = this.merchantWorkerList[0]['FirstName'] + ' ' + this.merchantWorkerList[0]['LastName'];
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } else if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + this.dbName]);
                }
              } break;
          }
        });
  }
  getPaymentTypes() {
    this.onlineGiftService.getPaymentTypesData().subscribe(data => {
      this.cardTypes = data.result.paymentResult.filter(filterList => filterList.Process_Electronically_Online__c === 1
        && filterList.Active__c === 1 && filterList.Id !== 'a0M1H00000UeiV6UAJ');
      this.orderId = data.result.Id;
      this.cardType = this.cardTypes[0]['Id'];
      this.cardName = this.cardTypes[0]['Abbreviation__c'];
      this.electronicPaymentData = data.result.paymentResult.filter(filterList => filterList.Name === 'Electronic Payment');
      if (this.electronicPaymentData.length > 0) {
        this.paymentCardId = this.electronicPaymentData[0]['Id'];
      } else {
        this.paymentCardId = this.cardTypes[0]['Id'];
      }

    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + this.dbName]);
          }
        }
      });
  }
  /**
   //  * need to ask client for payment url
   //  */
  makePayment() {
    if (this.merchantId && this.merchantId) {
      if (this.cardNumber) {
        this.cardType = this.commonService.getCardType(this.cardNumber);
      }
      let paymentData;
      const d = new Date();
      const cvvtest = /^[0-9]{3,4}$/;
      const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      // const tokenbody = this.commonService.createPaymentToken(clientData);
      const url = config.ANYWHERECOMMERCE_PAYMENT_API;
      if (this.clientFstName === '' || this.clientFstName === undefined) {
        this.PckgError = 'ONLINE_GIFT.CARDHOLDER_FRSTNAME_REQUIRED';
        window.scrollTo(0, 400);
      } else if (this.clientLstName === '' || this.clientLstName === undefined) {
        this.PckgError = 'ONLINE_GIFT.CARDHOLDER_LASTNAME_REQUIRED';
      } else if (this.cardHolderEmail === '') {
        this.PckgError = 'ONLINE_GIFT.CARDHOLDER_EMAIL_REQUIRED';
        window.scrollTo(0, 400);
      } else if (this.cardHolderEmail !== '' && !EMAIL_REGEXP.test(this.cardHolderEmail)) {
        this.PckgError = 'ONLINE_GIFT.CARDHOLDER_EMAIL_NOT_VALID';
      } else if (this.cardNumber === '' || this.cardNumber === undefined) {
        this.PckgError = 'ONLINE_GIFT.CARD_NUMBER_REQUIRED';
        window.scrollTo(0, 400);
      } else if (this.cardNumber < 0) {
        this.PckgError = 'ONLINE_GIFT.CARD_NUMBER_ONLY_NUMBER_MAY_ENTERED';
        window.scrollTo(0, 400);
      } else if (this.cardNumber.length >= 0 && this.cardNumber.length < 14) {
        this.PckgError = 'ONLINE_GIFT.ENTER_VALID_CARD_NUMBER';
        window.scrollTo(0, 400);
      } else if (this.cvv === '' || this.cvv === undefined) {
        this.PckgError = 'ONLINE_GIFT.CVV_REQUIRED';
        window.scrollTo(0, 400);
      } else if (this.cvv.length < 3 || this.cvv.length > 4) {
        this.PckgError = 'ONLINE_GIFT.CVV_LENGTH_SHOULD_3_REQUIRED';
        window.scrollTo(0, 400);
      } else if (this.cvv < 0) {
        this.PckgError = 'ONLINE_GIFT.CVV_ONLY_NUMBER_ENTERED';
        window.scrollTo(0, 400);
      } else if (!cvvtest.test(this.cvv)) {
        this.PckgError = 'ONLINE_GIFT.CVV_ONLY_NUMBER_ENTERED';
        window.scrollTo(0, 400);
      } else if (this.MailingPostalCode === '' || this.MailingPostalCode === undefined) {
        this.PckgError = 'ONLINE_GIFT.POSTAL_CODE_SHOULD_REQUIRED';
        window.scrollTo(0, 400);
      } else {
        if ((this.expYear <= d.getFullYear()) && ((this.expMonth < d.getMonth() + 1))) {
          this.error = 'ONLINE_GIFT.INVALID_EXPIRY_DATE';
          window.scrollTo(0, 400);
        } else {
          this.giftPurchaseObj.online_c = 1;
          this.giftPurchaseObj['firstname'] = this.clientFstName;
          this.giftPurchaseObj['lastname'] = this.clientLstName;
          this.giftPurchaseObj['email'] = this.cardHolderEmail;

          if (this.withoutLogin === 'any') {
            this.onlineGiftService.onlineGiftPurchaseWithoutlogin(this.giftPurchaseObj).subscribe((data) => {
              this.apptId = data['result']['apptId'];
              this.apptName = data['result']['apptName'];
              this.clientId = data['result']['clientId'];
              const giftNum = data['result']['giftData']['Gift_Number__c'];
              if (data['result']['creationDate'] && data['result']['lastModifiedDate']) {
                this.creationDate = this.commonService.getUsrDtStrFrmDBStr(data['result']['creationDate']);
                this.lastModifiedDate = this.commonService.getUsrDtStrFrmDBStr(data['result']['lastModifiedDate']);
              }
              const clientData1 = {
                cardNumber: this.cardNumber,
                currency: 'USD',
                cvv: this.cvv,
                amount: this.giftPurchaseObj.Amount,
                expMonth: ('0' + this.expMonth).slice(-2),
                expYear: this.expYear.toString().slice(-2),
                onlinegiftWithOutLogin: true,
                dbName: localStorage.getItem('param')
              };
              this.onlineGiftService.cloverPaymentWithOutLogin(clientData1, this.withoutLogin).subscribe(
                data2 => {
                  const paymentData1 = data2['result'];
                  if (paymentData1 && paymentData1['result'] === 'APPROVED') {
                    paymentData1.Gift_Number__c = giftNum;
                    this.savePaymentsData(paymentData1);
                    this.deletePaymnt = true;
                  } else {
                    if (this.deletePaymnt) {
                      this.deleteDataWhenPaymentFailed(this.apptId);
                    } else {
                      this.error = 'ONLINE_GIFT.ERROR_OCCURED_INVALID_DETAILS';
                      window.scrollTo(0, 400);
                      this.mainDiv = false;
                      this.clear();
                    }
                  }
                },
                error => {
                  const status = JSON.parse(error['status']);
                  const statuscode = JSON.parse(error['_body']).status;
                  switch (status) {
                    case 500:
                      break;
                    case 400:
                      if (statuscode === '2040') {
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                      } else if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                          localStorage.setItem('page', this.router.url);
                          this.router.navigate(['clientlogin/' + this.dbName]);
                        }
                      } break;
                  }
                });
              // this.router.navigate([localStorage.getItem('page')]);
            },
              error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                  case 500:
                    break;
                  case 400:
                    if (statuscode === '2085' || statuscode === '2071') {
                      if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                      }
                    }
                    break;
                }
              });
          } else {
            const clientData1 = {
              cardNumber: this.cardNumber,
              currency: 'USD',
              cvv: this.cvv,
              amount: this.giftPurchaseObj.Amount,
              expMonth: ('0' + this.expMonth).slice(-2),
              expYear: this.expYear.toString().slice(-2)
            };
            this.onlineGiftService.onlineGiftPurchase(this.giftPurchaseObj).subscribe((data) => {
              this.apptId = data['result']['apptId'];
              this.apptName = data['result']['apptName'];
              this.clientId = data['result']['clientId'];
              const giftNum = data['result']['giftData']['Gift_Number__c'];
              if (data['result']['creationDate'] && data['result']['lastModifiedDate']) {
                this.creationDate = this.commonService.getUsrDtStrFrmDBStr(data['result']['creationDate']);
                this.lastModifiedDate = this.commonService.getUsrDtStrFrmDBStr(data['result']['lastModifiedDate']);
              }
              this.onlineGiftService.cloverPayment(clientData1, this.withoutLogin).subscribe(
                data2 => {
                  const paymentData1 = data2['result'];
                  if (paymentData1 && paymentData1['result'] === 'APPROVED') {
                    paymentData1.Gift_Number__c = giftNum;
                    this.savePaymentsData(paymentData1);
                    this.deletePaymnt = true;
                  } else {
                    if (this.deletePaymnt) {
                      this.deleteDataWhenPaymentFailed(this.apptId);
                    } else {
                      this.error = 'ONLINE_GIFT.ERROR_OCCURED_INVALID_DETAILS';
                      window.scrollTo(0, 400);
                      this.mainDiv = false;
                      this.clear();
                    }
                  }
                },
                error => {
                  const status = JSON.parse(error['status']);
                  const statuscode = JSON.parse(error['_body']).status;
                  switch (status) {
                    case 500:
                      break;
                    case 400:
                      if (statuscode === '2040') {
                        this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                        window.scrollTo(0, 0);
                      } else if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                          localStorage.setItem('page', this.router.url);
                          this.router.navigate(['clientlogin/' + this.dbName]);
                        }
                      } break;
                  }
                });
              // this.router.navigate([localStorage.getItem('page')]);
            },
              error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                  case 500:
                    break;
                  case 400:
                    if (statuscode === '2085' || statuscode === '2071') {
                      if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                      }
                    }
                    break;
                }
              });
          }
        }
      }
    } else {
      this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 7000 });
    }
  }
  deleteDataWhenPaymentFailed(apptId) {
    this.onlineGiftService.deleteThePaymentFailedRecords(apptId).subscribe(
      data => {
        const dataStatus = data['result'];
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + this.dbName]);
              }
            }
            break;
        }
      }
    );
  }
  savePaymentsData(paymentData) {
    // if (this.cardNumber) {
    //   const cardType = this.commonService.getCardType(this.cardNumber);
    //   // this.cardType  = this.cardTypes.filter(obj=> obj)
    // }
    let approvalCode = '';
    let refCode = '';
    if (paymentData === null) {
      approvalCode = '';
      refCode = '';
    } else {
      // approvalCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      // this.authCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      // refCode = paymentData.PAYMENTRESPONSE.UNIQUEREF[0];
      // this.giftNumber = paymentData.Gift_Number__c;
      approvalCode = paymentData['authCode'];
      // this.authCode = paymentData.PAYMENTRESPONSE.APPROVALCODE[0];
      refCode = paymentData['paymentId'];
      this.giftNumber = paymentData.Gift_Number__c;
    }
    const paymentObj = {
      'apptId': this.apptId,
      'merchantAccnt': this.merchantAccntName,
      'paymentGateWay': 'Clover',
      'firstrName': this.clientFstName,
      'lastName': this.clientLstName,
      'cardHolderName': this.clientFstName + ' ' + this.clientLstName,
      'cardHolderEmail': this.cardHolderEmail,
      'cardNumber': this.cardNumber,
      'zipCode': this.MailingPostalCode,
      'expMonth': this.expMonth,
      'expYear': this.expYear,
      'cvv': this.cvv,
      'amountToPay': this.giftPurchaseObj.Amount,
      'approvalCode': approvalCode,
      'refCode': refCode,
      'isGiftPurchase': true,
      'giftPurchaseObj': this.giftPurchaseObj,
      'paymentType': this.paymentCardId,
      'giftNumber': paymentData.Gift_Number__c,
      'Online__c': 1
    };
    this.onlineGiftService.addToPaymentsTicket(paymentObj, this.withoutLogin)
      .subscribe(data1 => {
        const dataObj = data1['result'];
        this.sendEmailReciept();
        this.toastermessage = this.translateService.get('LOGIN.PAYMENT_SUCCESS');
        this.toastr.success(this.toastermessage.value, null, { timeOut: 3000 });
        this.mainDiv = false;
        const element = document.getElementById('countdownId');
        element.style.display = 'none';
        const highestTimeoutId = setTimeout(';');
        for (let i = 0; i < highestTimeoutId; i++) {
          clearTimeout(i);
        }
        // if (this.withoutLogin === 'any') {
        //   this.router.navigate(['/']).then(() => { this.router.navigate(['/onlinegift/any']); });
        // } else {
        //   this.router.navigate(['/onlinebook']);
        // }
      },
        error => {
        });
  }
  // changeCard(type) {
  //   this.cardType = type;
  // }
  clearErr() {
    this.PckgError = '';
    this.giftErr = '';
    this.error = '';
  }
  purchaseGift() {
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.giftPurchaseObj['Appt_Date_Time__c'] = this.commonService.getDBDatTmStr(new Date());
    this.giftPurchaseObj['dbName'] = this.dbName;
    this.giftPurchaseObj['Amount'] = this.giftAmount;
    // this.giftPurchaseObj['Amount'] = this.giftPurchaseObj.listedAmount === 'other' ? this.giftAmount : this.giftPurchaseObj.listedAmount;
    if (!this.giftPurchaseObj['Amount']) {
      this.giftErr = 'ONLINE_GIFT.AMOUNT_IS_REQUIRED';
      window.scrollTo(0, 0);
    } else if (this.giftPurchaseObj['Amount'] < 1) {
      this.giftErr = 'ONLINE_GIFT.AMOUNT_SHOULD_GREATERTHAN_0';
      window.scrollTo(0, 0);
    } else if (!(/^(?!0)\d{1,9}(\.\d{1,2})?$/g).test(this.giftPurchaseObj['Amount'])) {
      this.giftErr = 'ONLINE_GIFT.ONLY_POSITIVE_WHOLE_NUMBER_ENTERED';
      window.scrollTo(0, 0);
    } else if (!this.giftPurchaseObj['RecipientName']) {
      this.giftErr = 'ONLINE_GIFT.RECIPIENT_NAME_IS_REQUIRED';
      window.scrollTo(0, 0);
    } else if (!this.giftPurchaseObj['RecipientEmail']) {
      this.giftErr = 'ONLINE_GIFT.RECEIPIENT_EMAIL_REQUIRED';
      window.scrollTo(0, 0);
      // } else if (this.giftPurchaseObj['deliveryType'] === this.deliveryTypes[0] ? !this.giftPurchaseObj['RecipientEmail'] : false) {
      //   this.giftErr = 'Recipient Email is required';
      //   window.scrollTo(0, 0);
      // } else if (!this.giftPurchaseObj['firstname']) {
      //   this.giftErr = 'Your First Name is required';
      //   window.scrollTo(0, 0);
      // } else if (!this.giftPurchaseObj['lastname']) {
      //   this.giftErr = 'Your Last Name is required';
      //   window.scrollTo(0, 0);
      // } else if (!this.giftPurchaseObj['email']) {
      //   this.giftErr = 'Your Email is required';
      //   window.scrollTo(0, 0);
    } else if (this.giftPurchaseObj['RecipientEmail'] ? !EMAIL_REGEXP.test(this.giftPurchaseObj['RecipientEmail']) : false) {
      this.giftErr = 'ONLINE_GIFT.INVALID_RECIPIENT_EMAIL_ADDRESS';
      window.scrollTo(0, 0);
      // } else if (!EMAIL_REGEXP.test(this.giftPurchaseObj['email'])) {
      //   this.giftErr = 'Invalid Email Address';
      //   window.scrollTo(0, 0);
    } else {
      this.showPayment = true;
      const element = document.getElementById('countdownId');
      element.style.display = 'block';
      // this.clientName = this.giftPurchaseObj['firstname'] + ' ' + this.giftPurchaseObj['lastname'];
      if (this.showPayment) {
        this.countdown();
      }
      // this.onlineGiftService.onlineGiftPurchase(this.giftPurchaseObj).subscribe((data) => {
      //   this.apptId = data['result']['apptId'];
      //   // this.router.navigate([localStorage.getItem('page')]);
      // },
      //   error => {
      //     const status = JSON.parse(error['status']);
      //     const statuscode = JSON.parse(error['_body']).status;
      //     switch (status) {
      //       case 500:
      //         break;
      //       case 400:
      //         if (statuscode === '2085' || statuscode === '2071') {
      //           if (this.router.url !== '/') {
      //             localStorage.setItem('page', this.router.url);
      //             this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
      //           }
      //         }
      //         break;
      //     }
      //   });
    }

  }
  cancel() {
    if (this.withoutLogin === 'any') {
      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
    } else {
      this.router.navigate(['/onlinebook']);
    }
  }
  isNumber(event: any) {
    const pattern = /[0-9.]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  amountChange() {
    this.giftAmount = 0;
    this.clearErr();
  }
  deleteAppt() {
    if (this.apptId) {
      this.deleteDataWhenPaymentFailed(this.apptId);
    }
  }
  countdown() {
    let element, endTime, hours, mins, msLeft, time;
    function twoDigits(n) {
      return (n <= 9 ? '0' + n : n);
    }
    function updateTimer() {
      msLeft = endTime - (+new Date);
      if (msLeft < 1000) {
        time = new Date(msLeft);
        document.getElementById('countdownId').style.color = 'red';
        const deleteRcrd = <HTMLInputElement>document.getElementById('deleteApptId');
        const deleteErroRcrd = <HTMLInputElement>document.getElementById('deleteErrorApptId');
        deleteRcrd.click();
        deleteErroRcrd.style.display = 'flex';
        deleteErroRcrd.innerHTML = '<span>Your payment session has timed out, and your request has been removed!</span>';
        setTimeout(reload, time.getUTCMilliseconds() + 8000);
      } else {
        time = new Date(msLeft);
        hours = time.getUTCHours();
        mins = time.getUTCMinutes();
        element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : 'Time Remaining: 0' + mins) + ':' + twoDigits(time.getUTCSeconds() + '');
        setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
      }
    }
    function reload() {
      window.location.reload();
    }
    element = document.getElementById('countdownId');
    endTime = (+new Date) + 1000 * (60 * 5 + 0) + 500;
    updateTimer();
  }
  /** in getpaymentTypesdata global method we are getting global company info and ticket reciept memo */
  getPaymentTypesDataGlobal() {
    this.onlineGiftService.getPaymentTypesDataGlobal().subscribe(data => {
      this.posLists = data.result['recieptMemo'][0]['JSON__c'];
      this.posDataObjList = JSON.parse(data.result['onlinePos'][0]['JSON__c']);
      this.merchantId = this.posDataObjList.merchantId;
      this.accessToken = this.posDataObjList.accessToken;
      this.addTest = this.posDataObjList.test;
      this.companyData = data.result['cmpInfo'][0];
      this.cardTypes = data.result.paymentResult.filter(filterList => filterList.Process_Electronically_Online__c === 1
        && filterList.Active__c === 1 && filterList.Id !== 'a0M1H00000UeiV6UAJ');
      this.orderId = data.result.Id;
      this.cardType = this.cardTypes[0]['Id'];
      this.cardName = this.cardTypes[0]['Abbreviation__c'];
      this.electronicPaymentData = data.result.paymentResult.filter(filterList => filterList.Name === 'Electronic Payment');
      if (this.electronicPaymentData.length > 0) {
        this.paymentCardId = this.electronicPaymentData[0]['Id'];
      } else {
        this.paymentCardId = this.cardTypes[0]['Id'];
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + this.dbName]);
          }
        }
      });
  }
  // below methods used for card holder email reciept//
  posDeviceList() {
    this.onlineGiftService.getPosdevices().subscribe(data => {
      this.posLists = data['result'][1].JSON__c;
    });
  }
  sendEmailReciept() {
    const HtmlData = document.getElementById('inner_cont').innerHTML;
    const style = '@import url(https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i);'
      + 'body{font-size:8pt; line-height:1;}*,h4{margin:0;line-height:1.1}.total,h4{text-align:center}*{font-family:Raleway,sans-serif;padding:0;font-size:14px}'
      + '.profile-list-view-txt{padding:20px 0;font-size:18px}h4{display:block;font-size:18px;font-weight:400;padding:0 0 10px}.check-profile-list-img,'
      + '.check-profile-list-name{font-family:Raleway,sans-serif!important;font-weight:400;text-align:center;float:none;width:100%;margin:0;padding:0}'
      + '.check-profile-list-name,.price-total,.sub-item-service{font-size:18px;font-weight:600}.checkoutlist,.drawer-button,.merchantDropDown{padding:0 15px}'
      + '.check-profile-list{display:flex;align-items:center;max-width:400px;width:100%;margin:0 auto 7px;cursor:pointer}.check-profile-list-img{width:61px;height:61px;'
      + 'float:left;margin-left:-70px}.check-profile-list-img img{width:61px;height:61px;border-radius:100%;object-fit:cover;border:1px solid #fff}'
      + '.check-profile-list-name{width:100%;line-height:1.1;text-transform:uppercase;color:#16337d;padding:20px 0}.check-profile-list-name.text-uppercase{padding-top:0}'
      + '.check-profile-list-name h4{font-size:18px;line-height:1;margin:0;padding:0}.check-profile-details-in{max-width:400px;width:100%;margin:0 auto}'
      + '.sub-item-service{display:table;line-height:20px;width:100%;margin-bottom:15px;color:#062675}.sub-item-service .sub-item1,.sub-item2,'
      + '.sub-item3{display:table-cell;width:auto}.listcharge,.total{display:inline-block;vertical-align:top;margin-top:15px}'
      + '.price-total{line-height:1;text-transform:uppercase;color:#16337d;width:126px}.total{width:136px;font-size:20px;color:#16337d}'
      + '.sub-item-service .sub-item3,.sub-item3 h4{text-align:right}.total-charge{margin-top:0;padding-bottom:20px}.sub-item-service '
      + '.ng-star-inserted{display:table;width:100%}.merchantDropDown{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1}.ml-50{margin-left:-30px}'
      + '.merchantDropDown select{padding:0 15px;height:27px;font-size:12px;line-height:12px;width:52%!important}.checkoutlist a:hover{text-decoration:underline!important}'
      + '.charge-btn{padding-top:3px;padding-bottom:4px;margin-top:12px}.sub-item-service.desc{margin-bottom:5px}.sub-item-service '
      + '.sub-item1 span.disc{margin-right:10px;text-transform:uppercase;padding:2px 5px 2px 10px}.sub-item3 span{text-decoration:line-through;padding-right:10px}'
      + '.sub-item-service .sub-item1,.sub-item3{white-space:nowrap;width:25%}.sub-item3 h4{padding:0}.sub-item-service .sub-item2{border-bottom:1px solid rgba(231,234,242,1)}'
      + '.sub-item-sub{padding:0 10%}.sub-item-sub .sub-item-service{font-weight:400}';
    const finalhtml = HtmlData + '<style>' + style + '</style>';
    const dataObj = {
      'apptName': this.apptName,
      'apptId': this.apptId,
      'clientName': this.clientFstName ? this.clientFstName + ' ' + this.clientLstName : 'NO CLIENT',
      'clientId': this.clientId,
      'clientEmail': this.cardHolderEmail,
      'htmlFile': finalhtml
    };
    const currentDate = new Date();
    const dtStr = currentDate.getFullYear()
      + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2)
      + '-' + ('0' + currentDate.getDate()).slice(-2)
      + ' ' + ('0' + currentDate.getHours()).slice(-2)
      + ':' + ('0' + currentDate.getMinutes()).slice(-2)
      + ':' + ('0' + currentDate.getSeconds()).slice(-2);
    dataObj['db'] = localStorage.getItem('param');
    dataObj['cname'] = localStorage.getItem('compname');
    dataObj['dt'] = dtStr;
    if (this.withoutLogin === 'any') {
      this.onlineGiftService.sendReciept1(dataObj).subscribe(data => {
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
        });
    } else {
      this.onlineGiftService.sendReciept(dataObj).subscribe(data => {
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
        });
    }
  }
  companyInfo() {
    this.onlineGiftService.getCompanyInfo().subscribe(data => {
      this.companyData = data['result'][0];
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/clientlogin/' + this.dbName]).then(() => { });
          }
        }
      });
  }

}
