import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelAppointmentReportComponent } from './cancelappointmentreport.component';
import { CancelAppointmentReportRoutingModule } from './cancelappointmentreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        CancelAppointmentReportRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        CancelAppointmentReportComponent
    ]
})
export class CancelAppointmentReportModule {
    
}
