import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RewardPointsService } from './rewardpointsreport.service';
import { ToastrService } from 'ngx-toastr';
import { JwtHelper } from 'angular2-jwt';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../../../common/common.service';
import { a, b } from '@angular/core/src/render3';
@Component({
  selector: 'app-reports-app',
  templateUrl: './rewardpointsreport.html',
  styleUrls: ['./rewardpointsreport.css'],
  providers: [RewardPointsService, CommonService],
})
export class RewardPointsComponent implements OnInit {
  datePickerConfig: any;
  startDate: any = new Date();
  endDate: any = new Date();
  minDate: any = new Date();
  staticData = [];
  selectType = '';
  SdateEdateError = '';
  itemsDisplay = false;
  toastermessage: any;
  rewardPointsData = [];
  decodeUserToken: any;
  decodedToken: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private rewardpointsservice: RewardPointsService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getStaticRewardValue();
  }

  getStaticRewardValue() {
    this.rewardpointsservice.getStaticRewardValue().subscribe(
      data => {
        this.staticData = data['rewards'];
        this.selectType = this.staticData[0].value;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  selectTypeOnChange(value) {
    this.selectType = value;
    // this.generateRewardPointsReport();
    if (this.rewardPointsData.length > 0) {
      if (this.selectType === 'Current Rewards Value') {
        this.rewardPointsData.sort((p, s) => s.Points_Balance__c - p.Points_Balance__c);
      } else if (this.selectType === 'Rewards Earned') {
        this.rewardPointsData.sort((p, s) => s.earnedPoints - p.earnedPoints);
      } else if (this.selectType === 'Rewards Used') {
        this.rewardPointsData.sort((p, s) => s.usedPoints - p.usedPoints);
      } else if (this.selectType === 'Client First Name') {
        this.rewardPointsData.sort((p, s) => p.FirstName.localeCompare(s.FirstName));
      } else if (this.selectType === 'Client Last Name') {
        this.rewardPointsData.sort((p, s) => p.LastName.localeCompare(s.LastName));
      }
    } else {
      this.rewardPointsData = [];
    }
  }

  generateRewardPointsReport() {
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startTime = (this.startDate.getFullYear() + '') + '-' + ('00' + (this.startDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.startDate.getDate()).slice(-2);
      const endTime = (this.endDate.getFullYear() + '') + '-' + ('00' + (this.endDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.endDate.getDate()).slice(-2);
      const stDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      const edDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      if (startTime > endTime) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else if ((startTime > stDate1) || (endTime > edDate1)) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.FUTURE_DATES_NOT_ALLOWED');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else {
        this.rewardPointsData = [];
        this.rewardpointsservice.getRewardPoints(startTime, endTime, this.selectType).subscribe(
          data => {
            this.rewardPointsData = data['result'];
            data['result'] = data['result'].filter(obj => obj.clientName !== null && obj.clientName !== '');
            if (data['result'].length === 0) {
              this.itemsDisplay = false;
              this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
              this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            } else {
              this.itemsDisplay = true;
              data['result'] = data['result'].filter(ele => ele.clientName !== null);
              data['result'].forEach(element => {
                if (element['lastVisit']) {
                  element['lastVisit'] = this.commonService.getUsrDtStrFrmDBStr(element['lastVisit']);
                }
              });
              this.rewardPointsData = data['result'];
              this.itemsDisplay = true;
            }
          },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
      }
    }
  }

  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.selectType + '"', 'selected value="' + this.selectType + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Reward Points Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .table>tbody>tr>td>a {
              color:black !important;
              cursor:none!important;
              text-decoration: None!important;;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
