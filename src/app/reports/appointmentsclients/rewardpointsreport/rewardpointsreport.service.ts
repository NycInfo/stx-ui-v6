import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClient } from '../../../common/http-client';


@Injectable()
export class RewardPointsService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getStaticRewardValue() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .map(this.extractData);
  }
  getRewardPoints(startTime, endTime, selectType) {
    const date = new Date();
    const date1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
    const time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    const currentDateTime = date1 + ' ' + time;
    const headers = new Headers();
    headers.append('currentDate', currentDateTime);
    return this.http.getHeader(this.apiEndPoint + '/api/reports/rewards/' + startTime + '/' + endTime + '/' + selectType, headers)
      .map(this.extractData);
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }

}
