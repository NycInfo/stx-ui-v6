import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientRetentionService } from './clientretention.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { DecimalPipe, DatePipe } from '@angular/common';
import { CommonService } from '../../../common/common.service';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-reports-app',
  templateUrl: './clientretention.html',
  styleUrls: ['./clientretention.css'],
  providers: [ClientRetentionService, CommonService],
})
export class ClientRetentionComponent implements OnInit {
  maxDate = new Date();
  public reportAnalysis = {
    'retentionType': 'New',
    'startDate': undefined,
    'endDate': this.maxDate,
    'noOfVisits': undefined,
    'worker': '',
    'type': 'Company',
  };
  cliType = '';
  itemsDisplay = false;
  workerTipsData: any;
  datePickerConfig: any;
  workerList: any = [];
  workerName: any;
  error: any;
  decodedToken: any;
  decodeUserToken: any;
  companyName: any;
  numberOfVisit: any;
  toastermessage: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private clientRetentionService: ClientRetentionService,
    private commonService: CommonService,
    private datePipe: DatePipe) {

    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  clientRetentionData = {};
  clientsList: any = [];
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getWorkerList();
  }
  loadCompanyInfo(data) {
    this.companyName = data.Name;
  }

  clear() {
    this.error = '';
    // this.reportDetails = {};
    this.itemsDisplay = false;
  }
  selectReportsType(value) {
    if (value === 'Company') {
      this.reportAnalysis.worker = '';
      this.workerName = '';
    }
  }

  selectWorker() {
    const selectedWorker = this.workerList.filter((worker) => worker.Id === this.reportAnalysis.worker);
    if (selectedWorker.length > 0) {
      this.workerName = selectedWorker[0].FirstName + ' ' + selectedWorker[0].LastName;
    }
  }
  getWorkerList() {
    this.clientRetentionService.getUserList().subscribe(data => {
      this.workerList = data['result'].filter((worker) => worker.IsActive === 1);
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  generateReport() {
    if (!this.reportAnalysis.startDate) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (!this.reportAnalysis.endDate) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (isNaN(this.reportAnalysis.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (isNaN(this.reportAnalysis.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (this.reportAnalysis.endDate.getTime() < this.reportAnalysis.startDate.getTime()) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (!this.reportAnalysis.noOfVisits) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NUMBER_OF_VISITS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else if (this.reportAnalysis.type !== 'Company' && this.reportAnalysis.worker === '') {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      window.scrollTo(0, 0);
    } else {
      this.itemsDisplay = true;
      const reportObj = {
        startDate: this.commonService.getDBDatStr(this.reportAnalysis.startDate),
        endDate: this.commonService.getDBDatStr(this.reportAnalysis.endDate),
        worker: this.reportAnalysis.worker,
        retentionType: this.reportAnalysis.retentionType
      };
      if (this.reportAnalysis.retentionType === 'Recurring') {
        this.cliType = 'Recurring Client Retention -';
      } else {
        this.cliType = 'New Client Retention -';
      }
      this.numberOfVisit = this.reportAnalysis.noOfVisits;
      this.clientRetentionService.getClientRetentionReport(reportObj).subscribe(data => {
        //  console.log(data);
        const result = data['result']['done'];
        const workerIdsSetPerClient = data['result']['clientWithWorkerId'];
        const clientVisit = new Map();
        const clientApptDates: any = new Map();
        const clients = new Map();
        this.clientsList = [];
        result.forEach((clientInfo) => {
          const apptDate = this.commonService.getDateFrmDBDateStr(clientInfo.Appt_Date_Time__c);
          if (clientVisit.has(clientInfo.Id)) {
            const noOfVisits = clientVisit.get(clientInfo.Id);
            const appts: Array<any> = clientApptDates.get(clientInfo.Id);
            appts.push(apptDate);
            clientApptDates.set(clientInfo.Id, appts);
            clientVisit.set(clientInfo.Id, noOfVisits + 1);
          } else {
            clientVisit.set(clientInfo.Id, 1);
            clientApptDates.set(clientInfo.Id, [apptDate]);
            clients.set(clientInfo.Id, { id: clientInfo.Id, clientName: clientInfo.clientName });
          }
        });
        let retainedCount = 0;
        clients.forEach((value, key) => {
          const clientObj: any = value;
          const currentDate = new Date();
          currentDate.setHours(0, 0, 0, 0);
          clientObj['firstVisitDate'] = clientApptDates.get(key)[0];
          const noOfAppointments = clientApptDates.get(key).length;
          clientObj['lastVisitDate'] = clientApptDates.get(key)[noOfAppointments - 1];
          clientObj['numVisitsAfterStartDate'] = clientVisit.get(key);
          const days = this.daysBetweenDates(currentDate, clientObj['lastVisitDate']);
          clientObj['numDaysSinceLastVisit'] = days >= 0 ? Number(days) : -days;
          clientObj['numDaysSinceLastVisit'] = parseInt(clientObj['numDaysSinceLastVisit'], 10);
          // the above line is used for converting whole numbers , issue num : 2185
          const numDaysFirstVisitToNow = this.daysBetweenDates(clientObj['firstVisitDate'], currentDate);
          clientObj['averageDaysBetweenVisits'] = Math.floor(this.calculateAverage(numDaysFirstVisitToNow, clientObj['numVisitsAfterStartDate']));
          if (clientObj['numVisitsAfterStartDate'] >= this.reportAnalysis.noOfVisits) {
            clientObj['isRetained'] = true;
            retainedCount++;
          } else {
            clientObj['isRetained'] = false;
          }
          const workers = workerIdsSetPerClient[key];
          clientObj['numWorkersSeen'] = workers.length;
          this.clientsList.push(clientObj);
        });

        this.clientRetentionData['Total_Recurring_Visits'] = this.clientsList.length;
        this.clientRetentionData['Total_Retained'] = retainedCount;
        this.clientRetentionData['Percent_Retained'] = this.calculatePercentage(retainedCount, this.clientsList.length);
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }
  }

  daysBetweenDates(startDate: Date, endDate: Date): number {
    const milliseconds = endDate.getTime() - startDate.getTime();
    const days = milliseconds / (24 * 60 * 60 * 1000);
    return days;
  }

  calculatePercentage(firstValue: number, secondValue: number): number {
    if (secondValue === 0) {
      return 0.00;
    } else {
      let percentage = (firstValue / secondValue) * 100;
      percentage = +percentage.toFixed(2);
      return percentage;
    }
  }
  calculateAverage(firstValue: number, secondValue: number): number {
    if (secondValue === 0) {
      return 0.00;
    } else {
      let percentage = (firstValue / secondValue);
      percentage = +percentage.toFixed(2);
      return percentage;
    }
  }

  isNumeric(event) {
    return this.commonService.IsNumeric(event);
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    // --- Retention Type ---//
    printContents = printContents.replace('value="' + this.reportAnalysis.retentionType + '"', 'selected value="' + this.reportAnalysis.retentionType + '"');
    // --- Number of Visits Required ---//
    printContents = printContents.replace('type="number"', 'type="number" value="' + this.reportAnalysis.noOfVisits + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    let popupContent = `
    <html>
      <head>
        <title>Client Retention</title>
        <style>
          .reportTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          .reportTable td,
          .reportTable th {
            border: 1px solid rgb(141, 141, 141);
            padding: 8px;
          }
          .reportTable tr:nth-child(even) {
            background-color: #f2f2f2;
          }
          .reportTable th {
            padding-top: 6px;
            padding-bottom: 6px;
            text-align: left;
            background-color: #204d74;
            color: white;
          }
        </style>
      </head>
      <body onload="window.print();window.close()">
        <div>
            New Client Retention: If a client's first visit is within the below date range, they
            will be included in the report.<br>
            Recurring Client Retention: If a client has had a recurring visit within the below date range, they
            will be included in the report.<br>
            Retention is calculated based on total visits starting with the selected Begin Date up to the date
            the report is generated.
            <table>
              <tr>
                <td><b>Retention Type: </b></td>
                <td>{{retenType}}</td>
              </tr>
              <tr>
                <td><b>Begin Date: </b></td>
                <td>${this.datePipe.transform(this.reportAnalysis.startDate, 'MM/dd/yyyy')}</td>
              </tr>
              <tr>
                <td><b>End Date: </b></td>
                <td>${this.datePipe.transform(this.reportAnalysis.endDate, 'MM/dd/yyyy')}</td>
              </tr>
              <tr>
                <td><b>Number of Visits Required: </b></td>
                <td>${this.reportAnalysis.noOfVisits}</td>
              </tr>
              <tr>
                <td><b>Report Type: </b></td>
                <td>${this.reportAnalysis.type}</td>
              </tr>
              {{selWorker}}
            </table>
            <br>
            {{mainCnt}}
        </div>
      </body>
    </html>`;
    if (this.reportAnalysis.retentionType === 'New') {
      popupContent = popupContent.replace('{{retenType}}', 'New Clients');
    } else {
      popupContent = popupContent.replace('{{retenType}}', 'Recurring Clients');
    }
    if (this.reportAnalysis.type === 'Company') {
      popupContent = popupContent.replace('{{selWorker}}', '');
    } else {
      popupContent = popupContent.replace('{{selWorker}}', `<tr><td><b>Worker: </b></td><td>${this.workerName}</td></tr>`);
    }
    if (this.clientsList.length === 0) {
      popupContent = popupContent.replace('{{mainCnt}}', '<div style="text-align: center">**** No Records Found ****</div>');
    } else {
      let dynCnt = `
      <table class="reportTable">
        <thead>
          <tr>
            <th>${this.companyName}</th>
            <th>New Client Retention - ${this.reportAnalysis.type} <br>
            Number Of Visits Required - ${this.reportAnalysis.noOfVisits}</th>
            <th> ${this.datePipe.transform(this.reportAnalysis.startDate, 'MM/dd/yyyy')} -
              ${this.datePipe.transform(this.reportAnalysis.endDate, 'MM/dd/yyyy')}</th>
          </tr>
        </thead>
      </table>
      <br>
      <table class="reportTable">
        <thead>
          <tr>
            <th>Retention Rate</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Total New Visits</td>
            <td>${this.clientRetentionData['Total_Recurring_Visits']}</td>
          </tr>
          <tr>
            <td>Total Retained</td>
            <td>${this.clientRetentionData['Total_Retained']}</td>
          </tr>
          <tr>
            <td>Percentage Retained</td>
            <td>${this.clientRetentionData['Percent_Retained']}</td>
          </tr>
        </tbody>
      </table>
      <br>
      <table class="reportTable">
        <tr>
          <th>Client Name</th>
          <th>First Visit</th>
          <th>Last Visit</th>
          <th># of Visits after Begin Date</th>
          <th># of Workers Seen</th>
          <th>Avg # Days between Visits</th>
          <th># Days Since Last Visit</th>
          <th>Retained</th>
        </tr>`;
      this.clientsList.forEach(element => {
        let chked = '';
        if (element.isRetained) {
          chked = 'checked';
        }
        dynCnt += `<tr *ngFor="let client of clientsList">
          <td>${element.clientName}</td>
          <td>${this.datePipe.transform(element.firstVisitDate, 'MM/dd/yyyy')}</td>
          <td>${this.datePipe.transform(element.lastVisitDate, 'MM/dd/yyyy')}</td>
          <td>${element.numVisitsAfterStartDate}</td>
          <td>${element.numWorkersSeen} </td>
          <td>${element.averageDaysBetweenVisits}</td>
          <td>${element.numDaysSinceLastVisit}</td>
          <td><input type="checkbox" ${chked}/></td>
        </tr>`;
      });
      dynCnt += `</table>`;
      popupContent = popupContent.replace('{{mainCnt}}', dynCnt);
    }
    popupWin.document.write(popupContent);
    popupWin.document.close();
  }
}
