import { Component, OnInit } from '@angular/core';
import { ForecastingReportService } from './forecastingreport.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
@Component({
  selector: 'app-reports-app',
  templateUrl: './forecastingreport.html',
  styleUrls: ['./forecastingreport.css'],
  providers: [ForecastingReportService],
})
export class ForecastingReportComponent implements OnInit {
  months: any;
  startmonth: any;
  years: any = [];
  startyears: any;
  monthvalue: any;
  currentDate = new Date();
  workersData: any = [];
  workersList: any = [];
  estimatedmemberval: any;
  estimatedmembershipvalue: any;
  estimatedproductvalue: any;
  estimatedprodval: any;
  estimatedpackagevalue: any;
  estimatedpackagval: any;
  estimatedgiftvalue: any;
  estimategiftval: any;
  totalforecast: any;
  currentMonth: any;
  currentYear: any;
  value: any;
  mvalue: any;
  showData = true;
  totalvalue: any;
  toastermessage: any;
  constructor(private router: Router,
    private forecastingservice: ForecastingReportService,
    private toastr: ToastrService,
    private translateService: TranslateService) {
  }
  ngOnInit() {
    this.getMonths();
    this.getyears();
    this.currentMonth = ('00' + (this.currentDate.getMonth() + 1)).slice(-2)
    this.currentYear = new Date().getFullYear();
    this.startmonth = ('00' + (this.currentDate.getMonth() + 1)).slice(-2);
    this.generateReport(this.startmonth, this.startyears);
  }
  getyears() {
    const year = new Date().getFullYear();
    for (let i = 0; i < 6; i++) {
      this.years.push(year + i);
      this.startyears = this.years[0];
    }
  }
  getMonths() {
    this.forecastingservice.getMonthsList().subscribe(
      data => {
        this.months = data['workerGoalMonths'];
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  month(monthval) {
    this.mvalue = monthval;
    this.getErrorMsg();
  }
  year(yearval) {
    this.value = yearval;
    this.getErrorMsg();
  }
  getErrorMsg() {
    this.showData = false;
    if (this.mvalue < this.currentMonth && (this.currentYear === (+this.startyears))) {
      this.toastermessage = this.translateService.get('Select future Month');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if (this.value < this.currentYear) {
      this.toastermessage = this.translateService.get('Select future year');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else {
      this.generateReport(this.startmonth, this.startyears);
      this.showData = true;
    }
  }
  generateReport(startmonth, startyears) {
    this.forecastingservice.sendMonthYear(this.startmonth, this.startyears).subscribe(
      data => {
        this.workersList = data['result']['workerResult'];
        this.estimatedmembershipvalue = data['result']['mebershipResult'];
        this.estimatedproductvalue = data['result']['productResult'];
        this.estimatedpackagevalue = data['result']['packageResult'];
        this.estimatedgiftvalue = data['result']['giftResult'];
        this.estimatedmemberval = this.estimatedmembershipvalue.mebershipPrice ? this.estimatedmembershipvalue.mebershipPrice / 3 : 0;
        this.estimatedprodval = this.estimatedproductvalue.productPrice ? this.estimatedproductvalue.productPrice / 3 : 0;
        this.estimatedpackagval = this.estimatedpackagevalue.packagePrice ? this.estimatedpackagevalue.packagePrice / 3 : 0;
        this.estimategiftval = this.estimatedgiftvalue.giftPrice ? this.estimatedgiftvalue.giftPrice / 3 : 0;
        this.totalvalue = 0;
        for (let i = 0; i < this.workersList.length; i++) {
          this.totalvalue += this.workersList[i].workerPrice;
        }
        this.totalforecast = this.estimatedmemberval + this.estimatedprodval + this.estimatedpackagval + this.estimategiftval + this.totalvalue;

      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.startmonth + '"', 'selected value="' + this.startmonth + '"');
    printContents = printContents.replace('value="' + this.startyears + '"', 'selected value="' + this.startyears + '"');

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>Forecasting report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}
