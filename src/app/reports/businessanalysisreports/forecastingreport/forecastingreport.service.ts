import { Injectable, Inject } from '@angular/core';
import { Response, Http } from '@angular/http';
import { HttpClient } from '../../../common/http-client';

@Injectable()
export class ForecastingReportService {

  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) {
  }
  getMonthsList() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .map(this.extractData);
  }
  sendMonthYear(monvalue, yearval) {
    return this.http.get(this.apiEndPoint + '/api/reports/forecasting/' + monvalue + '/' + yearval)
      .map(this.extractData);
  }
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
