import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReferralReportService } from './referralreport.service';
import { ToastrService } from 'ngx-toastr';
import { JwtHelper } from 'angular2-jwt';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './referralreport.html',
  styleUrls: ['./referralreport.css'],
  providers: [ReferralReportService, CommonService],
})
export class ReferralReportComponent implements OnInit {
  datePickerConfig: any;
  startDate: any = new Date();
  endDate: any = new Date();
  minDate: any = new Date();
  decodeUserToken: any;
  decodedToken: any;
  staticData = [];
  refferalData = [];
  selectType = '';
  SdateEdateError = '';
  itemsDisplay = false;
  numOfRef = '';
  totalReferrals = 0;
  toastermessage: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private referralReportService: ReferralReportService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getStaticRewardValue();
  }
  isNumeric(event) {
    return this.commonService.IsNumeric(event);
  }

  getStaticRewardValue() {
    this.referralReportService.getStaticRewardValue().subscribe(
      data => {
        this.staticData = data['referralTypes'];
        this.selectType = this.staticData[0].value;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  selectTypeOnChange(value) {
    this.selectType = value;
    // this.generateReferralReport();
    if (this.refferalData.length > 0) {
      if (this.selectType === 'Client Last Name') {
        this.refferalData.sort((a, b) => a.LastName.localeCompare(b.LastName));
      } else if (this.selectType === 'Client First Name') {
        this.refferalData.sort((a, b) => a.FirstName.localeCompare(b.FirstName));
      } else if (this.selectType === '# of Referrals') {
        this.refferalData.sort((a, b) => b.ReferCount - a.ReferCount);
      }
    } else {
      this.refferalData = [];
    }
  }
  generateReferralReport() {
    this.refferalData = [];
    this.totalReferrals = 0;
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (!this.numOfRef) {
      this.toastermessage = this.translateService.get('REFERRAL_REPORT.REFERRALS_SHOULD_BE_GREATERTHAN_ZERO');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startTime = (this.startDate.getFullYear() + '') + '-' + ('00' + (this.startDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.startDate.getDate()).slice(-2);
      const endTime = (this.endDate.getFullYear() + '') + '-' + ('00' + (this.endDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.endDate.getDate()).slice(-2);
      const stDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      const edDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      if (startTime > endTime) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else if ((startTime > stDate1) || (endTime > edDate1)) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.FUTURE_DATES_NOT_ALLOWED');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else {
        const obj = {
          startDate: this.commonService.getDBDatStr(this.startDate).split(' ')[0],
          endDate: this.commonService.getDBDatStr(this.endDate).split(' ')[0],
          numOfRef: this.numOfRef,
          selectType: this.selectType
        };
        this.referralReportService.getReferralReport(obj).subscribe(
          data => {
            if (data['result'].length > 0) {
              data['result'].forEach(element => {
                this.totalReferrals += element['ReferCount'];
                element['displayDates'] = element['Date'].split(',');
                element['displayClients'] = element['Referred'].split(',');
                for (let i = 0; i < element['displayDates'].length; i++) {
                  element['displayDates'][i] = this.commonService.getUsrDtStrFrmDBStr(element['displayDates'][i])[0];
                }
              });
              this.refferalData = data['result'];
              this.itemsDisplay = true;
            } else {
              this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
              this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
              this.refferalData = [];
              this.itemsDisplay = false;
            }
          },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      }
    }
  }



  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.selectType + '"', 'selected value="' + this.selectType + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Referral Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .table>tbody>tr>td>a {
              color:black !important;
              cursor:none!important;
              text-decoration: None!important;;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
