import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { TbpReportReportService } from './tbpreport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
@Component({
  selector: 'app-reports-app',
  templateUrl: './tbpreport.html',
  styleUrls: ['./tbpreport.css'],
  providers: [TbpReportReportService],
})
export class TbpReportReportComponent implements OnInit {
  bsValue = new Date();
  bsValue1 = new Date();
  maxDate = new Date();
  itemsDisplay = false;
  tbpList: any;
  CompanyName = '';
  datePickerConfig: any;
  toastermessage: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private tbpReportReportService: TbpReportReportService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
  }
  loadCompanyInfo(data) {
    this.CompanyName = data.Name;
  }
  generateReport() {
    const date = new Date();
    if (this.bsValue === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.bsValue.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.bsValue1 === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.bsValue1.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startTime = (this.bsValue.getFullYear() + '') + '-' + ('00' + (this.bsValue.getMonth() + 1)).slice(-2) + '-' + ('00' + this.bsValue.getDate()).slice(-2);
      const endTime = (this.bsValue1.getFullYear() + '') + '-' + ('00' + (this.bsValue1.getMonth() + 1)).slice(-2) + '-' + ('00' + this.bsValue1.getDate()).slice(-2);
      const stDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      const edDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      if (startTime > endTime) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else if ((startTime > stDate1) || (endTime > edDate1)) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.FUTURE_DATES_NOT_ALLOWED');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else {
        this.tbpReportReportService.getTbpReport(startTime, endTime).subscribe(
          data => {
            this.tbpList = data['result'][0];
            if (this.tbpList.length === 0) {
              this.itemsDisplay = false;
              this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.REPORT_NOT_FOUND');
              this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            } else {
              this.itemsDisplay = true;
            }
          },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      }
    }
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue.getMonth() + 1) + '/'
    + this.bsValue.getDate() + '/' + this.bsValue.getFullYear() + '"');
  printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue1.getMonth() + 1) + '/'
    + this.bsValue1.getDate() + '/' + this.bsValue1.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>TBP Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
}
}
