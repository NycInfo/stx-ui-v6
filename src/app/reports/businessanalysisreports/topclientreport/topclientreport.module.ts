import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopClientComponent } from './topclientreport.component';
import { TopClientRoutingModule } from './topclientreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        TopClientRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        TopClientComponent
    ]
})
export class TopClientModule {
}
