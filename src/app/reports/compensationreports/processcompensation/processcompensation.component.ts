import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ProcessCompensationService } from './processcompensation.service';
import { SetupCompMethodService } from '../../../setup/setupworkers/setupcompensationmethod/setupcompmethod.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../../../common/common.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { JwtHelper } from 'angular2-jwt';
@Component({
    selector: 'app-reports-app',
    templateUrl: './processcompensation.html',
    styleUrls: ['./processcompensation.css'],
    providers: [ProcessCompensationService, CommonService, SetupCompMethodService],
})
export class ProcessCompensationComponent implements OnInit {
    serviceFeeData = [];
    startDate: any;
    endDate: any;
    PrintSdate: any;
    PrintEdate: any;
    minDate = new Date();
    datePickerConfig: any;
    error: any;
    decodedToken: any;
    decodeUserToken: any;
    processCompObj = [];
    totalCount = 0;
    topBarPrintButt = false;
    arcButt = false;
    resetButt = false;
    archiveRecored = [];
    fullview = false;
    datePicDis = false;
    fullviewSetup = false;
    viewList: any;
    viewResdata = [];
    finAry = [];
    generateData = [];
    generateListRecored = [];
    generatRebookedServices = [];
    generateTicketCounts = [];
    generatProductSalesByProductLine = [];
    totalGrossServiceAmo = 0;
    totalGrossProductAmo = 0;
    totalNumberOfProducts = 0;
    totalNumberOfServices = 0;
    scalesData = [];
    generateSteps = [];
    generatServiceSalesByServiceGroup = [];
    generatproductSalesByInventoryGroup = [];
    generateFeeAmount = [];
    genOperandvalue: any = 0;
    srvcData = [];
    prctData = [];
    calScaleStep = 0;
    toastermessage: any;
    multlpleclick = true;
    @ViewChild('listModal') public listModal: ModalDirective;
    constructor(private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private translateService: TranslateService, private commonservice: CommonService,
        private setupCompMethodService: SetupCompMethodService,
        private processCompensationService: ProcessCompensationService) {
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
    }
    ngOnInit() {
        // ---Start of code for Permissions Implementation--- //
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedToken = {};
            this.decodeUserToken = {};
        }
        if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
        } else {
            this.decodedToken = {};
        }
        // ---End of code for Permissions Implementation--- //
        this.getscales();
    }

    selectAll() {
        this.processCompObj = this.processCompObj.map(function (item, index) {
            item['include'] = 1;
            return item;
        });
    }

    unSelectAll() {
        this.processCompObj = this.processCompObj.map(function (item, index) {
            item['include'] = 0;
            return item;
        });
    }

    generateReport() {
        this.resetButt = false;
        this.fullview = false;
        if (!this.startDate) {
            this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            this.processCompObj = [];
        } else if (!this.endDate) {
            this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            this.processCompObj = [];
        } else if (this.startDate > this.endDate) {
            this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            this.processCompObj = [];
        } else {
            // this two date used for print screen
            this.PrintSdate = (this.startDate.getMonth() + 1) + '/' + this.startDate.getDate() + '/' + this.startDate.getFullYear();
            this.PrintEdate = (this.endDate.getMonth() + 1) + '/' + this.endDate.getDate() + '/' + this.endDate.getFullYear();
            this.processCompensationService.workerCompensationList(this.startDate, this.endDate).subscribe(
                data => {
                    this.processCompObj = data['result'][0];
                    this.processCompObj.forEach(element => {
                        if (element.deduction === 0 || element.deduction) {
                            element.deduction = (element.deduction).toFixed(2);
                        }
                        if (element.extraPay === 0 || element.extraPay) {
                            element.extraPay = (element.extraPay).toFixed(2);
                        }
                    });
                    this.totalCount = data['result'][1];
                    if (this.processCompObj.length === 0) {
                        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_RESULTS_FOUND');
                        this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
                    }
                    const notEmpty = this.processCompObj.filter(function (obj) {
                        return obj.id !== '';
                    });
                    if (notEmpty.length > 0) {
                        this.resetButt = true;
                        this.fullview = true;
                        this.archiveRecored = [];
                        for (let i = 0; i < notEmpty.length; i++) {
                            if (notEmpty[i].id !== '') {
                                this.archiveRecored.push({
                                    'Id': notEmpty[i].id,
                                    'workerId': notEmpty[i].workerId
                                });
                            }
                        }
                    }
                },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                }
            );
        }
    }

    printDiv(value) {
        let printContents, popupWin;
        printContents = document.getElementById(value).innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Process Compensation</title>
          <style>
          //........Customized style.......
          table {
            border-collapse: collapse;
           }
          table, th, td {
              border: 0.5px solid black!important;
          }
          .pri td {
            padding:6px;
          }
          .arc {
            float:left;
            margin:12px;
          }
          .arc button {
            margin-top:10px;
          }
          .modal-body .btn {
            float:left;
            margin:12px;
          }
          .close {
              display:none!important;
          }
          .total {
            margin:6px;
          }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

    archiveButt() {
        const archiveObj = {
            'sDate': this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate(),
            'eDate': this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate(),
            'archiveArrObj': this.processCompObj.filter((obj) => obj.include === 1 || obj.include === true)
        };
        if (archiveObj.archiveArrObj.length > 0) {
            this.processCompensationService.archiveSer(archiveObj).subscribe(
                data => {
                    this.archiveRecored = [];
                    this.archiveRecored = data['result'];
                    for (let i = 0; i < this.processCompObj.length; i++) {
                        this.archiveRecored.forEach(element => {
                            if (this.processCompObj[i].workerId === element.workerId) {
                                this.processCompObj[i].id = element.Id;
                            }
                        });
                    }
                    this.resetButt = true;
                    this.arcButt = false;
                    this.fullview = true;
                },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }

    resetFun() {
        if (this.archiveRecored.length > 0) {
            this.processCompensationService.resetSer(this.archiveRecored).subscribe(
                data => {
                    const resetData = data['result'];
                    this.generateReport();
                },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }

    view(item) {
        if (this.multlpleclick) {
            this.multlpleclick = false;
            this.processCompensationService.workerCompensationList(this.startDate, this.endDate).subscribe(
                data => {
                    if (data['result'][0].length > 0) {
                        this.viewList = data['result'][0].filter(function (obj) { return obj.workerId === item.workerId; })[0];
                    }
                    this.generateFun(this.viewList, `'` + this.viewList.workerId + `'`);
                },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    this.multlpleclick = true;

                    switch (status) {
                        case 500:
                            break;
                        case 400:
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                }
            );
        }
    }
    generateButt() {
        let workerIds = '';
        this.topBarPrintButt = true;
        const notEmpty = this.processCompObj.filter(function (obj) {
            if (obj.include === 1 || obj.include === true) {
                workerIds += `'` + obj.workerId + `',`;
            }
            return obj.id !== '';
        });
        if (notEmpty.length === 0) {
            this.arcButt = true;
        }
        workerIds = workerIds.slice(0, -1);
        this.generateFun('', workerIds);
    }
    generateFun(item, workerIds) {

        const dateObj = {
            'stdate': this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate(),
            'eddate': this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate(),
        };
        this.processCompensationService.generateData(dateObj, workerIds).subscribe(
            data => {
                this.generateData = data['result']['serviceSalesByClientFlags'];
                this.generatRebookedServices = data['result']['RebookedServicesByDateRange'];
                this.generateTicketCounts = data['result']['ticketCounts'];
                this.generatProductSalesByProductLine = data['result']['productSalesByProductLine'];
                this.generatServiceSalesByServiceGroup = data['result']['serviceSalesByServiceGroup'];
                this.generatproductSalesByInventoryGroup = data['result']['productSalesByInventoryGroup'];
                this.generateSteps = data['result']['steps'];
                this.generateFeeAmount = data['result']['Fee_Amount__c'];
                this.srvcData = data['result']['srvcData'];
                this.prctData = data['result']['prctData'];
                this.serviceFeeData = data['result']['servicefeedata'];
                if (this.arcButt) {
                    this.getTotalAmountOfworker(this.srvcData, this.prctData, this.generateSteps); // this function used for get total amount
                }
                if (item) {
                    this.fullviewRes(item);
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    fullviewRes(item) {
        this.topBarPrintButt = true;
        this.processCompensationService.fullview(item.id, item.workerId, this.startDate, this.endDate).subscribe(
            data => {
                const temp1 = data['result']['result1'][0];
                const temp2 = data['result']['result1'][1];
                const temp3 = data['result']['result1'][2];
                const temp4 = data['result']['result'][4];
                this.finAry = [];
                for (let i = 0; i < temp1.length; i++) {
                    this.finAry.push({
                        'date': temp1[i]['serviceDate'],
                        'services': Number(temp1[i]['serviceTotal']) - Number(temp1[i].guestCharge),
                        'classes': 0.00,
                        'products': 0.00,
                        'productcount': 0.00,
                        'servicecount': 0.00,
                        'hoursWorked': 0
                    });
                }
                for (let i = 0; i < temp2.length; i++) {
                    let param = false;
                    for (let j = 0; j < this.finAry.length; j++) {
                        if (temp2[i]['productDate'] === this.finAry[j]['date']) {
                            param = true;
                            this.finAry[j]['products'] = temp2[i]['productTotal'];
                            break;
                        }
                    }
                    if (!param) {
                        this.finAry.push({
                            'date': temp2[i]['productDate'],
                            'services': 0.00,
                            'classes': 0.00,
                            'products': temp2[i]['productTotal'],
                            'productcount': temp2[i]['numberOfProducts'],
                            'servicecount': temp2[i]['numberOfServices'],
                            'hoursWorked': 0
                        });
                    }
                }
                for (let i = 0; i < temp3.length; i++) {
                    let param = false;
                    for (let j = 0; j < this.finAry.length; j++) {
                        if (temp3[i]['workDate'] === this.finAry[j]['date']) {
                            param = true;
                            this.finAry[j]['hoursWorked'] = temp3[i]['hoursWorked'];
                            break;
                        }
                    }
                    if (!param) {
                        this.finAry.push({
                            'date': temp3[i]['workDate'],
                            'services': 0.00,
                            'classes': 0.00,
                            'products': 0.00,
                            'productcount': 0.00,
                            'servicecount': 0.00,
                            'hoursWorked': temp3[i]['hoursWorked']
                        });
                    }
                }
                /* this method written to sort the date in chronological order(Ascending) */
                this.finAry.forEach(elem => {
                    if (elem['date']) {
                        elem['dt'] = this.commonservice.getDateFrmDBDateStr(elem['date']);
                    }
                });
                this.finAry.sort(function (a, b) {
                    return a.dt.setHours(0, 0, 0, 0) - b.dt.setHours(0, 0, 0, 0);
                });
                /* end of method */

                if (this.finAry.length > 0) {
                    this.finAry.forEach(element => {
                        this.totalGrossServiceAmo += element.services ? Number(element.services) : 0;
                        this.totalGrossProductAmo += element.products ? Number(element.products) : 0;
                        this.totalNumberOfProducts += element.productcount ? Number(element.productcount) : 0;
                        this.totalNumberOfServices += element.servicecount ? Number(element.servicecount) : 0;
                    });
                }
                this.viewResdata = JSON.parse(data['result']['result'][0][0].Steps__c);
                //  calculation
                for (let v = 0; v < this.viewResdata.length; v++) {

                    // ticket count
                    if (this.generateTicketCounts.length > 0) {
                        for (let t = 0; t < this.generateTicketCounts.length; t++) {
                            if (this.generateTicketCounts[t].workerId === item.workerId) {
                                if (this.viewResdata[v].operand === 'Tickets' && this.viewResdata[v].operator === 'Start With') {
                                    this.viewResdata[v].ticketNum = this.generateTicketCounts[t].ticketCount;
                                    this.viewResdata[v].ticRes = this.generateTicketCounts[t].ticketCount;
                                } else if (this.viewResdata[v].operand === 'Tickets') {
                                    this.viewResdata[v].ticketNum = this.generateTicketCounts[t].ticketCount;
                                }
                            }
                        }
                    }



                    // Gross service
                    if (this.viewResdata[v].operand === 'Gross Service') {
                        this.viewResdata[v].ticketNum = 0;
                        if (this.viewResdata[v].operand === 'Gross Service' && this.viewResdata[v].operandSubOption === 'All') {
                            this.viewResdata[v].ticketNum = this.totalGrossServiceAmo;
                        } else {
                            let serviceamount = 0;
                            let guestCharge = 0;
                            this.viewResdata[v].ticketNum = 0;
                            this.generatServiceSalesByServiceGroup.forEach(element => {
                                if (element.workerId === item.workerId) {
                                    if (element.serviceGroupName === this.viewResdata[v].operandSubOption) {
                                        serviceamount = element.serviceAmount;
                                        guestCharge += element.guestCharge;
                                        this.viewResdata[v].ticketNum = serviceamount - guestCharge;
                                    }
                                }
                            });
                        }
                    }
                    let operandvalue = 0;
                    // Salary
                    if (this.viewResdata[v].operand === 'Salary') {
                        this.viewResdata[v].ticketNum = item.salary;
                    } else if (this.viewResdata[v].operand === 'Hourly Wage') {  // Hourly Wage
                        if (item.hourlyWage) {
                            this.viewResdata[v].ticketNum = item.hourlyWage.toFixed(3);
                        } else {
                            this.viewResdata[v].ticketNum = 0.00;
                        }
                    } else if (this.viewResdata[v].operand === 'Hours Worked') { // Hours Worked
                        if (item.regularHours) {
                            this.viewResdata[v].ticketNum = item.regularHours.toFixed(3);
                        } else {
                            this.viewResdata[v].ticketNum = 0.00;
                        }
                        if (item.overtimeHours) {
                            this.viewResdata[v].ticketNum = (Number(item.overtimeHours) * 1.5) + item.regularHours;
                        }
                    } else if (this.viewResdata[v].operand === 'Days Worked') { // Days Worked
                        if (item.daysWorked) {
                            this.viewResdata[v].ticketNum = item.daysWorked;
                        } else {
                            this.viewResdata[v].ticketNum = 0;
                        }
                    } else if (this.viewResdata[v].operand === 'Services Performed' && this.viewResdata[v].operandSubOption === 'All') {
                        let nbrOfServices = 0;
                        this.viewResdata[v].ticketNum = 0;
                        temp1.forEach(element => {
                            nbrOfServices += element.numberOfServices ? Number(element.numberOfServices) : 0;
                            this.viewResdata[v].ticketNum = nbrOfServices;
                        });
                    } else if (this.viewResdata[v].operand === 'Services Performed' && this.viewResdata[v].operandSubOption !== 'All') { // Services Performed
                        let nbrOfServices = 0;
                        this.generatServiceSalesByServiceGroup.forEach(element => {
                            if (element.workerId === item.workerId) {
                                if (element.serviceGroupName === this.viewResdata[v].operandSubOption) {
                                    nbrOfServices += element.numberOfServices ? Number(element.numberOfServices) : 0;
                                    this.viewResdata[v].ticketNum = nbrOfServices;
                                }
                            }
                        });
                    } else if (this.viewResdata[v].operand === 'Gross Retail') {  // Gross Retail
                        this.viewResdata[v].ticketNum = 0;
                        if (this.viewResdata[v].operand === 'Gross Retail' && this.viewResdata[v].operandSubOption === 'All') {
                            this.viewResdata[v].ticketNum = this.totalGrossProductAmo;
                            // get Retail amount/numbers by product line
                        } else if (this.viewResdata[v].operand === 'Gross Retail' && this.viewResdata[v].operandSubOption !== 'All') {
                            let grossRetail = 0;
                            this.viewResdata[v].ticketNum = 0;
                            this.generatProductSalesByProductLine.forEach(element => {
                                if (element.workerId === item.workerId) {
                                    if (this.viewResdata[v].operandSubOption === element.productLineName) {
                                        grossRetail += element.productAmount ? Number(element.productAmount) : 0;
                                        this.viewResdata[v].ticketNum = grossRetail;
                                    }
                                }
                            });
                            this.generatproductSalesByInventoryGroup.forEach(element => {
                                if (element.workerId === item.workerId) {
                                    if (this.viewResdata[v].operandSubOption === element.inventoryGroupName) {
                                        grossRetail += element.productAmount ? Number(element.productAmount) : 0;
                                        this.viewResdata[v].ticketNum = grossRetail;
                                    }
                                }
                            });
                        }
                    } else if (this.viewResdata[v].operand === 'Products Sold') { // Products Sold
                        if (this.viewResdata[v].operand === 'Products Sold' && this.viewResdata[v].operandSubOption === 'All') {
                            let productSold = 0;
                            this.viewResdata[v].ticketNum = 0;
                            temp2.forEach(element => {
                                productSold += element.numberOfProducts ? Number(element.numberOfProducts) : 0;
                                this.viewResdata[v].ticketNum = productSold;
                            });
                        } else if (this.viewResdata[v].operand === 'Products Sold' && this.viewResdata[v].operandSubOption !== 'All') {
                            // get Retail amount/numbers by product line
                            // get Retail amount/numbers by inventory group
                            let productSold = 0;
                            this.viewResdata[v].ticketNum = 0;
                            this.generatProductSalesByProductLine.forEach(element => {
                                if (element.workerId === item.workerId) {
                                    if (this.viewResdata[v].operandSubOption === element.productLineName) {
                                        productSold += element.numberOfProducts ? Number(element.numberOfProducts) : 0;
                                        this.viewResdata[v].ticketNum = productSold;
                                    }
                                }
                            });
                        }
                    } else if (this.viewResdata[v].operand === 'Cost of Service Fee') {  // Cost of Service Fee
                        let costOfService = 0;
                        let tmpcostOfServCount = 0;
                        this.viewResdata[v].ticketNum = 0;
                        let temp;
                        let countbool = false;
                        for (let t = 0; t < this.srvcData.length; t++) {
                            let tempPrice = 0;
                            temp4.forEach(element => {
                                if ((this.srvcData[t].serviceId === element.wserviceId) && (element.Worker__c === item.workerId)) {
                                    if (element.Service_Fee_Percent__c != null && +element.Service_Fee_Percent__c > 0 && element.Price__c) {
                                        costOfService += (element.Price__c * element.Service_Fee_Percent__c / 100) * Number(this.srvcData[t].numberOfServices);
                                    } else if (element.Service_Fee_Percent__c != null && +element.Service_Fee_Percent__c > 0 && this.srvcData[t].serviceTotal) {
                                        costOfService += +this.srvcData[t].serviceTotal * +element.Service_Fee_Percent__c / 100;
                                    } else if (element.Service_Fee_Amount__c != null && +element.Service_Fee_Amount__c > 0) {
                                        costOfService += Number(element.Service_Fee_Amount__c) * Number(this.srvcData[t].numberOfServices);
                                    } else if (+element.Service_Fee_Amount__c <= 0 && +element.Service_Fee_Percent__c <= 0) {
                                        temp = (JSON.parse(element.Levels__c));
                                        tempPrice = element.Price__c;
                                        for (let k = 0; k < temp.length; k++) {
                                            if (temp[k]['levelNumber'] === element['Service_Level__c']) {
                                                countbool = true;
                                                if (temp[k]['Service_Fee_Amount__c']) {
                                                    costOfService += +temp[k]['Service_Fee_Amount__c'] ? +temp[k]['Service_Fee_Amount__c'] : 0;
                                                } else if (temp[k]['Service_Fee_Percent__c']) {
                                                    costOfService += (element.Price__c * temp[k].Service_Fee_Percent__c / 100) * Number(this.srvcData[t].numberOfServices);
                                                } else {
                                                    costOfService += 0;
                                                }
                                            } else {
                                                tmpcostOfServCount++;
                                            }
                                        }
                                    }
                                }
                            });
                            if (!countbool && tmpcostOfServCount > 0) {
                                if (temp[0]['Service_Fee_Amount__c']) {
                                    costOfService += +temp[0]['Service_Fee_Amount__c'] ? +temp[0]['Service_Fee_Amount__c'] : 0;
                                } else if (temp[0]['Service_Fee_Percent__c']) {
                                    costOfService += (tempPrice * temp[0].Service_Fee_Percent__c / 100) * Number(this.srvcData[t].numberOfServices);
                                } else {
                                    costOfService += 0;
                                }
                            }
                        }
                        this.viewResdata[v].ticketNum = costOfService;
                    } else if (this.viewResdata[v].operand === 'Number') { // Number
                        if (this.viewResdata[v].numeral) {
                            this.viewResdata[v].ticketNum = this.viewResdata[v].numeral;
                        } else {
                            this.viewResdata[v].ticketNum = 0;
                        }
                    } else if (this.viewResdata[v].operand === 'Result of Step') {  // Result of Step
                        this.viewResdata[v].ticketNum = this.viewResdata[v].numeral;
                        const stepVal = parseFloat(this.viewResdata[v].numeral);
                        operandvalue = this.viewResdata[(stepVal - 1)].result;
                    } else if (this.viewResdata[v].operand === 'Percent') { // Percent(
                        this.viewResdata[v].ticketNum = +parseFloat(this.viewResdata[v].numeral).toFixed(3);
                    } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                        let Scalec = [];
                        this.calScaleStep = 0;
                        /* for the issue 1275 it is added*/
                        const scaleObj = JSON.parse(this.scalesData.filter(obj => obj.Id === this.viewResdata[v].operand.split(':')[1])[0]['Scale__c']);
                        let result = this.viewResdata[v].numeral;
                        scaleObj.forEach(element => {
                            if (element['over'] <= this.viewResdata[v - 1]['result']
                                && element['upTo'] > this.viewResdata[v - 1]['result']) {
                                result = element['percent'];
                            }
                        });
                        this.viewResdata[v].ticketNum = result;
                        this.scalesData.forEach(obj => {
                            Scalec = JSON.parse(obj.Scale__c);
                            if (obj.Id === this.viewResdata[v].operand.split(':')[1]) {
                                this.viewResdata[v].operand = 'scale: ' + obj.Name;
                                if (this.viewResdata[v].operator === 'Multiply By') {
                                    if (this.viewResdata[v].Basis__c === 'Zero') {
                                        this.calScaleStep = (Number(this.viewResdata[v - 1].result) * Number(this.viewResdata[v].ticketNum) / 100);
                                    } else if (this.viewResdata[v].Basis__c === 'Step') {
                                        Scalec.forEach(element => {
                                            if (Number(this.viewResdata[v - 1].result) >= Number(element.upTo)) {
                                                this.calScaleStep += (element.upTo - element.over) * element.percent / 100;
                                            } else if (Number(this.viewResdata[v - 1].result) <= Number(element.upTo)) {
                                                if (Number(this.viewResdata[v - 1].result) > Number(element.over)) {
                                                    this.calScaleStep += (Number(this.viewResdata[v - 1].result) - element.over) * element.percent / 100;
                                                }
                                            }
                                        });

                                    }
                                }
                            }
                        });
                    }
                    if (!this.viewResdata[v].ticketNum) {
                        this.viewResdata[v].ticketNum = 0;
                    }
                    switch (this.viewResdata[v].operator) {
                        case ('Start With'):
                            this.viewResdata[v]['result'] = this.viewResdata[v].ticketNum;
                            if (this.viewResdata[v].operand === 'Result of Step') {
                                this.viewResdata[v]['result'] = operandvalue;
                            } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                                this.viewResdata[v]['result'] = 0;
                            }
                            break;
                        case ('Add'):
                            this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) + Number(this.viewResdata[v].ticketNum));
                            if (this.viewResdata[v].operand === 'Result of Step') {
                                this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) + operandvalue);
                            } else if (this.viewResdata[v].operand === 'Percent') {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result + this.viewResdata[v].ticketNum / 100;
                            } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result;
                            }
                            break;
                        case ('Multiply By'):
                            if (this.viewResdata[v].operand === 'Result of Step') {
                                this.viewResdata[v]['result'] = +parseFloat(this.viewResdata[v - 1].result).toFixed(2) * Number(operandvalue);
                            } else if (this.viewResdata[v].operand === 'Percent') {
                                this.viewResdata[v]['result'] = ((+parseFloat(this.viewResdata[v - 1].result).toFixed(2) * (+parseFloat(this.viewResdata[v].ticketNum).toFixed(2))) / 100);
                            } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                                this.viewResdata[v]['result'] = this.calScaleStep;
                            } else {
                                this.viewResdata[v]['result'] = +parseFloat(this.viewResdata[v - 1].result).toFixed(2) * (+parseFloat(this.viewResdata[v].ticketNum).toFixed(2));
                            }
                            break;
                        case ('Subtract'):
                            this.viewResdata[v]['result'] = this.viewResdata[v - 1].result - this.viewResdata[v].ticketNum;
                            if (this.viewResdata[v].operand === 'Result of Step') {
                                this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) - operandvalue);
                            } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result;
                            } else {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result - this.viewResdata[v].ticketNum;
                            }
                            break;
                        case ('Divide By'):
                            if (this.viewResdata[v].ticketNum && this.viewResdata[v].operand.split(':')[0] !== 'scale') {
                                this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) / this.viewResdata[v].ticketNum);
                                if (this.viewResdata[v].operand === 'Result of Step') {
                                    this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) / operandvalue);
                                } else if (this.viewResdata[v].operand === 'Percent') {
                                    this.viewResdata[v]['result'] = (parseFloat(this.viewResdata[v - 1].result) / Number(this.viewResdata[v].ticketNum) / 100);
                                }
                            } else {
                                this.viewResdata[v]['result'] = 0;
                            }
                            break;
                        case ('If Less Than'):
                            if (this.viewResdata[v].ticketNum < this.viewResdata[v - 1].result) {
                                this.viewResdata[v]['result'] = parseFloat(this.viewResdata[v].ticketNum);
                                if (this.viewResdata[v].operand === 'Result of Step') {
                                    this.viewResdata[v]['result'] = operandvalue;
                                } else if (this.viewResdata[v].operand.split(':')[0] === 'scale') {
                                    this.viewResdata[v]['result'] = 0;
                                }
                            } else {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result;
                            }
                            break;
                        case ('If More Than'):
                            if (this.viewResdata[v].ticketNum > this.viewResdata[v - 1].result) {
                                this.viewResdata[v]['result'] = parseFloat(this.viewResdata[v].ticketNum);
                                if (this.viewResdata[v].operand === 'Result of Step') {
                                    this.viewResdata[v]['result'] = operandvalue;
                                }
                            } else {
                                this.viewResdata[v]['result'] = this.viewResdata[v - 1].result;
                            }
                            break;
                        default:
                            this.viewResdata[v]['result'] = 0.00;
                    }
                }

                if (item.extraPay) {          // If Extra Pay
                    this.viewResdata.push({
                        'operator': 'Add',
                        'operand': 'Extra Pay',
                        'ticketNum': item.extraPay,
                        'result': this.viewResdata[(this.viewResdata.length) - 1].result + item.extraPay
                    });
                }
                if (item.deduction) {          // If Deduction
                    this.viewResdata.push({
                        'operator': 'Subtract',
                        'operand': 'Deduction',
                        'ticketNum': item.deduction,
                        'result': this.viewResdata[(this.viewResdata.length) - 1].result - item.deduction
                    });
                }

                this.datePicDis = false;
                this.fullviewSetup = true;
                this.listModal.show();
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (status) {
                    case 500:
                        break;
                    case 400:
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }

    getTotalAmountOfworker(srvData, prcData, workerData) {
        this.totalCount = 0;
        workerData.forEach(element => {
            element.Steps__c = JSON.parse(element.Steps__c); // 'Steps__c' string convert to json
        });
        for (let i = 0; i < this.processCompObj.length; i++) {
            if (this.processCompObj[i].include === 1 || this.processCompObj[i].include === true) {
                for (let w = 0; w < workerData.length; w++) {
                    if (this.processCompObj[i].workerId === workerData[w].Id) {
                        workerData[w].Steps__c.forEach((workerObj, index) => {
                            if (workerObj.operand === 'Hourly Wage') {                    // Hourly Wage
                                this.genOperandvalue = this.processCompObj[i].hourlyWage;
                            } else if (workerObj.operand === 'Salary') {                  // Salary
                                this.genOperandvalue = this.processCompObj[i].salary;
                            } else if (workerObj.operand === 'Hours Worked') {            // Hours Worked
                                this.genOperandvalue = this.processCompObj[i].regularHours;
                                // if overtimeHours is there
                                if (this.processCompObj[i].overtimeHours) {
                                    this.genOperandvalue = (Number(this.processCompObj[i].overtimeHours) * 1.5) + +this.processCompObj[i].regularHours;
                                }
                            } else if (workerObj.operand === 'Days Worked') {                   // Days Worked
                                this.genOperandvalue = this.processCompObj[i].daysWorked;
                            } else if (workerObj.operand === 'Services Performed' && workerObj.operandSubOption === 'All') {
                                let nbrOfServices = 0;
                                this.genOperandvalue = 0;
                                srvData.forEach(element => {
                                    if (element.Worker__c === this.processCompObj[i].workerId) {
                                        nbrOfServices += element.numberOfServices ? Number(element.numberOfServices) : 0;
                                        this.genOperandvalue = nbrOfServices;
                                    }
                                });
                            } else if (workerObj.operand === 'Services Performed' && workerObj.operandSubOption !== 'All') {         // Services Performed
                                let nbrOfServices = 0;
                                this.genOperandvalue = 0;
                                this.generatServiceSalesByServiceGroup.forEach(element => {
                                    if (element.workerId === workerData[w].Id) {
                                        if (workerObj.operandSubOption === element.serviceGroupName) {
                                            nbrOfServices += element.numberOfServices ? Number(element.numberOfServices) : 0;
                                            this.genOperandvalue = nbrOfServices;
                                        }
                                    }
                                });
                            } else if (workerObj.operand === 'Products Sold' && workerObj.operandSubOption === 'All') {                // Products Sold
                                let productSold = 0;
                                this.genOperandvalue = 0;
                                prcData.forEach(element => {
                                    if (element.Worker__c === this.processCompObj[i].workerId) {
                                        productSold += element.numberOfProducts ? Number(element.numberOfProducts) : 0;
                                        this.genOperandvalue = productSold;
                                    }
                                });
                            } else if (workerObj.operand === 'Products Sold' && workerObj.operandSubOption !== 'All') {
                                // get Retail amount/numbers by product line
                                let productSold = 0;
                                this.genOperandvalue = 0;
                                this.generatProductSalesByProductLine.forEach(element => {
                                    if (element.workerId === workerData[w].Id) {
                                        if (workerObj.operandSubOption === element.productLineName) {
                                            productSold += element.numberOfProducts ? Number(element.numberOfProducts) : 0;
                                            this.genOperandvalue = productSold;
                                        }
                                    }
                                });
                            } else if (workerObj.operand === 'Tickets') {    // Tickets
                                this.genOperandvalue = 0;
                                this.generateTicketCounts.forEach(ticCou => {
                                    if (ticCou.workerId === workerData[w].Id) {
                                        this.genOperandvalue = ticCou.ticketCount;
                                    }
                                });
                            } else if (workerObj.operand === 'Gross Service' && workerObj.operandSubOption === 'All') {
                                let serviceamount = 0;
                                let guestCharge = 0;
                                this.genOperandvalue = 0;
                                srvData.forEach(element => {
                                    if (element.Worker__c === this.processCompObj[i].workerId) {
                                        guestCharge += element.guestCharge;
                                        serviceamount += element.serviceTotal;
                                        this.genOperandvalue = serviceamount - guestCharge;
                                    }
                                });
                            } else if (workerObj.operand === 'Gross Service' && workerObj.operandSubOption !== 'All') {
                                let serviceamount = 0;
                                let guestCharge = 0;
                                this.genOperandvalue = 0;
                                this.generatServiceSalesByServiceGroup.forEach(element => {
                                    if (element.workerId === workerData[w].Id) {
                                        if (workerObj.operandSubOption === element.serviceGroupName) {
                                            guestCharge += element.guestCharge;
                                            serviceamount += element.serviceAmount;
                                            this.genOperandvalue = serviceamount - guestCharge;
                                        }
                                    }
                                });
                            } else if (workerObj.operand === 'Gross Retail' && workerObj.operandSubOption === 'All') {
                                let productamount = 0;
                                this.genOperandvalue = 0;
                                prcData.forEach(element => {
                                    if (element.Worker__c === this.processCompObj[i].workerId) {
                                        productamount += element.productTotal;
                                        this.genOperandvalue = productamount;
                                    }
                                });
                            } else if (workerObj.operand === 'Gross Retail' && workerObj.operandSubOption !== 'All') {
                                // get Retail amount/numbers by product line
                                // get Retail amount/numbers by inventory group
                                let grossRetail = 0;
                                this.genOperandvalue = 0;
                                this.generatProductSalesByProductLine.forEach(obj => {
                                    if (obj.workerId === workerData[w].Id) {
                                        if (workerObj.operandSubOption === obj.productLineName) {
                                            grossRetail += obj.productAmount ? Number(obj.productAmount) : 0;
                                            this.genOperandvalue = grossRetail;
                                        }
                                    }
                                });
                                this.generatproductSalesByInventoryGroup.forEach(element => {
                                    if (element.workerId === workerData[w].Id) {
                                        if (workerObj.operandSubOption === element.inventoryGroupName) {
                                            grossRetail += element.productAmount ? Number(element.productAmount) : 0;
                                            this.genOperandvalue = grossRetail;
                                        }
                                    }
                                });
                            } else if (workerObj.operand === 'Number') {     // Number
                                if (workerObj.numeral) {
                                    this.genOperandvalue = workerObj.numeral;
                                } else {
                                    this.genOperandvalue = 0;
                                }
                            } else if (workerObj.operand === 'Cost of Service Fee') {  // Cost of Service Fee
                                let costOfService = 0;
                                let tmpcostOfServCount = 0;
                                this.genOperandvalue = 0;
                                let temp;
                                let countbool = false;
                                for (let v = 0; v < this.srvcData.length; v++) {
                                    let tempPrice = 0;
                                    this.serviceFeeData.forEach(element => {
                                        if ((this.srvcData[v].Worker__c === workerData[w].Id) &&
                                            (element.Worker__c === this.srvcData[v].Worker__c) && (this.srvcData[v].serviceId === element.wserviceId)) {
                                            if (+element.Service_Fee_Percent__c != null && +element.Service_Fee_Percent__c > 0 && element.Price__c) {
                                                costOfService += (element.Price__c * +element.Service_Fee_Percent__c / 100) * Number(this.srvcData[v].numberOfServices);
                                            } else if (+element.Service_Fee_Percent__c != null && +element.Service_Fee_Percent__c > 0 && this.srvcData[v].serviceTotal) {
                                                costOfService += +this.srvcData[v].serviceTotal * +element.Service_Fee_Percent__c / 100;
                                            } else if (element.Service_Fee_Amount__c != null && element.Service_Fee_Amount__c > 0) {
                                                costOfService += Number(element.Service_Fee_Amount__c) * Number(this.srvcData[v].numberOfServices);
                                            } else if (element.Service_Fee_Amount__c <= 0 && +element.Service_Fee_Percent__c <= 0) {
                                                temp = (JSON.parse(element.Levels__c));
                                                tempPrice = element.Price__c;
                                                for (let k = 0; k < temp.length; k++) {
                                                    if (temp[k]['levelNumber'] === element['Service_Level__c']) {
                                                        countbool = true;
                                                        if (temp[k]['Service_Fee_Amount__c']) {
                                                            costOfService += +temp[k]['Service_Fee_Amount__c'] ? +temp[k]['Service_Fee_Amount__c'] : 0;
                                                        } else if (temp[k]['Service_Fee_Percent__c']) {
                                                            costOfService += (element.Price__c * temp[k].Service_Fee_Percent__c / 100) * Number(this.srvcData[v].numberOfServices);
                                                        } else {
                                                            costOfService += 0;
                                                        }
                                                    } else {
                                                        tmpcostOfServCount++;
                                                    }

                                                }

                                            }
                                        }
                                    });
                                    if (!countbool && tmpcostOfServCount > 0) {
                                        if (temp[0]['Service_Fee_Amount__c']) {
                                            costOfService += +temp[0]['Service_Fee_Amount__c'] ? +temp[0]['Service_Fee_Amount__c'] : 0;
                                        } else if (temp[0]['Service_Fee_Percent__c']) {
                                            costOfService += (tempPrice * temp[0].Service_Fee_Percent__c / 100) * Number(this.srvcData[v].numberOfServices);
                                        } else {
                                            costOfService += 0;
                                        }
                                    }
                                }
                                this.genOperandvalue = costOfService;
                            } else if (workerObj.operand === 'Result of Step') {
                                // Result of Step
                                this.genOperandvalue = 0;
                                const stepVal = parseFloat(workerObj.numeral);
                                this.genOperandvalue = workerData[w].Steps__c[(stepVal - 1)].genTotal;
                            } else if (workerObj.operand === 'Percent') {
                                this.genOperandvalue = 0;              // Percent
                                this.genOperandvalue = workerData[w].Steps__c[(index - 1)].genTotal ? workerObj.numeral : 0;
                            } else {
                                this.genOperandvalue = 0;
                            }

                            if (workerObj.operand.split(':')[0] === 'scale') {
                                let Scalec = [];
                                this.calScaleStep = 0;
                                /* for the issue 1842 it is added*/
                                const scaleObj = JSON.parse(this.scalesData.filter(obj => obj.Id === workerObj.operand.split(':')[1])[0]['Scale__c']);
                                let result = workerData[w].Steps__c[(index - 1)].numeral;
                                scaleObj.forEach(element => {
                                    if (element['over'] <= workerData[w].Steps__c[(index - 1)].genTotal
                                        && element['upTo'] > workerData[w].Steps__c[(index - 1)].genTotal) {
                                        result = element['percent'];
                                    }
                                });
                                workerData[w].ticketNum = result;
                                /* issue closed*/
                                this.scalesData.forEach(obj => {
                                    Scalec = JSON.parse(obj.Scale__c);
                                    if (obj.Id === workerObj.operand.split(':')[1]) {
                                        if (workerObj.operator === 'Multiply By') {
                                            if (workerObj.Basis__c === 'Zero') {
                                                this.calScaleStep = (Number(workerData[w].Steps__c[(index - 1)].genTotal) * Number(workerData[w].ticketNum) / 100);
                                            } else if (workerObj.Basis__c === 'Step') {
                                                Scalec.forEach(element => {
                                                    if (Number(workerData[w].Steps__c[(index - 1)].genTotal) >= Number(element.upTo)) {
                                                        this.calScaleStep += (element.upTo - element.over) * element.percent / 100;
                                                    } else {
                                                        if (Number(workerData[w].Steps__c[(index - 1)].genTotal) > Number(element.over)) {
                                                            this.calScaleStep += (Number(workerData[w].Steps__c[(index - 1)].genTotal) - element.over) * element.percent / 100;
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                            switch (workerObj.operator) {
                                case ('Start With'):
                                    workerObj['genTotal'] = this.genOperandvalue;
                                    if (workerObj.operand === 'Result of Step') {
                                        workerObj['genTotal'] = this.genOperandvalue;
                                    }
                                    break;
                                case ('Multiply By'):
                                    if (workerObj.operand === 'Result of Step') { // Result of Step
                                        workerObj['genTotal'] = +parseFloat(workerData[w].Steps__c[index - 1].genTotal).toFixed(2) * (+parseFloat(this.genOperandvalue).toFixed(2));
                                    } else if (workerObj.operand === 'Percent') { // Percent
                                        workerObj['genTotal'] = ((+parseFloat(workerData[w].Steps__c[index - 1].genTotal).toFixed(2) * (+parseFloat(this.genOperandvalue).toFixed(2))) / 100);
                                    } else if (workerObj.operand.split(':')[0] === 'scale') {
                                        workerObj['genTotal'] = this.calScaleStep;
                                    } else {
                                        workerObj['genTotal'] = +parseFloat(workerData[w].Steps__c[index - 1].genTotal).toFixed(2) * (+parseFloat(this.genOperandvalue).toFixed(2));
                                    }
                                    break;
                                case ('Add'):
                                    workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) + Number(this.genOperandvalue));
                                    if (workerObj.operand === 'Result of Step') {
                                        workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) + this.genOperandvalue);
                                    } else if (workerObj.operand === 'Percent') {
                                        workerObj['genTotal'] = workerData[w].Steps__c[index - 1].genTotal + this.genOperandvalue / 100;
                                    }
                                    break;
                                case ('Subtract'):
                                    workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) - Number(this.genOperandvalue));
                                    if (workerObj.operand === 'Result of Step') {
                                        workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) - this.genOperandvalue);
                                    }
                                    break;
                                case ('Divide By'):
                                    if (this.genOperandvalue) {
                                        workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) / this.genOperandvalue);
                                        if (workerObj.operand === 'Result of Step') {
                                            workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) / this.genOperandvalue);
                                        } else if (workerObj.operand === 'Percent') {
                                            workerObj['genTotal'] = (parseFloat(workerData[w].Steps__c[index - 1].genTotal) / Number(this.genOperandvalue) / 100);
                                        }
                                    } else {
                                        workerObj['genTotal'] = 0;
                                    }
                                    break;
                                case ('If Less Than'):
                                    if (this.genOperandvalue < workerData[w].Steps__c[index - 1].genTotal) {
                                        workerObj['genTotal'] = parseFloat(this.genOperandvalue);
                                        if (workerObj.operand === 'Result of Step') {
                                            workerObj['genTotal'] = this.genOperandvalue;
                                        }
                                    } else {
                                        workerObj['genTotal'] = workerData[w].Steps__c[index - 1].genTotal;
                                    }
                                    break;
                                case ('If More Than'):
                                    if (this.genOperandvalue > workerData[w].Steps__c[index - 1].genTotal) {
                                        workerObj['genTotal'] = parseFloat(this.genOperandvalue);
                                        if (workerObj.operand === 'Result of Step') {
                                            workerObj['genTotal'] = this.genOperandvalue;
                                        }
                                    } else {
                                        workerObj['genTotal'] = workerData[w].Steps__c[index - 1].genTotal;
                                    }
                                    break;
                                default:
                                    workerObj['genTotal'] = 0.00;
                            }

                        });
                        if (this.processCompObj[i].extraPay) {          // If Extra Pay
                            this.processCompObj[i].compensationAmount =
                                (Number(workerData[w].Steps__c[workerData[w].Steps__c.length - 1]['genTotal']) + Number(this.processCompObj[i].extraPay));
                        }
                        if (+this.processCompObj[i].deduction) {         // If Deduction
                            this.processCompObj[i].compensationAmount = (this.processCompObj[i].compensationAmount ? Number(this.processCompObj[i].compensationAmount) :
                                Number(workerData[w].Steps__c[workerData[w].Steps__c.length - 1]['genTotal'])) - Number(this.processCompObj[i].deduction);
                        }
                        if (!this.processCompObj[i].extraPay && !+this.processCompObj[i].deduction) {
                            this.processCompObj[i].compensationAmount = workerData[w].Steps__c[workerData[w].Steps__c.length - 1]['genTotal'];
                        }
                        this.totalCount += Number(this.processCompObj[i].compensationAmount);
                    }
                }
            } else {
                this.processCompObj[i].compensationAmount = 0;
            }
        }
    }

    getscales() {
        this.setupCompMethodService.getscales()
            .subscribe(res => {
                this.scalesData = res['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }

    closeListModal() {
        this.multlpleclick = true;
        this.listModal.hide();
        this.viewResdata = [];
        this.fullviewSetup = false;
        this.totalGrossServiceAmo = 0;
        this.totalGrossProductAmo = 0;
        this.totalNumberOfProducts = 0;
        this.totalNumberOfServices = 0;
        this.viewList = [];
        this.finAry = [];
    }

}
