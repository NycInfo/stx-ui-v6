import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { WorkerTipsService } from './workertips.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-reports-app',
  templateUrl: './workertips.html',
  styleUrls: ['./workertips.css'],
  providers: [WorkerTipsService],
})
export class WorkerTipsComponent implements OnInit {
  startDate = new Date();
  endDate = new Date();
  minDate = new Date();
  bsValue = new Date();
  bsValue1 = new Date();
  worker = 'company';
  workerId = '';
  itemsDisplay = false;
  userListData: any;
  datePickerConfig: any;
  error: any;
  decodedToken: any;
  decodeUserToken: any;
  companyName: any;
  workerTipsList: any;
  reportTypes = ['Company Tips', 'Worker Tips'];
  totalTipsAmount = 0;
  tipsWithDates = [];
  tipsAmount = [];
  tipsAmountWithDate = [];
  type: any;
  toastermessage: any;
  constructor(
    private workerTipsService: WorkerTipsService,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
     // ---Start of code for Permissions Implementation--- //
     try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getUserList();
    this.type = this.reportTypes[0];
  }
  loadCompanyInfo(data) {
    this.companyName = data.Name;
  }
  generateReport() {
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      const endDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
      const obj = {
        'begindate': startDate,
        'enddate': endDate,
        'used': this.worker,
        'workerId': this.workerId
      };
      // this.itemsDisplay = true;
      this.workerTipsService.getWorkerTipsRecords(obj).subscribe(data => {
        this.workerTipsList = [];
        let tipsData: Array<any> = data['result']['data'][0];
        tipsData = tipsData.concat(data['result']['data'][1]);
        if (tipsData.length === 0) {
          this.itemsDisplay = false;
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
        } else {
          this.itemsDisplay = true;
          const dates = data['result']['dates'];
          this.tipsWithDates = [];
          this.tipsAmount = [];
          this.totalTipsAmount = 0;
          this.tipsAmountWithDate = [];
          dates.forEach((gDate) => {
            // const tipsWithSameDate = [];
            let tipsDateAmount = 0;
            const tipsWithSameDate = tipsData.filter((obj2) => gDate === obj2.aptDt);
            if (tipsWithSameDate.length > 0) {
              // console.log('jddvkjxvn');
              const tipsWithSameId = [];
              const tipsWithDate = [];
              const dupData = [];
              dupData.push(tipsWithSameDate);
              tipsWithSameDate.forEach((obj1) => {
                if (dupData.indexOf(obj1.Name) === -1) {
                  const tmpAry = tipsWithSameDate.filter((obj3) => obj3.Name === obj1.Name);
                  const tmpAry1 = [];
                  const uniqueObj = tmpAry.filter((data1, i) => tmpAry.findIndex((data2) => data1.Worker__c === data2.Worker__c) === i);
                  let tipsPerAppt = 0;
                  uniqueObj.forEach((data4) => {
                    const obj3: any = {
                      Service_Sales__c: 0,
                      Tip_Amount__c: 0,
                      Tip_Left_in_Drawer__c: 0,
                      Tip_Paid_Out__c: 0
                    };
                    tmpAry.forEach((data5, i) => {
                      if (data4.Worker__c === data5.Worker__c) {
                        obj3.Name = data5.Name;
                        obj3.clientName = data5.clientName;
                        obj3.userName = data5.userName;
                        obj3.aptDt = data5.aptDt;
                        if (Object.keys(data5).filter((key) => key === 'Service_Sales__c').length > 0) {
                          obj3.Service_Sales__c += data5.Service_Sales__c;
                        } else {
                          obj3.Tip_Amount__c += data5.Tip_Amount__c;
                          obj3.Tip_Left_in_Drawer__c += +data5.Tip_Left_in_Drawer__c;
                          obj3.Tip_Paid_Out__c += + data5.Tip_Paid_Out__c;
                          tipsPerAppt += data5.Tip_Amount__c;
                          this.totalTipsAmount += data5.Tip_Amount__c;
                          tipsDateAmount += data5.Tip_Amount__c;
                        }
                      }
                    });
                    tmpAry1.push(obj3);
                  });
                  tipsWithDate.push(tipsPerAppt);
                  tipsWithSameId.push(tmpAry1);
                  dupData.push(obj1.Name);
                }
              });
              if (tipsWithSameId.length > 0) {
                this.tipsWithDates.push(tipsWithSameId);
                this.tipsAmount.push(tipsWithDate);
                this.tipsAmountWithDate.push(tipsDateAmount);
              }
            }
          });
          for (let i = 0; i < this.tipsWithDates.length; i++) {
            for (let t = 0; t < this.tipsWithDates[i].length; t++) {
              for (let u = 0; u < this.tipsWithDates[i][t].length; u++) {
                this.tipsWithDates[i]['date'] = this.tipsWithDates[i][t][u]['aptDt'];
              }
            }
          }
        } /* else end */
        // console.log(this.tipsWithDates);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }
  }
  getUserList() {
    this.workerTipsService.getUserList()
      .subscribe(data => {
        this.userListData = data['result'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  clearErr() {
    this.error = '';
  }
  onChangeUser(id) {
    this.itemsDisplay = false;
    if (!id || id === '') {
      this.worker = 'company';
      this.workerId = '';
      this.type = this.reportTypes[0];
    } else {
      this.workerId = id;
      this.worker = undefined;
      this.type = this.reportTypes[1];
    }
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.workerId + '"', 'selected value="' + this.workerId + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>Worker tips Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}
