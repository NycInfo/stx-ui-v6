import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { OnHandProductService } from './onhandproduct.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-reports-app',
  templateUrl: './onhandproduct.html',
  styleUrls: ['./onhandproduct.css'],
  providers: [OnHandProductService],
})
export class OnHandProductComponent implements OnInit {
  showProdList: any = false;
  productLineList: any;
  pdLineId: any;
  totalSize = 0;
  isGenerate = false;
  noDataErr: any;
  itemsDisplay = false;
  productSalesObj = [];
  inventoryGroups: any;
  totalHand = 0;
  totalCost = 0;
  decodedToken: any;
  decodeUserToken: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private onHandProductService: OnHandProductService) {

  }
  ngOnInit() {
 // ---Start of code for Permissions Implementation--- //
     try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getProductLine();
    this.getInventoryGroups();
  }
  pdLineListOnChange(value) {
    this.pdLineId = value;
  }
  /*-- Method to get product lines list(active) --*/
  getProductLine() {
    this.onHandProductService.getProductLineDetails(1)
      .subscribe(data => {
        this.productLineList = data['result'];
        this.pdLineId = this.productLineList[0]['Id'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  getInventoryGroups() {
    /*--- Method to get inventory groups ---*/
    this.onHandProductService.getInventoryGroupData()
      .subscribe(data => {
        this.inventoryGroups = data['result'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  generateReport() {
    this.productSalesObj = [];
    const productObj = {
      'pdLine': this.pdLineId
    };
    this.onHandProductService.generateReport(productObj).subscribe(data => {
      this.isGenerate = true;
      const temp = data['result'];
      this.totalSize = temp.length;
      const dataList = [];
      this.totalHand = 0;
      this.totalCost = 0;
      for (let i = 0; i < this.inventoryGroups.length; i++) {
        const tempProd = temp.filter((obj) => obj.Inventory_Group__c === this.inventoryGroups[i]['inventoryGroupName']);
        let totalQtyHand = 0;
        let onHandCost = 0;
        tempProd.forEach(element => {
          totalQtyHand = totalQtyHand + element['Quantity_On_Hand__c'];
          onHandCost = onHandCost + element['On_Hand_Cost__c'];
        });
        dataList.push(tempProd);
        const tempArr = temp.filter((obj) => obj.Inventory_Group__c === this.inventoryGroups[i]['inventoryGroupName']);
        dataList[i]['inventoryGroupName'] = this.inventoryGroups[i]['inventoryGroupName'];
        dataList[i]['size'] = tempArr ? tempArr.length : 0;
        dataList[i]['totalHand'] = totalQtyHand;
        dataList[i]['totalHandCost'] = onHandCost;
      }
      this.productSalesObj = dataList.filter((obj) => obj.length);
     // console.log(this.productSalesObj )
      this.productSalesObj.forEach(element => {
        this.totalHand += element.totalHand;
        this.totalCost += element.totalHandCost;
      });
      if (this.productSalesObj.length === 0) {
        this.noDataErr = '**Data Not Found';
      } else {
        this.noDataErr = '';
      }
    },
      error => {
        this.itemsDisplay = false;
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.pdLineId + '"', 'selected value="' + this.pdLineId + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
      <head>
        <title>On Hand product Report</title>
        <style>
        table {
          border-collapse: collapse;
        }
        table, th, td {
            border: 0.5px solid black;
        }
        .pri td {
          padding:6px;
        }
        .arc {
          float:left;
          margin:12px;
        }
        .arc button {
          margin-top:10px;
        }
        .total {
          margin:6px;
        }
        </style>
      </head>
      <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
    popupWin.document.close();
  }
}
