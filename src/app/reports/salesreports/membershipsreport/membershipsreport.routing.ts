import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MembershipsreportComponent } from './membershipsreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MembershipsreportComponent,
                children: [
                    {
                        path: '',
                        component: MembershipsreportComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MembershipsReportRoutingModule {
}
