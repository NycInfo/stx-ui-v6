import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ProductSalesByRankService } from './productsalesbyrank.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { ManageInventoryService } from '../../../../app/inventory/manageinventory/manageinventory.service';
import { ReportServiceSalesService } from '../../../../app/reports/salesreports/reportservicesales/reportservicesales.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './productsalesbyrank.html',
  styleUrls: ['./productsalesbyrank.css'],
  providers: [ProductSalesByRankService, ManageInventoryService, ReportServiceSalesService],
})
export class ProductSalesByRankComponent implements OnInit {
  startDate = new Date();
  endDate = new Date();
  minDate = new Date();
  decodedToken: any;
  decodeUserToken: any;
  itemsDisplay: any = false;
  workerTipsData: any;
  datePickerConfig: any;
  type: any;
  proLineName: any = '';
  workerName: any = '';
  includeUnsold: any = false;
  error: any;
  productLinesData: any;
  productLine = '';
  reportTypes = ['Company', 'Worker'];
  showWorkers: any = false;
  workerList = [];
  category = 'A';
  categorySales = 0;
  runningTotal = 0;
  totalSales = 0;
  categoryListA = [];
  categoryListB = [];
  categoryListC = [];
  categoryListD = [];
  AsubTotal = 0;
  BsubTotal = 0;
  CsubTotal = 0;
  DsubTotal = 0;
  totalamount = 0;
  toastermessage: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private productSalesByRankservice: ProductSalesByRankService,
    private manageInventoryService: ManageInventoryService,
    private reportServiceSalesService: ReportServiceSalesService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getProductLines();
    this.getWorkerList();
    this.type = this.reportTypes[0];
  }
  getProductLines() {
    this.manageInventoryService.getProductLinesData().subscribe(
      data => {
        this.productLinesData = data['result'].filter(
          filterList => filterList.Active__c === 1);
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
            break;
        }
      }
    );
  }
  getWorkerList() {
    this.reportServiceSalesService.getWorkerList().subscribe(data => {
      this.workerList = [];
      this.workerList = data['result']
        .filter(filterList => filterList.IsActive);
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  generateReport() {
    this.clear();
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.type === 'Worker' && this.workerName === '') {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      const edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
      const searchObj = {
        'startDate': stDate,
        'endDate': edDate,
        'pdLine': this.productLine,
        'type': this.type,
        // 'unsold': this.includeUnsold,
        'worker': this.workerName
      };
      this.productSalesByRankservice.productsSearch(searchObj).subscribe(
        data => {
          let results = [];
          this.categoryListA = [];
          this.categoryListB = [];
          this.categoryListC = [];
          this.categoryListD = [];
          this.runningTotal = 0;
          this.DsubTotal = 0;
          if (data['result'][0].length === 0 && data['result'][1].length === 0) {
            this.itemsDisplay = false;
            this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
          } else {
            if (data['result'][0] && data['result'][0].length > 0) {
              results = data['result'][0];
            }
            if (data['result'][1] && data['result'][1].length > 0) {
              this.categoryListD = this.categoryListD.concat(data['result'][1]);
            }
            this.itemsDisplay = true;
            results.forEach((obj, i) => {
              this.totalSales += obj.Net_Price__c; /* total sales */
            });
            if (this.totalSales) {
              results.forEach((obj, i) => {
                if (this.category === 'A') {
                  if (this.runningTotal / this.totalSales >= 0.6) {
                    this.categorySales = 0;
                    this.category = 'B';
                  } else {
                    this.categorySales += obj.Net_Price__c;
                    this.runningTotal += obj.Net_Price__c;
                    this.categoryListA.push(obj);
                  }
                }
                if (this.category === 'B') {
                  if (this.runningTotal / this.totalSales >= 0.8) {
                    this.categorySales = 0;
                    this.category = 'C';
                  } else {
                    this.categorySales += obj.Net_Price__c;
                    this.runningTotal += obj.Net_Price__c;
                    this.categoryListB.push(obj);
                  }
                }
                if (this.category === 'C') {
                  if (this.runningTotal / this.totalSales >= 0.95) {
                    this.categorySales = 0;
                    this.category = 'D';
                  } else {
                    this.categorySales += obj.Net_Price__c;
                    this.runningTotal += obj.Net_Price__c;
                    this.categoryListC.push(obj);
                  }
                }
                if (this.category === 'D') {
                  this.categorySales += obj.Net_Price__c;
                  this.runningTotal += obj.Net_Price__c;
                  this.categoryListD.push(obj);
                }
              });
              this.categoryListA.forEach((obj, i) => {
                this.AsubTotal += obj.Net_Price__c;
              });
              this.categoryListB.forEach((obj, i) => {
                this.BsubTotal += obj.Net_Price__c;
              });
              this.categoryListC.forEach((obj, i) => {
                this.CsubTotal += obj.Net_Price__c;
              });
              this.categoryListD.forEach((obj, i) => {
                if (obj.Net_Price__c) {
                  this.DsubTotal += obj.Net_Price__c;
                } else {
                  this.DsubTotal += 0;
                }
              });
            }
          } /* Show msg Else end */
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              }
              break;
          }
        }
      );
    }
  }
  reportTypeOnChange(value) {
    this.itemsDisplay = false;
    this.type = value;
    if (value === 'Worker') {
      this.showWorkers = true;
    } else {
      this.showWorkers = false;
      this.workerName = '';
    }
  }
  workerListOnChange(value) {
    this.itemsDisplay = false;
    this.workerName = value;

    // this.workerName = value.split('$')[1];
  }

  /*Method to change product line value */
  onProductLineChange(value) {
    this.productLine = value;
  }
  clearMsg() {
    this.error = '';
  }
  clear() {
    this.totalSales = 0;
    this.categoryListA = [];
    this.categoryListB = [];
    this.categoryListC = [];
    this.categoryListD = [];
    this.AsubTotal = 0;
    this.BsubTotal = 0;
    this.CsubTotal = 0;
    this.DsubTotal = 0;
    this.category = 'A';
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.type + '"', 'selected value="' + this.type + '"');
    printContents = printContents.replace('value="' + this.proLineName + '"', 'selected value="' + this.proLineName + '"');
    printContents = printContents.replace('value="' + this.workerName + '"', 'selected value="' + this.workerName + '"');

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>Product sales by rank Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}

