import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { AccountBalanceService } from './accountbalance.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../../../common/common.service';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-reports-app',
  templateUrl: './accountbalance.html',
  styleUrls: ['./accountbalance.css'],
  providers: [AccountBalanceService, CommonService],
})
export class AccountBalanceComponent implements OnInit {

  balancetype = 'All balances';
  clientAmountData = [];
  Allbalance: any;
  Balancedue: any;
  Accountcredits: any;
  clientAmountData1 = [];
  sumvalue: any;
  currentDate = new Date();
  createdDate: any = [];
  toastermessage: any;
  Value: any;
  decodedToken: any;
  decodeUserToken: any;
  constructor(private accountbalanceservice: AccountBalanceService,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private commonService: CommonService
  ) {
  }
  ngOnInit() {
     // ---Start of code for Permissions Implementation--- //
     try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
  } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
  }
  if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
  } else {
      this.decodedToken = {};
  }
  // ---End of code for Permissions Implementation--- //
    this.getClientAmountData();
    // this.createdDate = this.commonService.getDBDatTmStr(this.date);
    this.Value = this.balanceType[0];
  }
  balanceType(value) {
    this.Value = value;
    if (value === 'Allbalance') {
      this.clientAmountData = this.clientAmountData1.filter(object => object.amount !== 0);
    } else if (value === 'Balancedue') {
      this.clientAmountData = this.clientAmountData1.filter(Object => Object.amount > 0);
    } else if (value === 'Accountcredits') {
      this.clientAmountData = this.clientAmountData1.filter(Object => Object.amount < 0);
    }
    this.sumvalue = 0;
    for (let i = 0; i < this.clientAmountData.length; i++) {
      this.sumvalue += this.clientAmountData[i].amount;
    }
  }
  getClientAmountData() {
    this.accountbalanceservice.getClientAmountData().subscribe(
      data => {
        this.clientAmountData1 = data['result'];
        if (this.clientAmountData1.length > 0) {
          setTimeout(() => {
            this.balanceType('Allbalance');
          }, 1000);
          this.createdDate = this.commonService.getDBDatTmStr(this.currentDate);
        } else {
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_CLIENTS_FOUND');
          this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.Value + '"', 'selected value="' + this.Value + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Account Balances</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
