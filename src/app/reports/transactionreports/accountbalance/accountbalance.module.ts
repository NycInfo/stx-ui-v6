import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from 'ng2-translate';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AccountBalanceComponent } from './accountbalance.component';
import { AccountBalanceRoutingModule } from './accountbalance.routing';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        AccountBalanceRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        AccountBalanceComponent
    ]
})
export class AccountBalanceModule {
}
