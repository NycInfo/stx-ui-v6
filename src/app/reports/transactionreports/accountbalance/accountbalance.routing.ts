import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountBalanceComponent } from './accountbalance.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountBalanceComponent,
                children: [
                    {
                        path: '',
                        component: AccountBalanceComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountBalanceRoutingModule {
}
