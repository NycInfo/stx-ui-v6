import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { PaymentDetailsService } from './paymentdetails.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
import { CheckOutEditTicketService } from '../../../checkout/editticket/checkouteditticket.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './paymentdetails.html',
  styleUrls: ['./paymentdetails.css'],
  providers: [PaymentDetailsService, CommonService, CheckOutEditTicketService],
})
export class PaymentDetailsComponent implements OnInit {
  // export class PaymentDetailsComponent implements OnInit, AfterViewInit {
  date = new Date();
  startDate = new Date();
  maxDate = new Date();
  endDate = new Date();
  isGenerate = false;
  workerTipsData: any;
  datePickerConfig: any;
  reporttype = 'Company';
  WorkerList = true;
  seleWorker = '';
  paymentsList: any;
  merchantWorkerList: any = [];
  merchantAccntName: any = '';
  paymentGateWay: any = '';
  error: any;
  type: any;
  workerName: '';
  toastermessage: any;
  decodedToken: any;
  decodeUserToken: any;
  paymentsTotal = 0;
  refundsTotal = 0;
  grandTotal = 0;
  trigger = true;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private checkOutEditTicketService: CheckOutEditTicketService,
    private commonService: CommonService,
    private paymentDetailsService: PaymentDetailsService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getWorkerMerchants();
    // this.updateHeaderDate(this.bsValue, this.bsValue1);
    this.type = this.reporttype[0];
  }
  // ngAfterViewInit() {
  //   this.headerDateFormat();
  // }
  headerDateFormat() {
    let sDate;
    let sMonth;
    let eDate;
    let eMonth;
    if (this.startDate.getDate() < 10) {
      sDate = '0' + this.startDate.getDate();
    } else {
      sDate = this.startDate.getDate();
    }
    if ((this.startDate.getMonth() + 1) < 10) {
      sMonth = '0' + (this.startDate.getMonth() + 1);
    } else {
      sMonth = (this.startDate.getMonth() + 1);
    }
    if (this.endDate.getDate() < 10) {
      eDate = '0' + this.endDate.getDate();
    } else {
      eDate = this.endDate.getDate();
    }
    if ((this.endDate.getMonth() + 1) < 10) {
      eMonth = '0' + (this.endDate.getMonth() + 1);
    } else {
      eMonth = (this.endDate.getMonth() + 1);
    }
    // const displayName = document.getElementById('displayNameId');
    // displayName.innerHTML = ' Electronic Payment Details ' + sMonth + '/' + sDate + '/' + this.startDate.getFullYear() + ' - ' + eMonth + '/' + eDate + '/' + this.endDate.getFullYear();
  }
  generateReport() {
    // this.headerDateFormat();
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else if ((this.WorkerList === false) && (this.seleWorker === 'Select Worker')) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
    } else {
      if (this.trigger) {
        this.trigger = false;
        const stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
        const edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
        const dataObj = {
          startDate: stDate,
          endDate: edDate,
          worker: this.seleWorker,
          type: (this.WorkerList === false) ? 'Worker' : 'Company'
        };
        this.paymentDetailsService.getPaymentReport(dataObj).subscribe(data => {
          this.paymentsList = data['result'];
          if (this.paymentsList.length === 0) {
            this.isGenerate = false;
            this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
          } else {
            this.paymentsTotal = 0;
            this.refundsTotal = 0;
            this.grandTotal = 0;
            this.paymentsList.forEach(element => {
              element['Appt_Date_Time__c'] = this.commonService.getUsrDtStrFrmDBStr(element['Appt_Date_Time__c']);
              if (element['Amount_Paid__c'] > 0) {
                this.paymentsTotal += element['Amount_Paid__c'];
              }
              if (element['Amount_Paid__c'] < 0) {
                this.refundsTotal += element['Amount_Paid__c'];
              }
              if (element['Amount_Paid__c']) {
                this.grandTotal += element['Amount_Paid__c'];
              }
            });
            this.isGenerate = true;
          }
          this.trigger = true;
        }, error => {
          this.trigger = true;
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              }
              break;
          }
        });
      }
    }
  }
  reportType(value) {
    this.type = value;
    if (value === 'Company') {
      this.WorkerList = true;
      this.seleWorker = '';
    } else {
      this.WorkerList = false;
      this.seleWorker = this.merchantAccntName;
    }
  }
  getWorkerMerchants() {
    this.checkOutEditTicketService.getWorkerMerchantsData()
      .subscribe(data => {
        this.merchantWorkerList.push({
          Payment_Gateway__c: 'AnywhereCommerce', FirstName: 'Select',
          LastName: 'Worker', Id: ''
        });
        if (data['result'] && data['result'].length > 0) {
          this.merchantWorkerList = this.merchantWorkerList.concat(data['result']);
        }
        // for default values
        // this.paymentGateWay = this.merchantWorkerList[0]['Payment_Gateway__c'];
        this.merchantAccntName = this.merchantWorkerList[0]['FirstName'] + ' ' + this.merchantWorkerList[0]['LastName'];
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } else if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }
  merchantOnChange(value) {
    const temp = this.merchantWorkerList.filter((obj) => obj.Id === value)[0];
    this.workerName = temp.Id;
    this.merchantAccntName = temp.FirstName + ' ' + temp.LastName;
    this.seleWorker = this.merchantAccntName;
    // this.paymentGateWay = temp.Payment_Gateway__c;
  }

  // updateHeaderDate(bgnDate: Date, endDate: Date) {
  //   const displayName = document.getElementById('displayNameId');
  //   displayName.innerHTML = 'TICKET DETAILS ' + (bgnDate.getMonth() + 1) + '/' + bgnDate.getDate() + '/' + bgnDate.getFullYear() +
  //     ' - ' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' + endDate.getFullYear();
  // }
  // datepickerChange(event) {
  //   this.updateHeaderDate(this.bsValue, this.bsValue1);
  // }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.workerName + '"', 'selected value="' + this.workerName + '"');
    printContents = printContents.replace('value="' + this.type + '"', 'selected value="' + this.type + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Electronic Payment Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }

}
