import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SetupCompanyhrsService } from './setupcompanyhrs.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { CommonService } from '../../../common/common.service';
import { JwtHelper } from 'angular2-jwt';

@Component({
    selector: 'app-setupcompanyhours-popup',
    templateUrl: './setupcompanyhours.html',
    styleUrls: ['./setupcompanyhours.component.css'],
    providers: [SetupCompanyhrsService, CommonService]
})
export class SetupCompanyHoursComponent implements OnInit {
    minDate: Date;
    toastermessage: any;
    bsValue: any;
    datePickerConfig: any;
    paramsId: any;
    decodedToken: any;
    decodeUserToken: any;
    /*new hours */
    toDate: any;
    fromDate: any;
    ceDate: any = new Date();
    csDate: any = new Date();
    hoursNote: any = '';
    displayList = [];
    displayTempList = [];
    getHoursData: any = [];
    unqCustList: any = [];
    showExpiry: any = false;
    uniqueId = '';
    customData = 'COMPANY HOURS';
    actionPopup = 'add';
    hrserror: any;
    disNote: Boolean = false;
    constructor(private SetupCompanyhrsServices: SetupCompanyhrsService,
        private cdRef: ChangeDetectorRef,
        @Inject('defaultCountry') public defaultCountry: string,
        private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private translateService: TranslateService,
        private sanitizer: DomSanitizer,
        private commonService: CommonService) {
        this.minDate = new Date();
        this.datePickerConfig = Object.assign({},
            {
                showWeekNumbers: false,
                containerClass: 'theme-blue',
            });
        this.route.queryParams.subscribe(params => {
            this.paramsId = route.snapshot.params['id'];
        });
    }
    ngOnInit() {
        // ---Start of code for Permissions Implementation--- //
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
          } catch (error) {
            this.decodedToken = {};
            this.decodeUserToken = {};
          }
          if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
          } else {
            this.decodedToken = {};
          }
        /* new hours */
        this.getCustomHours();
    }
    saveHours(id) {
        if (this.toDate === null || this.toDate === '') {
            this.hrserror = 'SETUP_COMPANY_HOURSE.VALID_NOBLANK_START_DATE_FIELD';
        } else if (!id && this.toDate.setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
            this.hrserror = 'SETUP_COMPANY_HOURSE.NO_VALID_NOBLANK_START_DATE_FIELD';
        } else if (this.fromDate && this.fromDate < this.toDate) {
            this.hrserror = 'SETUP_COMPANY_HOURSE.NO_VALID_END_DATE_FIELD';
        } else {
            if (this.fromDate) {
                this.ceDate = this.fromDate.getFullYear() + '-' + (this.fromDate.getMonth() + 1) + '-' + this.fromDate.getDate();
            } else {
                this.ceDate = this.toDate.getFullYear() + '-' + (this.toDate.getMonth() + 1) + '-' + this.toDate.getDate();
            }
            const hoursData = {
                'toDate': this.toDate.getFullYear() + '-' + (this.toDate.getMonth() + 1) + '-' + this.toDate.getDate(),
                'fromDate': this.ceDate,
                'hoursNote': this.hoursNote,
                'customType': this.customData,
            };
            if (id) {
                hoursData['groupId'] = id;
            }
            this.SetupCompanyhrsServices.postCustomHours(hoursData).subscribe(
                data => {
                    data = data['result'];
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.getCustomHours();
                    this.hrserror = '';
                    this.cancelHrs('save');
                    this.hoursNote = '';
                    this.actionPopup = 'add';
                },
                error => {
                    const status = JSON.parse(error['_body']).result;
                    const statuscode = JSON.parse(error['_body'])['status'];
                    switch (JSON.parse(error['_body']).status) {
                        case '2097':
                            this.hrserror = 'SETUP_COMPANY_HOURSE.DUPLICATE_RECORD';
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            );
        }
    }
    cancelHrs(data) {
        this.toDate = '';
        this.fromDate = '';
        this.hoursNote = '';
        this.actionPopup = 'add';
        this.disNote = false;
        this.hrserror = '';
        if (data === 'cancel') {
            this.router.navigate(['/setup/company']);
        }
    }
    clrHrserr() {
        this.hrserror = '';
    }

    getCustomHours() {
        this.SetupCompanyhrsServices.getCompanyCustomhours().subscribe(
            data => {
                this.getHoursData = data['result'];
                this.displayList = [];
                this.displayTempList = [];
                this.unqCustList = [];
                this.getHoursData.forEach(obj => {
                    if (this.unqCustList.indexOf(obj['UserId__c']) === -1) {
                        this.unqCustList.push(obj['UserId__c']);
                    }
                });
                this.unqCustList.forEach(obj => {
                    const tempList = this.getHoursData.filter(obj2 => obj2['UserId__c'] === obj);
                    let curDate = new Date();
                    curDate = this.commonService.getDateFrmDBDateStr(curDate.getFullYear()
                        + '-' + ('0' + (curDate.getMonth() + 1)).slice(-2)
                        + '-' + ('0' + curDate.getDate()).slice(-2));
                    const tempDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);
                    let expStatus = true;
                    if (tempDate < curDate) {
                        expStatus = false;
                    }
                    const tempDate2 = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
                    let delStatus = true;
                    if (tempDate2 < curDate) {
                        delStatus = false;
                    }
                    if (tempList[0]['date'] === tempList[tempList.length - 1]['date']) {
                        this.displayList.push({
                            'uniqId': obj,
                            'message': 'COMPANY DAYS OFF: ' + tempList[0]['date'] + ' (' + tempList[0]['Name'] + ')',
                            'expStatus': expStatus,
                            'delStatus': delStatus
                        });
                    } else {
                        this.displayList.push({
                            'uniqId': obj,
                            'message': 'COMPANY DAYS OFF: ' + tempList[0]['date'] + ' - ' + tempList[tempList.length - 1]['date'] + ' (' + tempList[0]['Name'] + ')',
                            'expStatus': expStatus,
                            'delStatus': delStatus
                        });
                    }


                });
                this.displayList.forEach(obj => obj['message'] = obj['message'].replace('()', ''));
                this.showExpiredHrs();
            },
            error => {
                const status = JSON.parse(error['_body']).result;
                const statuscode = JSON.parse(error['_body'])['status'];
                if (statuscode === '2085' || statuscode === '2071') {
                    this.router.navigate(['/']).then(() => { });
                }
            }
        );
    }
    showExpiredHrs() {
        if (!this.showExpiry) {
            this.displayTempList = this.displayList.filter(
                filterList => filterList.expStatus === true);
        } else {
            this.displayTempList = this.displayList;
        }
    }
    /* delete custom hrs */
    deleteHoursData(id) {
        this.SetupCompanyhrsServices.deleteCustomHours(id).subscribe(
            data => {
                const data1 = data['result'];
                if (data1) {
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_DELETE_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.getCustomHours();
                    this.actionPopup = 'add';
                    this.cancelHrs('delete');
                }
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    editHoursData(id) {
        this.disNote = false;
        this.uniqueId = id;
        this.actionPopup = 'edit';
        const tempList = this.getHoursData.filter(obj2 => obj2['UserId__c'] === id);
        if (tempList[0]['date'] === tempList[tempList.length - 1]['date']) {
            this.toDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
            this.fromDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
        } else {
            this.toDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
            this.fromDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);

        }
        this.hoursNote = tempList[0]['Name'];
    }

    deleteFn(id) {
        this.disNote = true;
        this.uniqueId = id;
        this.actionPopup = 'delete';
        const tempList = this.getHoursData.filter(obj2 => obj2['UserId__c'] === id);
        if (tempList[0]['date'] === tempList[tempList.length - 1]['date']) {
            this.toDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
            this.fromDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
        } else {
            this.toDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
            this.fromDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);

        }
        this.hoursNote = tempList[0]['Name'];
    }
}
