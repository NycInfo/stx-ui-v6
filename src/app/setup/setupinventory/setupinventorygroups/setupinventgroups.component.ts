/*---
    Setup inventorygroups component having following methods:
    * ngOnInit(): This method is used for on page load
    * saveGroups(): This method is used to save inventory groups
    * getInvetoryGroups(): This method is used to get inventory groups
    * showData(groupData): This method is used to show data
    * editInventorygroupData(): This method is used to update inventory groups
    * addNew(): This method is used to add new inventory group
    * cancel(): This method is used to cancel all previous changes
    * clear(): This method is used to clear fields
    * clearErrMsg(): This method is used to clear error messages
---*/
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { SetupInventGroupsService } from './setupinventgroups.service';
@Component({
    selector: 'app-setupinventgroups-popup',
    templateUrl: './setupinventgroups.html',
    styleUrls: ['./setupinventorygroups.css'],
    providers: [SetupInventGroupsService]
})
export class SetupInventGroupsComponent implements OnInit {
    addDiv = true;
    editDiv = false;
    hideTable = true;
    disableClass = '';
    disable = true;
    groupList: any;
    inventoryGroupName: any;
    taxable: any;
    sortOrder: any;
    groupObj: any = {};
    error: any;
    groupDetails: any;
    inventoryGroups: any;
    updateInventoryGroupName: any;
    updateTaxable: any;
    updateSortOrder: any;
    productGroupEditData: any = {};
    groupEditStatus: any;
    oldInventoryGroupName: any;
    toastermessage: any;
    button = false;
    constructor(private toastr: ToastrService,
        private translateService: TranslateService,
        private setupInventGroupsService: SetupInventGroupsService,
        private router: Router) {
    }
    /*--- Method for on page load ---*/
    ngOnInit() {
        this.getInvetoryGroups();
    }
    /*--- Method to save inventory groups ---*/
    saveGroups() {
        if (this.inventoryGroupName === '' || this.inventoryGroupName === undefined || this.inventoryGroupName === 'undefined') {
            this.error = 'COMMON.VALID_NOBLANK_NAME';
        } else {
            this.groupObj.inventoryGroupName = this.inventoryGroupName;
            this.groupObj.taxable = this.taxable;
            this.groupObj.sortOrder = this.sortOrder;
            this.setupInventGroupsService.saveInventoryGroupData(this.groupObj)
                .subscribe(
                    data => {
                        this.groupDetails = data['data'];
                        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                        this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                        this.disableClass = '';
                        this.addDiv = true;
                        this.disable = true;
                        this.editDiv = false;
                        this.clear();
                        this.getInvetoryGroups();
                    },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;
                        switch (JSON.parse(error['_body']).status) {
                            case '2036':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2038':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    /*--- Method to get inventory groups ---*/
    getInvetoryGroups() {
        this.setupInventGroupsService.getInventoryGroupData()
            .subscribe(data => {
                this.inventoryGroups = data['result'];
                this.groupList = this.inventoryGroups;
                for (let i = 0; i < this.groupList.length; i++) {
                    if (this.groupList[i].sortOrder === '' || this.groupList[i].sortOrder === null) {
                        this.groupList[i].sortOrder = 0;
                    }
                }
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    /*--- Method to show data ---*/
    showData(groupData) {
        this.addDiv = false;
        this.editDiv = true;
        this.disable = false;
        this.disableClass = 'disabled';
        this.updateInventoryGroupName = groupData.inventoryGroupName;
        this.updateTaxable = groupData.taxable;
        if (groupData.sortOrder === '' || groupData.sortOrder === null) {
            this.updateSortOrder = 0;
        } else {
            this.updateSortOrder = groupData.sortOrder;
        }
        this.oldInventoryGroupName = groupData.inventoryGroupName;

        this.setupInventGroupsService.getInventoryGroupDependencyData(this.updateInventoryGroupName)
            .subscribe(data => {
                const invGrpData = data['result'];
                if (invGrpData && invGrpData.length > 0) {
                    this.button = false;
                } else {

                    this.button = true;
                }
            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /*--- Method to delete inventory group ---*/
    deleteInvGroup() {
        this.setupInventGroupsService.deleteInvGrp(this.updateInventoryGroupName)
            .subscribe(data => {
                const invGrpData = data['result'];
                this.groupEditStatus = data['data'];
                this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_DELETE_SUCCESS');
                this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                this.disableClass = '';
                this.addDiv = true;
                this.disable = true;
                this.editDiv = false;
                this.getInvetoryGroups();
                this.clear();
            }, error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    /*--- Method to update inventory groups ---*/
    editInventorygroupData() {
        if (this.updateInventoryGroupName === '' || this.updateInventoryGroupName === undefined ||
            this.updateInventoryGroupName === 'undefined') {
            this.error = 'COMMON.VALID_NOBLANK_NAME';
        } else {
            this.productGroupEditData.inventoryGroupName = this.updateInventoryGroupName;
            this.productGroupEditData.taxable = this.updateTaxable;
            this.productGroupEditData.sortOrder = this.updateSortOrder;
            this.setupInventGroupsService.editProductGroupData(this.oldInventoryGroupName,
                this.updateInventoryGroupName, this.productGroupEditData)
                .subscribe(
                    data => {
                        this.groupEditStatus = data['data'];
                        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
                        this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                        this.disableClass = '';
                        this.addDiv = true;
                        this.disable = true;
                        this.editDiv = false;
                        this.getInvetoryGroups();
                        this.clear();
                    },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;
                        switch (JSON.parse(error['_body']).status) {
                            case '2036':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    /*--- Method to add new inventory group ---*/
    addNew() {
        this.clear();
        this.disableClass = 'disabled';
        this.addDiv = true;
        this.disable = false;
    }
    /*--- Method to cancel previous changes ---*/
    cancel() {
        this.clear();
        this.addDiv = true;
        this.editDiv = false;
        this.disableClass = '';
        this.disable = true;
    }
    /*--- Method to clear fields ---*/
    clear() {
        this.inventoryGroupName = '';
        this.taxable = '';
        this.sortOrder = '';
        this.error = '';
    }
    /*--- Method to clear error message ---*/
    clearErrMsg() {
        this.error = '';
    }

    /* method to restrict specialcharecters  */
    alphaNumOnly(e) {
        const specialKeys = new Array();
        specialKeys.push(8); // Backspace
        specialKeys.push(9); // Tab
        specialKeys.push(46); // Delete
        specialKeys.push(36); // Home
        specialKeys.push(35); // End
        specialKeys.push(37); // Left
        specialKeys.push(39); // Right
        const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
            (keyCode >= 35 && keyCode <= 38) || (keyCode >= 40 && keyCode <= 47) ||
            (keyCode >= 63 && keyCode <= 64) || (keyCode >= 93 && keyCode <= 96) ||
            (keyCode >= 125 && keyCode <= 126) || (keyCode >= 58 && keyCode <= 59) ||
            (keyCode >= 32 && keyCode < 34) || (keyCode > 60 && keyCode < 62) || (keyCode > 90 && keyCode < 92) ||
            (keyCode > 122 && keyCode < 124) || (keyCode > 123 && keyCode < 125) ||
            (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        return ret;
    }
}

