/*
    Setup Product Line component has the following methods.
    * ngOnInit(): This method is used to load methods on page load
    * getProductLine(): This method is used to get product lines
    * getInventoryGroups(): This method is used to get inventory groups
    * getInactiveProductLine(value): This method is used to get inactive or active inventory product lines
    * createProductLine(): This method is used to create product line
    * showData(productlinelist): This method is used to show data
    * editInventoryProductLines(): This method is used to edit inventory product line
    * addNew(): This method is used to add new inventory product line
    * deleteProductLine(): This method is used to delete product line
    * cancel(): This method is used to cancel changes
    * clear(); This method is used to clear data
    * clearmessage(): This method is used to clear error message
*/
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { SetupProductLinesService } from './setupprodlines.service';
@Component({
    selector: 'app-setupprodlines-popup',
    templateUrl: './setupprodlines.html',
    styleUrls: ['./setupprodlines.css'],
    providers: [SetupProductLinesService]
})
export class SetupProductLinesComponent implements OnInit {
    toastermessage: any;
    addDiv = true;
    editDiv = false;
    disableEnable: any;
    enableDisable: any = true;
    active: any;
    Pname: any;
    inActive: any = false;
    productLineName: any;
    productColor = this.defaultColor;
    color = this.defaultColor;
    rows = [];
    measures = [];
    updatemeasures = [];
    inventoryGroups: any;
    unitOfMeasures: any;
    productLineList: any;
    productLineObj: {};
    productLineData: any;
    error: any;
    updateId: any;
    updateActive: any;
    updateProductLineName: any;
    updateProductColor: any;
    updateInventoryGroups: any;
    updateUnitOfMeasures: any;
    inventoryGroupList: any;
    updateInventoryGroupsList: any;
    updateProductLineObj: any;
    updateProductLine: any;
    statuscode: any;
    removeName: any;
    updatedefaultcolor: any;
    constructor(private toastr: ToastrService,
        private productLinesService: SetupProductLinesService,
        private router: Router,
        private translateService: TranslateService,
        @Inject('defaultColor') public defaultColor: string,
        @Inject('defaultType') public defaultType: string,
        @Inject('defaultActive') public defaultActive: string,
        @Inject('defaultInActive') public defaultInActive: string
    ) {
    }
    /*-- Method used to add inventory groups,
     units of measure and removing them --*/
    addRows() {
        this.rows.push({ inventoryGroups: this.inventoryGroupList[0].inventoryGroupName });
    }
    onChangeInventGroups(value, i) {
        this.rows[i]['inventoryGroups'] = value;
    }
    addMeasures() {
        this.measures.push({ unitOfMeasures: '' });
    }
    addUpdateMeasures() {
        this.updatemeasures.push({ unitOfMeasures: '' });
    }
    deleteFieldValue(row, index) {
        this.rows.splice(index, 1);
    }
    deleteFieldValue3(index) {
        this.updatemeasures.splice(index, 1);
    }
    deleteFieldValue2(inventorygrouplist, index) {
        this.removeName = inventorygrouplist.inventoryGroups;
        this.productLinesService.removeInventoryGroup(this.removeName).subscribe(data => {
            this.updateInventoryGroupsList.splice(index, 1);
        }, error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (JSON.parse(error['_body']).status) {
                case '2040':
                    this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                    window.scrollTo(0, 0);
                    break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    /*-- On load method --*/
    ngOnInit() {
        this.getProductLine();
        this.getInventoryGroups();
    }
    /*-- Method to get product lines list(active) --*/
    getProductLine() {
        this.productLinesService.getProductLineDetails(this.inActive ? 0 : 1)
            .subscribe(data => {
                this.productLineList = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    /*-- Method to get inventory groups --*/
    getInventoryGroups() {
        this.productLinesService.getInventoryGroups()
            .subscribe(data => {
                this.inventoryGroupList = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    /*-- Method to get product lines all --*/
    getInactiveProductLine(value) {
        this.inActive = value;
        this.getProductLine();
    }
    /*-- Method to create product line --*/
    createProductLine() {
        if (this.productLineName === undefined || this.productLineName === 'undefined' || this.productLineName === '') {
            this.error = 'SETUP_INVENTORY_PRODUCT_LINE.VALID_NOBLANK_PRODUCT_LINE_NAME';
        } else if (this.rows === undefined || this.rows.length === 0) {
            this.error = 'SETUP_INVENTORY_PRODUCT_LINE.VALID_NOBLANK_INVENTORY_GROUP_NAME';
        } else {
            if (this.active === undefined || this.active === false) {
                this.active = this.defaultInActive;
            } else {
                this.active = this.defaultActive;
            }
            this.productLineObj = {
                'active': this.active,
                'productLineName': this.productLineName,
                'productColor': this.productColor === '' ? '#ffffff' : this.productColor,
                'inventoryGroups': this.rows,
                'unitOfMeasures': this.measures.filter(filterList => filterList.unitOfMeasures)
            };
            this.productLinesService.createProductLine(this.productLineObj)
                .subscribe(data => {
                    this.productLineData = data['result'];
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.getProductLine();
                    this.enableDisable = true;
                    this.disableEnable = false;
                    this.clear();
                }, error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            window.scrollTo(0, 0);
                            break;
                        case '2042':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            window.scrollTo(0, 0);
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    showData(productlinelist) {
        this.updatedefaultcolor = productlinelist.Color__c;
        const type = this.defaultType;
        this.updateId = productlinelist.Id;
        this.Pname = productlinelist.Name;
        this.productLinesService.getDeleteResponse(this.updateId, type, this.Pname).subscribe(data => {
            this.statuscode = JSON.parse(data['status']);
        }, error => {
            const status = JSON.parse(error['status']);
            this.statuscode = JSON.parse(error['_body']).status;
            if (this.statuscode === '2085' || this.statuscode === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
        this.productColor = this.defaultColor;
        this.rows = [];
        this.measures = [];
        this.addDiv = false;
        this.editDiv = true;
        this.enableDisable = false;
        this.disableEnable = true;
        this.getProductLine();
        this.updateId = productlinelist.Id;
        this.updateActive = productlinelist.Active__c;
        this.updateProductLineName = productlinelist.Name;
        this.updateProductColor = productlinelist.Color__c;
        this.updateInventoryGroups = productlinelist.Groups__c;
        this.rows = JSON.parse(productlinelist.Groups__c);
        this.updateUnitOfMeasures = JSON.parse(productlinelist.Units_of_Measure__c);
        this.productLinesService.productDependencyToDisableInvGrp(this.updateId).subscribe(data => {
            const dataObj = data['result'];
            for (let i = 0; i < this.rows.length; i++) {
                for (let j = 0; j < dataObj.length; j++) {
                    if (this.rows[i]['inventoryGroups'] === dataObj[j]['Inventory_Group__c']) {
                        this.rows[i].isDependency = true;
                    }
                }
            }
        }, error => {
            const status = JSON.parse(error['status']);
            this.statuscode = JSON.parse(error['_body']).status;
            if (this.statuscode === '2085' || this.statuscode === '2071') {
                if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                }
            }
        });
    }
    /*-- Method to edit product line --*/
    editInventoryProductLines() {
        let unitOfMeasure;
        unitOfMeasure = this.updateUnitOfMeasures;
        this.unitOfMeasures = unitOfMeasure.concat(this.updatemeasures);
        if (this.updateProductLineName === undefined || this.updateProductLineName === 'undefined' || this.updateProductLineName === '') {
            this.error = 'SETUP_INVENTORY_PRODUCT_LINE.VALID_NOBLANK_PRODUCT_LINE_NAME';
        } else if (this.rows === undefined || this.rows.length === 0) {
            this.error = 'SETUP_INVENTORY_PRODUCT_LINE.VALID_NOBLANK_INVENTORY_GROUP_NAME';
        } else {
            this.updateActive = this.updateActive ? this.defaultActive : this.defaultInActive;
            this.updateProductLineObj = {
                'updateActive': this.updateActive,
                'updateProductLineName': this.updateProductLineName,
                'updateProductColor': this.updateProductColor === '' ? '#ffffff' : this.updateProductColor,
                'updateInventoryGroups': this.rows,
                'updateUnitOfMeasures': this.unitOfMeasures.filter(filterList => filterList.unitOfMeasures)
            };
            this.productLinesService.updateInventoryProductLine(this.updateProductLineObj, this.updateId)
                .subscribe(data => {
                    this.updateProductLine = data['result'];
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.editDiv = true;
                    this.addDiv = false;
                    this.disableEnable = false;
                    this.enableDisable = true;
                    this.getProductLine();
                    this.clear();
                    this.rows = [];
                    this.updatemeasures = [];
                }, error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            window.scrollTo(0, 0);
                            break;
                        case '2042':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            window.scrollTo(0, 0);
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    /*--- Method to add div ---*/
    addNew() {
        this.active = true;
        this.error = '';
        this.rows = [];
        this.measures = [];
        this.addDiv = true;
        this.enableDisable = false;
        this.disableEnable = true;
        this.editDiv = false;
        // if (this.inActive === 0) {
        //     this.inActive = 1;
        // }
        // this.productLinesService.getProductLineDetails(this.inActive)
        //     .subscribe(data => {
        //         this.productLineList = data['result'];
        //     },
        //         error => {
        //             const errStatus = JSON.parse(error['_body'])['status'];
        //             if (errStatus === '2085' || errStatus === '2071') {
        //                 if (this.router.url !== '/') {
        //                     localStorage.setItem('page', this.router.url);
        //                     this.router.navigate(['/']).then(() => { });
        //                 }
        //             }
        //         });
    }
    /* method is used to set default color for the field when field is empty */
    colorCheck() {
        this.productColor = this.defaultColor;
    }
    updatecolorCheck() {
        this.updateProductColor = this.updatedefaultcolor;
    }
    /*-- Method to delete product line --*/
    deleteProductLine() {
        this.productLinesService.deleteProductLine(this.updateId, this.Pname)
            .subscribe(data => {
                this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_DELETE_SUCCESS');
                this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                this.enableDisable = true;
                this.disableEnable = false;
                this.addDiv = true;
                this.editDiv = false;
                this.getProductLine();
                this.clear();
            }, error => {
                const status = JSON.parse(error['status']);
                this.statuscode = JSON.parse(error['_body']).status;
                if (this.statuscode === '2085' || this.statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });

    }
    /*--- Method used to cancel changes ---*/
    cancel() {
        this.productColor = this.defaultColor;
        this.editDiv = false;
        this.addDiv = true;
        this.rows = [];
        this.updatemeasures = [];
        this.enableDisable = true;
        this.disableEnable = false;
        this.inActive = 0;
        //  this.getProductLine();
        this.clear();
    }
    /*-- Method to clear product line --*/
    clear() {
        this.active = '';
        this.productLineName = '';
        this.color = this.defaultColor;
        this.error = '';
    }
    /*-- Method to create error --*/
    clearmessage() {
        this.error = '';
    }

    /* method to restrict specialcharecters  */
    alphaNumOnly(e) {
        const specialKeys = new Array();
        specialKeys.push(8); // Backspace
        specialKeys.push(9); // Tab
        specialKeys.push(46); // Delete
        specialKeys.push(36); // Home
        specialKeys.push(35); // End
        specialKeys.push(37); // Left
        specialKeys.push(39); // Right
        const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
            (keyCode >= 35 && keyCode <= 38) || (keyCode >= 40 && keyCode <= 47) ||
            (keyCode >= 63 && keyCode <= 64) || (keyCode >= 93 && keyCode <= 96) ||
            (keyCode >= 125 && keyCode <= 126) || (keyCode >= 58 && keyCode <= 59) ||
            (keyCode >= 32 && keyCode < 34) || (keyCode > 60 && keyCode < 62) || (keyCode > 90 && keyCode < 92) ||
            (keyCode > 122 && keyCode < 124) || (keyCode > 123 && keyCode < 125) ||
            (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        return ret;
    }
}
