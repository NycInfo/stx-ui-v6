import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { SetupbillboardgoalsService } from './setupbillboardgoals.service';
import * as config from '../../../app.config';
@Component({
  selector: 'app-setupbillboardgoals-popup',
  templateUrl: './setupbillboardgoals.html',
  styleUrls: ['./setupbillboardgoals.css'],
  providers: [SetupbillboardgoalsService]
})
export class SetupbillboardgoalsComponent implements OnInit {
  tabsArray = [true, false, false, false, false, false, false];
  activeTabClass = ['active', '', '', '', '', '', ''];
  automaticCheck: any = false;
  goalOrPerformance: any = 'goal';
  percent = '';
  error = '';
  month: any;
  yearsDataList: any = [];
  monthsData: any;
  year: any;
  billGoalsList: any = [];
  CompanyName: any;
  imgPath = config.S3_URL;
  currentDate = new Date();
  workerList: any = [];
  workerListData: any;
  obj: any = [];
  standardGoalValue: any;
  selectedTabValue: any;
  goal: any;
  performance: any;
  constructor(private toastr: ToastrService,
    private setupbillboardgoalsService: SetupbillboardgoalsService,
    private router: Router,
    private translateService: TranslateService) {
  }
  ngOnInit() {
    this.getMonths();
    this.getYear();
    this.month = ('00' + (this.currentDate.getMonth() + 1)).slice(-2);
    this.getBillboards(this.month, this.year);
  }
  upateTabs(index) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (i === index) {
        this.tabsArray[i] = true;
        this.activeTabClass[i] = 'active';
      } else {
        this.tabsArray[i] = false;
        this.activeTabClass[i] = '';
      }
    }
  }
  getMonths() {
    this.setupbillboardgoalsService.getMonthsTypes().subscribe(
      data => {
        this.monthsData = data['workerGoalMonths'];
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getYear() {
    const years = new Date().getFullYear();
    for (let i = 0; i <= 31; i++) {
      this.yearsDataList.push(years + i);
      this.year = this.yearsDataList[0];
    }
  }
  changeMonth(value) {
    this.month = value;
    this.workerList = [];
    this.standardGoalValue = '';
    this.automaticCheck = false;
    this.percent = '';
    this.goalOrPerformance = 'goal';
    this.getBillboards(this.month, this.year);
  }
  changeYear(value) {
    this.year = value;
    this.workerList = [];
    this.standardGoalValue = '';
    this.automaticCheck = false;
    this.percent = '';
    this.goalOrPerformance = 'goal';
    this.getBillboards(this.month, this.year);
  }
  loadCompanyInfo(data) {
    this.CompanyName = data.Name;
  }
  clear() {
    this.error = '';
  }
  cancel() {
    this.router.navigate(['/setup/workers']);
  }
  getWorkerList() { // for workerlist
    this.setupbillboardgoalsService.getWorkerList().subscribe(data => {
      this.workerListData = data['result'];
      for (let i = 0; i < this.workerListData.length; i++) {
        if (this.standardGoalValue && this.selectedTabValue === 'serviceSales') {
          this.workerList[i]['serviceSales'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'rebook') {
          this.workerList[i]['rebook'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'averageTicket') {
          this.workerList[i]['averageTicket'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'retailToService') {
          this.workerList[i]['retailToService'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'productSales') {
          this.workerList[i]['productSales'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'retailPerTicket') {
          this.workerList[i]['retailPerTicket'] = this.standardGoalValue;
        } else if (this.standardGoalValue && this.selectedTabValue === 'retailPerGuest') {
          this.workerList[i]['retailPerGuest'] = this.standardGoalValue;
        } else {
          this.workerListData[i]['serviceSales'] = '';
          this.workerListData[i]['rebook'] = '';
          this.workerListData[i]['averageTicket'] = '';
          this.workerListData[i]['retailToService'] = '';
          this.workerListData[i]['productSales'] = '';
          this.workerListData[i]['retailPerTicket'] = '';
          this.workerListData[i]['retailPerGuest'] = '';
          this.workerList.push({
            'Id': this.workerListData[i]['Id'],
            'Image': this.workerListData[i]['Image'],
            'workerName': this.workerListData[i]['workerName'],
            'averageTicket': this.workerListData[i]['averageTicket'],
            'productSales': this.workerListData[i]['productSales'],
            'rebook': this.workerListData[i]['rebook'],
            'retailPerGuest': this.workerListData[i]['retailPerGuest'],
            'retailPerTicket': this.workerListData[i]['retailPerTicket'],
            'retailToService': this.workerListData[i]['retailToService'],
            'serviceSales': this.workerListData[i]['serviceSales']
          });
        }
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  saveBillboardGoalsData() {
    if (this.automaticCheck === true && this.percent === '') {
      this.error = 'Percentage is Required';
      window.scrollTo(0, 0);
    } else {
      this.obj = [];
      for (let i = 0; i < this.workerList.length; i++) {
        this.obj.push([this.workerList[i]['Id'],
        {
          'serviceSales': this.workerList[i]['serviceSales'],
          'rebook': this.workerList[i]['rebook'],
          'averageTicket': this.workerList[i]['averageTicket'],
          'retailToService': this.workerList[i]['retailToService'],
          'productSales': this.workerList[i]['productSales'],
          'retailPerTicket': this.workerList[i]['retailPerTicket'],
          'retailPerGuest': this.workerList[i]['retailPerGuest'],
        }]);
      }
      if (this.goalOrPerformance === 'goal') {
        this.goal = 1;
        this.performance = 0;
      } else {
        this.goal = 0;
        this.performance = 1;
      }
      const billBoardObj = {
        'billBoardData': this.obj,
        'month': this.month,
        'year': this.year,
        'automaticCheck': this.automaticCheck ? 1 : 0,
        'percent': +this.percent,
        'goal': this.goal,
        'performance': this.performance
      };
      this.setupbillboardgoalsService.saveBillboardGoalsData(billBoardObj).subscribe(data => {
        this.toastr.success('BillboardGoals saved successfully', null, { timeOut: 1500 });
        this.router.navigate(['/setup/workers']);
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }
  }
  getBillboards(month, year) {
    this.setupbillboardgoalsService.getBillboardGoals(month, year).subscribe(data => {
      this.billGoalsList = data['result'];
      if (this.billGoalsList.length > 0) {
        for (let i = 0; i < this.billGoalsList.length; i++) {
          this.automaticCheck = this.billGoalsList[i]['Automatically_Inc_Monthly__c'] ? true : false;
          if (this.billGoalsList[i]['Automatically_Inc_Monthly_Value__c'] === 0) {
            this.percent = '';
          } else {
            this.percent = this.billGoalsList[i]['Automatically_Inc_Monthly_Value__c'];
          }
          if (this.billGoalsList[i]['Goal__c'] === 1) {
            this.goalOrPerformance = 'goal';
          } else {
            this.goalOrPerformance = 'performance';
          }
          const temp = JSON.parse(this.billGoalsList[i]['Options__c']);
          if (this.standardGoalValue && this.selectedTabValue === 'serviceSales') {
            this.workerList[i]['serviceSales'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'rebook') {
            this.workerList[i]['rebook'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'averageTicket') {
            this.workerList[i]['averageTicket'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'retailToService') {
            this.workerList[i]['retailToService'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'productSales') {
            this.workerList[i]['productSales'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'retailPerTicket') {
            this.workerList[i]['retailPerTicket'] = this.standardGoalValue;
          } else if (this.standardGoalValue && this.selectedTabValue === 'retailPerGuest') {
            this.workerList[i]['retailPerGuest'] = this.standardGoalValue;
          } else {
            this.workerList.push({
              'Id': this.billGoalsList[i]['Worker__c'],
              'Image': this.billGoalsList[i]['Image'],
              'workerName': this.billGoalsList[i]['workerName'],
              'averageTicket': temp['averageTicket'],
              'productSales': temp['productSales'],
              'rebook': temp['rebook'],
              'retailPerGuest': temp['retailPerGuest'],
              'retailPerTicket': temp['retailPerTicket'],
              'retailToService': temp['retailToService'],
              'serviceSales': temp['serviceSales'],
            });
          }
        }
      } else {
        this.getWorkerList();
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  applyToAll(standardGoalValue, tabSelected) {
    this.selectedTabValue = tabSelected;
    this.standardGoalValue = standardGoalValue;
    if (this.standardGoalValue) {
      this.getBillboards(this.month, this.year);
    }
  }
}
