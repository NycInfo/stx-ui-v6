/*
    Setup Product Line component has the following methods.
    * ngOnInit(): This method is used to load methods on page load
    * getProductLine(): This method is used to get product lines
    * getInventoryGroups(): This method is used to get inventory groups
    * getInactiveProductLine(value): This method is used to get inactive or active inventory product lines
    * createProductLine(): This method is used to create product line
    * showData(productlinelist): This method is used to show data
    * editInventoryProductLines(): This method is used to edit inventory product line
    * addNew(): This method is used to add new inventory product line
    * deleteProductLine(): This method is used to delete product line
    * cancel(): This method is used to cancel changes
    * clear(); This method is used to clear data
    * clearmessage(): This method is used to clear error message
*/
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { SetupcompansationscalesService } from './setupcompansationscales.service';
import { SetupCompMethodService } from '../setupcompensationmethod/setupcompmethod.service';
@Component({
    selector: 'app-setupcompansationscales-popup',
    templateUrl: './setupcompansationscales.html',
    styleUrls: ['./setupcompensationscales.css'],
    providers: [SetupcompansationscalesService, SetupCompMethodService]
})
export class SetupcompansationscalesComponent implements OnInit {
    hideDelete: any;
    addDiv = true;
    disableClass = '';
    disable = true;
    editDiv = false;
    name: any;
    active: any;
    upTo: any;
    scalesObj: any;
    scalesData: any;
    basisValue: any;
    scales = [];
    staticArray: any = {};
    basisData: any;
    scalesDetails: any;
    scalesEditDetails: any;
    error: any;
    error1: any;
    error2: any;
    over: any;
    scaleId: any;
    updateName: any;
    updateActive: any;
    updateScales = [];
    updateBasis: any;
    scalesUpdateObj: any = {};
    showPlus = true;
    hidePlus: any;
    uptoValidation: any;
    lineNumber: any;
    percentValidation: any;
    toastermessage: any;
    duplcateErr: any;
    operandvalue = 0;
    methodsListing = [];
    decodedUserToken: any;
    constructor(private toastr: ToastrService,
        private setupcompansationscalesService: SetupcompansationscalesService,
        private router: Router,
        private translateService: TranslateService,
        private setupCompMethodService: SetupCompMethodService,
        @Inject('defaultColor') public defaultColor: string,
        @Inject('defaultType') public defaultType: string) {
    }
    ngOnInit() {
        try {
            this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedUserToken = {};
        }
        this.getCompansationScales();
        this.getBasisOfScales();
        this.getCompansationMethods();
        this.staticArray = { 'over': 0, 'upTo': '', 'percent': '' };
        this.scales.push(this.staticArray);
        this.updateScales.push(this.staticArray);
    }
    getCompansationScales() {
        this.setupcompansationscalesService.getscales()
            .subscribe(data => {
                this.scalesData = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    getBasisOfScales() {
        this.setupcompansationscalesService.getBasis()
            .subscribe(data => {
                this.basisData = data['basis'];
                this.basisValue = this.basisData[0].type;
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    basisDataList(value) {
        this.basisValue = value;
    }
    saveCompansationScales() {
        for (let i = 0; i < this.scales.length; i++) {
            if (this.scales.length > i + 1) {
                if (this.scales[i].upTo !== '') {
                    this.scales[i + 1].over = parseInt(this.scales[i].upTo, 10);
                } else {
                    this.scales[i + 1].over = 0;
                }
            }
        }
        // this.checkIfDuplicateScaleExists(this.scales);
        if (this.name === '' || this.name === undefined || this.name === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        }
        if (this.scales.length > 0) {
            for (let i = 0; i < this.scales.length; i++) {
                if (this.scales[i].upTo < 0 || this.scales[i].upTo > 999999.99) {
                    this.uptoValidation = this.translateService.get('VALIDATION_MSG.UPTO_LIMIT');
                    // this.lineNumber = i + 1;
                    // this.error1 = 'Line  ' + this.lineNumber + ':' + this.uptoValidation.value;
                    this.error1 = this.uptoValidation.value;
                }
            }
            for (let i = 0; i < this.scales.length; i++) {
                if (this.scales[i].percent < 0 || this.scales[i].percent > 100) {
                    this.percentValidation = this.translateService.get('VALIDATION_MSG.PERCENT_LIMIT');
                    // this.lineNumber = i + 1;
                    this.error2 = this.percentValidation.value;
                    // this.error2 = 'Line  ' + this.lineNumber + ':' + this.percentValidation.value;
                }
            }
        }
        if (this.error1 === '' && this.error2 === '' && this.error === '' && this.duplcateErr === '') {
            if (this.active === undefined || this.active === false) {
                this.active = 0;
            } else if (this.active === true) {
                this.active = 1;
            }
            this.scalesObj = {
                'name': this.name,
                'active': this.active,
                'scales': this.scales,
                'basisValue': this.basisValue
            };
            this.setupcompansationscalesService.createScales(this.scalesObj)
                .subscribe(
                    data => {
                        this.scalesDetails = data['data'];
                        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                        this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                        this.disableClass = '';
                        this.disable = true;
                        this.addDiv = true;
                        this.editDiv = false;
                        this.clear();
                        this.getCompansationScales();
                    },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;

                        switch (JSON.parse(error['_body']).status) {
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2043':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    showData(scalesdata) {
        this.disableClass = 'disabled';
        this.addDiv = false;
        this.disable = false;
        this.editDiv = true;
        this.scaleId = scalesdata.Id;
        this.updateName = scalesdata.Name;
        this.updateActive = scalesdata.Active__c;
        this.updateScales = JSON.parse(scalesdata.Scale__c);
        this.updateBasis = scalesdata.Basis__c;
        const test = this.updateScales;
        this.hidePlus = this.updateScales.length;
        if (this.hidePlus > 1) {
            this.hideDelete = true;
        }
    }
    editCompansationScales() {
        for (let i = 0; i < this.updateScales.length; i++) {
            if (this.updateScales.length > i + 1) {
                if (this.updateScales[i].upTo !== '') {
                    this.updateScales[i + 1].over = parseInt(this.updateScales[i].upTo, 10);
                } else {
                    this.updateScales[i + 1].over = 0;
                }
            }
        }
        this.error1 = '';
        this.error2 = '';
        // this.checkIfDuplcateUpdateScaleExists(this.updateScales);
        if (this.updateName === '' || this.updateName === undefined || this.updateName === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        } else {
            if (this.updateScales.length > 0) {
                for (let i = 0; i < this.updateScales.length; i++) {
                    if (this.updateScales[i].upTo < 0 || this.updateScales[i].upTo > 999999.99) {
                        this.uptoValidation = this.translateService.get('VALIDATION_MSG.UPTO_LIMIT');
                        // this.lineNumber = i + 1;
                        // this.error1 = 'Line  ' + this.lineNumber + ':' + this.uptoValidation.value;
                        this.error1 = this.uptoValidation.value;
                    }
                }
                for (let i = 0; i < this.updateScales.length; i++) {
                    if (this.updateScales[i].percent < 0 || this.updateScales[i].percent > 100) {
                        this.percentValidation = this.translateService.get('VALIDATION_MSG.PERCENT_LIMIT');
                        // this.lineNumber = i + 1;
                        this.error2 = this.percentValidation.value;
                        // this.error2 = 'Line  ' + this.lineNumber + ':' + this.percentValidation.value;
                    }
                }
            }
            if (this.error1 === '' && this.error2 === '') {
                if (this.updateActive === undefined || this.updateActive === false) {
                    this.updateActive = 0;
                } else if (this.updateActive === true) {
                    this.updateActive = 1;
                }
                this.scalesUpdateObj = {
                    'updateName': this.updateName,
                    'updateActive': this.updateActive,
                    'updateScales': this.updateScales,
                    'updateBasis': this.updateBasis,
                    'updateId': this.scaleId
                };
                this.comMethodCal(this.scalesUpdateObj);
                this.setupcompansationscalesService.editScales(this.scalesUpdateObj, this.scaleId)
                    .subscribe(
                        data => {
                            this.scalesEditDetails = data['data'];
                            this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
                            this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                            this.disableClass = '';
                            this.disable = true;
                            this.addDiv = true;
                            this.editDiv = false;
                            this.clear();
                            this.getCompansationScales();
                        },
                        error => {
                            const status = JSON.parse(error['status']);
                            const statuscode = JSON.parse(error['_body']).status;
                            switch (JSON.parse(error['_body']).status) {
                                case '2033':
                                    this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                    break;
                                case '2044':
                                    this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                    break;
                            }
                            if (statuscode === '2085' || statuscode === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
            }
        }
    }
    deleteFieldValue(scales, index) {
        this.scales.splice(index, 1);
        this.hidePlus = this.scales.length;
        if (this.hidePlus <= 20) {
            this.showPlus = true;
        }
        if (this.hidePlus === 1) {
            this.hideDelete = false;
        }
        this.duplcateErr = '';
    }
    addNew(upto, ind) {
        if (this.scales[ind].upTo !== '') {
            this.scales = this.scales.slice(0, ind + 1).concat({ 'over': upto, 'upTo': '', 'percent': '' }).concat(this.scales.slice(ind + 1));
        } else {
            this.scales = this.scales.slice(0, ind + 1).concat({ 'over': 0, 'upTo': '', 'percent': '' }).concat(this.scales.slice(ind + 1));
        }
        if (this.scales[ind].upTo === '') {
            this.scales[ind]['upTo'] = 0;
        }
        if (this.scales[ind].percent === '') {
            this.scales[ind]['percent'] = 0;
        }
        this.hidePlus = this.scales.length;
        if (this.hidePlus === 20) {
            this.showPlus = false;
        }
        if (this.hidePlus > 1) {
            this.hideDelete = true;
        }
    }
    editAddNew(upto, ind) {
        const test = this.updateScales;
        this.hidePlus = this.updateScales.length;
        if (this.hidePlus === 20) {
            this.showPlus = false;
        }
        if (this.hidePlus > 1) {
            this.hideDelete = true;
        }
        if (this.updateScales[ind].upTo !== '') {
            this.updateScales = this.updateScales.slice(0, ind + 1).concat({ 'over': upto, 'upTo': '', 'percent': '' }).concat(this.updateScales.slice(ind + 1));
        } else {
            this.updateScales = this.updateScales.slice(0, ind + 1).concat({ 'over': 0, 'upTo': '', 'percent': '' }).concat(this.updateScales.slice(ind + 1));
        }
        if (this.updateScales[ind].upTo === '') {
            this.updateScales[ind]['upTo'] = 0;
        }
        if (this.updateScales[ind].percent === '') {
            this.updateScales[ind]['percent'] = 0;
        }
        // for (let i = 0; i < this.updateScales.length; i++) {
        //     if (this.updateScales.length > i + 1) {
        //         if (this.updateScales[i].upTo !== '') {
        //             this.updateScales[i + 1].over = parseInt(this.updateScales[i].upTo, 10);
        //         } else {
        //             this.updateScales[i + 1].over = 0;
        //         }
        //     }
        // }
    }
    editDeleteFieldValue(scales, index) {
        this.updateScales.splice(index, 1);
        this.hidePlus = this.updateScales.length;
        if (this.hidePlus <= 20) {
            this.showPlus = true;
        }
        if (this.hidePlus === 1) {
            this.hideDelete = false;
        }
        this.duplcateErr = '';
    }
    createNewRecord() {
        this.scales = [];
        this.scales.push(this.staticArray);
        this.addDiv = true;
        this.disable = false;
        this.disableClass = 'disabled';
        this.editDiv = false;
        this.active = true;
        this.getBasisOfScales();
    }
    cancel() {
        this.addDiv = true;
        this.disableClass = '';
        this.disable = true;
        this.editDiv = false;
        this.error = '';
        this.scales = [];
        this.duplcateErr = '';
        this.clear();
    }
    clear() {
        this.name = '',
            this.active = '',
            this.scales = [],
            this.staticArray = { 'over': 0, 'upTo': '', 'percent': '' };
        this.scales.push(this.staticArray);
        this.hideDelete = false;
        this.basisValue = '',
            this.over = '';
        this.error = '';
        this.duplcateErr = '';

    }
    clearErrMsg() {
        this.error = '';
        this.error1 = '';
        this.error2 = '';
        this.duplcateErr = '';
        this.uptoValidation = '';
    }
    clearErrMsg1() {
        this.error = '';
    }
    getCompansationMethods() {
        this.setupCompMethodService.getMethods()
            .subscribe(data => {
                this.methodsListing = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    comMethodCal(res) {
        const DAYS_WORKED = 5;
        const HOURLY_WAGE = 10;
        const HOURS_WORKED = 40;
        const SALARY = 500;
        const TICKETS = 20;
        const CLASS_SALES = 250;
        const CLASSES_PERFORMED = 5;
        const SERVICES_PERFORMED = 30;
        const PRODUCTS_SOLD = 20;
        const GROSS_SERVICE = 1000;
        const GROSS_RETAIL = 200;
        const COST_OF_SERVICE_FEE = 17.50;
        const currentScale = [];
        for (let i = 0; i < this.methodsListing.length; i++) {  // Method loop
            if (typeof this.methodsListing[i].Steps__c === 'string') {
                this.methodsListing[i].Steps__c = JSON.parse(this.methodsListing[i].Steps__c);
            }
            // Steps_c loop
            for (let j = 0; j < this.methodsListing[i].Steps__c.length; j++) {
                if (this.methodsListing[i].Steps__c[j].operand.split(':')[1] === res.updateId) { // this for loop check for (operand id )
                    this.methodsListing[i].Steps__c[j].Basis__c = res.updateBasis;

                    for (let t = 0; t < this.methodsListing[i].Steps__c.length; t++) {
                        if (this.methodsListing[i].Steps__c[t].operand === 'Salary') {
                            this.operandvalue = SALARY;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Hourly Wage') {
                            this.operandvalue = HOURLY_WAGE;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Hours Worked') {
                            this.operandvalue = HOURS_WORKED;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Days Worked') {
                            this.operandvalue = DAYS_WORKED;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Services Performed') {
                            this.operandvalue = SERVICES_PERFORMED;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Products Sold') {
                            this.operandvalue = PRODUCTS_SOLD;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Classes Performed') {
                            this.operandvalue = CLASSES_PERFORMED;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Tickets') {
                            this.operandvalue = TICKETS;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Gross Service') {
                            this.operandvalue = GROSS_SERVICE;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Gross Retail') {
                            this.operandvalue = GROSS_RETAIL;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Class Sales') {
                            this.operandvalue = CLASS_SALES;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Cost of Service Fee') {
                            this.operandvalue = COST_OF_SERVICE_FEE;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Number') {
                            this.operandvalue = parseInt(this.methodsListing[i].Steps__c[t].numeral, 10);
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Percent') {
                            this.operandvalue = this.methodsListing[i].Steps__c[t].numeral / 100;
                        } else if (this.methodsListing[i].Steps__c[t].operand === 'Result of Step') {
                            const stepVal = parseInt(this.methodsListing[i].Steps__c[t].numeral, 10);
                            this.operandvalue = this.methodsListing[i].Steps__c[stepVal - 1].result;
                        } else {
                            this.operandvalue = 0;
                        }
                        switch (this.methodsListing[i].Steps__c[t].operator) {
                            case ('Start With'):
                                this.methodsListing[i].Steps__c[t]['result'] = this.methodsListing[i].Steps__c[t]['result'];
                                break;
                            case ('Add'):
                                this.methodsListing[i].Steps__c[t]['result'] = Number(this.methodsListing[i].Steps__c[t - 1].result) + Number(this.operandvalue);
                                break;
                            case ('Subtract'):
                                this.methodsListing[i].Steps__c[t]['result'] = Number(this.methodsListing[i].Steps__c[t - 1].result) - Number(this.operandvalue);
                                break;
                            case ('Multiply By'):
                                if (this.methodsListing[i].Steps__c[t].Basis__c === 'Zero') {
                                    res.updateScales.forEach(element => {
                                        if (Number(this.methodsListing[i].Steps__c[t - 1].result) <= Number(element.upTo)) {
                                            if (Number(this.methodsListing[i].Steps__c[t - 1].result) > Number(element.over)) {
                                                this.methodsListing[i].Steps__c[t].numeral = element.percent;
                                            }
                                        }
                                    });
                                    this.methodsListing[i].Steps__c[t]['result'] = Number(this.methodsListing[i].Steps__c[t - 1].result) *
                                        Number(this.methodsListing[i].Steps__c[t].numeral) / 100;
                                } else if (this.methodsListing[i].Steps__c[t].Basis__c === 'Step') {
                                    let calScaleStep = 0;
                                    res.updateScales.forEach(obj => {
                                        if (Number(this.methodsListing[i].Steps__c[t - 1].result) >= Number(obj.upTo)) {
                                            calScaleStep += (obj.upTo - obj.over) * obj.percent / 100;
                                        } else if (Number(this.methodsListing[i].Steps__c[t - 1].result) <= Number(obj.upTo)) {
                                            if (Number(this.methodsListing[i].Steps__c[t - 1].result) > Number(obj.over)) {
                                                this.methodsListing[i].Steps__c[t]['numeral'] = obj.percent;
                                                calScaleStep += (Number(this.methodsListing[i].Steps__c[t - 1].result) - obj.over) * obj.percent / 100;
                                            }
                                        }
                                    });
                                    this.methodsListing[i].Steps__c[t]['result'] = calScaleStep;
                                } else {
                                    this.methodsListing[i].Steps__c[t]['result'] = Number(this.methodsListing[i].Steps__c[t - 1].result) *
                                        Number(this.operandvalue);
                                }
                                break;
                            case ('Divide By'):
                                this.methodsListing[i].Steps__c[t]['result'] = Number(this.methodsListing[i].Steps__c[t - 1].result) / Number(this.operandvalue);
                                break;
                            case ('If Less Than'):
                                if (this.methodsListing[i].Steps__c[t].numeral < this.methodsListing[i].Steps__c[t - 1].result) {
                                    this.methodsListing[i].Steps__c[t]['result'] = this.operandvalue;
                                } else {
                                    this.methodsListing[i].Steps__c[t]['result'] = this.methodsListing[i].Steps__c[t - 1].result;
                                }
                                break;
                            case ('If More Than'):
                                if (this.methodsListing[i].Steps__c[t].numeral > this.methodsListing[i].Steps__c[t - 1].result) {
                                    this.methodsListing[i].Steps__c[t]['result'] = this.operandvalue;
                                } else {
                                    this.methodsListing[i].Steps__c[t]['result'] = this.methodsListing[i].Steps__c[t - 1].result;
                                }
                                break;
                            default:
                                this.methodsListing[i].Steps__c[t]['result'] = 0.00;
                        }
                    }
                    currentScale.push(this.methodsListing[i]);
                }
            }
        }
        if (currentScale.length > 0) {
            this.setupCompMethodService.editMethods('scale', currentScale).subscribe(data => {
                const ddsas = data['result'];
            },
                error => {
                    const status = JSON.parse(error['status']);
                    const statuscode = JSON.parse(error['_body']).status;
                    switch (JSON.parse(error['_body']).status) {
                        case '2033':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            break;
                        case '2043':
                            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                            break;
                    }
                    if (statuscode === '2085' || statuscode === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
        }
    }
    openUpgradeOption() {
        this.toastermessage = this.translateService.get('Please upgrade your package to perform this action');
        this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
    }

    /* method to restrict specialcharecters  */
    alphaNumOnly(e) {
        const specialKeys = new Array();
        specialKeys.push(8); // Backspace
        specialKeys.push(9); // Tab
        specialKeys.push(46); // Delete
        specialKeys.push(36); // Home
        specialKeys.push(35); // End
        specialKeys.push(37); // Left
        specialKeys.push(39); // Right
        const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
            (keyCode >= 35 && keyCode <= 38) || (keyCode >= 40 && keyCode <= 47) ||
            (keyCode >= 63 && keyCode <= 64) || (keyCode >= 93 && keyCode <= 96) ||
            (keyCode >= 125 && keyCode <= 126) || (keyCode >= 58 && keyCode <= 59) ||
            (keyCode >= 32 && keyCode < 34) || (keyCode > 60 && keyCode < 62) || (keyCode > 90 && keyCode < 92) ||
            (keyCode > 122 && keyCode < 124) || (keyCode > 123 && keyCode < 125) ||
            (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        return ret;
    }
}
