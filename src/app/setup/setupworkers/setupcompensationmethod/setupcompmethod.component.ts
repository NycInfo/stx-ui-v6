import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { SetupCompMethodService } from './setupcompmethod.service';
import { JwtHelper } from 'angular2-jwt';
@Component({
    selector: 'app-setupcompmethod-popup',
    templateUrl: './setupcompmethod.html',
    styleUrls: ['./setupcompmethod.css'],
    providers: [SetupCompMethodService]
})
export class SetupCompMethodComponent implements OnInit {
    enableDisable = true;
    addDiv = true;
    disableClass = '';
    editDiv = false;
    name: any;
    active: any;
    action: any;
    error: any;
    operator: any;
    operand: any;
    value: any;
    methodObj: any = {};
    updateMethodObj: any = {};
    methodsData: any;
    methodsListing: any;
    rows = [];
    actionsStaticData: any;
    valuesStaticData: any;
    operandvalue: any = 0;
    rowsLength: any;
    updateName: any;
    updateActive: any;
    updateId: any;
    rowLength: any;
    showPlus = true;
    hidePlus: any;
    hideDelete: any;
    servicesList: any = [];
    inventoryGroups: any = [];
    productLin: any = [];
    optionalDiv: any;
    scalesData: any;
    toFix: any;
    toastermessage: any;
    StaticData: any;
    calScaleStep = 0;
    StaticData1: any;
    decodedUserToken: any;
    constructor(private toastr: ToastrService,
        private translateService: TranslateService,
        private setupCompMethodService: SetupCompMethodService,
        private router: Router,
        @Inject('defaultActive') public defaultActive: string,
        @Inject('defaultInActive') public defaultInActive: string) {
    }
    ngOnInit() {
        try {
            this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedUserToken = {};
        }
        this.getCompansationMethods();
        this.getStaticValues();
        this.getStaticActions();
        this.getCompansationScales();
        this.getServiceGroupsStatic();
        this.getStaticDataofProductlineAndGrossretail();
        this.rowsLength = false;
        this.active = true;
        this.optionalDiv = -1;
    }
    getCompansationMethods() {
        this.setupCompMethodService.getMethods()
            .subscribe(data => {
                this.methodsListing = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    getStaticValues() {
        this.setupCompMethodService.getValues()
            .subscribe(data => {
                this.valuesStaticData = data['compensationmethods'];
                this.value = this.valuesStaticData[0].value;
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    getStaticActions() {
        this.setupCompMethodService.getActions()
            .subscribe(data => {
                this.actionsStaticData = data['actionmethods'];
                this.action = this.actionsStaticData[0].action;
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    saveCompensationMethods() {
        if (this.name === '' || this.name === undefined || this.name === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        } else {
            if (this.active === undefined || this.active === false) {
                this.active = this.defaultInActive;
            }
            if (this.active === true) {
                this.active = this.defaultActive;
            }
            this.updateSampleCalculation();
            this.methodObj = {
                'name': this.name,
                'isScale': 0,
                'active': this.active,
                'methodsJson': this.rows
            };
            this.setupCompMethodService.saveMethods(this.methodObj)
                .subscribe(data => {
                    this.methodsData = data['result'];
                    this.getCompansationMethods();
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.addDiv = true;
                    this.disableClass = '';
                    this.enableDisable = true;
                    this.editDiv = false;
                    this.rows = [];
                    this.name = '';
                },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;

                        switch (JSON.parse(error['_body']).status) {
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2043':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    updateSampleCalculation() {
        this.toFix = '.00';
        const DAYS_WORKED = 5;
        const HOURLY_WAGE = 10;
        const HOURS_WORKED = 40;
        const SALARY = 500;
        const TICKETS = 20;
        const CLASS_SALES = 250;
        const CLASSES_PERFORMED = 5;
        const SERVICES_PERFORMED = 30;
        const SERVICES_PERFORMED1 = 5;
        const PRODUCTS_SOLD = 20;
        const PRODUCTS_SOLD1 = 4;
        const GROSS_SERVICE = 1000;
        const GROSS_SERVICE1 = 100;
        const GNEW_CLIENTS = 200;
        const GRECURRING_CLIENTS = 800;
        const GBOOKED_ONLINE = 400;
        const GBOOKED_IN_HOUSE = 600;
        const GSTANDING_APPOINTMENTS = 100;
        const GSERVICES_REBOOKED = 100;
        const SNEW_CLIENTS = 10;
        const SRECURRING_CLIENTS = 20;
        const SBOOKED_ONLINE = 10;
        const SBOOKED_IN_HOUSE = 20;
        const SSTANDING_APPOINTMENTS = 5;
        const SSERVICES_REBOOKED = 5;
        const GROSS_RETAIL = 200;
        const GROSS_RETAIL1 = 20;
        const COST_OF_SERVICE_FEE = 17.50;
        for (let i = 0; i < this.rows.length; i++) {
            if (this.rows[i].operand.split(':')[0] === 'scale') {
                let Scalec = [];
                this.scalesData.forEach(element => {
                    if (element.Id === this.rows[i].operand.split(':')[1]) {
                        this.rows[i]['Basis__c'] = element.Basis__c;
                        Scalec = JSON.parse(element.Scale__c);
                        if (element.Basis__c === 'Zero') {
                            Scalec.forEach(obj => {
                                if (Number(this.rows[i - 1].result) <= Number(obj.upTo)) {
                                    if (Number(this.rows[i - 1].result) > Number(obj.over)) {
                                        this.rows[i]['numeral'] = obj.percent;
                                    }
                                }
                            });
                        } else if (element.Basis__c === 'Step') {
                            this.calScaleStep = 0;
                            Scalec.forEach(obj => {
                                if (Number(this.rows[i - 1].result) >= Number(obj.upTo)) {
                                    this.calScaleStep += (obj.upTo - obj.over) * obj.percent / 100;
                                } else if (Number(this.rows[i - 1].result) <= Number(obj.upTo)) {
                                    if (Number(this.rows[i - 1].result) > Number(obj.over)) {
                                        this.rows[i]['numeral'] = obj.percent;
                                        this.calScaleStep += (Number(this.rows[i - 1].result) - obj.over) * obj.percent / 100;
                                    }
                                }
                            });
                        }
                    }
                });
            }
            if (this.rows[i].operand === 'Salary') {
                this.operandvalue = SALARY;
            } else if (this.rows[i].operand === 'Hourly Wage') {
                this.operandvalue = HOURLY_WAGE;
            } else if (this.rows[i].operand === 'Hours Worked') {
                this.operandvalue = HOURS_WORKED;
            } else if (this.rows[i].operand === 'Days Worked') {
                this.operandvalue = DAYS_WORKED;
            } else if (this.rows[i].operand === 'Services Performed' && (this.rows[i]['operandSubOption'] === 'All')) {
                this.operandvalue = SERVICES_PERFORMED;
            } else if (this.rows[i].operand === 'Services Performed' && (this.servicesList[i].length > 0)
                && (this.rows[i]['operandSubOption'] !== 'New Clients') && (this.rows[i]['operandSubOption'] !== 'Recurring Clients')
                && (this.rows[i]['operandSubOption'] !== 'Booked Online') && (this.rows[i]['operandSubOption'] !== 'Booked in-house')
                && (this.rows[i]['operandSubOption'] !== 'Service Rebooked') && (this.rows[i]['operandSubOption'] !== 'Standing Appointments')) {
                this.operandvalue = SERVICES_PERFORMED1;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'New Clients')) {
                this.operandvalue = SNEW_CLIENTS;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'Recurring Clients')) {
                this.operandvalue = SRECURRING_CLIENTS;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'Booked Online')) {
                this.operandvalue = SBOOKED_ONLINE;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'Booked in-house')) {
                this.operandvalue = SBOOKED_IN_HOUSE;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'Service Rebooked')) {
                this.operandvalue = SSERVICES_REBOOKED;
            } else if (this.rows[i].operand === 'Services Performed' &&
                (this.rows[i]['operandSubOption'] === 'Standing Appointments')) {
                this.operandvalue = SSTANDING_APPOINTMENTS;
            } else if (this.rows[i].operand === 'Products Sold' && (this.rows[i]['operandSubOption'] === 'All')) {
                this.operandvalue = PRODUCTS_SOLD;
            } else if (this.rows[i].operand === 'Products Sold' && (this.servicesList[i].length > 0)) {
                this.operandvalue = PRODUCTS_SOLD1;
            } else if (this.rows[i].operand === 'Classes Performed') {
                this.operandvalue = CLASSES_PERFORMED;
            } else if (this.rows[i].operand === 'Tickets') {
                this.operandvalue = TICKETS;
            } else if (this.rows[i].operand === 'Gross Service' && (this.rows[i]['operandSubOption'] === 'All')) {
                this.operandvalue = GROSS_SERVICE;
            } else if (this.rows[i].operand === 'Gross Service' && (this.servicesList[i].length > 0)
                && (this.rows[i]['operandSubOption'] !== 'New Clients') && (this.rows[i]['operandSubOption'] !== 'Recurring Clients')
                && (this.rows[i]['operandSubOption'] !== 'Booked Online') && (this.rows[i]['operandSubOption'] !== 'Booked in-house')
                && (this.rows[i]['operandSubOption'] !== 'Service Rebooked') && (this.rows[i]['operandSubOption'] !== 'Standing Appointments')) {
                this.operandvalue = GROSS_SERVICE1;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'New Clients')) {
                this.operandvalue = GNEW_CLIENTS;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'Recurring Clients')) {
                this.operandvalue = GRECURRING_CLIENTS;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'Booked Online')) {
                this.operandvalue = GBOOKED_ONLINE;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'Booked in-house')) {
                this.operandvalue = GBOOKED_IN_HOUSE;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'Service Rebooked')) {
                this.operandvalue = GSERVICES_REBOOKED;
            } else if (this.rows[i].operand === 'Gross Service' &&
                (this.rows[i]['operandSubOption'] === 'Standing Appointments')) {
                this.operandvalue = GSTANDING_APPOINTMENTS;
            } else if (this.rows[i].operand === 'Gross Retail' && (this.rows[i]['operandSubOption'] === 'All')) {
                this.operandvalue = GROSS_RETAIL;
            } else if (this.rows[i].operand === 'Gross Retail' && (this.servicesList[i].length > 0)) {
                this.operandvalue = GROSS_RETAIL1;
            } else if (this.rows[i].operand === 'Class Sales') {
                this.operandvalue = CLASS_SALES;
            } else if (this.rows[i].operand === 'Cost of Service Fee') {
                this.operandvalue = COST_OF_SERVICE_FEE;
            } else if (this.rows[i].operand === 'Number') {
                this.operandvalue = parseFloat(this.rows[i].numeral);
            } else if (this.rows[i].operand === 'Percent') {
                this.operandvalue = this.rows[i].numeral / 100;
            } else if (this.rows[i].operand === 'Result of Step') {
                const stepVal = parseFloat(this.rows[i].numeral);
                this.operandvalue = this.rows[stepVal - 1].result;
            } else {
                this.operandvalue = 0;
            }
            switch (this.rows[i].operator) {
                case ('Start With'):
                    if (this.rows[i].operand === 'Percent') {
                        this.rows[i]['result'] = this.operandvalue;
                    } else {
                        this.rows[i]['result'] = this.operandvalue;
                    }
                    break;
                case ('Add'):
                    this.rows[i]['result'] = (parseFloat(this.rows[i - 1].result) + this.operandvalue);
                    break;
                case ('Subtract'):
                    this.rows[i]['result'] = (parseFloat(this.rows[i - 1].result) - this.operandvalue);
                    break;
                case ('Multiply By'):
                    this.rows[i]['result'] = (parseFloat(this.rows[i - 1].result) * this.operandvalue);
                    if (this.rows[i].operand.split(':')[0] === 'scale' && this.rows[i].operator === 'Multiply By'
                        && this.rows[i].Basis__c === 'Zero') {
                        this.rows[i]['result'] = Number(this.rows[i - 1].result) * Number(this.rows[i].numeral) / 100;
                    } else if (this.rows[i].operand.split(':')[0] === 'scale' && this.rows[i].operator === 'Multiply By'
                        && this.rows[i].Basis__c === 'Step') {
                        this.rows[i]['result'] = this.calScaleStep;
                    }
                    break;
                case ('Divide By'):
                    this.rows[i]['result'] = (parseFloat(this.rows[i - 1].result) / this.operandvalue);
                    if (this.rows[i].operand.split(':')[0] === 'scale' && this.rows[i].operator === 'Divide By') {
                        this.rows[i]['result'] = 0;
                    }
                    break;
                case ('If Less Than'):
                    if (this.operandvalue < this.rows[i - 1].result) {
                        this.rows[i]['result'] = parseFloat(this.operandvalue);
                    } else {
                        this.rows[i]['result'] = this.rows[i - 1].result;
                    }
                    break;
                case ('If More Than'):
                    if (this.operandvalue > this.rows[i - 1].result) {
                        this.rows[i]['result'] = parseFloat(this.operandvalue);
                    } else {
                        this.rows[i]['result'] = this.rows[i - 1].result;
                    }
                    break;
                default:
                    this.rows[i]['result'] = 0.00;
            }
        }
    }
    showData(methods) {
        this.toFix = '.00';
        this.rows = [];
        this.editDiv = true;
        this.addDiv = false;
        this.enableDisable = false;
        this.disableClass = 'disabled';
        this.updateId = methods.Id;
        this.updateName = methods.Name;
        this.updateActive = methods.Active__c;
        this.rows = JSON.parse(methods.Steps__c);
        for (let j = 0; j < this.rows.length; j++) {
            if (this.rows[j].operand === 'Services Performed') {
                this.setupCompMethodService.getServices()
                    .subscribe(data => {
                        this.servicesList[j] = data['result']
                            .filter(filterList => !filterList.isSystem);
                        // this.rows[j]['operandSubOption'] = this.servicesList[j][0].serviceGroupName;
                    },
                        error => {
                            const errStatus = JSON.parse(error['_body'])['status'];
                            if (errStatus === '2085' || errStatus === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
            } else if (this.rows[j].operand === 'Gross Retail' || this.rows[j].operand === 'Products Sold') {
                const invGrpData = [];
                this.setupCompMethodService.getProductLineDetails()
                    .subscribe(data => {
                        this.setupCompMethodService.getInventoryGroupData().subscribe(data1 => {
                            for (let k = 0; k < data1['result'].length; k++) {
                                invGrpData.push({ 'Name': data1['result'][k].inventoryGroupName });
                            }
                            this.inventoryGroups = invGrpData;
                            this.productLin = data['result'];
                            this.servicesList[j] = data['result'].concat(invGrpData);
                            // this.rows[j]['operandSubOption'] = this.servicesList[j][0].Name;
                        }, error => {
                            const errStatus = JSON.parse(error['_body'])['status'];
                            if (errStatus === '2085' || errStatus === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
                    },
                        error => {
                            const errStatus = JSON.parse(error['_body'])['status'];
                            if (errStatus === '2085' || errStatus === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
            } else if (this.rows[j].operand === 'Gross Service') {
                this.setupCompMethodService.getServices()
                    .subscribe(data => {
                        this.servicesList[j] = data['result']
                            .filter(filterList => !filterList.isSystem);
                        // this.rows[j]['operandSubOption'] = this.servicesList[j][0].serviceGroupName;
                    },
                        error => {
                            const errStatus = JSON.parse(error['_body'])['status'];
                            if (errStatus === '2085' || errStatus === '2071') {
                                if (this.router.url !== '/') {
                                    localStorage.setItem('page', this.router.url);
                                    this.router.navigate(['/']).then(() => { });
                                }
                            }
                        });
            }

        }
        if (this.rows.length > 1) {
            this.hideDelete = true;
        }
    }
    editCompensationMethods() {
        if (this.updateName === '' || this.updateName === undefined || this.updateName === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        } else {
            if (this.updateActive === undefined || this.updateActive === false) {
                this.updateActive = this.defaultInActive;
            }
            if (this.updateActive === true) {
                this.updateActive = this.defaultActive;
            }
            this.updateSampleCalculation();
            this.updateMethodObj = {
                'name': this.updateName,
                'isScale': 0,
                'active': this.updateActive,
                'methodsJson': this.rows,
                'page': 'method'
            };

            this.setupCompMethodService.editMethods(this.updateId, this.updateMethodObj)
                .subscribe(data => {
                    this.methodsData = data['result'];
                    this.getCompansationMethods();
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.addDiv = true;
                    this.enableDisable = true;
                    this.disableClass = '';
                    this.editDiv = false;
                    this.rows = [];
                },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;
                        switch (JSON.parse(error['_body']).status) {
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2043':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    getCompansationScales() {
        this.setupCompMethodService.getscales()
            .subscribe(data => {
                this.scalesData = data['result'];
                this.updateSampleCalculation();
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    change(value, i) {
        this.rows[i]['operandSubOption'] = value;
    }
    onValueChange(value, row, i) {
        if (value === 'Services Performed') {
            this.rowLength = i;
            this.setupCompMethodService.getServices()
                .subscribe(data => {
                    this.servicesList[i] = data['result']
                        .filter(filterList => !filterList.isSystem);
                    this.rows[i]['operandSubOption'] = 'All';
                },
                    error => {
                        const errStatus = JSON.parse(error['_body'])['status'];
                        if (errStatus === '2085' || errStatus === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        } else if (value === 'Gross Service') {
            this.rowLength = i;
            this.setupCompMethodService.getServices()
                .subscribe(data => {
                    this.servicesList[i] = data['result']
                        .filter(filterList => !filterList.isSystem);
                    this.rows[i]['operandSubOption'] = 'All';
                },
                    error => {
                        const errStatus = JSON.parse(error['_body'])['status'];
                        if (errStatus === '2085' || errStatus === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        } else if (value === 'Gross Retail') {
            this.rowLength = i;
            const invGrpData = [];
            this.setupCompMethodService.getProductLineDetails()
                .subscribe(data => {
                    this.setupCompMethodService.getInventoryGroupData().subscribe(data1 => {
                        for (let k = 0; k < data1['result'].length; k++) {
                            invGrpData.push({ 'Name': data1['result'][k].inventoryGroupName });
                        }
                        this.servicesList[i] = data['result'].concat(invGrpData);
                        this.rows[i]['operandSubOption'] = 'All';
                    }, error => {
                        const errStatus = JSON.parse(error['_body'])['status'];
                        if (errStatus === '2085' || errStatus === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
                },
                    error => {
                        const errStatus = JSON.parse(error['_body'])['status'];
                        if (errStatus === '2085' || errStatus === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        } else if (value === 'Products Sold') {
            this.rowLength = i;
            const invGrpData = [];
            this.setupCompMethodService.getProductLineDetails()
                .subscribe(data => {
                    this.servicesList[i] = data['result']
                        .filter(filterList => !filterList.isSystem);
                    this.rows[i]['operandSubOption'] = 'All';
                },
                    error => {
                        const errStatus = JSON.parse(error['_body'])['status'];
                        if (errStatus === '2085' || errStatus === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });

        } else {
            this.optionalDiv = -1;
            this.servicesList[i] = [];
        }
    }
    addRows(index) {
        this.optionalDiv = -1;
        this.rows.splice(index, 0, {
            'step': 0,
            'operator': this.action,
            'operandSubOption': '',
            'result': '',
            'operand': this.value,
            'numeral': '', 'description': ''
        });
        for (let i = 0; i < this.rows.length; i++) {
            this.rows[i].step = i + 1;
        }
        this.hidePlus = this.rows.length;
        if (this.hidePlus === 40) {
            this.showPlus = false;
        }
        if (this.hidePlus > 1) {
            this.hideDelete = true;
        }
        this.servicesList.splice(index, 0, []);
    }
    deleteFieldValue(rows, index) {
        this.rows.splice(index, 1);
        for (let i = 0; i < this.rows.length; i++) {
            this.rows[i].step = i + 1;
        }
        this.hidePlus = this.rows.length;
        if (this.hidePlus <= 40) {
            this.showPlus = true;
        }
        if (this.hidePlus === 1) {
            this.hideDelete = false;
        }
        this.servicesList.splice(index, 1);
        if (index === 0) {
            this.rows[0].operator = 'Start With';
        }
    }
    addNewRecord() {
        this.addDiv = true;
        this.enableDisable = false;
        this.disableClass = 'disabled';
        this.editDiv = false;
        this.addRows(0);
    }
    cancel() {
        this.addDiv = true;
        this.enableDisable = true;
        this.disableClass = '';
        this.editDiv = false;
        this.error = '';
        this.rows = [];
        this.hideDelete = false;
        this.servicesList = [];
        this.clear();
    }
    clear() {
        this.name = '';
    }
    clearErrMsg() {
        this.error = '';
    }
    /* method to restrict specialcharecters  */
    numOnly(event: any) {
        const pattern = /[0-9.]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    alphaNumOnly(e) {
        const specialKeys = new Array();
        specialKeys.push(8); // Backspace
        specialKeys.push(9); // Tab
        specialKeys.push(46); // Delete
        specialKeys.push(36); // Home
        specialKeys.push(35); // End
        specialKeys.push(37); // Left
        specialKeys.push(39); // Right
        const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
            (keyCode >= 35 && keyCode <= 38) || (keyCode >= 40 && keyCode <= 47) ||
            (keyCode >= 63 && keyCode <= 64) || (keyCode >= 93 && keyCode <= 96) ||
            (keyCode >= 125 && keyCode <= 126) || (keyCode >= 58 && keyCode <= 59) ||
            (keyCode >= 32 && keyCode < 34) || (keyCode > 60 && keyCode < 62) || (keyCode > 90 && keyCode < 92) ||
            (keyCode > 122 && keyCode < 124) || (keyCode > 123 && keyCode < 125) ||
            (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        return ret;
    }

    getServiceGroupsStatic() {
        this.setupCompMethodService.getStaticServiceGroups().subscribe(
            data => {
                this.StaticData = data['STATIC DATA OF SERVICE PERFORMED AND GROSSSERVICE'];
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    getStaticDataofProductlineAndGrossretail() {
        this.setupCompMethodService.getStaticServiceGroups().subscribe(
            data => {
                this.StaticData1 = data['STATIC DATA OF PRODUCT SOLD AND GROSS RETAIL'];
            },
            error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            });
    }
    openUpgradeOption() {
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.SHOULD_BE_VALID_PACKAGE');
        this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
    }
}
