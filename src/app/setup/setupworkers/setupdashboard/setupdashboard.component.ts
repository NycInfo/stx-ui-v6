import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { SetupdashboardService } from './setupdashboard.service';
@Component({
  selector: 'app-setupdashboard-popup',
  templateUrl: './setupdashboard.html',
  styleUrls: ['./setupdashboard.css'],
  providers: [SetupdashboardService]
})
export class SetupdashboardComponent implements OnInit {
  decodedToken: any;
  decodeUserToken: any;
  reportTypes = ['Company', 'Worker'];
  workerList = [];
  selectType: any;
  showWorkers = true;
  workerName: any;
  worker = '';
  toastermessage: any;
  bookedAll = false;
  bookedDay = false;
  bookedWeek = false;
  bookedMonth = false;
  bookedYear = false;
  bookedTicktAll = false;
  bookedTicktDay = false;
  bookedTicktWeek = false;
  bookedTicktMonth = false;
  bookedTicktYear = false;
  rebookdAll = false;
  rebookdDay = false;
  rebookdWeek = false;
  rebookdMonth = false;
  rebookdYear = false;
  topWrkSrvcAll = false;
  topWrkSrvcDay = false;
  topWrkSrvcWeek = false;
  topWrkSrvcMonth = false;
  topWrkSrvcYear = false;
  topWrkPrdAll = false;
  topWrkPrdDay = false;
  topWrkPrdWeek = false;
  topWrkPrdMonth = false;
  topWrkPrdYear = false;
  newClientAll = false;
  newClientDay = false;
  newClientWeek = false;
  newClientMonth = false;
  newClientYear = false;
  bookdOnlineAll = false;
  bookdOnlineDay = false;
  bookdOnlineWeek = false;
  bookdOnlineMonth = false;
  bookdOnlineYear = false;
  retailSrvcAll = false;
  retailSrvcDay = false;
  retailSrvcWeek = false;
  retailSrvcMonth = false;
  retailSrvcYear = false;
  srvcSaleAll = false;
  srvcSaleDay = false;
  srvcSaleWeek = false;
  srvcSaleMonth = false;
  srvcSaleYear = false;
  prodctSaleAll = false;
  prodctSaleDay = false;
  prodctSaleWeek = false;
  prodctSaleMonth = false;
  prodctSaleYear = false;
  avgTicktAll = false;
  avgTicktDay = false;
  avgTicktWeek = false;
  avgTicktMonth = false;
  avgTicktYear = false;
  avgSrvcAll = false;
  avgSrvcDay = false;
  avgSrvcWeek = false;
  avgSrvcMonth = false;
  avgSrvcYear = false;
  avgProdctAll = false;
  avgProdctDay = false;
  avgProdctWeek = false;
  avgProdctMonth = false;
  avgProdctYear = false;
  dashBoardJsonData: any = [];
  orginalData: any = [];
  retailPerGuestAll = false;
  retailPerGuestDay = false;
  retailPerGuestWeek = false;
  retailPerGuestMonth = false;
  retailPerGuestYear = false;
  /** */
  stcData: any = {};
  dashBrdData = [];
  constructor(private toastr: ToastrService,
    private setupdashboardService: SetupdashboardService,
    private router: Router,
    private translateService: TranslateService) {
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for permissions Implementation--- //
    this.selectType = this.reportTypes[0];
    this.onloadData();

  }
  getWorkerList() { // for workerlist
    this.setupdashboardService.getWorkerList().subscribe(data => {
      this.workerList = [];
      this.workerList = data['result'];
      this.worker = this.workerList[0].Id;
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  reportTypeOnChange(value) { // change select type
    this.selectType = value;
    if (value === 'Worker') {
      this.getWorkerList();
      this.showWorkers = false;

    } else {
      this.showWorkers = true;
    }
    this.getDashBoardData('');
  }
  workerListOnChange(value) { // worker list onChange
    this.worker = value;
    this.getDashBoardData(this.worker);
  }
  saveDashBoardData() { // save data
    if (this.selectType === 'Worker' && !this.worker) {
      this.toastr.error('Select Worker', null, { timeOut: 1500 });
    } else {
      const reqObj = {
        'add': [],
        'edit': []
      };
      for (let i = 0; i < this.dashBrdData.length; i++) {
        const tempAry = this.orginalData.filter(obj => obj['Worker__c'] === this.dashBrdData[i]['Worker__c']);
        if (tempAry.length > 0) {
          if (JSON.stringify(tempAry[0]['JSON__c']) !== JSON.stringify(this.dashBrdData[i]['JSON__c'])) {
            reqObj['edit'].push(this.dashBrdData[i]);
          }
        } else {
          reqObj['add'].push(this.dashBrdData[i]);
        }
      }
      if (reqObj['add'].length > 0 || reqObj['edit'].length > 0) { // add objects and edited objects are only saving
        this.setupdashboardService.saveDashBoardData(reqObj).subscribe(
          data => {
            this.toastr.success('Dashboard saved successfully', null, { timeOut: 1500 });
            this.router.navigate(['/setup/workers']);
          },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (JSON.parse(error['_body']).status) {
              case '2033':
                break;
              case 500:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      } else {
        this.toastr.success('Dashboard saved successfully', null, { timeOut: 1500 });
        this.router.navigate(['/setup/workers']);
      }
    }
  }
  cancel() {
    this.router.navigate(['/setup/workers']);
  }
  /* creating new object */
  addData(worker) {
    const obj = {};
    this.stcData =
      [
        {
          'Booked':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'BookedTicketd':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'Rebooked':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'TopWorkerServiceSales':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'TopWorkerProductSales':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'NewClients':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'BookedOnline':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'RetailToService':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'ServiceSales':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'ProductSales':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'AverageTicket':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'AverageService':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'AverageProduct':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        },
        {
          'RetailPerGuest':
          {
            'all': false,
            'day': false,
            'week': false,
            'month': false,
            'year': false
          }
        }
      ];
    obj['Worker__c'] = worker;
    obj['JSON__c'] = this.stcData;
    if (this.dashBrdData.filter(data => data['Worker__c'] === this.worker).length === 0) { // no record is exists so create new
      this.dashBrdData.push(obj);
    }
  }
  changeObjects(value, value1, type, woker, checked) { // click events
    for (let i = 0; i < this.dashBrdData.length; i++) {
      if (type === 'Company' && this.dashBrdData[i]['Worker__c'] === '') {
        for (let j = 0; j < this.dashBrdData[i]['JSON__c'].length; j++) {
          if (value1 !== 'all') {
            if (this.dashBrdData[i]['JSON__c'][j][value]) {
              this.dashBrdData[i]['JSON__c'][j][value][value1] = checked;
            }
          } else {
            if (this.dashBrdData[i]['JSON__c'][j][value]) {
              this.dashBrdData[i]['JSON__c'][j][value]['all'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['day'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['week'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['month'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['year'] = checked;
            }
          }
        }
      } else if (type === 'Worker' && this.dashBrdData[i]['Worker__c'] === woker) {
        for (let j = 0; j < this.dashBrdData[i]['JSON__c'].length; j++) {
          if (value1 !== 'all') {
            if (this.dashBrdData[i]['JSON__c'][j][value]) {
              this.dashBrdData[i]['JSON__c'][j][value][value1] = checked;
            }
          } else {
            if (this.dashBrdData[i]['JSON__c'][j][value]) {
              this.dashBrdData[i]['JSON__c'][j][value]['all'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['day'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['week'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['month'] = checked;
              this.dashBrdData[i]['JSON__c'][j][value]['year'] = checked;
            }
          }

        }
      }
    }
  }
  onloadData() { // onload data
    this.setupdashboardService.getDashBoardData(this.worker).subscribe(data => {
      this.orginalData = JSON.parse(JSON.stringify(data['result']));
      this.dashBrdData = JSON.parse(JSON.stringify(data['result']));
      this.getDashBoardData('');
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });

  }
  getDashBoardData(worker) { // disaplaying data for HTML
    let tempData: any = [];
    let dshBrdData = [];
    if (this.selectType === 'Company') {
      tempData = this.dashBrdData.filter((obj) => obj.Worker__c === '');
      this.worker = '';
    } else {
      if (worker) {
        dshBrdData = this.dashBrdData.filter(item => item.Worker__c !== '');
        tempData = this.dashBrdData.filter((obj) => obj.Worker__c === worker);
      } else {
        this.clear();
      }
    }
    if (tempData.length > 0) {
      this.dashBoardJsonData = tempData[0]['JSON__c'];
      this.bookedAll = this.dashBoardJsonData[0]['Booked']['all'];
      this.bookedDay = this.dashBoardJsonData[0]['Booked']['day'];
      this.bookedWeek = this.dashBoardJsonData[0]['Booked']['week'];
      this.bookedMonth = this.dashBoardJsonData[0]['Booked']['month'];
      this.bookedYear = this.dashBoardJsonData[0]['Booked']['year'];

      this.bookedTicktAll = this.dashBoardJsonData[1]['BookedTicketd']['all'];
      this.bookedTicktDay = this.dashBoardJsonData[1]['BookedTicketd']['day'];
      this.bookedTicktWeek = this.dashBoardJsonData[1]['BookedTicketd']['week'];
      this.bookedTicktMonth = this.dashBoardJsonData[1]['BookedTicketd']['month'];
      this.bookedTicktYear = this.dashBoardJsonData[1]['BookedTicketd']['year'];

      this.rebookdAll = this.dashBoardJsonData[2]['Rebooked']['all'];
      this.rebookdDay = this.dashBoardJsonData[2]['Rebooked']['day'];
      this.rebookdWeek = this.dashBoardJsonData[2]['Rebooked']['week'];
      this.rebookdMonth = this.dashBoardJsonData[2]['Rebooked']['month'];
      this.rebookdYear = this.dashBoardJsonData[2]['Rebooked']['year'];

      this.topWrkSrvcAll = this.dashBoardJsonData[3]['TopWorkerServiceSales']['all'];
      this.topWrkSrvcDay = this.dashBoardJsonData[3]['TopWorkerServiceSales']['day'];
      this.topWrkSrvcWeek = this.dashBoardJsonData[3]['TopWorkerServiceSales']['week'];
      this.topWrkSrvcMonth = this.dashBoardJsonData[3]['TopWorkerServiceSales']['month'];
      this.topWrkSrvcYear = this.dashBoardJsonData[3]['TopWorkerServiceSales']['year'];

      this.topWrkPrdAll = this.dashBoardJsonData[4]['TopWorkerProductSales']['all'];
      this.topWrkPrdDay = this.dashBoardJsonData[4]['TopWorkerProductSales']['day'];
      this.topWrkPrdWeek = this.dashBoardJsonData[4]['TopWorkerProductSales']['week'];
      this.topWrkPrdMonth = this.dashBoardJsonData[4]['TopWorkerProductSales']['month'];
      this.topWrkPrdYear = this.dashBoardJsonData[4]['TopWorkerProductSales']['year'];

      this.newClientAll = this.dashBoardJsonData[5]['NewClients']['all'];
      this.newClientDay = this.dashBoardJsonData[5]['NewClients']['day'];
      this.newClientWeek = this.dashBoardJsonData[5]['NewClients']['week'];
      this.newClientMonth = this.dashBoardJsonData[5]['NewClients']['month'];
      this.newClientYear = this.dashBoardJsonData[5]['NewClients']['year'];

      this.bookdOnlineAll = this.dashBoardJsonData[6]['BookedOnline']['all'];
      this.bookdOnlineDay = this.dashBoardJsonData[6]['BookedOnline']['day'];
      this.bookdOnlineWeek = this.dashBoardJsonData[6]['BookedOnline']['week'];
      this.bookdOnlineMonth = this.dashBoardJsonData[6]['BookedOnline']['month'];
      this.bookdOnlineYear = this.dashBoardJsonData[6]['BookedOnline']['year'];

      this.retailSrvcAll = this.dashBoardJsonData[7]['RetailToService']['all'];
      this.retailSrvcDay = this.dashBoardJsonData[7]['RetailToService']['day'];
      this.retailSrvcWeek = this.dashBoardJsonData[7]['RetailToService']['week'];
      this.retailSrvcMonth = this.dashBoardJsonData[7]['RetailToService']['month'];
      this.retailSrvcYear = this.dashBoardJsonData[7]['RetailToService']['year'];

      this.srvcSaleAll = this.dashBoardJsonData[8]['ServiceSales']['all'];
      this.srvcSaleDay = this.dashBoardJsonData[8]['ServiceSales']['day'];
      this.srvcSaleWeek = this.dashBoardJsonData[8]['ServiceSales']['week'];
      this.srvcSaleMonth = this.dashBoardJsonData[8]['ServiceSales']['month'];
      this.srvcSaleYear = this.dashBoardJsonData[8]['ServiceSales']['year'];

      this.prodctSaleAll = this.dashBoardJsonData[9]['ProductSales']['all'];
      this.prodctSaleDay = this.dashBoardJsonData[9]['ProductSales']['day'];
      this.prodctSaleWeek = this.dashBoardJsonData[9]['ProductSales']['week'];
      this.prodctSaleMonth = this.dashBoardJsonData[9]['ProductSales']['month'];
      this.prodctSaleYear = this.dashBoardJsonData[9]['ProductSales']['year'];

      this.avgTicktAll = this.dashBoardJsonData[10]['AverageTicket']['all'];
      this.avgTicktDay = this.dashBoardJsonData[10]['AverageTicket']['day'];
      this.avgTicktWeek = this.dashBoardJsonData[10]['AverageTicket']['week'];
      this.avgTicktMonth = this.dashBoardJsonData[10]['AverageTicket']['month'];
      this.avgTicktYear = this.dashBoardJsonData[10]['AverageTicket']['year'];

      this.avgSrvcAll = this.dashBoardJsonData[11]['AverageService']['all'];
      this.avgSrvcDay = this.dashBoardJsonData[11]['AverageService']['day'];
      this.avgSrvcWeek = this.dashBoardJsonData[11]['AverageService']['week'];
      this.avgSrvcMonth = this.dashBoardJsonData[11]['AverageService']['month'];
      this.avgSrvcYear = this.dashBoardJsonData[11]['AverageService']['year'];

      this.avgProdctAll = this.dashBoardJsonData[12]['AverageProduct']['all'];
      this.avgProdctDay = this.dashBoardJsonData[12]['AverageProduct']['day'];
      this.avgProdctWeek = this.dashBoardJsonData[12]['AverageProduct']['week'];
      this.avgProdctMonth = this.dashBoardJsonData[12]['AverageProduct']['month'];
      this.avgProdctYear = this.dashBoardJsonData[12]['AverageProduct']['year'];

      this.retailPerGuestAll = this.dashBoardJsonData[13]['RetailPerGuest']['all'];
      this.retailPerGuestDay = this.dashBoardJsonData[13]['RetailPerGuest']['day'];
      this.retailPerGuestWeek = this.dashBoardJsonData[13]['RetailPerGuest']['week'];
      this.retailPerGuestMonth = this.dashBoardJsonData[13]['RetailPerGuest']['month'];
      this.retailPerGuestYear = this.dashBoardJsonData[13]['RetailPerGuest']['year'];
    } else {
      this.clear(); // when length is zero clear is mandatory
      for (let i = 0; i < dshBrdData.length; i++) {
        if (dshBrdData[i]['Worker__c'] !== worker) {
          this.addData(this.worker);
        }
      }
      if (dshBrdData.length === 0) { // create new object
        this.addData(this.worker);
      }
    }
  }
  clear() { // clearing all json data to false and empty
    this.bookedAll = false;
    this.bookedDay = false;
    this.bookedWeek = false;
    this.bookedMonth = false;
    this.bookedYear = false;
    this.bookedTicktAll = false;
    this.bookedTicktDay = false;
    this.bookedTicktWeek = false;
    this.bookedTicktMonth = false;
    this.bookedTicktYear = false;
    this.rebookdAll = false;
    this.rebookdDay = false;
    this.rebookdWeek = false;
    this.rebookdMonth = false;
    this.rebookdYear = false;
    this.topWrkSrvcAll = false;
    this.topWrkSrvcDay = false;
    this.topWrkSrvcWeek = false;
    this.topWrkSrvcMonth = false;
    this.topWrkSrvcYear = false;
    this.topWrkPrdAll = false;
    this.topWrkPrdDay = false;
    this.topWrkPrdWeek = false;
    this.topWrkPrdMonth = false;
    this.topWrkPrdYear = false;
    this.newClientAll = false;
    this.newClientDay = false;
    this.newClientWeek = false;
    this.newClientMonth = false;
    this.newClientYear = false;
    this.bookdOnlineAll = false;
    this.bookdOnlineDay = false;
    this.bookdOnlineWeek = false;
    this.bookdOnlineMonth = false;
    this.bookdOnlineYear = false;
    this.retailSrvcAll = false;
    this.retailSrvcDay = false;
    this.retailSrvcWeek = false;
    this.retailSrvcMonth = false;
    this.retailSrvcYear = false;
    this.srvcSaleAll = false;
    this.srvcSaleDay = false;
    this.srvcSaleWeek = false;
    this.srvcSaleMonth = false;
    this.srvcSaleYear = false;
    this.prodctSaleAll = false;
    this.prodctSaleDay = false;
    this.prodctSaleWeek = false;
    this.prodctSaleMonth = false;
    this.prodctSaleYear = false;
    this.avgTicktAll = false;
    this.avgTicktDay = false;
    this.avgTicktWeek = false;
    this.avgTicktMonth = false;
    this.avgTicktYear = false;
    this.avgSrvcAll = false;
    this.avgSrvcDay = false;
    this.avgSrvcWeek = false;
    this.avgSrvcMonth = false;
    this.avgSrvcYear = false;
    this.avgProdctAll = false;
    this.avgProdctDay = false;
    this.avgProdctWeek = false;
    this.avgProdctMonth = false;
    this.avgProdctYear = false;
    this.retailPerGuestAll = false;
    this.retailPerGuestDay = false;
    this.retailPerGuestWeek = false;
    this.retailPerGuestMonth = false;
    this.retailPerGuestYear = false;
  }
  selectAll(value, num) {  // all click event for true and false conditions
    if (num === '1') {
      if (value) {
        this.bookedDay = true;
        this.bookedWeek = true;
        this.bookedMonth = true;
        this.bookedYear = true;
      } else {
        this.bookedAll = false;
        this.bookedDay = false;
        this.bookedWeek = false;
        this.bookedMonth = false;
        this.bookedYear = false;
      }
    } else if (num === '2') {
      if (value) {
        this.bookedTicktDay = true;
        this.bookedTicktWeek = true;
        this.bookedTicktMonth = true;
        this.bookedTicktYear = true;
      } else {
        this.bookedTicktAll = false;
        this.bookedTicktDay = false;
        this.bookedTicktWeek = false;
        this.bookedTicktMonth = false;
        this.bookedTicktYear = false;
      }
    } else if (num === '3') {
      if (value) {
        this.rebookdDay = true;
        this.rebookdWeek = true;
        this.rebookdMonth = true;
        this.rebookdYear = true;
      } else {
        this.rebookdAll = false;
        this.rebookdDay = false;
        this.rebookdWeek = false;
        this.rebookdMonth = false;
        this.rebookdYear = false;
      }
    } else if (num === '4') {
      if (value) {
        this.topWrkSrvcDay = true;
        this.topWrkSrvcWeek = true;
        this.topWrkSrvcMonth = true;
        this.topWrkSrvcYear = true;
      } else {
        this.topWrkSrvcAll = false;
        this.topWrkSrvcDay = false;
        this.topWrkSrvcWeek = false;
        this.topWrkSrvcMonth = false;
        this.topWrkSrvcYear = false;
      }
    } else if (num === '5') {
      if (value) {
        this.topWrkPrdDay = true;
        this.topWrkPrdWeek = true;
        this.topWrkPrdMonth = true;
        this.topWrkPrdYear = true;
      } else {
        this.topWrkPrdAll = false;
        this.topWrkPrdDay = false;
        this.topWrkPrdWeek = false;
        this.topWrkPrdMonth = false;
        this.topWrkPrdYear = false;
      }
    } else if (num === '6') {
      if (value) {
        this.newClientDay = true;
        this.newClientWeek = true;
        this.newClientMonth = true;
        this.newClientYear = true;
      } else {
        this.newClientAll = false;
        this.newClientDay = false;
        this.newClientWeek = false;
        this.newClientMonth = false;
        this.newClientYear = false;
      }
    } else if (num === '7') {
      if (value) {
        this.bookdOnlineDay = true;
        this.bookdOnlineWeek = true;
        this.bookdOnlineMonth = true;
        this.bookdOnlineYear = true;
      } else {
        this.bookdOnlineAll = false;
        this.bookdOnlineDay = false;
        this.bookdOnlineWeek = false;
        this.bookdOnlineMonth = false;
        this.bookdOnlineYear = false;
      }
    } else if (num === '8') {
      if (value) {
        this.retailSrvcDay = true;
        this.retailSrvcWeek = true;
        this.retailSrvcMonth = true;
        this.retailSrvcYear = true;
      } else {
        this.retailSrvcAll = false;
        this.retailSrvcDay = false;
        this.retailSrvcWeek = false;
        this.retailSrvcMonth = false;
        this.retailSrvcYear = false;
      }
    } else if (num === '9') {
      if (value) {
        this.srvcSaleDay = true;
        this.srvcSaleWeek = true;
        this.srvcSaleMonth = true;
        this.srvcSaleYear = true;
      } else {
        this.srvcSaleAll = false;
        this.srvcSaleDay = false;
        this.srvcSaleWeek = false;
        this.srvcSaleMonth = false;
        this.srvcSaleYear = false;
      }
    } else if (num === '10') {
      if (value) {
        this.prodctSaleDay = true;
        this.prodctSaleWeek = true;
        this.prodctSaleMonth = true;
        this.prodctSaleYear = true;
      } else {
        this.prodctSaleAll = false;
        this.prodctSaleDay = false;
        this.prodctSaleWeek = false;
        this.prodctSaleMonth = false;
        this.prodctSaleYear = false;
      }
    } else if (num === '11') {
      if (value) {
        this.avgTicktDay = true;
        this.avgTicktWeek = true;
        this.avgTicktWeek = true;
        this.avgTicktYear = true;
      } else {
        this.avgTicktAll = false;
        this.avgTicktDay = false;
        this.avgTicktWeek = false;
        this.avgTicktMonth = false;
        this.avgTicktYear = false;
      }
    } else if (num === '12') {
      if (value) {
        this.avgSrvcDay = true;
        this.avgSrvcWeek = true;
        this.avgSrvcMonth = true;
        this.avgSrvcYear = true;
      } else {
        this.avgSrvcAll = false;
        this.avgSrvcDay = false;
        this.avgSrvcWeek = false;
        this.avgSrvcMonth = false;
        this.avgSrvcYear = false;
      }
    } else if (num === '13') {
      if (value) {
        this.avgProdctDay = true;
        this.avgProdctWeek = true;
        this.avgProdctMonth = true;
        this.avgProdctYear = true;
      } else {
        this.avgProdctAll = false;
        this.avgProdctDay = false;
        this.avgProdctWeek = false;
        this.avgProdctMonth = false;
        this.avgProdctYear = false;
      }
    } else if (num === '14') {
      if (value) {
        this.retailPerGuestDay = true;
        this.retailPerGuestWeek = true;
        this.retailPerGuestMonth = true;
        this.retailPerGuestYear = true;
      } else {
        this.retailPerGuestAll = false;
        this.retailPerGuestDay = false;
        this.retailPerGuestWeek = false;
        this.retailPerGuestMonth = false;
        this.retailPerGuestYear = false;
      }
    }
  }
}
