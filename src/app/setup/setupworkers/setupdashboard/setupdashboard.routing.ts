import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SetupdashboardComponent } from './setupdashboard.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: SetupdashboardComponent,
                children: [
                    {
                        path: '',
                        component: SetupdashboardComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SetupdashboardRoutingModule {
}
