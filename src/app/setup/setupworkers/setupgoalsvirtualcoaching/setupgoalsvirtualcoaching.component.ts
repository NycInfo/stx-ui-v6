import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { SetupgoalsvirtualcoachingService } from './setupgoalsvirtualcoaching.service';
@Component({
    selector: 'app-setupgoalsvirtualcoaching-popup',
    templateUrl: './setupgoalsvirtualcoaching.html',
    styleUrls: ['./setupgoalsvirtualcoaching.css'],
    providers: [SetupgoalsvirtualcoachingService]
})
export class SetupgoalsvirtualcoachingComponent implements OnInit {
    excellent = '';
    veryGood = '';
    good = '';
    fair = '';
    poor = '';
    veryPoor = '';
    virtualData: any = {};
    toastermessage: any;
    constructor(private toastr: ToastrService,
        private setupgoalsvirtualcoachingService: SetupgoalsvirtualcoachingService,
        private router: Router,
        private translateService: TranslateService) {
    }
    ngOnInit() {
        this.getData();
    }
    reset(num) {
        if (num === '1') {
            this.excellent = 'Strike a pose!  Keep up the GREAT work!';
        } else if (num === '2') {
            this.veryGood = 'You’re right there!  Keep going strong!';
        } else if (num === '3') {
            this.good = 'Making progress each day makes a BIG difference.';
        } else if (num === '4') {
            this.fair = 'You can TOTALLY do this.  Set a small goal for today and achieve it!';
        } else if (num === '5') {
            this.poor = 'Deep breath, you’ve got this!  Take 2 mins and brainstorm how to do better on your next guest.';
        } else if (num === '6') {
            this.veryPoor = 'The difference between try and triumph is a little bit of ‘umph!';
        }
    }
    saveData() {
        const Obj = {
            excellent: this.excellent,
            verygood: this.veryGood,
            good: this.good,
            fair: this.fair,
            poor: this.poor,
            verypoor: this.veryPoor
        };
        this.setupgoalsvirtualcoachingService.saveData(Obj)
            .subscribe(data => {
                const dataa = data['result'];
                this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_SUCCESS');
                this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                this.router.navigate(['/setup/workers']);
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });

    }
    getData() {
        this.setupgoalsvirtualcoachingService.getData()
            .subscribe(data => {
                this.virtualData = data['result'];
                if (this.virtualData['excellent']) {
                    this.excellent = this.virtualData['excellent'];
                } else {
                    this.excellent = 'Strike a pose!  Keep up the GREAT work!';
                }
                if (this.virtualData['verygood']) {
                    this.veryGood = this.virtualData['verygood'];
                } else {
                    this.veryGood = 'You’re right there!  Keep going strong!';
                }
                if (this.virtualData['good']) {
                    this.good = this.virtualData['good'];
                } else {
                    this.good = 'Making progress each day makes a BIG difference.'
                }
                if (this.virtualData['fair']) {
                    this.fair = this.virtualData['fair'];
                } else {
                    this.fair = 'You can TOTALLY do this. Set a small goal for today and achieve it!';
                }
                if (this.virtualData['poor']) {
                    this.poor = this.virtualData['poor'];
                } else {
                    this.poor = 'Deep breath, you’ve got this!  Take 2 mins and brainstorm how to do better on your next guest.'
                }
                if (this.virtualData['verypoor']) {
                    this.veryPoor = this.virtualData['verypoor'];
                } else {
                    this.veryPoor = 'The difference between try and triumph is a little bit of ‘umph!'
                }
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });

    }
    cancel() {
        this.router.navigate(['/setup/workers']);
    }
}
