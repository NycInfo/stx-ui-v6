import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupgoalsvirtualcoachingComponent } from './setupgoalsvirtualcoaching.component';
import { SetupgoalsvirtualcoachingRoutingModule } from './setupgoalsvirtualcoaching.routing';
import { TranslateModule } from 'ng2-translate';
import { ShareModule } from '../../../common/share.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        SetupgoalsvirtualcoachingRoutingModule,
        TranslateModule,
        FormsModule,
        ShareModule
    ],
    declarations: [
        SetupgoalsvirtualcoachingComponent
    ]
})
export class SetupgoalsvirtualcoachingModule {
}
