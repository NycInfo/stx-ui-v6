
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClient } from '../../../common/http-client';

@Injectable()
export class SetupgoalsvirtualcoachingService {
  constructor(private http: HttpClient,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
    /*-- Method to save data --*/
    saveData(obj) {
      return this.http.post(this.apiEndPoint + '/api/savevirtualdata/', obj)
      .map(this.extractData);
    }
    getData() {
      return this.http.get(this.apiEndPoint + '/api/getvirtualdata')
      .map(this.extractData);
    }

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
