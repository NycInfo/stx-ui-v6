import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';
import { JwtHelper } from 'angular2-jwt';
import { SetupPermissionsService } from './setuppermissions.service';

@Component({
    selector: 'app-setuppermissions-popup',
    templateUrl: './setuppermissions.html',
    styleUrls: ['./setuppermissions.css'],
    providers: [SetupPermissionsService]
})

export class SetupPermissionsComponent implements OnInit {
    permissionsList: any;
    permissionsStaticJsonList: any;
    objectKeys: any;
    sortData: any;
    Name: any;
    Category: any;
    dataObj: any;
    permissionsData: any;
    toastermessage: any;
    error: any;
    updateId: any;
    disableClass = '';
    disable = true;
    addDiv = true;
    EditDiv = false;
    decodedUserToken: any;
    constructor(private toastr: ToastrService,
        private translateService: TranslateService,
        private setupPermissionsService: SetupPermissionsService,
        private router: Router) { }
    ngOnInit() {
        try {
            this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedUserToken = {};
        }
        this.getPermissions();
    }
    getPermissions() {
        this.setupPermissionsService.getPermissionsData()
            .subscribe(data => {
                this.permissionsList = data['result'];
            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    getPermissionsStaticJson() {
        this.setupPermissionsService.getPermissionsStaticData()
            .subscribe(data => {
                this.permissionsStaticJsonList = data['permissions'];
                this.objectKeys = Object.keys(this.permissionsStaticJsonList);
                this.sortData = this.permissionsStaticJsonList['Home'];
                this.Category = this.objectKeys[0];

            },
                error => {
                    const errStatus = JSON.parse(error['_body'])['status'];
                    if (errStatus === '2085' || errStatus === '2071') {
                        if (this.router.url !== '/') {
                            localStorage.setItem('page', this.router.url);
                            this.router.navigate(['/']).then(() => { });
                        }
                    }
                });
    }
    showData(permissionData) {
        this.sortData = '';
        this.permissionsStaticJsonList = '';
        this.disableClass = 'disabled';
        this.addDiv = false;
        this.disable = false;
        this.EditDiv = true;
        this.updateId = permissionData.Id;
        this.Name = permissionData.Name;
        this.permissionsStaticJsonList = JSON.parse(permissionData.Authorized_Pages__c);
        this.objectKeys = Object.keys(this.permissionsStaticJsonList);
        this.Category = this.objectKeys[0];
        this.sortData = this.permissionsStaticJsonList['Home'];
    }
    addNew() {
        this.disableClass = 'disabled';
        this.addDiv = true;
        this.disable = false;
        this.EditDiv = false;
        this.Name = '';
        this.getPermissionsStaticJson();

    }
    cancel() {
        this.disableClass = '';
        this.addDiv = true;
        this.EditDiv = false;
        this.disable = true;
        this.Name = '';
        this.sortData = [];
        this.error = '';
    }
    onValueChange(value) {
        this.Category = value;
        if (value === 'Setup Company') {
            this.sortData = this.permissionsStaticJsonList['Setup Company'];
        } else if (value === 'Setup Other') {
            this.sortData = this.permissionsStaticJsonList['Setup Other'];
        } else if (value === 'Setup Service') {
            this.sortData = this.permissionsStaticJsonList['Setup Service'];
        } else if (value === 'Setup Inventory') {
            this.sortData = this.permissionsStaticJsonList['Setup Inventory'];
        } else if (value === 'Setup Workers') {
            this.sortData = this.permissionsStaticJsonList['Setup Workers'];
        } else if (value === 'Clients') {
            this.sortData = this.permissionsStaticJsonList['Clients'];
        } else if (value === 'Appointments') {
            this.sortData = this.permissionsStaticJsonList['Appointments'];
        } else if (value === 'Inventory') {
            this.sortData = this.permissionsStaticJsonList['Inventory'];
        } else if (value === 'Tickets') {
            this.sortData = this.permissionsStaticJsonList['Tickets'];
        } else if (value === 'Reports') {
            this.sortData = this.permissionsStaticJsonList['Reports'];
        } else if (value === 'Marketing') {
            this.sortData = this.permissionsStaticJsonList['Marketing'];
        } else if (value === 'Home') {
            this.sortData = this.permissionsStaticJsonList['Home'];
        }
    }
    selectAll() {
        for (let i = 0; i < this.sortData.length; i++) {
            this.sortData[i]['allowAcces'] = true;
        }
    }
    unSelectAll() {
        for (let i = 0; i < this.sortData.length; i++) {
            this.sortData[i]['allowAcces'] = false;
        }
    }
    savePermissionSet() {
        if (this.Name === '' || this.Name === undefined || this.Name === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        } else {
            this.dataObj = {
                'Name': this.Name,
                // 'Category': this.Category,
                'Authorized_Pages__c': this.permissionsStaticJsonList
            };
            this.setupPermissionsService.savePermissionsData(this.dataObj)
                .subscribe(data => {
                    this.permissionsData = data['result'];
                    this.getPermissions();
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_CREATE_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.disableClass = '';
                    this.addDiv = true;
                    this.EditDiv = false;
                    this.disable = true;
                    this.clearErrMsg();
                },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;

                        switch (JSON.parse(error['_body']).status) {
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2043':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    editPermissionSet() {
        if (this.Name === '' || this.Name === undefined || this.Name === 'undefined') {
            this.error = 'VALIDATION_MSG.NAME_REQUIRED';
        } else {
            this.dataObj = {
                'Name': this.Name,
                // 'Category': this.Category,
                'Authorized_Pages__c': this.permissionsStaticJsonList
            };
            this.setupPermissionsService.editPermissionsData(this.updateId, this.dataObj)
                .subscribe(data => {
                    if (data['result'] && data['result'].length > 0) {
                        localStorage.setItem('rights', data['result']);
                    }
                    this.permissionsData = data['result'];
                    this.getPermissions();
                    this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_EDIT_SUCCESS');
                    this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
                    this.disableClass = '';
                    this.addDiv = true;
                    this.EditDiv = false;
                    this.disable = true;
                    this.clearErrMsg();
                },
                    error => {
                        const status = JSON.parse(error['status']);
                        const statuscode = JSON.parse(error['_body']).status;

                        switch (JSON.parse(error['_body']).status) {
                            case '2033':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                            case '2043':
                                this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                                break;
                        }
                        if (statuscode === '2085' || statuscode === '2071') {
                            if (this.router.url !== '/') {
                                localStorage.setItem('page', this.router.url);
                                this.router.navigate(['/']).then(() => { });
                            }
                        }
                    });
        }
    }
    clearErrMsg() {
        this.error = '';
    }
    openUpgradeOption() {
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.SHOULD_BE_VALID_PACKAGE');
        this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
    }

    /* method to restrict specialcharecters (< , > , \ , ',") */
    alphaNumOnly(e) {
        const specialKeys = new Array();
        specialKeys.push(8); // Backspace
        specialKeys.push(9); // Tab
        specialKeys.push(46); // Delete
        specialKeys.push(36); // Home
        specialKeys.push(35); // End
        specialKeys.push(37); // Left
        specialKeys.push(39); // Right
        const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
            (keyCode >= 35 && keyCode <= 38) || (keyCode >= 40 && keyCode <= 47) ||
            (keyCode >= 63 && keyCode <= 64) || (keyCode >= 93 && keyCode <= 96) ||
            (keyCode >= 125 && keyCode <= 126) || (keyCode >= 58 && keyCode <= 59) ||
            (keyCode >= 32 && keyCode < 34) || (keyCode > 60 && keyCode < 62) || (keyCode > 90 && keyCode < 92) ||
            (keyCode > 122 && keyCode < 124) || (keyCode > 123 && keyCode < 125) ||
            (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        return ret;
    }
}
